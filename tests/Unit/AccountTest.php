<?php

namespace tests\Unit;

use Modules\Wallet\Entities\Account;
use Modules\Wallet\Http\Controllers\Api\AccountController;
use Tests\TestCase;

class account_test extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_index_Method_response_in_AccountController()
    {
        $accounts = new AccountController();
        $this->assertNotNull($accounts->index());
        $this->assertNotEmpty($accounts->index());
        $this->assertNotFalse($accounts->index());
    }


    public function test_get_show_Method_response_in_AccountController()
    {
        $account = Account::find(1);
        $this->assertNotNull($account);
        $this->assertInstanceOf(Account::class, $account);
    }

    public function test_get_data_from_createMethod_in_AccountController()
    {
        $header = [
            "content-type" => "application/json",
            "Accept" => "application/json",
        ];
        $response = $this->withHeaders($header)->json("POST", "api/account/register", $this->get_fake_data());
        $response->assertStatus(200);
    }

    public function test_get_data_from_deleteMethod_in_AccountController()
    {
        //assert delete status code 204 (no content)
        $id = 35;
        $this->post("api/account/delete/{$id}")->assertStatus(204);

    }


    private function get_fake_data(): array
    {
        return $data = [
            "name" => "ali",
            "email" => "test@all.com",
            "password" => 123,
            "mobile" => "09385727006",
            "app_version" => "1.1",
            "os_version" => "4.4.4",
            "imei" => "123456789",
            "db_version" => "5.8",
            "app_id" => "1234"
        ];
    }


}
