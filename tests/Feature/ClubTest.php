<?php

namespace Modules\Club\Tests\Feature;

use App\repo\UserDB;
use App\User;
use Modules\Attachment\Entities\Attachment;
use Modules\Branch\Entities\Merchant;
use Modules\Club\Entities\Club;
use Modules\Event\Entities\Event;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Product;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\Campaign;
use Modules\Wallet\Entities\Credit;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\Process;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Entities\Rule;
use Modules\Wallet\Entities\Transaction;
use Modules\Wallet\Entities\Wallet;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class clubTest extends TestCase
{
    use WithFaker;
    private $accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk1ZDZiOTQ4NzhkMjhmMDQwOTZkNGEyMDVkMzkwYTUwNWQyNGRmMjY0ZmVhM2I5MzhjMThjOGE2ZDliOTZlZDQ4YTAxNjZkNWZhMWQ4MTE1In0.eyJhdWQiOiIzIiwianRpIjoiOTVkNmI5NDg3OGQyOGYwNDA5NmQ0YTIwNWQzOTBhNTA1ZDI0ZGYyNjRmZWEzYjkzOGMxOGM4YTZkOWI5NmVkNDhhMDE2NmQ1ZmExZDgxMTUiLCJpYXQiOjE1NzMzNjc1MDAsIm5iZiI6MTU3MzM2NzUwMCwiZXhwIjoxNTczNDUzOTAwLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.rBhtlOjVoo2BkQDVGTObsVk5-81ZZe3WICHF1GZH62-MllRv2TFSnYXnYG2j-zwKi6r2xMb73rgTccGixF9e5_r2mEIu-hXMxT22DRfimp_-GylzYdhZeGf_yXLj5spggFEy4sS_-qxvG2jvB4ZYJlaTeELY9QNDVTroNlirapnf4fVLOmq030za2tRGzHMhirwboNseGBZ69SzSrSjLyyc5vf2GkLJLUn8nVyt89kYvAukwoEDTEGhgnPW3yJ2Rt7UypGgjbXDEmtBbdMmpZOOz5yTa-iOVoiOjNb2ruRfhikAiVb-ivt9599IhmMJaus4WGWRjs68gg1P3Kh7rT8BQj7RznG6QdJNZrzsKF2UULqEovoCquCVidbUFPJ5gi0wXCCQEsH3wD9d7H2wsDSlbIa5BmHFySZJJnmdluXJtLXNYhAZUAN3cs9rA2-yAdFMHYy9G-IO_LnbCCzjkLUf3-JtVDmUjNT1w9on9-3ARAodj2YA_jP6PsR3_8ka-cyC02B-efRPDuIOVe_RLmgU3Snph3k1C7rLqn4GQA4EfU_ltGVn5pR-7YtvJroXwYU_C1G5H7AuLSUFHI45J8pVmtUGMsO1l9BAzBK72Bl3xPrvl6MbjfM48eUN1dW1KkEPth9MTULdkoC2NeNCyewejZsd0dtjpCpS8p3t4t7c";
    private $sms_code = "1111";
    private $campaign_id;
    private $rule_id;

    public function test_create_wallet()
    {
        //truncate database
        Wallet::truncate();
        Club::truncate();
        User::truncate();
        AccountType::truncate();
        Cost::truncate();
        Event::truncate();
        Product::truncate();
        Category::truncate();
        Attachment::truncate();
        Account::truncate();
        Credit::truncate();
        Order::truncate();
        ProductOrder::truncate();
        Transaction::truncate();
        Campaign::truncate();
        \DB::table("club_accounts")->truncate();
        \DB::table("products_categories")->truncate();
        \DB::table("order_cost")->truncate();

        $header = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "Bearer {$this->accessToken}",
        ];

        $walletType = ["rials", "point"];
        foreach ($walletType as $item) {
            $data = [
                "title" => $item,
                "type" => $item
            ];
            $response = $this->withHeaders($header)->json("POST", "api/wallet/create", $data);
        }

        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @return void
     */
    public function test_create_club()
    {
        Club::truncate();
        $faker = $this->faker;
        $data = [
            "name" => "loviuona",
            "code" => $faker->numberBetween(10, 100),
            "exchange_rate_point" => $faker->randomNumber(1),
            "exchange_rate_price" => $faker->randomNumber(1),
            "club_mode" => $faker->randomElement(["reward_only"]),
            "point_to_card" => $faker->boolean(50),
            "good_lock_delay" => $faker->boolean(40),
            "good_lock_point" => $faker->randomNumber(3),
            "good_lock_date" => $faker->date("Y-m-d"),
            "delay_penalty" => $faker->randomNumber(2),
            "give_point_at" => $faker->date("Y-m-d"),
            "point_expired_at" => $faker->date("Y-m-d"),
            "can_edit_point" => $faker->boolean(50),
            "can_refund" => $faker->boolean(50),
            "can_refund_until" => $faker->date("Y-m-d"),
            "can_cash_out_by_admin" => $faker->boolean(50),
            "can_cash_out" => $faker->boolean(50),
            "show_point" => $faker->boolean(50),
            "generate_card_immediately" => $faker->boolean(50),
            "card_expired_at" => $faker->date("Y-m-d"),
            "gift_card_expired_at" => $faker->date("Y-m-d"),
            "show_revocation_card_for_admin" => $faker->boolean(50),
            "minimum_shop_to_use_point" => $faker->randomNumber(3),
            "free_shiping" => $faker->boolean(50),
            "minimum_wage" => $faker->randomNumber(1),
            "maximum_wage" => $faker->randomNumber(3),
            "card_type" => $faker->randomElement(["mifare", "magnet"]),
            "checkout_time" => $faker->date("Y-m-d"),
        ];

        $header = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "Bearer {$this->accessToken}",
        ];
        $response = $this->withHeaders($header)->json("POST", "api/clubs", $data);
        $response->assertStatus(200);
    }

    /**
     * create campaign for club
     */
    public function test_create_campaign_for_club()
    {
        Campaign::truncate();
        $clubInstance = Club::all()->first();
        \Auth::loginUsingId(1);

        $data = [
            "club_id" => $clubInstance->id,
            "started_at" => "",
            "expired_at" => "",
            "budget" => 1000,
            "budget_consumed" => null,

            "type" => "campaign",
            "wallet_id" => "2",
            "title" => "روز مادر",
            "subtitle" => "تست",
            "description" => "تست",
            "balance_type" => "ziro",
            "min_account_amount" => 0,
            "max_account_amount" => 0,
            "min_transaction_amount" => 0,
            "max_transaction_amount" => 0,
            "legal" => false,
            "interest_rate" => 0,
            "interest_period" => "daily"
        ];

        $header = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "Bearer {$this->accessToken}",
        ];
        $response = $this->withHeaders($header)->json("POST", "api/campaigns", $data);

        /*$campaignAccountId = \DB::table("account_types as types")
            ->where("types.type", "=", "campaign")
            ->join("accounts", "types.id", "=", "accounts.account_type_id")
            ->select("accounts.id")
            ->first();*/

        $campaignInstance = Campaign::first();

        /**
         * create rule for campaign
         */
        $rulesData = [
            "campaign_id" => $campaignInstance->id,
            "params" => '$amount',
            "operators" => '>=',
            "values" => 1,
            "revoked" => false,
        ];
        $rulesInstance = Rule::create($rulesData);

        /**
         * create process for campaign
         */
        $processData = [
            "rule_id" => $rulesInstance->id,
            "class" => "test",
            "methods" => 'public function test($params){dd($params);}',
            "params" => 'test',
            "method" => 'test',
        ];
        $processInstance = Process::create($processData);

        $response->assertStatus(200);
    }

    /*-----------------create account type and user for club(customer and merchant) ---------------------*/
    public function test_create_account_type()
    {
        $faker = $this->faker;
        $types = ["merchant", "customer"];
        foreach ($types as $type) {
            for ($i = 1; $i <= 2; ++$i) {
                $data = [
                    "type" => $type,
                    "wallet_id" => $i,
                    "title" => $faker->text(20),
                    "subtitle" => $faker->text(30),
                    "description" => $faker->text(100),
                    "balance_type" => $faker->randomElement(["negative", "positive", "ziro"]),
                    "min_account_amount" => 0,
                    "max_account_amount" => 0,
                    "min_transaction_amount" => 0,
                    "max_transaction_amount" => 0,
                    "legal" => false,
                    "interest_rate" => 1,
                    "interest_period" => $faker->randomElement(["daily", "weekly", "yearly"]),
                    "revoked" => false,
                ];

                $header = [
                    "Content-Type" => "application/json",
                    "Accept" => "application/json",
                    "Authorization" => "Bearer {$this->accessToken}",
                ];

                $response = $this->withHeaders($header)->json("POST", 'api/accountType', $data);
            }
        }
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_register()
    {
        $faker = $this->faker;
        $name = $faker->name;
        $data = [
            "name" => $name,
            "email" => "{$faker->text(5)}@test.com",
            "password" => bcrypt(123),
            "mobile" => "093757270" . $faker->randomNumber(2),
            "birthday" => $faker->date("Y-m-d"),
            "gender" => $faker->randomElement(['male', 'female']),
            "type" => "customer",
            "club_id" => "1",
            "account_type" => "customer",
            "register_type" => "mobile",
        ];

        $header = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "Bearer {$this->accessToken}",
        ];
        $response = $this->withHeaders($header)->json("POST", "api/user/register", $data);
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @return void
     */
    public function test_merchant_register()
    {
        //login with merchant user
        $userDB = new UserDB();
        $getUserMerchant = $userDB->getUserWithType("club_admin");
        \Auth::loginUsingId($getUserMerchant->id);

        $data = [
            "name" => "korosh",
            "title" => "kafe viona",
            "description" => "باشگاه مشتریان",
            "address" => "tehran-jeyhon",
            "phone" => "0217854521",
            "parent_id" => 0,
        ];

        $header = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "Bearer {$this->accessToken}",
        ];
        $response = $this->withHeaders($header)->json("POST", "api/branch/merchants", $data);
        $response->assertStatus(200);
    }

    /*----------------------create category and product test ----------------------*/
    /**
     * create fake category test for club
     */
    public function test_create_category()
    {
        $faker = $this->faker;

        //truncate category table
        Category::truncate();

        for ($i = 0; $i < 10; ++$i) {
            $data = [
                "name" => $faker->randomElement([
                    "نوشیدنی ها",
                    "دم نوش ها",
                    "عرقیات",
                    "دسرها",
                    "کیک ها",
                    "کیک ها",
                    "شیک ها",
                    "نوشیدنیها",
                    "آبمیوه ها و کو کتل ها",
                    "ساندویچ ها",
                    "اسنک",
                ]),
                "subtitle" => $faker->text(20),
                "description" => $faker->text(50),
                "parent_id" => 0
            ];

            $headers = [
                "Content-Type" => "application/json",
                "Accept" => "application/json",
                "Authorization" => "Bearer {$this->accessToken}"
            ];

            $response = $this->withHeaders($headers)->json(
                "POST",
                "api/product/category",
                $data
            );
        }
        $response->assertStatus(200);
    }

    /**
     * test for create fake product.
     */
    public function test_create_product()
    {
        $faker = $this->faker;

        //truncate product table
        Product::truncate();

        //get all category id
        $category_id = Category::get()->pluck("id");

        //get some merchant user and submit product for them
        $merchants = Merchant::first();
        foreach ($merchants as $merchant) {
            for ($i = 0; $i < 20; ++$i) {
                $data = [
                    "name" => $faker->randomElement([
                        "کافه گلاسه",
                        "ترامیسو",
                        "بستنی مخصوص ویونا",
                        "برنانی گرم",
                        "کیک موکا",
                        "کیک موتلا",
                        "آیس آمریکانو",
                        "تارت موز فندق",
                        "آیس لاته",
                        "شیک کارامل",
                    ]),
                    "merchant_id" => $merchant->id,
                    "title" => $faker->title,
                    "category_id" => $faker->randomElement($category_id->toArray()),
                    "description" => $faker->text(50),
                    "costs" => [
                        "rials" => $faker->randomNumber(2),
                        "point" => $faker->randomNumber(1, true)]
                ];
                $headers = [
                    "Content-Type" => "application/json",
                    "Accept" => "application/json",
                    "Authorization" => "Bearer {$this->accessToken}"
                ];
                $response = $this->withHeaders($headers)->json("POST", "api/products", $data);
            }
            $response->assertStatus(200);
        }
//        $this->assertTrue(true);
    }

    /**
     * assign product to category
     */
    public function test_create_relation_between_product_and_category()
    {
        $faker = $this->faker;
        $product = Product::all()->pluck("id");
        $categories = Category::all();
        foreach ($categories as $category) {
            $category->products()->sync($faker->randomElements($product, 2, false));
        }
        $this->assertTrue(true);
    }

    /*----------------- create test for event (for buy some thing) -----------------*/
    /**
     * assign account to club
     */
    public function test_make_relation_club_with_account()
    {
        \DB::table("club_accounts")->truncate();
        //sync user with club
        $account = Account::pluck("id")->all();
        $club = Club::first();
        $club->accounts()->sync($account);
        self::assertTrue(true);
    }

    /*--------------create order test--------------*/
    public function test_create_product_order()
    {
        Order::truncate();
        ProductOrder::truncate();
        Transaction::truncate();
        Event::truncate();

        $merchantInstance = Merchant::first();

        $customerInstance = User::where("type", "=", "customer")->first();
        if (!$customerInstance instanceof User) {
            dd("customer id is not exist");
        }

        //customer user login
        \Auth::loginUsingId($customerInstance->id);
        $costsRial = Cost::where("wallet_id", 1)->take(2)->get();
        $costsPoint = Cost::where("wallet_id", 2)->take(2)->get();

        $data = [
            "costs" => [
                "rials" => [
                    $costsRial[0]->cost => 2,
                    $costsRial[1]->cost => 1
                ],
                "point" => [
                    $costsPoint[0]->cost => 1,
                    $costsPoint[1]->cost => 3
                ]
            ],
            "customer_id" => $customerInstance->id,
            "merchant_id" => $merchantInstance->id,
            "description" => "description"
        ];

        $header = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "Bearer {$this->accessToken}",
        ];
        $instance = $this->withHeaders($header)->json("POST", "api/product/orders", $data);
        $instance->assertStatus(200);
    }
}
