<?php

namespace Tests\Feature;

use App\User;
use Modules\Product\Entities\Cost;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\repo\WalletDB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TableOrderTest extends TestCase
{
    use WithFaker;
//    use RefreshDatabase;
    private static $accessToken = "";

    /**
     * first step (login with merchant user)
     */
    public function test_login()
    {
        //get merchant user randomly
        $merchantUserInstance = User::where("type", "merchant")->first();

        //send mobile number for validation
        $data = [
            "mobile" => $merchantUserInstance->mobile
        ];

        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "accept-language" => "en",
        ];
        $response = $this->withHeaders($headers)->json("POST", "api/user/login", $data);

        //send validation code
        $data = [
            "mobile" => $merchantUserInstance->mobile,
            "sms_code" => 1111,
        ];
        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "accept-language" => "en",
        ];
        $response2 = $this->withHeaders($headers)->json("POST", "api/user/mobile_confirm", $data);
        self::$accessToken = $response2->json()["data"]["accessToken"];
        $response2->assertStatus(200);
    }

    /**
     * charge customer account with random bank account
     */
    public function test_payment_from_bank()
    {
        //get customer user randomly
        $customerUser = User::where("type", "like", "%customer%")->first();

        //get bank account
        $bankAccountInstance = Account::with('accountType')->whereHas("accountType", function ($query) {
            $query->where("type", "like", "%bank%");
        })->first();

        //get order id who is not paid
        $data = [
            "customer_id" => $customerUser,
            "amount" => $this->faker->numberBetween(1000, 100000),
            "bank_account_id" => $bankAccountInstance->id,
        ];

        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "accept-language" => "en",
            "authorization" => "Bearer " . self::$accessToken
        ];
        $response = $this->withHeaders($headers)->json("POST", "api/payment", $data);
        $response->assertStatus(200);
    }

    /**
     *create gift or table order
     * @return void
     */
    public function test_create_gift_and_table_orders()
    {
        //get merchant user for login
        $merchantUserInstance = User::where("type", "merchant")->first();
//        \Auth::loginUsingId($merchantUserInstance->id);

        //get customer user randomly
        $customerUser = User::where("type", "like", "%customer%")->first();

        //get cost id (type == point) randomly
        $walletDB = new WalletDB();
        $walletInstance = $walletDB->getTreasuryAccountWithWalletType("point");
        $cost = Cost::where("wallet_id", $walletInstance->id)->first();

        $data = [
            "customer_id" => $customerUser->id,
            "author_id" => 1,
            //gift id
//            "cost_id" => $cost->id,
            //table reserve
            "tables" => [
                rand(1, 10),
                rand(1, 10)
            ]
        ];
        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "accept-language" => "en",
            "authorization" => "Bearer " . self::$accessToken
        ];
        $response = $this->withHeaders($headers)->json("POST", "api/table/order", $data);
        $response->assertStatus(200);
    }

    /**
     * paid table order and give point
     */
    public function test_payment_table_orders()
    {
        //get order id who is not paid
        $order = Order::whereNull("paid_at")->latest("created_at")->first();
        if (!$order instanceof Order) {
            dd("you dont have any order");
        }

        $data = [
            "order_id" => $order->id,
            "amount" => $this->faker->numberBetween(1000, 10000)
        ];

        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "accept-language" => "en",
            "authorization" => "Bearer " . self::$accessToken
        ];
        $response = $this->withHeaders($headers)->json("POST", "api/table/order/{$order->id}", $data);
        $response->assertStatus(200);
    }

    public function test_run()
    {

        //get login
        $this->test_login();
        echo "merchant user login successfully \n";

        //charger my rials credit from bank account
        $this->test_payment_from_bank();
        echo "charge customer rials account credit successfully \n";

        //submit table order
        $this->test_create_gift_and_table_orders();
        echo "order submit successfully \n";

        //payment last order submit
        $this->test_payment_table_orders();
        echo "payment successfully \n";
    }
}
