docker exec -ti app php artisan custom:reset
docker exec -ti app php artisan custom:fresh
docker exec -ti app php artisan db:seed
docker exec -ti app php artisan passport:install
