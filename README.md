# Loyalty


# Installation (without docker)

at the first clone project under the loyalty vhost

`in the terminal write blow commands`
```
 cp .env.example .env
 composer install (or update)
 php artisan key:generate
 ```
 `edit .env file`
 * db user & db pass & db name
 
 `create loyalty db`
 ```
php artisan migrate
php artisan passport:install
```

### Requirements

* php7 or above  
* mysql
* nginx/apache
* redis 
* php extensions: 
    *   php redis
    *   php7.2-sqlite3 (ubuntu) 
    *   uncomment extension=pdo_sqlite
    *   mysql_xdevapi
    *   php7.2-bcmath
 

# Docker Installation

Note : `use docker without sudo`
```
docker-compose up -d
```

### Requirements
* docker
* docker compose
