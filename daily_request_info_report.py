#!/usr/bin/python3.6

###########################################################
#
# using pymsql and tar utility.
#
# Written by : MmrDev
# Tested with : Python 3.6
#
##########################################################

import time
from datetime import date
import os
import pipes
import pymysql
import json
import re


class Db:
    def __init__(self, db_user, db_pass,
                 db_name,
                 db_host='localhost',
                 db_port='3306'):
        self.db_url = db_host
        self.db_port = db_port
        self.db_user = db_user
        self.db_pass = db_pass
        self.db_name = db_name

    def dbConnection(self):
        db_conn = pymysql.connect(host=self.db_url,
                                  user=self.db_user,
                                  password=self.db_pass)
        return db_conn

    def dbQuery(self, sql, conn):
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute(sql)
        return cur


class File:
    def __init__(self, name):
        self.filename = name

    def write(self, logString):
        file = os.path.join(address, self.filename)
        text_file = open(file, "a+")
        text_file.write(logString)
        text_file.write('\n')
        text_file.close()

    def read(self, fName):
        file = os.path.join(address, self.filename)
        text_file = open(file, "r")
        text_file.read(file)
        return text_file

    def deleteContent(self, file):
        file.seek(0)
        file.truncate()


db_url = 'localhost'
db_port = '3306'
db_user = 'root'
db_pass = 'Menta@123'
db_name = 'loyalty'
address = 'storage/request_info/'
ip = '127.0.0.1'
file_name = 'daily_request_info_{}.json'.format(date.today())

def main():
    db = Db(db_user, db_pass, db_url)
    conn = db.dbConnection()
    sql = "select url, count(url) as count from 3rdParty.request_info where date(created_at) = '" + format(
        date.today()) + "' and ip = '" + ip + "' group by url"
    cur = db.dbQuery(sql, conn)
    file = File(file_name)
    file.write('[')
    for row in cur:
        name = row['url'].split('/')[5]
        # if name.isnumeric():
        #     name = row['url'].split('/')[-2]
        logRow = {
                     'name': name,
                     'count': row['count'],
                     'url': row['url'],
                 },
        logRow = str(logRow).replace("'", '"')
        logRow = logRow.replace('(', '')
        logRow = logRow.replace(')', '')
        file.write("%s" % logRow)
    file.write(']')
    logRow = open(os.path.join(address, file_name), 'r').read()
    f = open(os.path.join(address, file_name), 'r+')
    f.truncate(0)
    logRow = logRow[::-1]
    logRow = logRow.replace(',', '', 1)
    logRow = logRow[::-1]
    file.write(logRow)
if __name__ == '__main__':
    main()
