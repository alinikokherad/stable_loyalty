<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    public $validate = [];

    protected $fillable = [
        "user_id",
        "application_id",
        "settings",
        "revoked",
    ];

    public function applications()
    {
        return $this->belongsTo(Application::class, "application_id");
    }


}
