<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DorrisAddUserManualEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $userInstance;
    public $message;

    /**
     * DorrisAddUserManualEvent constructor.
     * @param $userInstance
     * @param $message
     */
    public function __construct($userInstance, $message)
    {
        $this->userInstance = $userInstance;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
