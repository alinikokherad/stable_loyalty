<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RequestRegisterCafeViunaSmsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $mobile;
    public $message;

    /**
     * RequestRegisterCafeViunaSmsEvent constructor.
     * @param $mobile
     * @param $pinCode
     * @param $app_link
     */
    public function __construct($mobile, $pinCode, $app_link)
    {
        $this->message = "به باشگاه مشتریان کافه ویونا (لاویونا)
خوش آمدید
رمز عضویت شما: {$pinCode}
لینک دانلود اپ کافه ویونا:
{$app_link}";

        $this->mobile = $mobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
