<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RegisterDorisCustomerSmsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var string
     */
    public $message;
    public $mobile;

    /**
     * RegisterDorisCustomerSmsEvent constructor.
     * @param $mobile
     * @param $card_number
     * @param $card_password
     */
    public function __construct($mobile, $card_number, $card_password)
    {
        $this->message = "به باشگاه طلایی مشتریان درریس
خوش آمدید
شماره کارت عضویت شما:
{$card_number}
رمز کارت شما: {$card_password}";
        $this->mobile = $mobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
