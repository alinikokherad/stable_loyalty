<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendDorisGiftSmsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var string
     */
    public $mobile;
    public $message;

    /**
     * SendDorisGiftSmsEvent constructor.
     * @param $mobile
     * @param $point
     * @param $total_point
     */
    public function __construct($mobile, $point, $total_point)
    {
        $this->message = "باشگاه طلایی مشتریان درریس
امتیاز دریافتی شما در این خرید:
{$point} امتیاز
موجودی امتیاز شما: {$total_point}
با تشکر از حضور و خرید شما";
        $this->mobile = $mobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
