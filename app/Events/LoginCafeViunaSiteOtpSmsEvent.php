<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LoginCafeViunaSiteOtpSmsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var string
     */
    public $message;
    public $mobile;

    /**
     * LoginCafeViunaSiteOtpSmsEvent constructor.
     * @param $mobile
     * @param $sms_code
     */
    public function __construct($mobile, $sms_code)
    {
        $this->message = "باشگاه مشتریان کافه ویونا (لاویونا)
رمز یکبار مصرف برای ورود به سایت:
{$sms_code}";
        $this->mobile = $mobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
