<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DorisPanelLoginOtpEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var string
     */
    public $message;
    public $mobile;

    /**
     * DorrisPanelLoginOtpEvent constructor.
     * @param $mobile
     * @param $sms_code
     */
    public function __construct($mobile, $sms_code)
    {
        $this->message = "باشگاه طلایی مشتریان درریس
رمز ورود شما به سایت باشگاه: {$sms_code}";
        $this->mobile = $mobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
