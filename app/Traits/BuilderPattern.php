<?php


namespace App\Traits;


trait BuilderPattern
{
    protected $item = [];

    /**
     * BuilderPattern constructor.
     * @param $item
     */
    public function __construct()
    {
        $allMethods = get_class_methods($this);
        foreach ($allMethods as $key => $method) {
            if ($method == "get") {
                unset($allMethods[$key]);
            } elseif ($method == "__construct") {
                unset($allMethods[$key]);
            }
        }
        $allMethods = get_class_vars(get_called_class());
        $this->item = $allMethods;
//        $this->item = $allMethods["method"];
    }


    /**
     * return finally data
     * @return array
     */
    protected function get()
    {
        return $this->item;
    }
}
