<?php


namespace App\Traits;


use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;

trait SmsSender
{

    public function smsSend($api_key = null, $sender, $message, $receptor = [])
    {
        //input normalization
        if (!is_array($receptor)) {
            $receptor = (array)$receptor;
        }

        try {
            $api = new KavenegarApi($api_key);
            $result = $api->Send($sender, $receptor, $message);
            $response = [];
            if ($result) {
                foreach ($result as $r) {
                    $response["messageid"] = $r->messageid;
                    $response["message"] = $r->message;
                    $response["status"] = $r->status;
                    $response["statustext"] = $r->statustext;
                    $response["sender"] = $r->sender;
                    $response["receptor"] = $r->receptor;
                    $response["date"] = $r->date;
                    $response["cost"] = $r->cost;
                }
            }
            \Log::info($response);
        } catch (ApiException $e) {
            \Log::info($e->errorMessage());
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => $e->errorMessage(),
                ], 400);
        } catch (HttpException $e) {
            \Log::info($e->errorMessage());
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => $e->errorMessage(),
                ], 400);
        }
        return response()
            ->json([
                "message" => __("messages.information"),
                "data" => $response,
            ], 200);
    }
}
