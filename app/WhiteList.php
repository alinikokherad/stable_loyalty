<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhiteList extends Model
{
    protected $connection = 'sqlite';
    protected $table = 'white_list';
    protected $fillable = ["mobile", "user_id"];
}
