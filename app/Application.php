<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{

    public $validate = [];

    protected $fillable = [
        "name",
        "revoked",
    ];

    public function settings()
    {
        return $this->hasMany(UserSetting::class,"application_id");
    }


}
