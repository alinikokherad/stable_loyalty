<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use ErrorException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     * @param \Exception $exception
     * @return void
     * @throws
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        dd(
                    $exception->getMessage(),
                    $exception->getFile(),
                    $exception->getLine()
        );
        if ($request->wantsJson()) {
            $exception = $this->prepareException($exception);
            if ($exception instanceof ErrorException) {
                return response()->json(['message' => __('messages.some_thing_went_wrong'), "error" => $exception->getMessage()], 400);
            }
            if ($exception instanceof QueryException) {
                return response()->json(['message' => __('messages.query_error'), "error" => $exception->getMessage()], 400);
            }
            if ($exception instanceof CreateAccountException) {
                return response()->json(['message' => __('messages.account_error'), "error" => $exception->getMessage()], 400);
            }
            if ($exception instanceof CreateWalletException) {
                return response()->json(['message' => __('messages.wallet_error'), "error" => $exception->getMessage()], 400);
            }
            if ($exception instanceof CreateOrderException) {
                return response()->json(['message' => __('messages.order_error'), "error" => $exception->getMessage()], 400);
            }
            if ($exception instanceof MerchantException) {
                return response()
                    ->json([
                        'message' => __("messages.400"),
                        'error' => $exception->getMessage(),
                    ], 400);
            }
            if ($exception instanceof \LogicException) {
                return response()
                    ->json([
                        'message' => __("messages.invalid_argument_exception"),
                        'error' => $exception->getMessage(),
                    ], 400);
            }
        }
        return parent::render($request, $exception);
    }
}
