<?php

namespace App\Exceptions;

use App\Traits\Response;
use Exception;

class ProductOrderException extends Exception
{
    //
    use Response;

    public function render($request)
    {
        $code = parent::getCode();
        $message = parent::getMessage();
        return $this->errorResponse("400", __("messages.{$message}"), $code);
    }
}
