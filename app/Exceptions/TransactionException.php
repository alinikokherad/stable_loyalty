<?php

namespace App\Exceptions;

use Exception;

class TransactionException extends Exception
{
    public function render($request)
    {
        return response()
            ->json([
                "message"=>"some thing went wrong",
                "error"=>"transaction is not create"
            ],400);
    }
}
