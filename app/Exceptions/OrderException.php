<?php

namespace App\Exceptions;

use App\Traits\Response;
use Exception;

class OrderException extends Exception
{
    use Response;

    public function render($request)
    {
        $code = $this
            ->getCode();
        $message = $this
            ->getMessage();

        return $this
            ->errorResponse("400", __("messages." . $message), $code);
    }
}
