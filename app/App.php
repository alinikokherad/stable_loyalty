<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    //
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, "app_id");
    }
}
