<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clubArriveRules extends Model
{
    protected $table = "user_club";
    protected $fillable = [
        "club_id",
        "user_id",
        "access",
    ];
}
