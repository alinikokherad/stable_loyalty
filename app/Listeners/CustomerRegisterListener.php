<?php

namespace App\Listeners;

use App\Events\CustomerRegisterEvent;
use App\Exceptions\CreateAccountException;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Events\AccountCreatedEvent;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\WalletDB;
use mysql_xdevapi\Exception;

class CustomerRegisterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @throws CreateAccountException
     * @throws \Throwable
     */
    public function handle($event)
    {
        $accountDB = new AccountDB();
        $walletDB = new walletDB();
        $clubDB = new ClubDB();
        $data = $event->data;
        try {
            /*---------- create account transaction --------*/
            \DB::beginTransaction();

            //get account type id for account
            $data["account_type"] = "customer";
            $accountTypeDB = new AccountTypeDB();
            $accountTypeInstances = $accountTypeDB->getAccountTypeWithType($data["account_type"]);

            //if create account type dose not exist
            if (!$accountTypeInstances->first() instanceof AccountType) {
                throw new \Exception(__("messages.please_create_account_type_for_{$data["account_type"]}"), 500);
            }

            //create account for each wallet type
            foreach ($accountTypeInstances as $accountTypeInstance) {
                $walletTreasuryInstance = $walletDB->getTreasuryAccountInstanceWithWalletId($accountTypeInstance->wallet_id);
                //create account
                $accountData = [
                    "belongs_to" => $event->user->id,
                    "belongs_type" => "user",
                    "treasury_account_id" => $walletTreasuryInstance->id,
                    "account_type_id" => $accountTypeInstance->id,
                ];
                $accountInstance = $accountDB->create($accountData);
                $data["treasury_account_id"] = $walletTreasuryInstance->id;

                if (!$accountInstance instanceof Account) {
                    throw new \Exception(__("messages.system_cannot_create_account_for_user"), 500);
                }

                //after account create this event happen (create credit for account)
                event(new AccountCreatedEvent($accountInstance, $data));
            }
            \DB::commit();

            //TODO شرایط لازم برای ورود یک کاربر به یک باشگاه اینجا اعمال خواهد شد
            //sync user with free club
            $allFreeClub = $clubDB->getFreeClubs();
            $event->user->clubs()->sync($allFreeClub);

        } catch (\Exception $e) {
            \DB::rollBack();
            throw new CreateAccountException("some thing went wrong:{$e->getMessage()}");
        }
    }
}
