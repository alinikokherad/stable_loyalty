<?php

namespace App\Listeners;

use App\Jobs\SendSmsJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegisterSendSmsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     */
    public function handle($event)
    {
        //sms code put into queue for send with sms server
        SendSmsJob::dispatchNow($event->mobile, $event->message);
//            ->delay(10)
//            ->onQueue("send_sms");
    }
}
