<?php

namespace App\Listeners;

use App\Jobs\SendCustomerPasswordSmsJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserPasswordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        SendCustomerPasswordSmsJob::dispatchNow($event->userInstance);
    }
}
