<?php

namespace App\Listeners;

use App\Jobs\SendDorisGiftSmsJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDorisGiftSmsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $mobile = $event->mobile;
        $message = $event->message;
        //call job for send pin code for user
        SendDorisGiftSmsJob::dispatchNow($mobile, $message);
    }
}
