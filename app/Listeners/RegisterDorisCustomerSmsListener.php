<?php

namespace App\Listeners;

use App\Jobs\RegisterDorisCustomerSmsJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterDorisCustomerSmsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $mobile = $event->mobile;
        $message = $event->message;
        //call job for send pin code for user
        RegisterDorisCustomerSmsJob::dispatchNow($mobile, $message);
    }
}
