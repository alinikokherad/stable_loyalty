<?php


namespace App\ObjectFactory;


use App\Address;
use App\Application;
use App\Card;
use App\Menu;
use App\repo\UserDB;
use App\repo\WhiteListDB;
use App\RequestInfo;
use App\Role;
use App\User;
use App\UserSetting;
use App\WhiteList;
use Modules\Attachment\Entities\Attachment;
use Modules\Branch\Entities\Merchant;
use Modules\Branch\repo\MerchantDB;
use Modules\Club\Entities\Bootstrap;
use Modules\Club\Entities\Club;
use Modules\Club\Entities\ClubAccount;
use Modules\Club\Entities\ClubLevel;
use Modules\Club\repo\ClubDB;
use Modules\Crm\Entities\Ticket;
use Modules\Event\Entities\Event;
use Modules\Event\repo\EventDB;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Table;
use Modules\Product\Repo\CategoryDB;
use Modules\Product\repo\CostDB;
use Modules\Product\repo\ProductDB;
use Modules\Product\Repo\TableDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\Campaign;
use Modules\Wallet\Entities\Credit;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\Process;
use Modules\Wallet\Entities\Rule;
use Modules\Wallet\Entities\Transaction;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\Entities\WalletType;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\CampaignDB;
use Modules\Wallet\repo\CreditDB;
use Modules\Wallet\repo\OrderDB;
use Modules\Wallet\repo\TransactionDB;
use Modules\Wallet\repo\WalletDB;
use mysql_xdevapi\Exception;

/**
 * Class ModelFactory
 * @package App\ObjectFactory
 */
class ModelFactory
{
    /**
     * @param $className string
     * @param null $args integer|bool
     * @return Address|Application|Menu|AddressDB|ApplicationDB|AttachmentDB|BootstrapDB|ClubAccountDB|ClubLevelDB|MenuDB|ProcessDB|requestInfoDB|RoleDB|RuleDB|WalletTypeDB|UserDB|WhiteListDB|RequestInfo|Role|User|UserSetting|WhiteList|Attachment|Merchant|MerchantDB|Bootstrap|Club|ClubAccount|ClubLevel|ClubDB|Ticket|Event|EventDB|Category|Cost|Product|Table|CategoryDB|CostDB|ProductDB|TableDB|Account|AccountType|Campaign|Credit|Order|Process|Rule|Transaction|Wallet|WalletType|AccountDB|AccountTypeDB|CampaignDB|CreditDB|OrderDB|TransactionDB|WalletDB
     */
    public static function build(string $className, $args = null)
    {
        switch ($className) {
            case "User":
                return new User();
                break;
            case "Card":
                return new Card();
                break;
            case "UserDB":
                return new UserDB();
                break;
            case "Account":
                return new Account();
                break;
            case "AccountDB":
                return new AccountDB();
                break;
            case "AccountTypeDB":
                return new AccountTypeDB();
                break;
            case "AccountType":
                return new AccountType();
                break;
            case "CampaignDB":
                return new CampaignDB();
                break;
            case "Campaign":
                return new Campaign();
                break;
            case "Table":
                return new Table();
                break;
            case "TableDB":
                return new TableDB();
                break;
            case "Address":
                return new Address();
                break;
            case "AddressDB":
                return new AddressDB();
                break;
            case "Application":
                return new Application();
                break;
            case "ApplicationDB":
                return new ApplicationDB();
                break;
            case "Menu":
                return new Menu();
                break;
            case "MenuDB":
                return new MenuDB();
                break;
            case "requestInfo":
                return new requestInfo();
                break;
            case "requestInfoDB":
                return new requestInfoDB();
                break;
            case "RoleDB":
                return new RoleDB();
                break;
            case "Role":
                return new Role();
                break;
            case "UserSetting":
                return new UserSetting();
                break;
            case "Attachment":
                return new Attachment();
                break;
            case "AttachmentDB":
                return new AttachmentDB();
                break;
            case "Merchant":
                return new Merchant();
                break;
            case "MerchantDB":
                return new MerchantDB();
                break;
            case "Bootstrap":
                return new Bootstrap();
                break;
            case "BootstrapDB":
                return new BootstrapDB();
                break;
            case "Club":
                return new Club();
                break;
            case "ClubDB":
                return new ClubDB();
                break;
            case "ClubAccount":
                return new ClubAccount();
                break;
            case "ClubAccountDB":
                return new ClubAccountDB();
                break;
            case "ClubLevel":
                return new ClubLevel();
                break;
            case "ClubLevelDB":
                return new ClubLevelDB();
                break;
            case "Ticket":
                return new Ticket();
                break;
            case "Event":
                return new Event();
                break;
            case "EventDB":
                return new EventDB();
                break;
            case "Category":
                return new Category();
                break;
            case "CategoryDB":
                return new CategoryDB();
                break;
            case "Cost":
                return new Cost();
                break;
            case "CostDB":
                return new CostDB();
                break;
            case "Product":
                return new Product();
                break;
            case "ProductDB":
                return new ProductDB();
                break;
            case "Credit":
                return new Credit();
                break;
            case "CreditDB":
                return new CreditDB();
                break;
            case "Order":
                return new Order();
                break;
            case "OrderDB":
                return new OrderDB();
                break;
            case "Process":
                return new Process();
                break;
            case "ProcessDB":
                return new ProcessDB();
                break;
            case "Rule":
                return new Rule();
                break;
            case "RuleDB":
                return new RuleDB();
                break;
            case "Transaction":
                return new Transaction();
                break;
            case "TransactionDB":
                return new TransactionDB();
                break;
            case "Wallet":
                return new Wallet();
                break;
            case "WalletDB":
                return new WalletDB();
                break;
            case "WalletType":
                return new WalletType();
                break;
            case "WalletTypeDB":
                return new WalletTypeDB();
                break;
            case "WhiteListDB":
                return new WhiteListDB();
                break;
            case "WhiteList":
                return new WhiteList();
                break;
            default:
                throw new Exception("this class is not exist in repository");
        }
    }
}
