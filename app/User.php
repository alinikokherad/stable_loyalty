<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Modules\Account\Entities\Role;
use Modules\Attachment\Entities\Attachment;
use Modules\Branch\Entities\Merchant;
use Modules\Tier\Entities\Tier;
use Modules\Wallet\Entities\Account;
use Modules\Club\Entities\Club;
use Modules\Wallet\Entities\ProductOrder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * Class User
 *
 * @package App
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $mobile
 * @property string|null $mobile_verified_at
 * @property string|null $password
 * @property string|null $birthday
 * @property string|null $gender
 * @property string $type
 * @property int $otp
 * @property string $tier
 * @property string|null $qr_code
 * @property string $status
 * @property int $revoked
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Account\Entities\AccountAttribute[] $accountAttributes
 * @property-read int|null $account_attributes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $accounts
 * @property-read int|null $accounts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Club\Entities\Club[] $clubs
 * @property-read int|null $clubs_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Account\Entities\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMobileVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereOtp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereQrCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $settings
 * @property int|null $merchant_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\ProductOrder[] $productOrders
 * @property-read int|null $product_orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMerchantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSettings($value)
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Searchable;

    protected $fillable = [
        "author_id",
        "name",
        "family",
        "email",
        "mobile",
        "degree",
        "pinCode",
        "mobile_verified_at",
        "password",
        "birthday",
        "gender",
        "type",
        "otp",
        "tier_id",
        "settings",
        "status",
        "revoked",
        "merchant_id",
    ];
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $validate = [
        "password" => "required",
        "mobile" => "required",
        "type" => "required",
        "register_type" => "required",
        "account_type" => "required",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'pinCode',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //delete credit record when we delete account
    /*    public static function boot()
        {
            parent::boot();

            static::deleting(function ($account) { // before delete() method call this
                $account->credit()->delete();
                // do the rest of the cleanup...
            });
        }*/

    //authentication with custom username
    /*    public function findForPassport($username)
        {
            return $this->where('username', $username)->first();
        }*/


    /*------------------ tnt search -------------------*/
    public $asYouType = true;

    /**
     * Get the indexable data array for the model.
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }

    /*----------------------- relations ---------------------------*/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(Account::class, "belongs_to", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany(Club::class, "user_club", "user_id", "club_id")->withPivot("access");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role', 'user_id', 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productOrders()
    {
        return $this->hasMany(ProductOrder::class, "user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function merchants()
    {
        return $this->belongsToMany(Merchant::class, "merchant_user", "user_id", "merchant_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function addresses()
    {
        return $this->morphOne(Address::class, "addressable");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function attaches()
    {
        return $this->morphOne(Attachment::class, "attachable");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tier()
    {
        return $this->belongsTo(Tier::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        return $this->hasMany(Card::class, "user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function app()
    {
        return $this->belongsTo(App::class);
    }
}
