<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    protected $table = "terminals";
    protected $fillable = [
        "user_id",
        "merchant_id",
        "terminal_id",
        "serial",
        "description",
    ];

    public function users()
    {
        return $this->hasMany(User::class, "id", "user_id");
    }
}
