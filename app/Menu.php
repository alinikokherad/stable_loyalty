<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property int $id
 * @property int $menu_id
 * @property int $parent_id
 * @property string $title
 * @property string|null $subtitle
 * @property string $font_icon
 * @property string|null $link
 * @property int $priority_view
 * @property int $revoked
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Menu extends Model
{
    protected $fillable = [
        'menu_id',
        'parent_id',
        'title',
        'subtitle',
        'link',
        'font_icon',
        'status',
        'priority_view',
        'revoked',
    ];
}
