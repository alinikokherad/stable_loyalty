<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RequestInfo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RequestInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RequestInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RequestInfo query()
 * @mixin \Eloquent
 */
class RequestInfo extends Model
{
    public $validate = [];

    protected $table = "request_info";

    protected $fillable = [
        "ip",
        "url",
        "method",
        "memory",
        "response_status",
        "duration",
    ];
}
