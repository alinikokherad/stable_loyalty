<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\TransactionDB;

class TransactionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $transactionData;

    /**
     * TransactionJob constructor.
     * @param $transactionData
     */
    public function __construct($transactionData)
    {
        $this->transactionData = $transactionData;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        //call transaction repository
        $transactionDB = new TransactionDB();
        $accountDB = new AccountDB();

        //create transaction record
        $transactionInstance = $transactionDB
            ->create($this->transactionData);

        if (!$transactionInstance) {
            \Log::error("transaction_not_create==>" . json_encode($this->transactionData));
        }

        //get account instance
        $fromAccountInstance = $accountDB->get($this->transactionData["from_account_id"]);
        $toAccountInstance = $accountDB->get($this->transactionData["to_account_id"]);

        /*----------- calculate accounts credit for from_account -------------*/
        //get from_account credit
        $old_credit = $fromAccountInstance->credits->first();
        $new_credit = ($old_credit->amount) - ($this->transactionData["amount"]);

        //update from_account credit
        $old_credit->update([
            "amount" => $new_credit
        ]);

        /*----------- calculate accounts credit for to_account -------------*/
        //get to_account credit
        $old_credit = $toAccountInstance->credits->first();
        $new_credit = ($old_credit->amount) + ($this->transactionData["amount"]);

        //update from_account credit
        $old_credit->update([
            "amount" => $new_credit
        ]);

        return $transactionInstance;
    }
}
