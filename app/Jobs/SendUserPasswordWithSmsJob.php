<?php

namespace App\Jobs;

use App\Http\Controllers\Api\SmsController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendUserPasswordWithSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $userInstance;
    public $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userInstance, $message = null)
    {
        $this->userInstance = $userInstance;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //create smsController for send sms
        $smsController = new SmsController();
        $api_key = env("API_KEY", "324A522B5239534D5A456B6F4472436B4D422F32306A4935337771556E4B4C6564684E335769735A2F30773D");
        $sender = env("SENDER", "10001007001000");
        if (empty($this->message)) {
//            $message = "رمز ورود شما به باشگاه مشتریان کافه ویونا: {$this->userInstance->pinCode}";
            $message = "باشگاه مشتریان کافه ویونا (لاویونا)
رمز عضویت شما: {$this->userInstance->pinCode}";
            //set log for sms
            \Log::info("sent sms to ====>{$this->userInstance->mobile} and pin code is {$this->userInstance->pinCode}");
        } else {
            $message = $this->message;
        }
        $smsController->send($api_key, $sender, $message, $this->userInstance->mobile);
    }
}
