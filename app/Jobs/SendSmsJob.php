<?php

namespace App\Jobs;

use App\Http\Controllers\Api\SmsController;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Modules\Account\Entities\Account;
use Dotenv\Dotenv;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Account
     */
    private $mobile;
    private $message;

    /**
     * SendSmsJob constructor.
     * @param $mobile
     * @param $message
     */
    public function __construct($mobile, $message)
    {
        $this->mobile = $mobile;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $smsController = new SmsController();
        $smsController->send($this->mobile, $this->message);
    }
}
