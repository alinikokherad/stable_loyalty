<?php


namespace App\Classes\Ordering;


use App\Classes\Ordering\Interfaces\Cancel;
use App\Classes\Ordering\Interfaces\Create;
use App\Classes\Ordering\Interfaces\Init;
use App\Classes\Ordering\Interfaces\Reverse;
use App\Exceptions\OrderException;
use App\Traits\Response;
use Illuminate\Support\Str;
use Modules\Club\repo\ClubDB;
use Modules\Product\Entities\Product;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\OrderDB;

class Order implements Create, Cancel, Reverse, Init
{
    use Response, WalletXChange, Credit, Transaction, Campaign, Event;

    //construct define this
    public static $orderDB;

    //init method define these
    public $items;
    public $orderInstance;
    public $fromAccountInstance;
    public $toAccountInstance;
    public $fromAccountsClubsInstances;
    public $toAccountsClubsInstances;
    public $orderData;

    //getProductOrder method define this
    public $productOrderInstance;

    //commonClub method define this
    public $commonClubInstance;

    public function __construct($productOrderInstance = null)
    {
        if (isset($productOrderInstance)) {
            $this->productOrderInstance = $productOrderInstance;
        }
        self::$orderDB = new OrderDB();
    }

    /**
     * step1
     * @param $items
     * @return $this
     * @throws \Exception
     */
    public function init($items)
    {
        $accountDB = new AccountDB();
        $clubDB = new ClubDB();
        if ($items instanceof \Modules\Wallet\Entities\Order) {
            $this->orderInstance = $items;
            $this->productOrderInstance = isset($items->productOrder) ? $items->productOrder : null;
            $this->fromAccountInstance = $accountDB->findOrFail($items->from_account_id);
            $this->toAccountInstance = $accountDB->findOrFail($items->to_account_id);

        } else {
            $item = $this->inputNormalize($items);
            $this->items = $item;
            $this->fromAccountInstance = $accountDB->findOrFail($item["from_account_id"]);
            $this->toAccountInstance = $accountDB->findOrFail($item["to_account_id"]);
        }

        try {
            $this->fromAccountsClubsInstances = $clubDB->findClubsWithAccountId($this->fromAccountInstance->id);
            $this->toAccountsClubsInstances = $clubDB->findClubsWithAccountId($this->toAccountInstance->id);

            //create order data
            $orderData = [
                "from_account_id" => $item["from_account_id"] ?? $this->fromAccountInstance->id,
                "to_account_id" => $item["to_account_id"] ?? $this->toAccountInstance->id,
                "amount" => $this->items["amount"] ?? $this->orderInstance->amount,
                "goods_id" => (isset($this->productOrderInstance)) ?
                    $this->productOrderInstance->id :
                    (isset($this->items["goods_id"]) ?: null),
                "author_id" => isset($this->items["author_id"]) ? $this->items["author_id"] : (isset($this->orderInstance->author_id) ?: null),
                "from_treasury_account_id" => $this->fromAccountInstance->treasury_account_id,
                "to_treasury_account_id" => $this->toAccountInstance->treasury_account_id,
                "order_payment_id" => isset($this->items["order_payment_id"]) ? $this->items["order_payment_id"] : (isset($this->orderInstance->order_payment_id) ?: null),
                "refund" => isset($this->items["refund"]) ? $this->items["refund"] : (isset($this->orderInstance->refund) ?: null),
                "UID" => Str::uuid()->toString(),
                "cash_out" => isset($this->items["cash_out"]) ? $this->items["cash_out"] : (isset($this->orderInstance->cash_out) ?: null),
                "type" => isset($this->items["type"]) ? $this->items["type"] : (isset($this->orderInstance->type) ?: null),
            ];
            $this->orderData = $orderData;
        } catch (\Exception $exception) {
            throws(\Exception::class, $exception->getMessage(), $exception->getCode());
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function create()
    {
        //create fundamental order
        $this->orderInstance = self::$orderDB->create($this->orderData);
        return $this;
    }

    /**
     * @param null $order_id
     * @return $this
     * @throws OrderException
     */
    public function success($order_id = null)
    {
        $response = self::$orderDB->updatePaid_at($order_id ?? $this->orderInstance->id);
        if ($response) {
            $this->orderInstance = self::$orderDB->find($order_id ?? $this->orderInstance->id);
        }
    }

    /**
     * step3
     * @return $this
     * @throws OrderException
     */
    public function checkout()
    {
        try {
            \DB::beginTransaction();

            //check order checkout before or not
            if ($this->orderInstance->paid_at != null) {
                throw new \Exception("this_order_checkout_before", 400);
            }

            if ($this->needXChange()) {
                //create x_change transaction
                $this->initXChange()->checkXChange()->createXChangeCredit();
            } else {
                //create normal transaction
                $this->createCredit();
            }

            \DB::commit();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw new OrderException($exception->getMessage(), $exception->getCode());
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->orderInstance;
    }

    /**
     * @param $order_id
     * @return $this
     */
    public function cancel($order_id)
    {
        $response = self::$orderDB->updateRevoked($order_id);
        if ($response) {
            $this->orderInstance = self::$orderDB->find($order_id);
        }
        return $this;
    }

    /**
     * @param $order_id
     * @return $this
     */
    public function reverse($order_id)
    {
        $response = self::$orderDB->updateReverse($order_id);
        if ($response) {
            $this->orderInstance = self::$orderDB->find($order_id);
        }
        return $this;
    }

    /**
     * @param null $order_id
     * @return mixed|null
     */
    public function getProductOrder($order_id = null)
    {
        if ((!$this->productOrderInstance instanceof Product) && !empty($order_id)) {
            $this->productOrderInstance = self::$orderDB->getProductOrder($order_id);
        } elseif ((!$this->productOrderInstance instanceof Product) && !empty($this->orderInstance)) {
            $this->productOrderInstance = self::$orderDB->getProductOrder($this->orderInstance);
        }

        return $this->productOrderInstance;
    }

    /**
     * step 2
     * @return $this
     */
    public function commonClub()
    {
//        foreach ($this->fromAccountsClubsInstances as $clubInstance) {
        $this->commonClubInstance = $this
            ->fromAccountsClubsInstances
            ->first()
            ->whereIn("id", $this->toAccountsClubsInstances->pluck("id")->toArray())
            ->first();
        /*            if (!$this->commonClubInstance instanceof Club) {
                        throws(\Exception::class, "clubs_not_match", 400);
                    }*/
//        }
        return $this;
    }

    /**
     * @param $items
     * @return array
     * @throws \Exception
     */
    private function inputNormalize($items)
    {
        if (!is_array($items)) {
            $items = (array)$items;
        }

        if (!isset($items["from_account_id"]) || empty($items["from_account_id"])) {
            throw new \Exception("from_account_id_cannot_be_null", 400);
        }

        if (!isset($items["to_account_id"]) || empty($items["to_account_id"])) {
            throw new \Exception("to_account_id_cannot_be_null", 400);
        }

        if (!isset($items["amount"]) || empty($items["amount"])) {
            $items["amount"] = 0;
        }

        return $items;
    }


}
