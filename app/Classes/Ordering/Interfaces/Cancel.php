<?php


namespace App\Classes\Ordering\Interfaces;


interface Cancel
{
    function cancel($id);
}
