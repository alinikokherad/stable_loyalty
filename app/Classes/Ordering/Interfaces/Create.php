<?php


namespace App\Classes\Ordering\Interfaces;


interface Create
{
    function create();
}
