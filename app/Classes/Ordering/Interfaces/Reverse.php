<?php


namespace App\Classes\Ordering\Interfaces;


interface Reverse
{
    function reverse($id);
}
