<?php


namespace App\Classes\Ordering;


use Illuminate\Support\Str;
use Modules\Event\repo\EventDB;

trait Event
{
    public $eventInstance;

    /**
     * @param $eventData
     * @return $this
     */
    public function createEvent($eventData)
    {
        $eventDB = new EventDB();
        $this->eventInstance = $eventDB
            ->create($eventData);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->eventInstance;
    }
}
