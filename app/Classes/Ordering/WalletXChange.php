<?php


namespace App\Classes\Ordering;


use App\Classes\Ordering\Interfaces\Cancel;
use App\Classes\Ordering\Interfaces\Create;
use App\Classes\Ordering\Interfaces\Init;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\WalletDB;

trait WalletXChange
{

    public $xChangeInstance;

    //account wallet
    public $fromAccountWallet;
    public $toAccountWallet;

    //wallet agent
    public $fromAgentAccountInstance;
    public $toAgentAccountInstance;

    /**
     * @return bool
     */
    public function needXChange()
    {
        if ($this->fromAccountInstance->treasury_account_id != $this->toAccountInstance->treasury_account_id) {
            return true;
        }
        return false;
    }

    /**
     * @return $this
     */
    public function initXChange()
    {
        $accountDB = new AccountDB();
        $walletDB = new WalletDB();

        try {
            //find accounts wallet
            $this->fromAccountWallet = $walletDB->getWalletInstanceWithTreasuryAccountId($this->fromAccountInstance->treasury_account_id);
            $this->toAccountWallet = $walletDB->getWalletInstanceWithTreasuryAccountId($this->toAccountInstance->treasury_account_id);

            //find agent account
            $this->fromAgentAccountInstance = $accountDB->getAgentAccountInstance($this->fromAccountInstance->treasury_account_id, $this->toAccountWallet->id);
            $this->toAgentAccountInstance = $accountDB->getAgentAccountInstance($this->toAccountInstance->treasury_account_id, $this->fromAccountWallet->id);
        } catch (\Exception $exception) {
            throws(\Exception::class, "problem_with_find_agent_or_wallet", 400);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function checkXChange()
    {
        //get from treasury and to treasury relation
        $walletDB = new WalletDB();
        $this->xChangeInstance = $walletDB->getWalletXChange($this->fromAccountWallet->id, $this->toAccountWallet->id);
        if (!$this->xChangeInstance) {
            throws(\Exception::class, "wallet_x_change_not_define", 400);
        }
        return $this;
    }


}
