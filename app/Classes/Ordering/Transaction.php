<?php


namespace App\Classes\Ordering;


use App\Jobs\TransactionJob;
use Illuminate\Support\Str;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\repo\AccountDB;

trait Transaction
{
    public $transactionInstance;
    public $transactionData;

    /**
     * @param $item
     * @return $this
     * @throws \Exception
     */
    public function initTransaction($item)
    {
        $transactionData = [
            "from_account_id" => $item["from_account_id"],
            "to_account_id" => $item["to_account_id"],
            "order_id" => $this->orderInstance->id,
            "amount" => $item["amount"]
        ];

        $this->transactionNormalize($transactionData);
        $this->transactionData = $transactionData;

        //call transaction repository
        $accountDB = new AccountDB();

        //get account instance
        $fromAccountInstance = $accountDB->get($this->transactionData["from_account_id"]);
        $old_credit = $fromAccountInstance->credits->first();

        //todo شرط اینکه آیا اعتبار شخص میتواند کمتر از صفر شود اینجا بررسی می شود
//        $new_credit = ($old_credit->amount) - ($this->transactionData["amount"]);
//        if (($fromAccountInstance->accountType->balance_type == "positive" || $fromAccountInstance->accountType->balance_type == "zero") && ($new_credit < 0)) {
//            //do something or throw an exception
//            throw new \Exception("your_credit_not_enough_for_this_transaction");
//        }

        return $this;
    }

    /**
     * @param null $type
     * @return $this
     */
    public function createTransaction($type = null)
    {
        switch ($type) {
            case "gift":
                //TODO در اینجا مشخص میشود که هر تراکنشی هدیه ای چه زمانی(آنی، یک روز بعد، یک ماه بعد) اعمال شود
                if ($this->commonClubInstance->give_point_at == "one_day_later") {
                    $delay = now()->addDay(1);
                } elseif ($this->commonClubInstance->give_point_at == "one_month_later") {
                    $delay = now()->addMonth(1);
                } else {
                    $delay = now();
                }
                break;
            default:
                $delay = now();
        }

        try {
            $this->transactionInstance = TransactionJob::dispatchNow($this->transactionData);
//                ->onQueue("transaction")
//                ->delay($delay);

            //fire event
            $eventData = [
                "from_account_id" => $this->orderInstance->from_account_id,
                "to_account_id" => $this->orderInstance->to_account_id,
                "done_at" => now()->format("Y-m-d H:i:s"),
                "type_id" => "",
                "amount" => $this->orderInstance->amount,
                "UID" => Str::uuid()->toString(),
                "extra_value" => null,
            ];
            $this->createEvent($eventData);
        } catch (\Exception $exception) {
            throws(\Exception::class, $exception->getMessage(), $exception->getCode());
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transactionInstance;
    }

    /**
     * @param $items
     * @return mixed
     * @throws \Exception
     */
    private function transactionNormalize($items)
    {

        if (!isset($items["from_account_id"]) || empty($items["from_account_id"])) {
            throw new \Exception("from_account_id_cannot_be_null", 400);
        }

        if (!isset($items["to_account_id"]) || empty($items["to_account_id"])) {
            throw new \Exception("to_account_id_cannot_be_null", 400);
        }

        if (!isset($items["amount"])) {
            $items["amount"] = 0;
        }

        return $items;
    }
}
