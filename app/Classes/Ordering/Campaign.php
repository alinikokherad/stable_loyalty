<?php


namespace App\Classes\Ordering;


use Modules\Club\Entities\Club;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\repo\CampaignDB;

trait Campaign
{
    /**
     * @return $this|null
     */
    public function dispatchCampaign()
    {
        foreach ($this->commonClubInstance->campaigns as $campaignInstance) {

            foreach ($campaignInstance->rules as $ruleInstance) {
                $amount = $this->eventInstance->amount;
                $params = $ruleInstance->params;
                $operators = $ruleInstance->operators;
                $values = $ruleInstance->values;
                $statement = false;

                $phpEvalString = '$statement = ' . $params . $operators . $values . ';';

                //run rule from database
                eval($phpEvalString);

                //run rule
                if (!$statement) {
                    return null;
                }
                foreach ($ruleInstance->processes as $processInstance) {
                    //do action
                    $processClass = $processInstance->class;
                    $processMethod = $processInstance->methods;
                    $methodsCall = $processInstance->call;

//                    $processParams = str_replace('{{amount}}', $this->eventInstance->amount, $processParams);

                    //create class and its methods
                    $fileContent = file_get_contents(app_path("Process/ClassName.stub"));
                    $newFile = str_replace('$CLASSNAME$', $processClass, $fileContent);
                    $newFile = str_replace('$METHODNAMES$', $processMethod, $newFile);
//                    $newFile = str_replace('$PARAM$', $processParams, $newFile);
                    file_put_contents(app_path("Process/{$processClass}.php"), $newFile);


                    //call class and its methods & params
                    $processClass = "App\\Process\\$processClass";
                    $instanceOfClass = new $processClass();

                    //call class method and give it args
                    foreach ($methodsCall as $methodName => $params) {
                        $instanceOfClass->$methodName($params);
                    }
                }
            }
        }
        return $this;
    }

}
