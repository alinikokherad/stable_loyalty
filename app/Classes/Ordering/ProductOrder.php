<?php


namespace App\Classes\Ordering;


use App\Classes\Ordering\Interfaces\Cancel;
use App\Classes\Ordering\Interfaces\Create;
use App\Classes\Ordering\Interfaces\Init;
use App\Classes\Ordering\Interfaces\Reverse;
use App\Exceptions\ProductOrderException;
use App\repo\UserDB;
use App\Traits\Response;
use Modules\Branch\repo\MerchantDB;
use Modules\Club\repo\ClubDB;
use Modules\Product\repo\CostDB;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\ProductOrderDB;
use Modules\Wallet\repo\WalletDB;

class ProductOrder implements Init, Cancel, Create, Reverse
{
    use Response;

    //constructor method define this
    public static $productOrderDB;

    //init method define these
    private $item = [];
    protected $merchantInstance;
    protected $customerInstance;

    //create method define this
    protected $productOrderInstance;

    //orders method define this
    protected $ordersInstances;

    public function __construct()
    {
        self::$productOrderDB = new ProductOrderDB();
    }

    /**
     * initialize class
     * @param $item
     * @return $this
     * @throws \Exception
     */
    public function init($item)
    {
        //call repository
        $userDB = new UserDB();
        $merchantDB = new MerchantDB();

        if ($item instanceof \Modules\Wallet\Entities\ProductOrder) {
            $this->productOrderInstance = $item;
        } else {
            $item = $this->inputNormalize($item);
            $this->item = $item;
        }
        try {


            //get entity
            $this->merchantInstance = $merchantDB->findOrFail($item["merchant_id"] ?? $this->productOrderInstance->merchant_id);
            $this->customerInstance = $userDB->findOrFail($item["customer_id"] ?? $this->productOrderInstance->customer_id);

        } catch (\Exception $exception) {
            throw new ProductOrderException($exception->getMessage(), $exception->getCode());
        }
        return $this;
    }

    /**
     * @return $this
     * @throws ProductOrderException
     */
    public function create()
    {
        $costDB = new CostDB();
        $walletDB = new WalletDB();
        $accountDB = new AccountDB();

        try {
            \DB::beginTransaction();

            //create product_order
            $data = [
                "merchant_id" => $this->merchantInstance->id,
                "customer_id" => $this->customerInstance->id,
                "description" => $this->item["description"],
                "paid_at" => $this->item["paid_at"],
                "author_id" => $this->item["author_id"],
                "merchant_user_id" => $this->item["merchant_user_id"],
            ];
            $this->productOrderInstance = self::$productOrderDB->create($data);

            //create relation between productOrder and costs
            $this->productOrderInstance->costs()->sync($this->item["costs"]);

            //create order for this product_order
            foreach ($this->item["costs"] as $cost_id => $data) {
                //get customer club
                //get customer accounts witch accounts wallets club same with customer wallet

                //get product cost
                $cost = $costDB->getCost($cost_id) * $data["quantity"];

                //attain wallet and treasury instance
                $walletInstance = $walletDB->getWalletInstanceByCostId($cost_id);
                $treasuryAccountInstance = $accountDB->getTreasuryAccountByWalletId($walletInstance->id);

                //attain fromAccountInstance and toAccountInstance
                $fromAccountInstance = $accountDB
                    ->getAccountByUserIdAndTreasuryAccountId($this->customerInstance->id, $treasuryAccountInstance->id);
                $toAccountInstance = $accountDB
                    ->getAccountByMerchantIdAndTreasuryAccountId($this->merchantInstance->id, $treasuryAccountInstance->id);

                //create order
                $this->createOrder()
                    ->init([
                        "from_account_id" => $fromAccountInstance->id,
                        "to_account_id" => $toAccountInstance->id,
                        "amount" => $cost,
                        "author_id" => \Auth::id(),
                        "from_treasury_account_id" => $fromAccountInstance->treasury_account_id,
                        "to_treasury_account_id" => $toAccountInstance->treasury_account_id,
                        "order_payment_id" => isset($this->item["order_payment_id"]) ? $this->item["order_payment_id"] : null,
//                        "refund" => "",
//                        "cash_out" => "",
//                        "type" => "",
                    ])
                    ->create();
            }
            \DB::commit();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw new ProductOrderException($exception->getMessage(), $exception->getCode());
        }
        return $this;
    }

    /**
     * submit old cost amount for future
     * @return $this
     */
    public function evaluateCost()
    {
        $costDB = new CostDB();
        $orderCostFormat = [];
        foreach ($this->item["costs"] as $cost_id => $data) {
            $cost = $costDB->getCost($cost_id);
            $orderCostFormat[$cost_id] = [
                "quantity" => $data["quantity"],
                "cost" => $cost,
                "title" => isset($data["title"]) ? $data["title"] : null,
                "description" => isset($data["description"]) ? $data["description"] : null,
                "created_at" => now()->format("Y-m-d H:i:s"),
                "updated_at" => now()->format("Y-m-d H:i:s"),
            ];
        }
        $this->item["costs"] = $orderCostFormat;
        return $this;
    }

    /**
     * update product_orders paid_at
     * @param $productOrder_id
     * @return $this
     */
    public function success($productOrder_id = null)
    {
        $res = self::$productOrderDB->updatePaid_at($productOrder_id ?? $this->productOrderInstance->id);
        if ($res and !empty($productOrder_id)) {
            $this->productOrderInstance = self::$productOrderDB->find($productOrder_id);
        }
        return $this;
    }

    /**
     * cancel product_order
     * @param $productOrder_id
     * @return $this
     */
    public function cancel($productOrder_id)
    {
        $response = self::$productOrderDB->updateRevoked($productOrder_id);
        if ($response) {
            $this->productOrderInstance = self::$productOrderDB->find($productOrder_id);
        }
        return $this;
    }

    /**
     * @param $productOrder_id
     * @return $this
     */
    public function reverse($productOrder_id)
    {
        $response = self::$productOrderDB->updateReverse($productOrder_id);
        if ($response) {
            $this->productOrderInstance = self::$productOrderDB->find($productOrder_id);
        }
        return $this;
    }

    /**
     * get instance
     * @return mixed
     */
    public function get()
    {
        return $this->productOrderInstance->load("orders");
    }

    /**
     * get productOrders all orders
     * @param $productOrder_id
     * @return mixed
     */
    public function orders($productOrder_id = null)
    {
        if (!empty($productOrder_id)) {
            $this->ordersInstances = self::$productOrderDB->getOrdersInstance($productOrder_id);
        } elseif ($this->productOrderInstance instanceof \Modules\Wallet\Entities\ProductOrder) {
            $this->ordersInstances = $this->productOrderInstance->orders;
        } else {
            $this->ordersInstances = self::$productOrderDB->getOrdersInstance($this->productOrderInstance->id);
        }
        return $this;
    }

    /**
     * @param $productOrderInstance
     * @return $this
     * @throws \App\Exceptions\OrderException
     */
    public function checkout($productOrderInstance)
    {
        if ($productOrderInstance instanceof \Modules\Wallet\Entities\ProductOrder) {
            foreach ($productOrderInstance as $ordersInstance) {
                (new Order())
                    ->init($ordersInstance)
                    ->commonClub()
                    ->checkout();
            }
        } else {
            foreach ($this->ordersInstances as $ordersInstance) {
                (new Order())
                    ->init($ordersInstance)
                    ->commonClub()
                    ->checkout();
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->ordersInstances;
    }

    /**
     * create new object of order instance
     * @return Order
     */
    public function createOrder()
    {
        return new Order($this->productOrderInstance);
    }

    /**
     * return normalize input
     * @param $item
     * @return array
     * @throws \Exception
     */
    private function inputNormalize($item)
    {
        if (!is_array($item)) {
            $item = (array)$item;
        }

        if (!isset($item["merchant_id"]) || empty($item["merchant_id"])) {
            throw new \Exception("merchant_id_cannot_be_null", 400);
        }

        if (!isset($item["customer_id"]) || empty($item["customer_id"])) {
            throw new \Exception("customer_id_cannot_be_null", 400);
        }

        if (!isset($item["author_id"]) || empty($item["author_id"])) {
            $item["author_id"] = \Auth::id();
        }

        if (!isset($item["merchant_user_id"]) || empty($item["merchant_user_id"])) {
            $item["merchant_user_id"] = \Auth::id();
        }

        if (!isset($item["paid_at"]) || empty($item["paid_at"])) {
            $item["paid_at"] = null;
        }

        if (!isset($item["description"]) || empty($item["description"])) {
            $item["description"] = null;
        }

        return $item;
    }


}
