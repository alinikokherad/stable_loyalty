<?php


namespace App\Classes\Ordering;


use App\Events\AfterTransactionCreatedEvent;

trait Credit
{
    /**
     * @return $this
     */
    public function createCredit()
    {
        //from_account to treasury
        $firstTransactionData = [
            "from_account_id" => $this->fromAccountInstance->id,
            "to_account_id" => $this->fromAccountInstance->treasury_account_id,
            "amount" => $this->orderInstance->amount,
        ];

        //treasury to to_account
        $secondTransactionData = [
            "from_account_id" => $this->fromAccountInstance->treasury_account_id,
            "to_account_id" => $this->toAccountInstance->id,
            "amount" => $this->orderInstance->amount,
        ];

        $this->transactionInstance = $this
            ->initTransaction($firstTransactionData)
            ->createTransaction()
            ->initTransaction($secondTransactionData)
            ->createTransaction();

        //update order paid at
        $this->success();

        //after create transaction fire this event
        event(new AfterTransactionCreatedEvent());

        return $this;
    }

    /**
     * @return $this
     */
    private function createXChangeCredit()
    {
        //from_account to from_treasury
        $firstTransactionData = [
            "from_account_id" => $this->fromAccountInstance->id,
            "to_account_id" => $this->fromAccountInstance->treasury_account_id,
            "amount" => $this->orderInstance->amount,
        ];

        //treasury to to_agent
        $secondTransactionData = [
            "from_account_id" => $this->fromAccountInstance->treasury_account_id,
            "to_account_id" => $this->fromAgentAccountInstance->id,
            "amount" => $this->orderInstance->amount,
        ];

        //from_agent to to_treasury
        $thirdTransactionData = [
            "from_account_id" => $this->toAgentAccountInstance->id,
            "to_account_id" => $this->toAccountInstance->treasury_account_id,
            "amount" => $this->xChangeInstance->unit_amount * $this->orderInstance->amount
        ];

        //to_treasury to from_agent
        $fourthTransactionData = [
            "from_account_id" => $this->toAccountInstance->treasury_account_id,
            "to_account_id" => $this->toAccountInstance->id,
            "amount" => $this->xChangeInstance->unit_amount * $this->orderInstance->amount
        ];

        $this->transactionInstance = $this
            ->initTransaction($firstTransactionData)
            ->createTransaction()
            ->initTransaction($secondTransactionData)
            ->createTransaction()
            ->initTransaction($thirdTransactionData)
            ->createTransaction()
            ->initTransaction($fourthTransactionData)
            ->createTransaction();

        //update order paid at
        $this->success();

        //after create transaction fire this event
        event(new AfterTransactionCreatedEvent());
        return $this;
    }
}
