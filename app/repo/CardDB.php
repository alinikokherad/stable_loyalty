<?php


namespace App\repo;

use App\Card;
use Illuminate\Pagination\LengthAwarePaginator;

class CardDB
{

    /**
     * get all card from database
     * @return mixed
     */
    public function get()
    {
        $cardsInstances = Card::get();
        return $cardsInstances;
    }

    /**
     * get all card from database by paginator
     * @param $limit integer|boolean
     * @return LengthAwarePaginator
     */
    public function paginate($limit = null)
    {
        $cardsInstances = Card::paginate($limit);
        return $cardsInstances;
    }

    /**
     * insert record into cards table
     * @param $cardData array
     * @return Card
     */
    public function insert($cardData)
    {
        $cardInstance = Card::create($cardData);
        return $cardInstance;
    }

    /**
     * select data from cards table where $id
     * @param $id integer
     * @return Card|boolean
     */
    public function find($id)
    {
        $cardInstance = Card::where("id", $id)->first();
        if ($cardInstance instanceof Card) {
            return $cardInstance;
        }
        return false;
    }

    /**
     * update specific card into database
     * @param $cardData
     * @param $card_id
     * @return mixed
     */
    public function update($cardData, $card_id)
    {
        $updateResponse = Card::where("id", $card_id)->update($cardData);
        return $updateResponse;
    }

    /**
     * delete card from card id
     * @param $card_id
     */
    public function delete($card_id)
    {
        Card::where("id", $card_id)->delete();
    }

    /**
     * get card instance with card number
     * @param $card_number
     * @param bool $withUser
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getCardInstance($card_number, $withUser = false)
    {
        $cardInstance = Card::query()->where("card_number", $card_number);
        if ($withUser) {
            $cardInstance->with("user");
        }
        return $cardInstance->first();
    }

    /**
     * @param $user_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getCardWithUserId($user_id)
    {
        $cardInstance = Card::query()->whereHas("user", function ($query) use ($user_id) {
            $query->where("id", $user_id);
        })->first();
        if ($cardInstance instanceof Card) {
            return $cardInstance;
        }
        return false;
    }
}
