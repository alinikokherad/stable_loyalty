<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/21/19
 * Time: 12:10 PM
 */

namespace App\repo;


use App\Exceptions\ProductOrderException;
use App\User;
use Illuminate\Support\Carbon;
use Modules\Club\Entities\Club;
use Modules\Wallet\Transformers\ProductOrderResource;

class UserDB
{

    /**
     * @param $data
     * @return User|bool|\Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        $instance = User::create($data);
        if ($instance instanceof User) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function find($id)
    {
        $instance = User::where("id", $id)->with("productOrders.orders")->first();
        if ($instance instanceof User) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $data1
     * @param $data2
     * @return User|bool|\Illuminate\Database\Eloquent\Model
     */
    public function firstOrCreate($data1, $data2)
    {
        $input = $this->normalizeInput($data2);
        $userInstance = User::query()->firstOrCreate($data1, $input);
        if ($userInstance instanceof User) {
            return $userInstance;
        }
        return false;
    }

    /**
     * @param $data1
     * @param $data2
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    public function updateOrCreate($data1, $data2)
    {
        return User::updateOrCreate($data1, $data2);
    }

    /**
     * @param $id
     * @param $data
     * @return int
     */
    public function update($id, $data)
    {
        return User::where("id", $id)->update($data);
    }

    /**
     * @param $mobile
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUser($mobile)
    {
        $instance = User::where("mobile", $mobile)->with("cards")->first();
        if ($instance instanceof User) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @param bool $withAccount
     * @return User|User[]|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function get($id, $withAccount = false)
    {
        if ($withAccount) {
            $instance = User::where("id", $id)->with("accounts.credits")->first();
            return $instance;
        }
        $instance = User::find($id);
        if ($instance instanceof User) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $mobile
     * @param null $from
     * @param null $to
     * @param null $day
     * @param null $week
     * @param null $month
     * @return bool|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getUserProductOrder($mobile, $from = null, $to = null, $day = null, $week = null, $month = null)
    {
        if (isset($day) || isset($week) || isset($month)) {

            $instance = User::where("mobile", $mobile)
                ->where("status", "active")
                ->with("productOrders")
                ->first();

            //create condition
            if ($day != null) {
                $instance = $instance->productOrders->where("created_at", ">=", Carbon::now()->subDay($day));
            } elseif ($week != null) {
                $instance = $instance->productOrders->where("created_at", ">=", Carbon::now()->subWeek($week));
            } else {
                $instance = $instance->productOrders->where("created_at", ">=", Carbon::now()->subMonth($month));
            }

        } elseif (isset($from, $to)) {

            //create date format like created_at
            $from = date('Y-m-d' . ' 00:00:00', strtotime($from)); //need a space after dates.
            $to = date('Y-m-d' . ' 23:59:59', strtotime($to));

            $instance = User::where("mobile", $mobile)
//                ->where("status", "active")
                ->with("productOrders")
                ->first();
            $instance = $instance->productOrders->where("created_at", ">=", $from)->where("created_at", "<=", $to);
        } else {

            $instance = User::where("mobile", $mobile)
//                ->where("status", "active")
                ->with("productOrders")
                ->first();
            $instance = $instance->productOrders;
        }

        //if we dont have any result
        if (is_null($instance)) {
            return false;
        }

        $instance = ProductOrderResource::collection($instance);
        return $instance;
    }

    /**
     * @param string $string
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserWithType(string $string)
    {
        $instance = User::where("type", "=", $string)->first();
        if ($instance instanceof User) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $merchant_id
     * @return bool|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function waiterListByMerchantId($merchant_id)
    {
        $waiters = User::where("merchant_id", $merchant_id)->where("type", "waiter")->get();
        if ($waiters) {
            return $waiters;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["name"])) {
            $filter["name"] = $data["name"];
        }
        if (isset($data["family"])) {
            $filter["family"] = $data["family"];
        }
        if (isset($data["email"])) {
            $filter["email"] = $data["email"];
        }
        if (isset($data["mobile"])) {
            $filter["mobile"] = $data["mobile"];
        }
        if (isset($data["mobile_verified_at"])) {
            $filter["mobile_verified_at"] = $data["mobile_verified_at"];
        }
        if (isset($data["password"])) {
            $filter["password"] = $data["password"];
        }
        if (isset($data["birthday"])) {
            $filter["birthday"] = $data["birthday"];
        }
        if (isset($data["gender"])) {
            $filter["gender"] = $data["gender"];
        }
        if (isset($data["type"])) {
            $filter["type"] = $data["type"];
        }
        if (isset($data["otp"])) {
            $filter["otp"] = $data["otp"];
        }
        if (isset($data["tier_id"])) {
            $filter["tier_id"] = $data["tier_id"];
        }
        if (isset($data["qr_code"])) {
            $filter["qr_code"] = $data["qr_code"];
        }
        if (isset($data["pinCode"])) {
            $filter["pinCode"] = $data["pinCode"];
        }
        if (isset($data["settings"])) {
            $filter["settings"] = $data["settings"];
        }
        if (isset($data["status"])) {
            $filter["status"] = $data["status"];
        }

        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        if (isset($data["author_id"])) {
            $filter["author_id"] = $data["author_id"];
        }
        return $filter;
    }

    /*--------------------------- dorris methods ----------------------------*/
    /**
     * @param $card_number
     * @param null $password
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserByCardNumber($card_number, $password = null)
    {
        $userInstance = User::query()
            ->with("cards")
            ->whereHas("cards", function ($query) use ($card_number, $password) {
                $query->where("card_number", $card_number);
//                ->where("password", $password);
            })
            ->first();
        return $userInstance;
    }

    /**
     * @param $mobile
     * @param $password
     * @return User|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserWithCardPassword($mobile, $password = null)
    {
        $userInstance = User::query()->where("mobile", $mobile);
        if (!empty($password)) {
            $userInstance = $userInstance->whereHas("cards", function ($query) use ($password) {
                $query->where("password", $password);
            });
        }
        if ($userInstance->first() instanceof User) {
            return $userInstance->first();
        }
        return false;
    }

    /**
     * @param $to_account_id
     * @return User|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserWithAccountId($to_account_id)
    {
        $userInstance = User::whereHas("accounts", function ($query) use ($to_account_id) {
            $query->where("id", $to_account_id);
        })->first();
        if ($userInstance instanceof User) {
            return $userInstance;
        }
        return false;
    }

    /**
     * @param $mobile
     * @return User|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserCardWithMobile($mobile)
    {
        $userInstance = User::query()->where("mobile", $mobile)->with("cards")->whereHas("cards")->first();
        if ($userInstance instanceof User) {
            return $userInstance;
        }
        return false;
    }

    /**
     * @param int $user_id
     * @return bool|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUsersClub(int $user_id)
    {
        $clubInstance = Club::query()
            ->whereHas("users", function ($query) use ($user_id) {
                $query->where("users.id", $user_id);
            })
            ->get();
        if ($clubInstance->first() instanceof Club) {
            return $clubInstance;
        }
        return false;
    }

    /**
     * @param int $merchant_id
     * @return bool|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMerchantsClub(int $merchant_id)
    {
        $clubInstance = Club::query()
            ->whereHas("merchants", function ($query) use ($merchant_id) {
                $query->where("merchants.id", $merchant_id);
            })->get();
        if ($clubInstance->first() instanceof Club) {
            return $clubInstance;
        }
        return false;
    }

    /**
     * @param int|null $merchant_id
     * @param int|null $author_id
     * @param null $personnelIds
     * @param bool $complete
     * @return User|User[]|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder
     */
    public function getUserWithMerchantId(int $merchant_id = null, int $author_id = null, $personnelIds = null, $complete = false)
    {
        $usersList = User::query()->with("cards");

        if (!empty($merchant_id)) {
            $usersList = $usersList->where("merchant_id", $merchant_id);
        }

        if (!empty($author_id)) {
            $usersList = $usersList->where("author_id", $author_id);
        }

        if (!empty($personnelIds)) {
            $usersList = $usersList->whereIn("author_id", $personnelIds);
        }

        if (empty($complete)) {
            $usersList = $usersList->where("name")->whereNull("family");
        }

        $usersList = $usersList->get();

        if ($usersList->first() instanceof User) {
            return $usersList;
        }
        return false;
    }

    /**
     * insert new user into database
     * @param array $data
     * @return User|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function store(array $data)
    {
        $userInstance = User::query()->create($data);
        if ($userInstance instanceof User) {
            return $userInstance;
        }
        return false;
    }

    /**
     * @param $mobile
     * @param $userData
     * @return bool|int
     */
    public function updateByMobile($mobile, $userData)
    {
        $response = User::query()->where("mobile", $mobile)->update($userData);
        return $response;
    }

    /**
     * @param $user_id
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     * @throws \Exception
     */
    public function findOrFail($user_id)
    {
        $userInstance = User::query()
            ->where("id", $user_id)
            ->first();
        if ($userInstance instanceof User) {
            return $userInstance;
        }
        throw new \Exception("user_not_exist", 404);
    }

}
