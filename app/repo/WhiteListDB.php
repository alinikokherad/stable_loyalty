<?php


namespace App\repo;


use App\WhiteList;

/**
 * Class WhiteListDB
 * @package App\repo
 */
class WhiteListDB
{
    /**
     * @return WhiteList[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index($user_id = null)
    {
        if (empty($user_id)) {
            $whiteLists = WhiteList::all()->pluck("mobile")->toArray();
        } else {
            $whiteLists[] = WhiteList::where("user_id", $user_id)->pluck("mobile")->first();
        }
        return $whiteLists;
    }

    /**
     * @param $data
     * @return bool
     */
    public function insert($data)
    {
        $instance = WhiteList::query()
            ->create($data);
        return $instance;
    }

    /**
     * @param $id
     */
    public function destroy($mobile)
    {
        WhiteList::where("mobile", $mobile)->delete();
    }
}
