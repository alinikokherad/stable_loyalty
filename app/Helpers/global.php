<?php
/**
 * Created by PhpStorm.
 * User: mmrdev
 * Date: 23/09/19
 * Time: 10:21 AM
 */


if (!function_exists("deepFilter")) {
    function deepFilter(array $filters, $model, Request $r, $relations = null, $paginate)
    {
        $result = [];
        foreach ($filters as $key) {
            array_push($result, doFilters($key, $model, $r, $relations, false));
        }
        if ($paginate) {
            return collect($result)->paginate($r->page_limit, $r->page_number);
        }else{
            return $result;
        }
    }
}

/**
 * do filter
 * do multi filter on query(model)
 * @var
 * @param filters(array), model, request (for paginate) , relations (if exist in request)
 * @return paginated data
 */
 function doFilters(array $filter, $model, Request $r, $relations = null, $paginated = true)
{
    $rels = [$relations];
    foreach ($filter as $key) {
        if (hasRelation($key)) {
            $exploded = explode('.', $key[0]);
            $relModel = $exploded[0];
            $relFilter = [$exploded[1], $key[1], $key[2]];
            if (is_null($rels[0])) {
                $relation = [$relModel];
            } else {
                $relation = array_merge($rels[0], [$relModel]);
            }
            $model = $model::whereHas($relModel, function ($q) use ($relFilter) {
                $q->where($relFilter[0], $relFilter[1], $relFilter[2]);
            })->with($relation);
        } else {
            $model = $model->where($key[0], $key[1], $key[2]);
        }
    }
    if ($paginated) {
        return $model->paginate($r->input("page_limit"), ["*"], 'page', $r->input("page_number"));
    }else{
        return $model->get();
    }
}

function hasRelation($filter): bool
{
    $rell = explode('.', $filter[0]);
    if (count($rell) == 2) {
        return true;
    } else {
        return false;
    }
}
