<?php

if (!function_exists("throws")) {
    function throws($exceptionClass, $msg, $code)
    {
        throw new $exceptionClass(__("messages.{$msg}"), $code);
    }
}
