<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Branch\Entities\Merchant;

/**
 * App\Address
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Address query()
 * @mixin \Eloquent
 */
class Address extends Model
{
    protected $table = "addresses";
    protected $guarded = [];

    public function addressable()
    {
        return $this->morphTo();
    }
}
