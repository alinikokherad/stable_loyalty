<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Wallet\repo\WalletDB;
use Modules\Wallet\Transformers\AccountResource;
use Modules\Wallet\Transformers\ProductOrderResource;

class UserResource extends JsonResource
{
    public static $with_product_order = false;
    public static $with_account = false;
    public static $with_credit = false;
    public static $user_total_credit = true;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (self::$with_credit) {
            AccountResource::$with_credit = true;
        }

//        return parent::toArray($request);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "family" => $this->family,
            "email" => $this->email,
            "mobile" => $this->mobile,
            "mobile_verified_at" => $this->mobile_verified_at,
            "birthday" => $this->birthday,
            "gender" => $this->gender,
            "type" => $this->type,
            "otp" => $this->otp,
            "credits_value" => $this->when(self::$user_total_credit, $this->totalCredit()),
            "tier" => $this->tier,
            "qr_code" => $this->qr_code,
            "settings" => $this->settings,
            "status" => $this->status,
            "revoked" => $this->revoked,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "cards" => $this->cards,
            "attachment" => $this->whenLoaded("attaches"),
            "product_orders" => $this->when(self::$with_product_order, ProductOrderResource::collection($this->whenLoaded("productOrders"))),
            "accounts" => $this->when(self::$with_account, AccountResource::collection($this->accounts)),
        ];
    }

    private function totalCredit()
    {
        $total_credit = [];
        $walletDB = new WalletDB();
        $rials = $walletDB->getTreasuryAccountInstanceWithWalletType("rials");
        $point = $walletDB->getTreasuryAccountInstanceWithWalletType("point");
        foreach ($this->accounts as $account) {
            if ($account->treasury_id == $rials->id) {
                $total_credit["rials"] = $account->credits->first()->amount;
            } elseif ($account->treasury_id == $point->id) {
                $total_credit["point"] = $account->credits->first()->amount;
            }
        }
        return $total_credit;
    }

}
