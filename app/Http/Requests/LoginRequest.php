<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
//            "email" => "required|email",
//            "password" => "required|min:8",
            "mobile" => "required|min:11|max:11",
        ];
        /*    function ($attribute, $value, $fail) {
        if ($value === 'foo') {
            $fail($attribute . ' is invalid.');
        }
    }*/
    }


    public function messages()
    {
        return [
//            "email.email" => 'قالب ایمیل صحیح نمی باشد',
//            "email.required" => 'فیلد ایمیل اجباری میباشد',
//            "password.required" => 'فیلد پسوورد اجباری می باشد',
//            "password.min" => 'حداقل تعداد کاراکتر های پسوورد ۸ عدد میباشد'
            "mobile.min" => 'موبایل باید ۱۱ رقمی باشد',
            "mobile.required" => 'فیلد موبایل اجباری می باشد',
            "mobile.max" => 'موبایل باید ۱۱ رقمی باشد',
            "mobile.numeric" => 'موبایل باید عدد باشد',
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
