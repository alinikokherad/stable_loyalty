<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompleteProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "",
            "email" => "required",
            "birthday" => "date",
            "gender" => "string|min:2|max:20|nullable",
            "degree" => "string|max:50|min:3|nullable"
        ];
    }
}
