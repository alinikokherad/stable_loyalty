<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            "email" => "email|nullable",
            "mobile" => "required|max:11|min:11",
//            "club_id" => "array",
//            "register_type" => "string|nullable",
//            "account_type" => "string|required"
        ];
    }
}
