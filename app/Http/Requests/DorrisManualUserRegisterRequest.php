<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DorrisManualUserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentUri = \Route::getCurrentRoute()->uri;
        if ($currentUri == "api/version1/user") {
            return [
                "name" => "nullable",
                "family" => "nullable",
                "email" => "unique:users,email|email|nullable",
                "mobile" => "required|min:11|max:11|unique:users,mobile",
                "degree" => "nullable",
                "pinCode" => "min:3|max:15|nullable",
                "birthday" => "date|nullable",
                "gender" => "string|nullable",
                "type" => "string|nullable",
                "otp" => "nullable",
                "tier_id" => "nullable",
                "status" => "nullable",
                "merchant_id" => "nullable",
                "card_number" => "min:16|max:16|nullable|unique:cards,card_number",
            ];
        } elseif ($currentUri == "api/version1/user/{id}") {
            return [
                "name" => "nullable",
                "family" => "nullable",
                "email" => "email|nullable",
                "mobile" => "required|min:11|max:11",
                "degree" => "nullable",
                "pinCode" => "min:3|max:15|nullable",
                "birthday" => "date|nullable",
                "gender" => "string|nullable",
                "type" => "string|nullable",
                "otp" => "nullable",
                "tier_id" => "nullable",
                "status" => "nullable",
                "merchant_id" => "nullable",
                "card_number" => "min:16|max:16|nullable",
            ];
        }
    }

    public function messages()
    {
        return [

        ];
    }
}
