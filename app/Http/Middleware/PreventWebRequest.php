<?php

namespace App\Http\Middleware;

use Closure;

class PreventWebRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*        if ($request->acceptsHtml()) {
                    return abort(403, 'Something went wrong! you dont have permission');
                };*/
        return $next($request);
    }
}
