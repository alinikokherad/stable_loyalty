<?php

namespace App\Http\Middleware;

use Closure;
use \App\RequestInfo as RequestInfoModel;

class RequestInfo
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->url() == 'http://' . $request->server('HTTP_HOST') . '/requests'
            || $request->url() == 'https://' . $request->server('HTTP_HOST') . '/requests'
        ) {
            return $next($request);
        } else {
            $startTime = defined('LARAVEL_START') ? LARAVEL_START : $request->server('REQUEST_TIME_FLOAT');
            $requestInfo = [
                'ip' => $request->ip(),
                'method' => $request->method(),
                'url' => $request->url(),
                'memory' => round(memory_get_peak_usage(true) / 1024 / 1025, 1) . ' MB',
                'response_status' => Response($request)->getStatusCode(),
                'duration' => $startTime ? floor((microtime(true) - $startTime) * 1000) . ' ms' : null,
            ];
            RequestInfoModel::create($requestInfo);
            return $next($request);
        }
    }
}
