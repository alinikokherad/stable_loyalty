<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;

class EmailController extends Controller
{


    /**
     * @param Request $request
     * @param $user_id
     */
    public function send($user_id, $subject)
    {
        $user = User::findOrFail($user_id);
        $senderEmail = 'mentasystem@gmail.com';
        Mail::send(['html.view', 'text.view'], ['user' => $user], function ($message) use ($user, $subject, $senderEmail) {
            $message->from($senderEmail, 'Your Application');

            $message->to($user->email, $user->name);

            $message->subject($subject);
        });
    }
}
