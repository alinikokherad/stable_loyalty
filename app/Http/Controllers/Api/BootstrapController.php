<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Controllers\Api\ProductController;
use Modules\Product\Transformers\CategoryResource;
use Modules\Product\Transformers\ProductResource;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\repo\ProductOrderDB;
use Modules\Wallet\Transformers\ProductOrderResource;
use function foo\func;

class BootstrapController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (!\Gate::allows('customer')) {
            return $this->errorResponse("403", __("messages.access_denied"), 403);
        }
        //get user all information
        $currentUser = User::where("id", \Auth::id())->with(["attaches", "tier"])->first();
        $input = [
            "app" => "android",
            "version" => "1.0.0",
            "os" => [
                "kitkat",
                "lollipop",
                "ios9",
            ],
            "platform" => "nuva plus"
        ];
        $update = [
            "necessary" => [],
            "optional" => [],
        ];

        if (isset($currentUser->clubs->first()->campaigns) && !$currentUser->clubs->first()->campaigns->first()->attaches) {
            $campaign_img = $currentUser->clubs->first()->campaigns->last()->attaches->src;
        } else {
            $campaign_img = null;
        }
        $pages = [
            "about_us" => [
                "title" => "test",
                "subtitle" => "test",
                "description" => "test",
                "image" => [
                    "type" => "",
                    "src" => public_path($campaign_img),
                    "status" => "active",
                ],
            ],
            "home" => [
                "campaign_image" => [
                    "type" => "avatar",
                    "src" => public_path($campaign_img),
                    "status" => "active",
                ],
            ]
        ];

        //user credit history
        $credits = [];
        $productOrderDB = new ProductOrderDB();
        $walletInstance = Account::where('user_id', $currentUser->id)->with(['accountType.wallet', 'credits'])->get();
        $productOrdersInstance = $productOrderDB->getProductOrderWithUserId($currentUser->id);
        if ($productOrdersInstance->first() instanceof ProductOrder) {
            $old = ProductOrderResource::collection($productOrdersInstance);
            $currentUser->oldProductOrders = $old;
        } else {
            $currentUser->oldProductOrders = [];
        }
        foreach ($walletInstance as $item) {
            $wallet = $item->accountType->wallet->type;
            $credit = $item->credits->first()->amount;
            $credits[$wallet] = $credit;
        }

        //user order history
        $currentUserPointAccountInstance = Account::where("user_id", $currentUser->id)->where("treasury_id", 2)->first();
//            $receivePointOrders = Order::where("to_Account_id", $currentUserPointAccountInstance->id)->get()->pluck("amount");
//            $spendPointOrder = Order::where("from_account_id", $currentUserPointAccountInstance->id)->get()->pluck("amount");

        //list of point product
        $wallet_id = Wallet::where("type", "point")->first()->id;
        /*            $products = Product::with(["costs", "categories", "attaches"])
                        ->where("merchant_id", $currentUser->merchant_id)
                        ->whereHas("costs", function ($q) use ($wallet_id) {
                            $q->whereNotNull("cost")->where("wallet_id", "=", $wallet_id);
                        })
                        ->get()
                        ->take(6);*/

        $categories = Category::with(["attaches", "products.costs"])
            /*            ->whereHas("products", function ($q) use ($currentUser) {
                        $q->where("merchant_id", $currentUser->merchant_id);
                    })
                        ->whereHas("products.costs", function ($query) use ($wallet_id) {
                            $query->whereNotNull("cost")->where("wallet_id", "=", $wallet_id);
                        })
                        ->take(6);*/
            ->get();

//        CategoryResource::$withCost = true;
        CategoryResource::$with_product = true;
        CategoryResource::$withImage = true;
        $categories = CategoryResource::collection($categories);
//            $products = ProductResource::collection($products);

        unset($currentUser->clubs);
        $bootstrap = [
            "user" => $currentUser,
            "input" => $input,
            "update" => $update,
            "pages" => $pages,
            "credit" => $credits,
            "categories" => $categories,
//                "history" => [
//                    "received_point" => $receivePointOrders,
//                    "spend_point" => $spendPointOrder,
//                ],
        ];
        return response()
            ->json([
                "message" => __("messages.bootstrap_information"),
                "data" => $bootstrap
            ], 200);

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
