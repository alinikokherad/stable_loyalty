<?php

namespace App\Http\Controllers\Api;

use App\Card;
use App\Events\RecoveryPasswordCafeViunaSmsEvent;
use App\Events\RegisterDorisCustomerSmsEvent;
use App\Events\SendUserPasswordEvent;
use App\Events\CustomerRegisterEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssignCardRequest;
use App\Http\Requests\checkMobileRequest;
use App\Http\Requests\MobileRequest;
use App\Http\Requests\QrRequest;
use App\Http\Resources\UserResource;
use App\Jobs\SendCustomerPasswordSmsJob;
use App\repo\CardDB;
use App\repo\UserDB;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Modules\Branch\repo\MerchantDB;
use Modules\Crud\Http\Controllers\CrudController;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\repo\ProductOrderDB;
use Modules\Wallet\repo\WalletDB;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $page = \request()->has("page") ? \request("page") : 1;
        $limit = \request()->has("limit") ? \request("limit") : 10;
        $author_id = \request()->has("author_id") ? \request("author_id") : \Auth::user()->author_id;
        $merchant_id = \request()->has("merchant_id") ? \Auth::user()->merchant_id : null;
        $complete = \request()->has("complete") ? (bool)\request("complete") : false;


        //call merchant repository
        $merchantDB = new MerchantDB();
        $personnelInstances = $merchantDB->getMerchantsPersonnel(\Auth::user()->merchant_id);
        $personnelIds = $personnelInstances->pluck("id");

        //call user repository
        $userDB = new UserDB();
        $userList = $userDB->getUserWithMerchantId($merchant_id, $author_id, $personnelIds, $complete);
        if ($userList) {
            return $this->successResponse("info", $userList->paginate($limit, $page), 200);
        }
        return $this->errorResponse("400", __("messages.user_not_exist"), 404);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function tntSearch()
    {
        $page = \request()->has("page") ? \request("page") : 1;
        $limit = \request()->has("limit") ? \request("limit") : 10;
        $search = \request()->has("search") ? \request("search") : null;
        $complete = \request()->has("complete") ? (bool)\request("complete") : false;

        //call merchant repository
        $merchantDB = new MerchantDB();
        $personnelInstances = $merchantDB->getMerchantsPersonnel(\Auth::user()->merchant_id);
        $personnelIds = $personnelInstances->pluck("id");

        if (!empty($search)) {
            $usersList = User::search($search)->query(function ($query) use ($personnelIds) {
                $query->whereIn("author_id", $personnelIds);
            });
            if ($complete == false) {
                $usersList = $usersList->query(function ($query) {
                    $query->whereNull("name")->whereNull("family");
                });
            }
            $usersList = $usersList->get();
            return $this->successResponse("200", $usersList->paginate($limit, $page));
        }

        return $this->errorResponse("400", __("messages.user_not_found"), 404);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        $userId = \Auth::user()->id;
        $userInstance = User::where('id', $userId)->with('attaches')->first();
        $response = [];
        $walletInstance = Account::where('user_id', $userId)->with(['accountType.wallet', 'credits'])->get()->toArray();
        foreach ($walletInstance as $item) {
            $wallet = $item["account_type"]["wallet"]["type"];
            $credit = $item["credits"][0]["amount"];
            $response[$wallet] = $credit;
        }
        if (is_object($userInstance)) {
            $userInstance->credits_value = $response;
        }
        if ($userInstance instanceof User) {
            return $this->successResponse("user_profile", $userInstance, 200);
        }
        return $this->errorResponse("400", __('messages.not_login'), 400);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateQr()
    {
        $userInstance = \Auth::user();
        $qrCode = uidGenerate(uidGenerate(), uidGenerate(), 100, 1000) . str_random(10);
        Redis::set("user:{$qrCode}", $userInstance->id, 'EX', '3600');
        return $this->successResponse("renewQr", $qrCode, 200);
    }

    /**
     * @param QrRequest $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkQr(QrRequest $r)
    {
        if (\Gate::allows("merchant")) {
            $redisUser_id = Redis::get("user:{$r->qr_code}");
            $userInstance = User::where('id', $redisUser_id)
//                ->with("productOrders.orders")
                ->first();
            if ($userInstance instanceof User) {
                $productOrderDB = new ProductOrderDB();

                //get point treasury account
                $walletDB = new WalletDB();
                $pointAccountTreasury = $walletDB->getTreasuryAccountInstanceWithWalletType("point");


                //get all product with cost
//                $productDB = new ProductDB();
//                $allProductInstance = $productDB->minCost();
//                dd($allProductInstance);


                //sum old product order point
                $oldOrderCost = 0;
                if (!empty($userInstance->productOrders)) {
                    foreach ($userInstance->productOrders as $productOrder) {
                        foreach ($productOrder->orders as $order) {
                            if ($order->treasury_account_id == $pointAccountTreasury->id && $order->paid_at == null) {
                                $oldOrderCost += $order->amount;
                            }
                        }
                    }
                }

                //calculate all credit
                $credits = [];
                $accountInstance = Account::where('user_id', $userInstance->id)->with(['accountType.wallet', 'credits'])->get();
                foreach ($accountInstance as $item) {
                    $wallet = $item->accountType->wallet->type;
                    $credit = $item->credits->first()->amount;
                    $credits[$wallet] = $credit;
                }

                //add old product order to response
                $productOrdersInstance = $productOrderDB->getProductOrderWithUserId($userInstance->id);

                //check for old product order amount
                if (!empty($oldOrderCost)) {
                    $credits["point"] = $credits["point"] - $oldOrderCost;
                }

                $userInstance->credits_value = $credits;
                if (!$productOrdersInstance->isEmpty() && !empty($productOrdersInstance->first()->orders->where("type", "gift_order"))) {
                    $userInstance->old_order = $productOrdersInstance->first();
                    /*                    $userInstance->old_order = [
                                            "product" => $productOrdersInstance->first()->costs->first()->product->toArray(),
                                            "order" => $productOrdersInstance->first()->orders->where("type", "gift_order")->first()->toArray()
                                        ];*/
                } else {
                    $userInstance->old_order = false;
                }

                unset($userInstance->password);
                return $this->successResponse("information", $userInstance->getAttributes(), 200);
            }

            //TODO در اولین فرصت این کد بازنویسی شود
            $not_exist_user = [
                "user_not_exist" => [
                    "کاربر مورد نظر یافت نشد"
                ]
            ];
            return response()
                ->json([
                    "message" => __("400"),
                    "errors" => $not_exist_user

                ], 404);
        }

        return $this->errorResponse("400", __('messages.must_be_merchant'), 400);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function unknown()
    {
        //check for authentication (user must be merchant)
        if (!\Gate::allows("merchant")) {
            return $this->errorResponse("400", __('messages.must_be_merchant'), 403);
        }

        //get login user merchant id ,who is merchant's waiter
        $loginUserMerchantId = \Auth::user()->merchant_id;

        //get unknown user with current user merchant id who is merchant's waiter
        $userInstance = User::where("type", "unknown")
            ->where("merchant_id", $loginUserMerchantId)
            ->with("clubs")
            ->first();

        //check user exist
        if (!$userInstance instanceof User) {
            //TODO در اولین فرصت این کد بازنویسی شود
            $not_exist_user = [
                "user_not_exist" => [
                    "کاربر مورد نظر یافت نشد"
                ]
            ];
            return $this->errorResponse("400", $not_exist_user, 404);
        }

        $productOrderDB = new ProductOrderDB();

        //get point treasury account
        $walletDB = new WalletDB();
        $pointAccountTreasury = $walletDB->getTreasuryAccountInstanceWithWalletType("point");

        //get all product with cost
//                $productDB = new ProductDB();
//                $allProductInstance = $productDB->minCost();
//                dd($allProductInstance);

        //sum old product order point
        $oldOrderCost = 0;
        if (!empty($userInstance->productOrders)) {
            foreach ($userInstance->productOrders as $productOrder) {
                foreach ($productOrder->orders as $order) {
                    if ($order->treasury_account_id == $pointAccountTreasury->id && $order->paid_at == null) {
                        $oldOrderCost += $order->amount;
                    }
                }
            }
        }

        //calculate all credit
        $credits = [];
        $accountInstance = Account::where('user_id', $userInstance->id)->with(['accountType.wallet', 'credits'])->get();
        foreach ($accountInstance as $item) {
            $wallet = $item->accountType->wallet->type;
            $credit = $item->credits->first()->amount;
            $credits[$wallet] = $credit;
        }

        //add old product order to response
        $productOrdersInstance = $productOrderDB->getProductOrderWithUserId($userInstance->id);

        //check for old product order amount
        if (!empty($oldOrderCost)) {
            $credits["point"] = $credits["point"] - $oldOrderCost;
        }

        $userInstance->credits_value = $credits;
        if (!$productOrdersInstance->isEmpty() && !empty($productOrdersInstance->first()->orders->where("type", "gift_order"))) {
            $userInstance->old_order = $productOrdersInstance->first();
            /*                    $userInstance->old_order = [
                                    "product" => $productOrdersInstance->first()->costs->first()->product->toArray(),
                                    "order" => $productOrdersInstance->first()->orders->where("type", "gift_order")->first()->toArray()
                                ];*/
        } else {
            $userInstance->old_order = false;
        }

        unset($userInstance->password);
        return $this->successResponse("information", $userInstance->getAttributes(), 200);
    }

    /**
     * @param MobileRequest $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkMobile(MobileRequest $r)
    {
        if (!\Gate::allows("merchant")) {
            //check user authorization
            return $this->errorResponse("403", __('messages.must_be_merchant'), 403);
        }

        $userInstance = User::where('mobile', $r->mobile)
//                ->with("productOrders.orders")
            ->first();

        //check user exist
        if (!$userInstance instanceof User) {
            //when user not exist
            $userDB = new UserDB();
            $userData = [
                "mobile" => $r->mobile,
                "author_id" => \Auth::id(),
                "type" => "customer",
            ];
            $userInstance = $userDB->create($userData);
            event(new CustomerRegisterEvent($userInstance, $userData));
        }

        $productOrderDB = new ProductOrderDB();

        //get point treasury account
        $walletDB = new WalletDB();
        $pointAccountTreasury = $walletDB->getTreasuryAccountInstanceWithWalletType("point");

        //sum old product order point
        $oldOrderCost = 0;
        if (!empty($userInstance->productOrders)) {
            foreach ($userInstance->productOrders as $productOrder) {
                foreach ($productOrder->orders as $order) {
                    if ($order->treasury_account_id == $pointAccountTreasury->id && $order->paid_at == null) {
                        $oldOrderCost += $order->amount;
                    }
                }
            }
        }

        //calculate all credit
        $credits = [];
        $accountInstance = Account::where('user_id', $userInstance->id)->with(['accountType.wallet', 'credits'])->get();
        foreach ($accountInstance as $item) {
            $wallet = $item->accountType->wallet->type;
            $credit = $item->credits->first()->amount;
            $credits[$wallet] = $credit;
        }

        //add old product order to response
        $productOrdersInstance = $productOrderDB->getProductOrderWithUserId($userInstance->id);

        //check for old product order amount
        if (!empty($oldOrderCost)) {
            $credits["point"] = $credits["point"] - $oldOrderCost;
        }

        $userInstance->credits_value = $credits;
        if (!$productOrdersInstance->isEmpty() && !empty($productOrdersInstance->first()->orders->where("type", "gift_order"))) {
            $userInstance->old_order = $productOrdersInstance->first();
            /*                    $userInstance->old_order = [
                                    "product" => $productOrdersInstance->first()->costs->first()->product->toArray(),
                                    "order" => $productOrdersInstance->first()->orders->where("type", "gift_order")->first()->toArray()
                                ];*/
        } else {
            $userInstance->old_order = false;
        }

        unset($userInstance->password);
        return $this->successResponse("information", $userInstance->getAttributes(), 200);


    }

    /**
     * authentication with mobile and pinCode
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authMobile(Request $request)
    {
        $userInstance = User::where("mobile", $request->mobile)->first();
        if ($userInstance instanceof User) {
            if ($request->password == $userInstance->pinCode) {
                return $this->successResponse("user_authenticated_successfully", true, 200);
            }
            return $this->errorResponse("403", false, 403);
        }
        return $this->errorResponse("400", __("messages.user_not_exist"), 404);
    }

    /**
     * send recovery password for cafeviuna user
     * @param checkMobileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendPassword(checkMobileRequest $request)
    {
        //check user exist and has a password in cafeviuna
        $userDB = new UserDB();
        $userInstance = $userDB->getUser($request->mobile);
        if ($userInstance instanceof User) {
            if (empty($userInstance->pinCode)) {
                //cafeviuna customer recovery password or register password and send to him with sms
                $rand_key = rand(1000, 9999);
                User::query()->where("mobile", $request->mobile)->update(["pinCode" => $rand_key]);
                $userInstance->setAttribute("pinCode", $rand_key);
                event(new RecoveryPasswordCafeViunaSmsEvent($userInstance->mobile, $rand_key));
                return $this->successResponse("successfully_send_message", $userInstance, 200);
            }
            //send sms with password content for specific user
            event(new RecoveryPasswordCafeViunaSmsEvent($userInstance->mobile, $userInstance->pinCode));
            return $this->successResponse("successfully_send_message", $userInstance, 200);
        }
        //when user not exist
        return $this->errorResponse("400", __("messages.user_not_exist"), 404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function changeUserType(Request $request)
    {
        $userDB = new UserDB();
        $authController = new AuthController();

        if (!empty($request->user_id)) {

            //update user to operator
            $test = $response = $userDB->update($request->user_id, [
                "type" => $request->user_type,
                "merchant_id" => $request->merchant_id,
            ]);

            if ($response) {
                return response()
                    ->json([
                        "message" => __("messages.user_successfully_update")
                    ], 200);
            }
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => __("messages.user_type_not_change")
                ], 400);

        } else {
            dd("not yet");
            //create new user
            $userRequest = new Request();
            $requestData = [
                "name" => $request->name,
                "email" => $request->email,
                "mobile" => $request->mobile,
                "mobile_verified_at" => now(),
                "password" => bcrypt($request->password),
                "birthday" => $request->birthday,
                "gender" => $request->gender,
                "type" => "operator",
                "otp" => false,
            ];
            $userRequest = $userRequest->request($requestData);
            $authController->store($userRequest);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function waiterListByMerchantId()
    {
        $limit = \request()->has("limit") ? \request("limit") : 10;
        $page = \request()->has("page") ? \request("page") : 1;
        $userDB = new UserDB();
        if (\Auth::check() && \Auth::user()->type == "merchant") {
            $waiters = $userDB->waiterListByMerchantId(\Auth::user()->merchant_id);
            return response()
                ->json([
                    "message" => __("messages.waiters_list"),
                    "data" => $waiters->paginate($limit, $page),
                ], 200);
        }
        return response()
            ->json([
                "message" => __("messages.404"),
                "message" => __("messages.your_are_not_merchant_user"),
            ], 400);
    }

    /*---------------------------- dorris methods ----------------------------*/

    /**
     * assign specific card to specific user
     * @param AssignCardRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignCard(AssignCardRequest $request)
    {
        //call card repository
        $cardDB = new CardDB();

        //call user repository
        $userDB = new UserDB();

        //get user with mobile number
        $userInstance = $userDB->getUserWithCardPassword($request->mobile, $request->password);
        if (!$userInstance instanceof User) {
            \Log::alert("this user({$request->mobile}) not exist");
            return $this->errorResponse("400", __("messages.user_not_exist"), 404);
        }

        //check card number assignment
        $cardInstance = $cardDB->getCardInstance($request->card_number);
        if ($cardInstance instanceof Card) {
            \Log::alert("this card({$request->card_number}) assign before");
            return $this->errorResponse("400", __("messages.this_card_assign_before"), 409);
        }

        //insert new card and assign card number to user
        $cardPassword = rand(1000, 9999);
        $cardInstance = $cardDB->insert([
            "card_number" => $request->card_number,
            "password" => $cardPassword,
            "user_id" => $userInstance->id,
            "expired_at" => Carbon::now()->addYear(3)->format("Y-m-d"),
        ]);

        //send sms for user (card number and card password)
        event(new RegisterDorisCustomerSmsEvent($userInstance->mobile, $request->card_number, $cardPassword));

        //return success response of card instance
        return $this->successResponse("info", $cardInstance);
    }

    /**
     * after authentication return user info
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function info(Request $request)
    {
        //call user repository
        $userDB = new UserDB();

        //call card repository
        $cardDB = new CardDB();

        if (isset($request->card_number)) {

            //get user instance by card number and password
            $userInstance = $userDB->getUserByCardNumber($request->card_number, $request->password);
            if (!$userInstance instanceof User) {
                return $this->errorResponse("400", __("messages.user_not_exist"), 404);
            }
        } else {
            $userInstance = $userDB->getUser($request->mobile);
        }

        //get user info with resource
        $userResource = new UserResource($userInstance);

        //return success response
        return $this->successResponse("user_info", $userResource);
    }

    /*--------------------------- mohammad reza ---------------------------------*/
    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\Collection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function usersList(Request $r)
    {
        $filter = \request()->has("filters") ? \request("filters") : null;
        $relations = \request()->has("relations") ? \request("relations") : null;
        $paginate = \request()->has("paginate") ? \request("paginate") : false;
        $filters = json_decode($filter, true);
        $relations = json_decode($relations, true);

        $model = $this->getModel("user");

        if (!is_null($filters) && !is_null($relations)) {
            $result = $this->checkFilter($filters, $model, $relations);
            if (!is_null($result)) {
                return $result->paginate(\request("page"), \request("limit"));
            } else {
                return Response()->json(null, 400);
            }
        } elseif (!is_null($filters)) {
            $result = $this->checkFilter($filters, $model, $relations);
            if (!is_null($result)) {
                return $result;
            } else {
                return Response()->json(null, 400);
            }
        } elseif (!is_null($relations)) {
            return Response()->json(
                $model::with($relations)->paginate()
                , 200);
        } else {
            return Response()->json(
                $model->paginate($r->input("limit"), ["*"], 'page', $r->input("page"))
                , 200);
        }
    }

}
