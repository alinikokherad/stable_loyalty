<?php

namespace App\Http\Controllers\Api;

use App\Events\LoginCafeViunaSiteOtpSmsEvent;
use App\Events\RequestRegisterCafeViunaSmsEvent;
use App\Events\CustomerRegisterEvent;
use App\Events\UserRegisterSendSmsEvent;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Jobs\SendAppDownloadLinkJob;
use App\repo\UserDB;
use App\User;
use App\WhiteList;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Modules\Attachment\Entities\Attachment;
use Modules\Club\repo\ClubDB;
use Modules\Crud\Http\Controllers\CrudController;
use Modules\Wallet\repo\WalletDB;
use mysql_xdevapi\Exception;

class AuthController extends CrudController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request)
    {
        //register user into database
        $data = $request->all();

        //todo update user login or register or otp switch here
        $data["otp"] = ($request->input("register_type") == "mobile") ? true : false;
        if (!$data['otp']) {
//            $data["password"] = $request->has('password') ? bcrypt($request->input("password")) : null;
        } else {
            unset($data["password"]);
        }
        return $this->doRegister($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function mobileConfirm(Request $request)
    {
        $userDB = resolve(UserDB::class);
        $sms_code = $request->has("sms_code") ? $request->input("sms_code") : $request["sms_code"];
        $mobile = $request->has("mobile") ? $request->input("mobile") : $request["mobile"];

        //get user with mobile number
        $mobileExist = $userDB->getUser($mobile);
        if (!($mobileExist instanceof User)) {
            return $this->errorResponse("400", __("messages.invalid_mobile"), 400);
        }

        //check user in redis
        $userRedis = json_decode(Redis::get("user:$mobile"));
        if ((isset($userRedis->sms_code) && ($sms_code == $userRedis->sms_code) && ($mobile == $userRedis->mobile)) || isset($userRedis->mobile)) {

            //set mobile validation in the database
            User::where("mobile", $mobile)
                ->update(["mobile_verified_at" => Carbon::now()]);

            //attempt user
            $this->getAttempt($request, "mobile", $mobileExist);

            //create token
            $token = Auth::user()
                ->createToken(
                    "loyalty"
                );

            //check user complete profile
            $profile_complete = false;
            $currentUser = Auth::user();
            if (!is_null($currentUser->name) && !is_null($currentUser->family) && !is_null($currentUser->birthday)) {
                $profile_complete = true;
            }

            $token->membership = $profile_complete;
            return $this->successResponse("success_confirm", $token, 200);
        }

        return $this->errorResponse("error_confirm_message", __("messages.error_confirm"), 403);
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerRequest(RegisterRequest $request)
    {
        $userDB = new UserDB();
        $mobile = $request->input("mobile");
        $confirm = $request->has("confirm") ? $request->confirm : false;

        $userExist = $userDB->getUser($mobile);
        if ($userExist && $confirm == false) {
            return $this->successResponse("url_dont_sent", $userExist, 409);
        }

        //download link for mobile app
        $appUrl = "link";
        $pinCode = rand(1000, 9999);

        //register user
        $where = ["mobile" => $mobile];
        $data = [
            "mobile" => $mobile,
            "type" => "customer",
            "pinCode" => $pinCode
        ];
        $userInstance = $userDB->firstOrCreate($where, $data);

        if (empty($userInstance->pinCode)) {
            $userInstance->pinCode = $pinCode;
            $userInstance->save();
        }

        //call user register event (create account and credit)
        if (empty($userInstance->load("accounts")->accounts->first())) {
            event(new CustomerRegisterEvent($userInstance, $data));
        }

        //send cafeviuna customer app url with sms
        event(new RequestRegisterCafeViunaSmsEvent($mobile, $userInstance->pinCode, $appUrl));

        return $this->successResponse("url_sent", $userInstance, 200);
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try {
            $userDB = new UserDB();
            $mobile = $request->input("mobile");
            $userInstance = $userDB->getUser($mobile);
            if (!($userInstance instanceof User)) {
                //when user is not register
                $request = $request->all();
                $request["register_type"] = "mobile";
                if ($request['app_name'] == 'dorris_merchant_app_v1') {
                    return $this->errorResponse("403", __('messages.no_access'));
                } else {
                    return $this->doRegister($request);
                }
                \Log::info("user {$mobile} is registered");
            } else {
                //Todo اینجا رو درست کن
                //when user has into white list
                try {
                    $whiteListUser = WhiteList::query()
                        ->where("mobile", $mobile)
                        ->first();
                } catch (\Exception $exception) {
                    dd($exception->getMessage());
                }
                if ($whiteListUser instanceof WhiteList) {
                    $userInstance = $userDB->getUser($mobile);

                    //attempt user
                    $res = $this->getAttempt($request, "email", $userInstance);
                    if ($res === "password") {
                        return response()
                            ->json([
                                "message" => __("messages.400"),
                                "error" => __("messages.invalid_password_input")
                            ], 400);
                    }

                    //create token
                    $token = Auth::user()
                        ->createToken(
                            "loyalty",
                            [Auth::user()->type]
                        );

                    $token = $token->toArray();
                    $token["user_type"] = $userInstance->type;
                    return $this->successResponse("success_confirm", $token, 200);
                }

                //when user is registered who want to login
                //create random sms code and save that into redis database
                $sms_code = rand(1111, 9999);
//                $sms_code = 1111;
                $user = [
                    "mobile" => $mobile,
                    "sms_code" => $sms_code,
                ];
                Redis::set("user:$mobile", json_encode($user), 'EX', 300);

                //sms code put into queue for send with sms server
                event(new UserRegisterSendSmsEvent($userInstance, $sms_code, $request));

                //return response after register user
                return $this->successResponse("200", __("messages.confirm_mobile"));
            }
        } catch (\Exception $e) {
            return $this->errorResponse("400", __("messages.credential_is_incorrect"), 404);
        }
    }

    public function loginApp(LoginRequest $request)
    {
        try {
            $userDB = new UserDB();
            $mobile = $request->input("mobile");

            // check mobile
            $userInstance = $userDB->getUser($mobile);

            if (!($userInstance instanceof User)) {
                return $this->errorResponse("403", __('messages.no_access'), 403);
            } else {

                $userType = $userInstance->type;

                if ($userType != "merchant" && $userType != "personnel") {
                    return $this->errorResponse("403", __('messages.no_access'), 403);
                }

                //when user has into white list
                $whiteListUser = WhiteList::where("mobile", $mobile)->first();
                if ($whiteListUser instanceof WhiteList) {
                    $userInstance = User::where("mobile", $mobile)->first();

                    //attempt user
                    $res = $this->getAttempt($request, "email", $userInstance);
                    if ($res === "password") {
                        return $this->errorResponse("400", __("messages.invalid_password_input"), 400);
                    }
                    //create token
                    $token = Auth::user()
                        ->createToken(
                            "loyalty",
                            [Auth::user()->type]
                        );

                    // add userType to token 4 response
                    $token = $token->toArray();
                    $token["user_type"] = $userInstance->type;

                    return $this->successResponse("success_confirm", $token, 200);
                }
                return $this->errorResponse("403", __('messages.no_access'));
            }
        } catch (\Exception $e) {
            return $this->errorResponse("400", __("messages.credential_is_incorrect"), 404);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function renewToken()
    {
        $token = Auth::user()
            ->createToken(
                "loyalty"
            );
        return $this->successResponse("renewToken", $token->accessToken, 200);
    }

    /**
     * @param $request
     * @param $type
     * @param null $userInstance
     * @return bool|string
     * @throws \Exception
     */
    private function getAttempt($request, $type, $userInstance = null)
    {
        switch ($type) {
            case "mobile":
                $userInstance = \Auth::loginUsingId($userInstance->id);
                if ($userInstance instanceof User) {
                    return true;
                }
                return false;
                break;
            default:
                if (empty($request->input("password"))) {
                    return "password";
                }
                $res = \Auth::attempt([
                    'mobile' => $request->input("mobile"),
                    'password' => $request->input("password")
                ], false);
                if (!$res) {
                    \Log::alert("user " . $request->input("mobile") . "\"credential is incorrect\" and status code is 403");
                    throw new \Exception("credential_is_incorrect", 403);
//                    throw new \Exception("credential_is_incorrect", 403);
                }
        }
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    private function doRegister(array $data): \Illuminate\Http\JsonResponse
    {
        $userDB = new UserDB();
        try {
            /*------------ user register transaction -----------*/
            \DB::beginTransaction();
            $data1 = ["mobile" => $data["mobile"]];
            $data["tier_id"] = 1;

            //todo چه کاربری در چه مرچنتی ثبت شود
//            if (empty($data["merchant_id"]) || !isset($data["merchant_id"])) {
//                $data["merchant_id"] = 1;
//            }

            $data["author_id"] = Auth::check() ? Auth::id() : null;

            if (!empty($data["password"])) {
                $data["password"] = bcrypt($data["password"]);
            }

            $userInstance = $userDB->firstOrCreate($data1, $data);

            //TODO شرط ورود یک مشتری به یک باشگاه اینجا اعمال می شود
            //call club repository
            /*            $clubDB = new ClubDB();
                        $allClubsId = $clubDB->getFreeClubs();
                        if (!empty($allClubsId)) {
                            $userInstance->clubs()->sync($allClubsId);
                        }*/

            if ((isset($data["user_type"]) && $data["user_type"] == "personnel") ||
                (isset($data["type"]) && $data["type"] == "waiter") ||
                (isset($data["type"]) && $data["type"] == "super_admin") ||
                (isset($data["type"]) && $data["type"] == "merchant_admin") ||
                (isset($data["type"]) && $data["type"] == "club_admin")
            ) {
                WhiteList::query()->create([
                    "mobile" => $data["mobile"],
                    "user_id" => $userInstance->id,
                ]);
            }

            //if register with mobile
//            $data["register_type"] = "mobile";
            if ($data["register_type"] == "mobile") {
                $sms_code = $this->setRedis($data);

                //send sms code for login cafeViuna site
                event(new LoginCafeViunaSiteOtpSmsEvent($userInstance->mobile, $sms_code));
            }

            //call user register event
            event(new CustomerRegisterEvent($userInstance, $data));

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return $this->errorResponse("user_register_fail", $e->getMessage(), 400);
        }

        //return response after register user
        return $this->successResponse("confirm_mobile", $userInstance, 200);
    }

    /**
     * @param $mobile
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser($mobile)
    {
        if (!\Gate::allows("merchant")) {
            $userInstance = User::where("mobile", $mobile)
                ->orWhere("id", $mobile)
//                ->with("accounts.credits")
                ->first();

            if ($userInstance instanceof User) {
                $walletDB = new WalletDB();
                $rialsTreasuryAccount = $walletDB->getTreasuryAccountInstanceWithWalletType("rials");
                $pointTreasuryAccount = $walletDB->getTreasuryAccountInstanceWithWalletType("point");

                //calculate user rials and point credit
                $credits = [];
                foreach ($userInstance->accounts as $account) {
                    foreach ($account->credits as $credit) {
                        if ($credit->treasury_id == $rialsTreasuryAccount->id) {
                            $credits["rials"] = $credit->amount;
                        } elseif ($credit->treasury_id == $pointTreasuryAccount->id) {
                            $credits["point"] = $credit->amount;
                        }
                    }
                }

                //add user account credit to user instance
                $userInstance->user_credits = $credits;

                //unset user instance relationsShip
                unset($userInstance->accounts);

                return $this->successResponse("user_info", $userInstance, 200);
            }
            return $this->errorResponse("400", __("messages.user_not_found"), 400);
        }
        return $this->errorResponse("403", __("messages.access_denied"), 403);
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function completeProfile(Request $r)
    {
        $data["img"] = $r->has("img") ? \request()->file("img") : null;

        $idUser = \Auth::user()["id"];

        $rule = [
            "name" => "required|string|min:2|max:30",
            "family" => "required|string|min:2|max:80",
            "birthday" => "required",
            "email" => "unique:users,email",
        ];
        $this->validate($r, $rule);
        $profileData = [
            "name" => $r->name,
            "family" => $r->family,
            "email" => $r->email,
            "birthday" => $r->birthday,
            "gender" => $r->gender,
            "degree" => $r->degree,
        ];
        if (!isset($r->family)) {
            unset($profileData['family']);
        }
        if ($r->has("password")) {
            $profileData["password"] = bcrypt($r->password);
        }
        $userInstance = User::findOrFail($idUser)->update($profileData);
        if ($userInstance == true) {
            $userInstance = User::where('id', $idUser)->with('attaches')->first();
        }

        //after user update into data base image save into public folder
        //save image into public folder
        if (!empty($r->file("img"))) {
            $oldImage = isset($userInstance->attaches->src) ? $userInstance->attaches->src : null;
            if (!empty($oldImage)) {
                if (file_exists($oldImage)) {
                    unlink($oldImage);
                    $userInstance->attaches->delete();
                }
            }
            $newName = 'image' . \Carbon\Carbon::now()->isoFormat('H-i-s') . '.' . $r->file('img')->getClientOriginalExtension();
            $permission = $r->file('img')->move(public_path('uploads/users'), $newName);
            if (!$permission) {
                return response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.public_folder_denied")
                    ], 400);
            }
            $img = 'uploads/users/' . $newName;
            $attachable = new Attachment([
                "src" => $img
            ]);
            $attachable->save();
            User::find($idUser)->attaches()->save($attachable);
        }
        if ($userInstance) {
            return Response()->json([
                'message' => __('messages.user_complete_profile'),
                'data' => User::where('id', $idUser)->with("attaches")->get()
            ], 200);
        }
        return $this->errorResponse("400", __("messages.user_not_updated"), 400);
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function membership(Request $r)
    {
        $idUser = \Auth::id();
        $rule = [
            "name" => "required|string|min:2|max:30",
            "family" => "required|string|min:2|max:80",
            "birthday" => "required|date",
//            "email" => "unique:users,email",
        ];
        $this->validate($r, $rule);
        $profileData = [
            "name" => $r->name,
            "family" => $r->family,
//            "email" => $r->email,
            "birthday" => $r->birthday,
//            "gender" => $r->gender,
//            "degree" => $r->degree,
        ];
        if (!isset($r->family)) {
            unset($profileData['family']);
        }

        $userInstance = User::where("id", $idUser)->update($profileData);
        if ($userInstance == true) {
            $userInstance = User::where('id', $idUser)->first();


            return response()
                ->json([
                    "message" => __("messages.membership_is_ok"),
                    "data" => $userInstance,
                ], 200);
        }
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $r)
    {
        if (!is_null(\Auth::user())) {
            if (\Auth::user()->password == null) {
                $validatedData = $r->validate([
                    'new-password' => 'required',
                ]);
                if (!(\Hash::check($r->input('current-password'), bcrypt(Auth::user()->password)))) {
                    return Response()->json("your current password dose not match", 400);
                } else {
                    if (strcmp($r->input('current-password'), $r->input('new-password')) == 0) {
                        return Response()->json("New Password cannot be same as your current password. Please choose a different password.", 409);
                    }
                    $this->doChangePassword($r->input("new-password"), Auth::user()["id"]);
                    return Response()->json("password changed successfully", 200);
                }
            } elseif (\Auth::user()->password != null) {
                if ((\Hash::check($r->input('current-password'), Auth::user()->password))) {
                    return Response()->json("your current password dose not match", 400);
                } else {
                    if (strcmp($r->input('current-password'), $r->input('new-password')) == 0) {
                        return Response()->json("New Password cannot be same as your current password. Please choose a different password.", 409);
                    }
                    $this->doChangePassword($r->input("new-password"), Auth::user()["id"]);
                    return Response()->json("password changed successfully", 200);
                }
            }
        }
        return Response()->json(__("messages.user_not_exist"), 400);
    }

    /**
     * @param $newPassword
     * @param $idUser
     * @return bool
     */
    private function doChangePassword($newPassword, $idUser)
    {
        return User::findOrFail($idUser)->update([
            "password" => bcrypt($newPassword)
        ]);
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $r)
    {
        if (!is_null(\Auth::user())) {
            $validatedData = $r->validate([
                'new-password' => 'required',
            ]);
            if ((\Hash::check($r->input('new-password'), Auth::user()->password))) {
                return Response()->json("New Password cannot be same as your current password. Please choose a different password.", 409);
            } else {
                //Current password and new password are same
                $this->doChangePassword($r->input("new-password"), Auth::user()["id"]);
                return Response()->json("password changed successfully", 200);
            }
        }
        return Response()->json(__("messages.user_not_exist"), 400);
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\Collection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function usersList(Request $r)
    {
        $filter = \request()->has("filters") ? \request("filters") : null;
        $relations = \request()->has("relations") ? \request("relations") : null;
        $paginate = \request()->has("paginate") ? \request("paginate") : false;
        $filters = json_decode($filter, true);
        $relations = json_decode($relations, true);

        $model = $this->getModel("user");

        if (!is_null($filters) && !is_null($relations)) {
            $result = $this->checkFilter($filters, $model, $r, $relations, $paginate);
            if (!is_null($result)) {
                return $result;
            } else {
                return Response()->json(null, 400);
            }
        } elseif (!is_null($filters)) {
            $result = $this->checkFilter($filters, $model, $r, $relations, $paginate);
            if (!is_null($result)) {
                return $result;
            } else {
                return Response()->json(null, 400);
            }
        } elseif (!is_null($relations)) {
            return Response()->json(
                $model::with($relations)->paginate()
                , 200);
        } else {
            return Response()->json(
                $model->paginate($r->input("page_limit"), ["*"], 'page', $r->input("page_number"))
                , 200);
        }
    }

    /**
     * @param array $data
     * @return int
     */
    private function setRedis(array $data): int
    {
//create random sms code and save that into redis database
        $sms_code = rand(1000, 9999);
        $redisUser = json_encode([
            "mobile" => $data["mobile"],
            "sms_code" => $sms_code
        ]);
        Redis::set("user:{$data["mobile"]}", $redisUser, "EX", 300);
        return $sms_code;
    }
}
