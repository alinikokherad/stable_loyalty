<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;

/**
 * Class SmsController
 * @package App\Http\Controllers\Api
 */
class SmsController extends Controller
{
    /**
     * @param $receptor
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function send($receptor, $message)
    {
        //input normalization
        if (!is_array($receptor)) {
            $receptor = (array)$receptor;
        }

        $smsController = new SmsController();
        $api_key = env("API_KEY", "324A522B5239534D5A456B6F4472436B4D422F32306A4935337771556E4B4C6564684E335769735A2F30773D");
        $sender = env("SENDER", "10001007001000");

        try {
            $api = new KavenegarApi($api_key);
            $result = $api->Send($sender, $receptor, $message);
            $response = [];
            if ($result) {
                foreach ($result as $r) {
                    $response["messageid"] = $r->messageid;
                    $response["message"] = $r->message;
                    $response["status"] = $r->status;
                    $response["statustext"] = $r->statustext;
                    $response["sender"] = $r->sender;
                    $response["receptor"] = $r->receptor;
                    $response["date"] = $r->date;
                    $response["cost"] = $r->cost;
                }
            }
            \Log::info(json_encode($response));
        } catch (ApiException $e) {
            \Log::info($e->errorMessage());
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            return $this->errorResponse("400", $e->errorMessage());
        } catch (HttpException $e) {
            \Log::info($e->errorMessage());
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            return $this->errorResponse("400", $e->errorMessage());
        }
        return $this->successResponse("information", $response);
    }
}
