<?php

namespace App\Http\Controllers\Api;

use App\ObjectFactory\ModelFactory;
use App\repo\WhiteListDB;
use App\WhiteList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WhiteListController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $user_id = \request()->has("user_id") ? \request('user_id') : null;
        $whiteListDB = ModelFactory::build("WhiteListDB");
        $whiteListMobileNumbers = $whiteListDB->index($user_id);
        return $whiteListMobileNumbers;
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Database\Eloquent\Model|\Modules\Branch\Entities\Merchant
     */
    public function store(Request $request)
    {
        $whiteListDB = new WhiteListDB();
        $instance = $whiteListDB->insert($request->all());
        return $instance;
    }

    /**
     * @param $mobile
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($mobile)
    {
        $whiteListDB = ModelFactory::build("WhiteListDB");
        $res = $whiteListDB->destroy($mobile);
        return response()
            ->json([
                null
            ], 204);
    }
}
