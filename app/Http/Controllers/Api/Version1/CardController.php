<?php

namespace App\Http\Controllers\Api\Version1;

use App\Card;
use App\Http\Requests\CardRequest;
use App\repo\CardDB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CardController extends Controller
{
    //
    /**
     * return all cards table records
     * @return JsonResponse
     */
    public function index()
    {
        //check user authorization
        if (!\Gate::allows("personnel")) {
            return $this->errorResponse("403", __("messages.403"), 403);
        }

        //create instance from card repository
        $cardDB = new CardDB();

        //get all card from card repository
        $cardsInstances = $cardDB->get();

        //when all operation is successfully
        return $this
            ->successResponse("card_list", $cardsInstances->paginate(\request("limit") ?? 10, \request("page") ?? 1), 200);
    }

    /**
     * @param CardRequest $request
     * @return JsonResponse
     */
    public function store(CardRequest $request)
    {
        //check user authorization
        if (\Gate::allows("personnel")) {
            return $this->errorResponse("403", __("messages.403"), 403);
        }

        //create instance from card repository
        $cardDB = new CardDB();

        //insert data into repository
        $cardsInstances = $cardDB->insert($request->all());
        if ($cardsInstances instanceof Card) {
            //when all operation was successfully
            return $this->successResponse("card_insert", $cardsInstances, 200);
        }

        //when somethings went wrong into database
        return $this->errorResponse("400", __("messages.card_not_exist"), 400);
    }

    /**
     * @param $card_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($card_id)
    {
        //check user authorization
        if (\Gate::allows("personnel")) {
            return $this->errorResponse("403", __("messages.403"), 403);
        }

        //create instance from card repository
        $cardDB = new CardDB();

        //find specific card and return instance of card
        $cardInstances = $cardDB->find($card_id);
        if ($cardInstances) {
            return $this->successResponse("card_list", $cardInstances, 200);
        }

        //when something went wrong into database or card is not exist
        return $this->errorResponse("400", __("messages.card_not_exist"), 400);
    }

    /**
     * update specify card
     * @param Request $request
     * @param $card_id integer
     * @return  JsonResponse
     */
    public function update(Request $request, $card_id)
    {
        //check user authorization
        if (\Gate::allows("personnel")) {
            return $this->errorResponse("403", __("messages.403"), 403);
        }

        //create instance from card repository
        $cardDB = new CardDB();

        //update specify card
        $updateResponse = $cardDB->update($request->all(), $card_id);
        if ($updateResponse) {
            return $this->successResponse("card_update_successfully", __("messages.card_update_successfully"), 201);
        }
        return $this->errorResponse("card_not_exist", __("messages.card_not_exist"), 404);
    }

    /**
     * @param $card_id
     * @return JsonResponse
     */
    public function destroy($card_id)
    {
        //check user authorization
        if (\Gate::allows("personnel")) {
            return $this->errorResponse("403", __("messages.403"), 403);
        }

        //create instance from card repository
        $cardDB = new CardDB();

        //delete specify card
        $cardDB->delete($card_id);
        return $this->successResponse("card_delete_successfully", null, 204);
    }
}
