<?php

namespace App\Http\Controllers\Api\Version1;

use App\Events\DorisPanelLoginOtpEvent;
use App\Events\RecoveryDorisPasswordEvent;
use App\Events\RegisterDorisCustomerSmsEvent;
use App\Events\CustomerRegisterEvent;
use App\Http\Requests\DorrisManualUserRegisterRequest;
use App\Http\Requests\DorrisPanelLoginRequest;
use App\Http\Requests\MobileRequest;
use App\Http\Resources\UserResource;
use App\repo\CardDB;
use App\repo\UserDB;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Modules\Club\repo\ClubDB;

class DorrisAuthController extends Controller
{
    //
    public function auth(Request $request)
    {
        //call user repository
        $userDB = new UserDB();
        $userInstance = $userDB->getUser($request->mobile);

        if ($userInstance instanceof User) {
            //user register before and just show information
            $userResource = new UserResource($userInstance);

            return $this->successResponse("user_info", $userResource, 200);
        }

        //user not register before and most be register
        return $this->register($request);
    }

    private function register(Request $request)
    {
        $userDB = new UserDB();
        try {
            /*------------ user register transaction -----------*/
            \DB::beginTransaction();
            $data1 = ["mobile" => $request->mobile];
            $data["tier_id"] = 1;
            $data["merchant_id"] = isset($data["merchant_id"]) ? $data["merchant_id"] : 0;
            $data["type"] = isset($data["type"]) ? $data["type"] : "customer";
            $author_id = Auth::id();
            $data["author_id"] = $author_id;
            $userInstance = $userDB->firstOrCreate($data1, $data);

            //create random sms code and save that into redis database
            /*            $sms_code = 1111;
            //                $sms_code = rand(1111, 9999);
                        $redisUser = json_encode([
                            "mobile" => $data["mobile"],
                            "sms_code" => $sms_code
                        ]);
                        Redis::set("user:{$data["mobile"]}", $redisUser, "EX", 300);*/

            //sms code put into queue for send with sms server
//            event(new UserRegisterSendSmsEvent($userInstance, $sms_code, $data));

            //call user register event
            event(new CustomerRegisterEvent($userInstance, $data));
            \DB::commit();
            $userResource = new UserResource($userInstance);
            return $this->successResponse("user_register_success", $userResource, 201);
        } catch (\Exception $e) {
            \DB::rollBack();
            return $this->errorResponse("user_register_fail", $e->getMessage(), 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendPassword(Request $request)
    {
        //call user repository
        $userDB = new UserDB();

        $userInstance = $userDB->getUserCardWithMobile($request->mobile);
        if ($userInstance) {
            //send password with sms
            event(new RecoveryDorisPasswordEvent($userInstance->mobile, $userInstance->cards->first()->password));

            //send success response
            return $this->successResponse("200", __("messages.password_send_successfully"), 200);
        }

        //send error response
        return $this->errorResponse("400", __("messages.input_invalid"), 400);
    }

    /**
     * insert user by merchant admin
     * @param DorrisManualUserRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function manualRegister(DorrisManualUserRegisterRequest $request)
    {
        //call user repository
        $userDB = new UserDB();
        $data = $request->all();
        $data["author_id"] = Auth::id();
        if ($request->type != "customer") {
            $data["merchant_id"] = Auth::user()->merchant_id;
        }

        //register user or customer
        $userInstance = $userDB->store($data);
        event(new CustomerRegisterEvent($userInstance, $request->all()));

        //get doris club instance
        /*        $clubDB = new ClubDB();
                $dorisClubInstance = $clubDB->getClubWithName("doris");
                $userInstance->clubs()->sync([$dorisClubInstance->id]);*/

        if ($request->has("card_number")) {

            //call card repository
            $cardDB = new CardDB();
            $cardPassword = rand(1000, 9999);
            $cardDB->insert([
                "card_number" => $request->card_number,
                "password" => $cardPassword,
                "user_id" => $userInstance->id,
                "expired_at" => Carbon::now()->addYear(3),
            ]);

            //send doris user card number and password with sms
            event(new RegisterDorisCustomerSmsEvent($userInstance->mobile, $request->card_number, $cardPassword));
        }

        if ($userInstance) {
            return $this->successResponse("200", $userInstance);
        }

        return $this->errorResponse("400", __("messages.400"));
    }

    /**
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(DorrisManualUserRegisterRequest $request, $user_id)
    {
        //call user repository
        $userDB = new UserDB();
        $userData = $request->all();
        unset($userData["card_number"]);
        $response = $userDB->update(["id" => $user_id], $userData);

        if ($request->has("card_number")) {
            //get user card
            $cardDB = new CardDB();
            $cardInstance = $cardDB->getCardWithUserId($user_id);
            if (empty($cardInstance)) {
                //user dont has a card
                //call card repository
                $cardDB = new CardDB();
                $cardPassword = rand(1000, 9999);
                $cardDB->insert([
                    "card_number" => $request->card_number,
                    "password" => $cardPassword,
                    "user_id" => $user_id,
                    "expired_at" => Carbon::now()->addYear(3),
                ]);

                //get user instance
                $userInstance = $userDB->get($user_id);

                //send doris customer welcome and card password
                event(new RegisterDorisCustomerSmsEvent($userInstance->mobile, $request->card_number, $cardPassword));

            } elseif (!empty($cardInstance)) {
                //when user has a card
                $cardInstance->card_number = $request->card_number;
                $cardInstance->save();
            }
        }

        if ($response) {
            return $this->successResponse("200", __("messages.user_update_successfully"));
        }
        return $this->errorResponse("400", __("messages.400"));
    }

    /**
     * @param MobileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function panelLogin(MobileRequest $request)
    {
        //generate random sms code
        $smsCode = rand(1000, 9999);

        //register into redis user and sms code
        Redis::set("user:{$request->mobile}", $smsCode);

        //send dorris user otp (sms)
        event(new DorisPanelLoginOtpEvent($request->mobile, $smsCode));

        return $this->successResponse("enter_sms_code", __("messages.enter_sms_code"), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmSmsCode(DorrisPanelLoginRequest $request)
    {
        $userSmsCode = Redis::get("user:{$request->mobile}");

        if ($userSmsCode == $request->sms_code) {
            $userDB = new UserDB();
            $userInstance = $userDB->getUser($request->mobile);
            Auth::loginUsingId($userInstance->id);
            $token = Auth::user()->createToken("dorris", [$userInstance->type]);
            return $this->successResponse("login_success", $token, 200);
        }
        return $this->errorResponse("403", __("credential_is_incorrect"), 403);
    }


}
