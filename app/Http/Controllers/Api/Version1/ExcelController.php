<?php

namespace App\Http\Controllers\Api\Version1;

use App\repo\UserDB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function import(Request $request)
    {
        $file = $request->file("file");
        $res = Excel::toArray(new UsersImport, $file);
        $userDB = new UserDB();
        $failed = [];
        $success = 0;

        foreach ($res[0] as $key => $item) {
            if ($item[0] == "name" && $item[4] == "mobile") {
                continue;
            }
            $userData = [
                "name" => $item[0],
                "family" => $item[1],
                "email" => $item[2],
                "degree" => $item[3],
                "mobile" => "0" . $item[4],
                "birthday" => $item[5],
                "gender" => $item[6],
                "tier_id" => 1,
                "author_id" => \Auth::id(),
            ];

            $mobile = $userData["mobile"];
            unset($userData['mobile']);
            try {
                if (!preg_match('/[0-9]{11}/', $mobile)) {
                    throw new \Exception(__("messages.wrong_mobile_format"), 409);
                }

                if (!preg_match('/[a-zA-Z0-9._]*\@[a-zA-Z]*\.[a-zA-Z]*/', $userData["email"], $output_array)) {
                    throw new \Exception(__("messages.wrong_format_email"), 400);
                }

                $userDB->updateByMobile($mobile, $userData);
                $userUpdate = 0;
            } catch (\Exception $e) {
                $userUpdate = 1;
                if ($e->getCode() == 23000) {
                    array_push($failed, [
                        "mobile" => $mobile,
                        "error" => __("messages.email_conflict")
                    ]);
                }
                if ($e->getCode() == 409) {
                    array_push($failed, [
                        "mobile" => $mobile,
                        "error" => $e->getMessage()
                    ]);
                }
                if ($e->getCode() == 400) {
                    array_push($failed, [
                        "mobile" => $mobile,
                        "error" => $e->getMessage()
                    ]);
                }
            } finally {
                if (isset($userUpdate) && $userUpdate != 1) {
                    $success++;
                }
            }
        }
        $response = [
            "success" => $success,
            "failed" => $failed,
        ];
        return $this->successResponse("200", $response);
    }
}

