<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class homeController extends Controller
{

    public function dashboard(){
        return view('dashboard');
    }

    public function home()
    {
        return view("home");
    }
}
