<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BootstrapController extends Controller
{
    public function get(Request $request)
    {
        return [
            "menu" => [
                [
                    "id" => 1,
                    "title" => 'داشبورد',
                    'subtitle' => "subtitle",
                    "link" => '/dashboard/default',
                    "font_icon" => 'feather icon-home',
                    'menu_id' => 0,
                    'parent_id' => 0,
                    'status' => "active",
                    'priority_view' => 100,
                    'revoked' => false,
                ], [
                    "id" => 2,
                    "title" => 'داشبورد2',
                    'subtitle' => "subtitle2",
                    "link" => '/dashboard/default2',
                    "font_icon" => 'feather icon-home2',
                    'menu_id' => 0,
                    'parent_id' => 0,
                    'status' => "active",
                    'priority_view' => 100,
                    'revoked' => false,
                ],
            ]
        ];
    }
}
