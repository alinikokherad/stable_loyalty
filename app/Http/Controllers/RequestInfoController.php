<?php

namespace App\Http\Controllers;

use File;
use App\RequestInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Route;

class RequestInfoController extends Controller
{

    public function login(Request $r)
    {
        auth()->logout();
        auth()->attempt(['mobile' => $r->mobile, 'password' => $r->password]);
        if (auth()->check()) {
            return redirect()->route('requests');
        } else {
            return redirect()->home();
        }
    }

    public function logOut()
    {
        auth()->logout();
    }


    public function index(Request $r)
    {
        $requestInfo = RequestInfo::orderBy('id', 'desc')->paginate();
        return view('request', compact('requestInfo'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chart()
    {
        $from = \request()->has("from") ? \request("from") : null;
        $to = \request()->has("to") ? \request("to") : null;
        $routes = [];
        $dates = [];
        $routeCount = [];
        $filesInFolder = \File::files('/var/www/html/loyalty/storage/request_info/');
        $data = [];
        $finalDates = [];
        $url = [];
        foreach ($filesInFolder as $val => $path) {
            $contents = [];
            $file = pathinfo($path);
            $logic = config('requests.url');
            $contents = array(File::get(storage_path('request_info/' . $file['filename'] . '.json')));
            $decodedContents = json_decode($contents[0], true);
            $date = explode('_', $file['filename'])[3];

            array_push($dates, $date);
            $finalFile = [];
            foreach ($decodedContents as $c) {
                $c['date'] = $date;
                if (isset($from) && isset($to)){
                    if ($date < $from){
                        continue;
                    }elseif($date > $to){
                        continue;
                    }
                }
                array_push($finalFile, $c);
            }

            foreach ($finalFile as $item) {
                $logic[$item['name']][0] += $item['count'];
            }

            foreach ($logic as $name => $value) {
                $url[$name][] = $value[0];
            }

            array_push($routes,$finalFile);
            foreach ($routes as $item){
                array_merge($routes,$item);
            }
        }

        $dates = json_encode($dates);
        $routeCount = json_encode($url);
        return view('request_chart', compact('routes', 'dates', 'routeCount'));

    }
}
