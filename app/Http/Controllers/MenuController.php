<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Resources\LaravelSmartCrud\SmartCrudController;
use Illuminate\Http\Request;

class MenuController extends SmartCrudController
{
    /**
     * @return void
     */
    public function initController()
    {
        $this->model = new Menu();
        $this->middleware(['auth:api']);
    }

}
