<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Modules\Account\Entities\Account;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        //passport initialize
        Passport::routes();
        Passport::personalAccessTokensExpireIn(now()->addDay(1));
        Passport::tokensCan([
//            'admins' => 'check admin',
//            'members' => 'check member',
            'customer' => 'customer',
            'merchant_admin' => 'merchant admin',
            'personnel' => 'merchant personnel',
            'waiter' => "cafe viuna waiter",
            'club_admin' => "club admin",
            'super_admin' => "super admin",
//            'club',
//            'unknown',
//            'treasury'
        ]);

        //policy initialize
        $this->registerPolicies();

        Gate::define("customer", function ($user) {
            if ($user->type == "customer") {
                return true;
            }
            return false;
        });

        Gate::define("waiter", function ($user) {
            if ($user->type == "waiter") {
                return true;
            }
            return false;
        });

        Gate::define("merchant_admin", function ($user) {
            if ($user->type == "merchant_admin") {
                return true;
            }
            return false;
        });

        Gate::define("super_admin", function ($user) {
            if ($user->type == "super_admin") {
                return true;
            }
            return false;
        });

        Gate::define("club_admin", function ($user) {
            if ($user->type == "club_admin") {
                return true;
            }
            return false;
        });

        Gate::define("personnel", function ($user) {
            if ($user->type == "personnel") {
                return true;
            }
            return false;
        });
    }
}
