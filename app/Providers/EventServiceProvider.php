<?php

namespace App\Providers;

use App\Events\AfterTransactionCreatedEvent;
use App\Events\DorisPanelLoginOtpEvent;
use App\Events\DorrisAddUserManualEvent;
use App\Events\LoginCafeViunaSiteOtpSmsEvent;
use App\Events\RecoveryDorisPasswordEvent;
use App\Events\RecoveryPasswordCafeViunaSmsEvent;
use App\Events\RegisterDorisCustomerSmsEvent;
use App\Events\RequestRegisterCafeViunaSmsEvent;
use App\Events\SendDorisGiftSmsEvent;
use App\Events\SendUserPasswordEvent;
use App\Events\CustomerRegisterEvent;
use App\Events\UserRegisterSendSmsEvent;
use App\Listeners\AfterTransactionCreatedlistener;
use App\Listeners\DorisAddUserManualListener;
use App\Listeners\DorisPanelLoginOtpListener;
use App\Listeners\LoginCafeViunaSiteOtpSmsListener;
use App\Listeners\RecoveryDorisPasswordListener;
use App\Listeners\RecoveryPasswordCafeViunaSmsListener;
use App\Listeners\RegisterDorisCustomerSmsListener;
use App\Listeners\RequestRegisterCafeViunaSmsListener;
use App\Listeners\SendDorisGiftSmsListener;
use App\Listeners\SendUserPasswordListener;
use App\Listeners\CustomerRegisterListener;
use App\Listeners\UserRegisterSendSmsListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Branch\Events\AfterMerchantCreateEvent;
use Modules\Branch\Events\BeforeMerchantCreateEvent;
use Modules\Branch\Listeners\AfterMerchantCreateListener;
use Modules\Branch\Listeners\BeforeMerchantCreateListener;
use Modules\Club\Events\CreatedClubEvent;
use Modules\Club\Listeners\CreatedClubListener;
use Modules\Event\Events\AfterTransactionCreatedEventOld;
use Modules\Event\Listeners\AfterTransactionCreatedListenerOld;
use Modules\Product\Events\CreatedProductEvent;
use Modules\Product\Listeners\CreatedProductListener;
use Modules\Wallet\Events\AccountCreatedEvent;
use Modules\Wallet\Events\AfterPaymentSuccessEvent;
use Modules\Wallet\Events\GetCampaignEvent;
use Modules\Wallet\Events\WalletCreatedEvent;
use Modules\Wallet\Events\WalletDeletedEvent;
use Modules\Wallet\Listeners\AccountCreatedListener;
use Modules\Wallet\Events\CreateOrderEvent;
use Modules\Wallet\Events\AfterCreatedOrderEvent;
use Modules\Wallet\Listeners\AfterPaymentSuccessListener;
use Modules\Wallet\Listeners\CreateOrderListener;
use Modules\Wallet\Listeners\AfterCreatedOrderListener;
use Modules\Wallet\Listeners\GetCampaignListener;
use Modules\Wallet\Listeners\WalletCreatedListener;
use Modules\Wallet\Listeners\WalletDeletedListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        user account created event
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

//          user registered event
        CustomerRegisterEvent::class => [
            CustomerRegisterListener::class,
        ],


//          user account created event
        AccountCreatedEvent::class => [
            AccountCreatedListener::class
        ],

//        wallet register event
        WalletCreatedEvent::class => [
            WalletCreatedListener::class
        ],

//          club created event
        CreatedClubEvent::class => [
            CreatedClubListener::class
        ],

        //        order event
        AfterCreatedOrderEvent::class => [
            AfterCreatedOrderListener::class
        ],

        //when create event
        AfterTransactionCreatedEventOld::class => [
            AfterTransactionCreatedListenerOld::class
        ],

        //when create transaction
        AfterTransactionCreatedEvent::class => [
            AfterTransactionCreatedListener::class
        ],


        //after create event for transaction and get club id this event running
        GetCampaignEvent::class => [
            GetCampaignListener::class
        ],

        //delete relations wallet after delete wallet
        WalletDeletedEvent::class => [
            WalletDeletedListener::class
        ],

//        order created event
        CreateOrderEvent::class => [
            CreateOrderListener::class
        ],

//      product created event
        CreatedProductEvent::class => [
            CreatedProductListener::class
        ],

        //merchant create event
        BeforeMerchantCreateEvent::class => [
            BeforeMerchantCreateListener::class
        ],

        AfterMerchantCreateEvent::class => [
            AfterMerchantCreateListener::class
        ],

        //after payment success this event is fire
        AfterPaymentSuccessEvent::class => [
            AfterPaymentSuccessListener::class
        ],

        //for send user password in loviona
        SendUserPasswordEvent::class => [
            SendUserPasswordListener::class
        ],

        //for dorris add user manually
        DorrisAddUserManualEvent::class => [
            DorisAddUserManualListener::class
        ],

        /*---------------- sms event and listener ----------------*/
        //doris sms
        DorisPanelLoginOtpEvent::class => [
            DorisPanelLoginOtpListener::class
        ],

        //doris sms
        RecoveryDorisPasswordEvent::class => [
            RecoveryDorisPasswordListener::class
        ],

        //doris sms
        RegisterDorisCustomerSmsEvent::class => [
            RegisterDorisCustomerSmsListener::class
        ],

        //doris sms
        SendDorisGiftSmsEvent::class => [
            SendDorisGiftSmsListener::class
        ],

        //cafeViuna  sms
        LoginCafeViunaSiteOtpSmsEvent::class => [
            LoginCafeViunaSiteOtpSmsListener::class
        ],

        //cafeViuna  sms
        RecoveryPasswordCafeViunaSmsEvent::class => [
            RecoveryPasswordCafeViunaSmsListener::class
        ],

        //cafeViuna  sms
        RequestRegisterCafeViunaSmsEvent::class => [
            RequestRegisterCafeViunaSmsListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
