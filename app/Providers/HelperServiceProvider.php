<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $modulesHelpers = collect(glob(base_path("Modules/*/Helpers/*.php")));
        $appHelpers = collect(glob(base_path("/app/Helpers/*.php")));
        $helpers = $modulesHelpers->merge($appHelpers);
        $helpers->map(function($helper){
            require_once $helper;
        });
    }

}
