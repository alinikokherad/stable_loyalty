<?php

namespace App\Providers;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;
use Modules\Branch\Entities\Merchant;
use Modules\Product\Entities\Product;
use Modules\Product\Transformers\CategoryResource;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Campaign;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //check that app is local
        if ($this->app->isLocal()) {
            //if local register your services you require for development
//            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        } else {
            //else register your services you require for production
//            $this->app['request']->server->set('HTTPS', true);
        }


        Collection::macro('paginate', function ($pageLimit = 10, $page = null, $total = null, $pageName = 'page') {

            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            $test = new LengthAwarePaginator(
                $this->forPage($page, $pageLimit),

                $total ?: $this->count(),

                $pageLimit,

                $page,

                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),

                    'pageName' => $pageName,

                ]
            );
            return [
                "current_page" => $test->currentPage(),
                "data" => array_values($test->items()),
                "total" => $test->total(),
                "per_page" => $test->perPage(),
                "previous_page_url" => $test->previousPageUrl(),
                "next_page_url" => $test->nextPageUrl(),
                "last_page" => $test->lastPage(),
                "on_first_page" => $test->onFirstPage(),
            ];
        });
    }


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*for setting database*/
        \Schema::defaultStringLength(191);

        /*for polymorphic relation*/
        //morph map for file
        Relation::morphMap([
            "Account" => Account::class,
            "product" => Product::class,
            "Merchant" => Merchant::class,
            "User" => User::class,
            "Campaign" => Campaign::class,
        ]);


        /*for handle promotion*/
        //my codes goes here

        /*model resources*/
        CategoryResource::withoutWrapping();
    }
}
