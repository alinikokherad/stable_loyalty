<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//        'remember_token' => Str::random(10),
        "mobile" => "093757270" . $faker->numberBetween(10, 99),
        "mobile_verified_at" => date("Y-m-d H:i:s", strtotime(time())),
        "password" => '$2y$12$HnVfUIaqlS5dofHncNRsE.bwyQRBuztySsAry7b9tLecfhr5G60Ue',
        "birthday" => $faker->time("Y-m-d H:i:s"),
        "gender" => $faker->randomElement(["male", "female"]),
        "type" => "customer",
        "otp" => $faker->boolean(95),
        "qr_code" => $faker->uuid,
        "settings" => $faker->text(20),
        "status" => $faker->boolean(99),
        "revoked" => $faker->boolean(1),
    ];
});
