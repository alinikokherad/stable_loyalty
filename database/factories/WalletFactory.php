<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$wallets = ["rials" => "ریالی", "point" => "امتیازی"];
$factory->define(\Modules\Wallet\Entities\Wallet::class, function (Faker $faker) use ($wallets) {
    return [
        "title" => key($wallets),
        "type" => $wallets["rials"],
    ];
});
