<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Wallet\Entities\Credit;


$factory->define(Credit::class, function (Faker $faker) {
    return [
        "account_id" => null,
        "treasury_id" => $faker->randomElement([1, 2]),
        "amount" => 0,
    ];
});
