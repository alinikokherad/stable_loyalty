<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Wallet\Entities\Account;

//get treasury ids
/*$accountDB=new \Modules\Wallet\repo\AccountDB();
$treasuryIds=$accountDB->getAllTreasuryIds();
$factory->define(Account::class, function (Faker $faker)use($treasuryIds){
    return [
        "user_id" => null,
        "treasury_id" => $faker->randomElement($treasuryIds),
        "account_type_id" => $faker->randomNumber(1),
    ];
});*/


$factory->define(Account::class, function (Faker $faker) {
    return [
        "user_id" => 1,
        "treasury_id" => 1,
        "account_type_id" => 2,
    ];
});
