<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    $clubInstance = \Modules\Club\Entities\Club::first();
    return [
        "club_id" => $clubInstance->id,
        "title" => $faker->text(10),
        "subtitle" => $faker->text(15),
        "description" => $faker->text(30),
    ];
});
