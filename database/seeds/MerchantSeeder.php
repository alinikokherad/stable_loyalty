<?php

use Illuminate\Database\Seeder;
use Modules\Branch\Entities\Merchant;
use Modules\Branch\Http\Requests\MerchantRequest;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createMerchant();
    }

    private function createMerchant()
    {
        //get club-admin instance
        $userDB = new \App\repo\UserDB();
        $clubAdminUser = $userDB->getUserWithType("club_admin");
        Auth::loginUsingId($clubAdminUser->id);

        $merchantController = new \Modules\Branch\Http\Controllers\MerchantController();
        $request = new MerchantRequest();

        //create central branch for cafeViuna
        $data = [
            "name" => "cafeViuna",
            "title" => "کافه ویونا",
            "description" => " شعبه مرکزی کافه ویونا",
            "address" => "tehran-tajrish",
            "phone" => "021785452" . rand(0, 9),
            "parent_id" => 0,
        ];
        $merchantRequest = $request->replace($data);
        $test = $merchantController->store($merchantRequest);

        $merchantNames = ["loviuna", 'doris'];
        foreach ($merchantNames as $merchantName) {
            $data = [
                "name" => $merchantName,
                "title" => $merchantName != "dorris" ? "کافه ویونا" : "درریس",
                "description" => $merchantName != "dorris" ? "کافه ویونا" : "درریس",
                "address" => "tehran-jeyhon",
                "phone" => "021785452" . rand(0, 9),
                "parent_id" => $merchantName == "loviuna" ? $test->getData()->data->id : 0,
            ];
            $merchantRequest = $request->replace($data);
            $test = $merchantController->store($merchantRequest);
        }

        /*----------- create club relation ---------------*/
        //create relation between dorris merchant and dorris club
        $dorrisMerchantId = Merchant::query()->where("name", "doris")->pluck("id")->first();
        //get club
        $clubInstance = \Modules\Club\Entities\Club::query()->where("name", "doris")->first();
        $clubInstance->merchants()->sync([$dorrisMerchantId]);

        //create relation between loviuna merchant and cafeViuna club
        $coffeeViunaMerchantId = Merchant::query()->where("name", "loviuna")->orWhere("name", "cafeViuna")->get()->pluck("id");
        //get club
        $clubInstance = \Modules\Club\Entities\Club::query()->where("name", "cafeViuna")->first();
        $clubInstance->merchants()->sync($coffeeViunaMerchantId);
        /*------------- create user relation -------------*/
        $getAllMerchant = Merchant::all()->pluck("id");
        $merchantUserInstance = $userDB->getUserWithType("merchant_admin");
        $merchantUserInstance->merchants()->sync($getAllMerchant);

        $merchantUserInstance = $userDB->getUserWithType("personnel");
        $getDorisMerchant = Merchant::query()->where("name", "doris")->get()->pluck("id");
        $merchantUserInstance->merchants()->sync($getDorisMerchant);
    }
}
