<?php

use Illuminate\Database\Seeder;
use Modules\Club\Entities\Club;
use Modules\Wallet\Entities\Account;

class ClubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Club::truncate();
        $this->createClub();
    }

    /**
     * create club manual
     */
    private function createClub()
    {
        $clubController = new \Modules\Club\Http\Controllers\Api\ClubController();
        $data = [[
            "name" => "cafeViuna",
            "code" => 123,
            "exchange_rate_point" => 1,
            "exchange_rate_price" => 200000,
            "club_mode" => "reward_only",
            "point_to_card" => true,
            "good_lock_delay" => true,
            "good_lock_point" => 10000,
            "good_lock_date" => date("Y-m-d", time()),
            "delay_penalty" => 1,
            "give_point_at" => date("Y-m-d", time()),
            "point_expired_at" => date("Y-m-d", time()),
            "can_edit_point" => true,
            "can_refund" => true,
            "can_refund_until" => date("Y-m-d", time()),
            "can_cash_out_by_admin" => true,
            "can_cash_out" => true,
            "show_point" => true,
            "generate_card_immediately" => true,
            "card_expired_at" => date("Y-m-d", time()),
            "gift_card_expired_at" => date("Y-m-d", time()),
            "show_revocation_card_for_admin" => true,
            "minimum_shop_to_use_point" => 1,
            "free_shiping" => false,
            "minimum_wage" => 1,
            "maximum_wage" => 1,
            "card_type" => "magnet",
            "checkout_time" => date("Y-m-d", time()),
        ], [
            "name" => "doris",
            "code" => 123,
            "exchange_rate_point" => 3,
            "exchange_rate_price" => 2000000,
            "club_mode" => "reward_only",
            "point_to_card" => true,
            "good_lock_delay" => true,
            "good_lock_point" => 10000,
            "good_lock_date" => date("Y-m-d", time()),
            "delay_penalty" => 1,
            "give_point_at" => date("Y-m-d", time()),
            "point_expired_at" => date("Y-m-d", time()),
            "can_edit_point" => true,
            "can_refund" => true,
            "can_refund_until" => date("Y-m-d", time()),
            "can_cash_out_by_admin" => true,
            "can_cash_out" => true,
            "show_point" => true,
            "generate_card_immediately" => true,
            "card_expired_at" => date("Y-m-d", time()),
            "gift_card_expired_at" => date("Y-m-d", time()),
            "show_revocation_card_for_admin" => true,
            "minimum_shop_to_use_point" => 1,
            "free_shiping" => false,
            "minimum_wage" => 1,
            "maximum_wage" => 1,
            "card_type" => "magnet",
            "checkout_time" => date("Y-m-d", time()),
        ]];
        $i = 1;
        foreach ($data as $club) {
            //create club
            $clubRequest = new \Illuminate\Http\Request($club);
            $test = $clubController->store($clubRequest);
//            dd($test);
            //set rule for arrive to club
            $freeData = [
                "club_id" => $i,
                "user_id" => null,
                "access" => "subscriber"
            ];
            $rep = \App\clubArriveRules::query()->create($freeData);
            $i++;
        }
    }
}
