<?php

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\Account;

class AccountSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $this->createBankAccount();
    }

    /**
     * @throws Exception
     */
    private function createBankAccount()
    {
        $accountController = new \Modules\Wallet\Http\Controllers\Api\AccountController();
        $request = new \Illuminate\Http\Request();
        $accountDB = new \Modules\Wallet\repo\AccountDB();
        $walletDB = new \Modules\Wallet\repo\WalletDB();

        //get bank and cash and unknown account type
        $accountTypes = \Modules\Wallet\Entities\AccountType::query()
            ->where("type", "bank")
            ->orwhere("type", "cash")
            ->orwhere("type", "unknown")
            ->get();

        $banks = [
//            "bank-mellat",
//            "bank-passargad",
//            "bank-melli",
            "bank-persian",
//            "bank-resalat",
//            "bank-keshavarzi"
        ];
        foreach ($accountTypes as $accountType) {
            $treasuryInstance = $walletDB->getTreasuryAccountInstanceWithWalletId($accountType->wallet_id);
            if ($accountType->type == "bank") {
                foreach ($banks as $bank) {
                    $data = [
                        "belongs_to" => null,
                        "belongs_type" => $bank,
                        "treasury_account_id" => $treasuryInstance->id,
                        "account_type_id" => $accountType->id,
                        "amount" => 0,
                    ];
                    $request = $request->replace($data);
                    $accountController->store($request);
                }
            } else {
                $data = [
                    "belongs_to" => null,
                    "belongs_type" => $accountType->type,
                    "treasury_account_id" => $treasuryInstance->id,
                    "account_type_id" => $accountType->id,
                    "amount" => 0,
                ];
                $request = $request->replace($data);
                $accountController->store($request);
            }

        }

    }
}
