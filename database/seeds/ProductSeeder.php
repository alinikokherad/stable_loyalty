<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createProduct();
    }

    private function createProduct()
    {
        $productController = new \Modules\Product\Http\Controllers\Api\ProductController();
        $request = new \Illuminate\Http\Request();

        //get merchant user random
        $merchantUser = \App\User::where("type", "merchant_admin")->first();
        Auth::loginUsingId($merchantUser->id);

        //get all categories
        $categories = \Modules\Product\Entities\Category::all();

        //get merchant id
        $merchants = \Modules\Branch\Entities\Merchant::query()
            ->where("name", "loviuna")
            ->orwhere("name", "cafeViuna")
            ->get();

        $products = [
            "Americano" => "آمریکانو",
            "cappuccino" => "کاپوچینو",
            "latte" => "لاته",
            "mocha" => "موکا",
            "sperso" => "اسپرسو",
        ];
        foreach ($merchants as $merchant) {
            foreach ($categories as $category) {
                foreach ($products as $name => $title) {
                    $data = [
                        "merchant_id" => $merchant->id,
                        "name" => $name,
                        "title" => $title,
                        "description" => "description " . $name,
                        "category_id" => [$category->id],
                        "costs" => [
                            "1" => rand(100, 1000),
                            "4" => 10
                        ],
                    ];

                    $request = $request->replace($data);
                    $test = $productController->store($request);
//                dd($test);

                }
            }

        }

    }
}
