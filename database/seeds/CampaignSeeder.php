<?php

use Illuminate\Database\Seeder;
use Modules\Club\Entities\Club;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Campaign;
use Modules\Wallet\Http\Controllers\Api\CampaignController;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->CampaignCreate();
    }

    private function CampaignCreate()
    {
        $campaignController = new CampaignController();

        //club admin login
        $clubAdminInstance = \App\User::where("type", "=", "club_admin")->first();
        \Auth::loginUsingId($clubAdminInstance->id);

        $walletDB = new \Modules\Wallet\repo\WalletDB();
        $walletsInstance = $walletDB->getClubByNameAndWalletByType("doris", "point");

        $data = [
            "budget" => 1000,
            "type" => "campaign",
            "wallet_id" => $walletsInstance->id,
            "club_id" => $walletsInstance->clubs->first()->id,
            "title" => "روز مادر",
            "subtitle" => "کمپین روز مادر",
            "description" => "تست",
            "balance_type" => "positive",
            "min_account_amount" => 0,
            "max_account_amount" => 0,
            "min_transaction_amount" => 0,
            "max_transaction_amount" => 0,
            "legal" => false,
            "interest_rate" => 0,
            "interest_period" => "daily",
            "started_at" => now()->format("Y-m-d H:i:s"),
            "expired_at" => null,
            "budget_consumed" => null,
        ];

        $campaignRequest = new \Illuminate\Http\Request();
        $campaignRequest = $campaignRequest->replace($data);
        $test = $campaignController->store($campaignRequest);

        /*-------------------------- campaign for cafeViuna ---------------------*/
        $walletsInstance = $walletDB->getClubByNameAndWalletByType("cafeViuna", "point");

        $data = [
            "budget" => 1000,
            "type" => "campaign",
            "wallet_id" => $walletsInstance->id,
            "club_id" => $walletsInstance->clubs->first()->id,
            "title" => "روز مادر",
            "subtitle" => "کمپین روز مادر",
            "description" => "تست",
            "balance_type" => "positive",
            "min_account_amount" => 0,
            "max_account_amount" => 0,
            "min_transaction_amount" => 0,
            "max_transaction_amount" => 0,
            "legal" => false,
            "interest_rate" => 0,
            "interest_period" => "daily",
            "started_at" => now()->format("Y-m-d H:i:s"),
            "expired_at" => null,
            "budget_consumed" => null,
        ];

        $campaignRequest = new \Illuminate\Http\Request();
        $campaignRequest = $campaignRequest->replace($data);
        $test = $campaignController->store($campaignRequest);
    }
}
