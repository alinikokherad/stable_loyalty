<?php

use Illuminate\Database\Seeder;
use Modules\Tier\Entities\Tier;

class TierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createTiers();

    }

    public function createTiers()
    {
        //get club randomly
        $clubInstance = \Modules\Club\Entities\Club::all();

        //tire data
        $titles = [
            "bronze" => "برنزی",
            "silver" => "نقره ای",
            "gold" => "طلایی",
            "diamond" => "الماس",
        ];
        foreach ($clubInstance as $item) {
            foreach ($titles as $title => $subtitle) {
                $tireData = [
                    "club_id" => $item->id,
                    "title" => $title,
                    "subtitle" => $subtitle,
                    "description" => "description " . $title,
                    "revoked" => false,
                ];
                Tier::query()
                    ->create($tireData);
            }
        }


    }
}
