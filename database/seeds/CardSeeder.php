<?php

use Illuminate\Database\Seeder;

class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCard();
    }

    private function createCard()
    {
//        $dorrisClubInstance = \Modules\Club\Entities\Club::where("name", "dorris")->first();
        $users = \App\User::where("type", "customer")
//            ->whereHas("clubs", function ($query) use ($dorrisClubInstance) {
//                $query->where("name", $dorrisClubInstance->name);
//            })
            ->get();
        if (isset($users)) {
            $i = 0;
            foreach ($users as $user) {
                $cardData = [
//                    "card_number" => (string)\Illuminate\Support\Str::uuid(),
                    "card_number" => "111111111111111" . $i,
                    "password" => 1234,
                    "user_id" => $user->id,
                    "status" => "active",
                    "expired_at" => \Illuminate\Support\Carbon::now(),
                ];

                $cardController = new \App\Http\Controllers\Api\Version1\CardController();
                $request = new \App\Http\Requests\CardRequest();
                $cardRequest = $request->replace($cardData);
                $res = $cardController->store($cardRequest);
                $i++;
            }
        }
    }
}
