<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $this->createUser();
    }

    /**
     * @throws Exception
     */
    private function createUser()
    {
        $userController = new \App\Http\Controllers\Api\AuthController();
        $req = new \Illuminate\Http\Request();
        $names = [
            "super_admin" => "super_admin",
            "club_admin" => "club_admin",
//            "unknown" => "unknown",
            "sample" => "waiter",
            "sample2" => "merchant_admin",
            "example3" => "personnel",
            "ali" => "customer",
            "sara" => "customer"
        ];

        $i = 0;
        foreach ($names as $name => $type) {
            $data = [
                "name" => $name,
                "family" => $name,
                "email" => "{$name}" . "@init.com",
                "password" => 123,
                "mobile" => $i > 9 ? "093757270" . $i : "0937572700" . $i,
                "birthday" => \Illuminate\Support\Carbon::now()->format("Y-m-d"),
                "gender" => 'male',
                "type" => $type,
                "tier_id" => 1,
                "pinCode" => 123,
                "account_type" => "customer",
                "register_type" => "password",
            ];
            /*            if ($type == "personnel") {
                            $data["merchant_id"] = 2;
                        } elseif ($type == "waiter" || $name == "sample2") {
                            $data["merchant_id"] = 1;
                        } elseif ($type == "unknown") {
                            $data["merchant_id"] = 1;
                        } elseif ($type == "unknown1" || $name == "bahar" || $name == "naeim") {
                            $data["merchant_id"] = 2;
                            if ($name == "bahar") {
                                $data["mobile"] = "09125305269";
                            }
                            if ($name == "naeim") {
                                $data["mobile"] = "09366520822";
                            }
                        } else {
                            $data["merchant_id"] = 0;
                        }*/
            $userRequest = $req->replace($data);
            $test = $userController->register($userRequest);
//            dd($test);

//            if ($type == "personnel") {
//                $test->getData()->data->clubs()->sync([]);
//            }
            $i++;
        }
        //get club
//        $clubInstance = \Modules\Club\Entities\Club::first();

        //get all user type club_admin
//        $userDB = new \App\repo\UserDB();
//        $clubUser = $userDB->getUserWithType("club_admin");

        //sync club with user
//        $clubInstance->users()->sync([$clubUser->id => ["access" => "subscriber"]]);

        //create relation between customer and club
        /*        $allClubs = \Modules\Club\Entities\Club::query()->get();
                $customersInstance = \App\User::query()->get()->pluck("id");
                $userId = [];
                foreach ($customersInstance as $item) {
                    $userId[$item] = ["access" => "subscriber"];
                }
                foreach ($allClubs as $clubInstance) {
                    $clubInstance->users()->sync($userId);
                }*/
    }
}
