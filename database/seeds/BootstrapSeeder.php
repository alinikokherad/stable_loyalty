<?php

use Illuminate\Database\Seeder;

class BootstrapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createBootstrap();
    }

    private function createBootstrap()
    {

    }
}
