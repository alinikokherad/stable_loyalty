<?php

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\Credit;

class CreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubDB = new \Modules\Club\repo\ClubDB();
        $clubId = $clubDB->idList();
        factory(Credit::class, 2)->create([
            "club_id" => $clubId,
        ]);
    }
}
