<?php

use Illuminate\Database\Seeder;

class WhiteListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->whiteList();
    }

    private function whiteList()
    {
        //create login request
        $loginRequest = new \Illuminate\Http\Request();

        //create auth controller
        $whiteListController = new \App\Http\Controllers\Api\WhiteListController();

        //get users number
        $usersInstance = \App\User::all();
        foreach ($usersInstance as $userInstance) {
            $exist = \App\WhiteList::query()
                ->where("mobile", $userInstance->mobile)
                ->first();
            if ($exist) {
                continue;
            }
            $whiteListData = [
                "mobile" => $userInstance->mobile,
                "user_id" => $userInstance->id,
            ];
            $request = $loginRequest->replace($whiteListData);
            $res = $whiteListController->store($request);
        }
    }
}
