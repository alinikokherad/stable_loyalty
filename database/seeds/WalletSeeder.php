<?php

use App\User;
use Illuminate\Database\Seeder;
use Modules\Attachment\Entities\Attachment;
use Modules\Club\Entities\Club;
use Modules\Event\Entities\Event;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Product;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\Campaign;
use Modules\Wallet\Entities\Credit;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Entities\Transaction;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\Http\Controllers\Api\WalletController;
use Modules\Wallet\Http\Requests\WalletRequest;

class WalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createWallets();

    }

    /**
     * create wallet manually
     */
    public function createWallets()
    {
        //truncate database
        /*        Wallet::truncate();
                Club::truncate();
        //        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                User::truncate();
        //        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
                AccountType::truncate();
                Cost::truncate();
                Event::truncate();
                Product::truncate();
                Category::truncate();
                Attachment::truncate();
                Account::truncate();
                Credit::truncate();
                Order::truncate();
                ProductOrder::truncate();
                Transaction::truncate();
                Campaign::truncate();
                \DB::table("club_accounts")->truncate();
                \DB::table("products_categories")->truncate();
                \DB::table("order_cost")->truncate();*/
        $walletController = new WalletController();
        $walletRequest = new WalletRequest;

        $wallets = ["rials" => "ریالی", "point" => "امتیازی"];
        foreach ($wallets as $key => $wallet) {
            $data = [
                "title" => $wallet,
                "type" => $key,
                "x_change" => 1,
                "club_id" => 1
            ];
            $walletData = $walletRequest->replace($data);
            $test = $walletController->store($walletData);
//            dd($test);
        }

        $wallets = ["rials" => "ریالی", "point" => "امتیازی"];
        foreach ($wallets as $key => $wallet) {
            $data = [
                "title" => $wallet,
                "type" => $key,
                "x_change" => 1,
                "club_id" => 2,
            ];
            $walletData = $walletRequest->replace($data);
            $test = $walletController->store($walletData);
//            dd($test);
        }
    }
}
