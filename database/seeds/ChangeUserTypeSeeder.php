<?php

use Illuminate\Database\Seeder;

class ChangeUserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->changeUserTypeToWaiter();
        $this->changeUserTypeToMerchant();
    }

    private function changeUserTypeToWaiter()
    {
        //get change user type controller
        $userController = new \App\Http\Controllers\Api\UserController();
        $request = new \Illuminate\Http\Request();

        $userDB = new \App\repo\UserDB();
        $merchantDB = new \Modules\Branch\repo\MerchantDB();

        //get random customer user
        $randomUser = $userDB->getUserWithType("customer");
        if (is_null($randomUser)) {
            dd("customer user not exist");
        }

        //get random merchant
        $randomMerchant = $merchantDB->getRandomMerchant();

        $data = [
            "user_id" => $randomUser->id,
            "user_type" => "waiter",
            "merchant_id" => $randomMerchant->id,
        ];
        $request = $request->replace($data);
        $userController->changeUserType($request);
    }

    private function changeUserTypeToMerchant()
    {
        //get change user type controller
        $userController = new \App\Http\Controllers\Api\UserController();
        $request = new \Illuminate\Http\Request();

        $userDB = new \App\repo\UserDB();
        $merchantDB = new \Modules\Branch\repo\MerchantDB();

        //get random customer user
        $randomUser = $userDB->getUserWithType("customer");
        if (is_null($randomUser)) {
            dd("customer user not exist");
        }

        //get random merchant
        $randomMerchant = $merchantDB->getRandomMerchant();

        $data = [
            "user_id" => $randomUser->id,
            "user_type" => "merchant",
            "merchant_id" => $randomMerchant->id,
        ];
        $request = $request->replace($data);
        $userController->changeUserType($request);
    }
}
