<?php

use Illuminate\Database\Seeder;
use Modules\Wallet\Entities\Process;
use Modules\Wallet\Entities\Rule;

class RuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRule();
    }

    private function createRule()
    {
        //get campaign random
        $campaignDB = new \Modules\Wallet\repo\CampaignDB();
        $campaignsInstance = $campaignDB->all();
        foreach ($campaignsInstance as $campaignInstance) {
            $rulesData = [
                "campaign_id" => $campaignInstance->id,
                "params" => '$PARAM$',
                "operators" => '>=',
                "values" => 1,
                "revoked" => false,
            ];

            $rulesInstance = Rule::create($rulesData);

            $processData = [
                "rule_id" => $rulesInstance->id,
                "class" => "First",
                "methods" => 'public function say($params){dd($params["ali"]);}',
                "call" => json_encode(["say" => ["ali"]]),
            ];
            $processInstance = Process::create($processData);
        }
    }
}
