<?php

use Illuminate\Database\Seeder;
use Modules\Branch\Entities\Merchant;
use Modules\Product\Entities\Table;
use Modules\Product\Http\Controllers\TableController;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createTable();
    }

    /**
     * create manual table for club
     */
    private function createTable()
    {
        Table::truncate();
        $tableController = new TableController();
        $tableRequest = new \Illuminate\Http\Request();

        //log in with merchant user to access allowed
        $merchant_user = \App\User::where("type", "merchant_admin")->first();
        Auth::loginUsingId($merchant_user->id);

        $merchantInstance = Merchant::query()
            ->where("name", "like", "%lov%")
            ->whereHas("users", function ($query) use ($merchant_user) {
                $query->where("users.id", $merchant_user->id);
            })->first();

        //create table
        $names = [1 => "میز1", 2 => "میز2", 3 => "میز3", 4 => "میز4", 5 => "میز5", 6 => "میز6", 7 => "میز7", 8 => "میز8", 9 => "میز9", 10 => "میز10"];
        foreach ($names as $name => $title) {
            $tableData = [
                "name" => $name,
                "title" => $title,
                "description" => 2,
                "qr_code" => "sj31l331jfs13",
                "status" => "open",
                "revoked" => false,
                "merchant_id" => $merchantInstance->id,
            ];
            $tableData = $tableRequest->replace($tableData);
            $res = $tableController->store($tableData);
//            dd($res);
        }
        $tableData = [
            "name" => "takeAway",
            "title" => "بیرون بر",
            "description" => 2,
            "qr_code" => "sj31l331jfs13",
            "status" => "multiple",
            "revoked" => false,
            "merchant_id" => $merchantInstance->id,
        ];
        $tableData = $tableRequest->replace($tableData);
        $res = $tableController->store($tableData);
    }
}
