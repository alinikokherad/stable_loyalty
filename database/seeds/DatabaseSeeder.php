<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(ClubSeeder::class);
        $this->call(TierSeeder::class);
        $this->call(WalletSeeder::class);
        $this->call(AccountTypeSeeder::class);
        $this->call(AccountSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MerchantSeeder::class);
        $this->call(TableSeeder::class);
        $this->call(CampaignSeeder::class);
        $this->call(RuleSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ProductSeeder::class);


//        $this->call(OrderSeeder::class);
//        $this->call(ChargeAccountSeeder::class);
//        $this->call(OrderSubmitSeeder::class);
//        $this->call(OrderPaySeeder::class);
//        $this->call(ChargeCreditSeeder::class);
        /*----------------------------- Dorris Seeders --------------------------------*/
        $this->call(CardSeeder::class);
        $this->call(WhiteListSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
