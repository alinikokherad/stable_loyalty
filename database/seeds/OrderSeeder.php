<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOrder();
    }

    private function createOrder()
    {
        //get random customer id
        $userDB = new \App\repo\UserDB();
        $customerInstance = $userDB->getUserWithType("customer");

        //get random merchant
        $merchantDB = new \Modules\Branch\repo\MerchantDB();
        $merchantInstance = $merchantDB->getRandomMerchant();

        //get random rial cost
        $costDB = new \Modules\Product\repo\CostDB();
        $rialsCost = $costDB->getCostWithWalletType("rials");
        $pointCost = $costDB->getCostWithWalletType("point");

        //product order controller
        $productOrderController = new \Modules\Wallet\Http\Controllers\Api\ProductOrderController();

        //define request instance
        $request = new \Illuminate\Http\Request();

        //login with waiter
        $waiter = $userDB->getUserWithType("waiter");

        //get random merchant user instance
        $merchantUserInstance = $userDB->getUserWithType("merchant");
        Auth::loginUsingId($merchantUserInstance->id);

        $data = [
            "costs" => [
                "rials" => [
                    $rialsCost->id => 2,
                ],
                "point" => [
                    $pointCost->id => 3
                ]
            ],
            "customer_id" => $customerInstance->id,
//            "merchant_id" => $merchantInstance->id,
            "author_id" => $waiter->id,
            "description" => "some description",
        ];
        $request = $request->replace($data);
        $productOrderController->store($request);
    }
}
