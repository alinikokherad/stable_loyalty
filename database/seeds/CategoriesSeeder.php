<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCategories();
    }

    private function createCategories()
    {
        \Modules\Product\Entities\Category::truncate();
        $categoryController = new \Modules\Product\Http\Controllers\CategoryController();
        $request = new \Illuminate\Http\Request();

        //get club id
        $merchantInstances = \Modules\Branch\Entities\Merchant::query()
            ->where("name", "loviuna")
            ->orwhere("name", "cafeViuna")
            ->get();

        $catNames = [
            "drink" => "نوشیدنی ها",
//            "دم نوش ها",
//            "عرقیات",
//            "دسرها",
//            "کیک ها",
//            "شیک ها",
//            "آبمیوه ها و کو کتل ها",
//            "ساندویچ ها",
        ];

        foreach ($merchantInstances as $merchantInstance) {
            foreach ($catNames as $catName => $title) {
                $data = [
                    "name" => $catName,
                    "subtitle" => $title,
                    "description" => $title,
                    "parent_id" => 0,
                    "merchant_id" => $merchantInstance->id
                ];
                $request = $request->replace($data);
                $test = $categoryController->store($request);
            }
        }
    }
}
