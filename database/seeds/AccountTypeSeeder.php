<?php

use Illuminate\Database\Seeder;
use Modules\Wallet\Http\Controllers\Api\AccountTypeController;

class AccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAccountType();
    }

    private function createAccountType()
    {
        $walletDb = new \Modules\Wallet\repo\WalletDB();
        $accountTypeController = new AccountTypeController();
        $accountTypeRequest = new \Illuminate\Http\Request();
        $types = ["merchant" => "zero", "customer" => "zero", "cash" => "negative", "unknown" => "negative", "personnel" => "negative", "waiter" => "negative", "bank" => "negative"];
        $walletIds = $walletDb->getAllIds();

        foreach ($types as $type => $balance) {
            foreach ($walletIds as $walletId) {
                $data = [
                    "type" => $type,
                    "wallet_id" => $walletId,
                    "title" => $type,
                    "subtitle" => $type,
                    "description" => $type,
                    "balance_type" => $balance,
                    "min_account_amount" => 0,
                    "max_account_amount" => 0,
                    "min_transaction_amount" => 0,
                    "max_transaction_amount" => 0,
                    "legal" => false,
                    "interest_rate" => 1,
                    "interest_period" => "daily",
                    "revoked" => false,
                ];
                $req = $accountTypeRequest->replace($data);
                $accountTypeController->store($req);
            }
        }
    }
}
