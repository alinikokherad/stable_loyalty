<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_user', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger("user_id")->index();
//            $table->foreign("user_id")->references("id")->on("users");

            $table->unsignedInteger("merchant_id")->index();
//            $table->foreign("merchant_id")->references("id")->on("merchants");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_user');
    }
}
