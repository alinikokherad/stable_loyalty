<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('author_id')->nullable()->default(null);
            $table->string('name')->nullable();
            $table->string('family')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->enum('type', ["personnel", "customer", 'merchant_admin', "waiter", "treasury", "club_admin", 'super_admin', "unknown"]);
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('pinCode')->nullable();
            $table->string('degree')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->date('birthday')->nullable();
            $table->enum('gender', ["male", "female"])->nullable();
            $table->boolean('otp')->default(true);
            $table->unsignedInteger('tier_id')->nullable();
//            $table->string('qr_code')->nullable();
            $table->text('settings')->nullable();
            $table->string('status')->default('active');
            $table->boolean('revoked')->default(false);
//            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
