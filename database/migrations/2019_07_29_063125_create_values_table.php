<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("account_id")->unsigned();
//            $table->foreign('account_id')->references('id')->on("account")->onUpdate("cascade");
            $table->integer("attribute_id")->unsigned();
//            $table->foreign('attribute_id')->references('id')->on("attributes")->onUpdate("cascade");
            $table->string("value");
            $table->string("status")->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->boolean("revoked")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('values');
    }
}
