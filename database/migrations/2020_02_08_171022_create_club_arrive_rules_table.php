<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubArriveRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_arrive_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("club_id")->nullable();
            $table->bigInteger("user_id")->nullable();
            $table->enum("access", ["manager", "subscriber", "viewer"])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_arrive_rules');
    }
}
