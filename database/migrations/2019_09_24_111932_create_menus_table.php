<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');

            //Hierarchy
            $table->unsignedBigInteger('menu_id')
                ->index();

            // children
            $table->unsignedInteger("parent_id")
                ->default(0);

            $table->foreign('menu_id')
                ->on('menus')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            // Content
            $table->string('title');
            $table->string('subtitle')
                ->nullable();

            // Font Icon
            $table->string('font_icon', 64);

            $table->string('link')
                ->nullable();

            $table->unsignedInteger('priority_view')
                ->default(100);


            // Revoked
            $table->boolean('revoked')
                ->default(false);

            // status
            $table->string('status', 20)
                ->default("active");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
