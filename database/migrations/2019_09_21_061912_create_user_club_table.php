<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserClubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_club', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("club_id");
            $table->unsignedInteger("user_id")->nullable();
            $table->enum("access", ["manager", "subscriber", "viewer"])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_club');
    }
}
