<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body, html {
            background: #181E24;
            padding-top: 10px;
            height: 100%;
        }

        .wrapper {
            width: 90%;
            height: 75%;
            display: flex;
            overflow: hidden;
            margin: 0 auto;
            padding: 30px 30px;
            background: #fff;
            border-radius: 4px;
            justify-content: center;
        }

        canvas {
            height: 50%;
        }

        h1 {
            font-family: Roboto;
            color: #fff;
            font-weight: 200;
            text-align: center;
            display: block;
            text-decoration: none;
        }

        .myclass {
            width: 50% !important;
            height: 100% !important;
            display: block;
            margin-right: 20px;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
            background-color: white
        }


        th {
            width: 50% !important;
            text-align: left;
            table-layout: fixed;
            border-collapse: collapse;
            text-decoration: underline;
        }

        td, th {
            text-align: left;
            padding: 12px;
            border: solid 1px #ccc;
            font-size: 12px;
            position: relative;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .my-custom-scrollbar {
            position: relative;
            height: 50%;
            overflow: auto;
        }

        .table-wrapper-scroll-y {
            display: block;
            width: 100%;
            height: 75%;
        }

        .shadow-z-1 {
            -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
            0 1px 2px 0 rgba(0, 0, 0, .24);
            -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
            0 1px 2px 0 rgba(0, 0, 0, .24);
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
            0 1px 2px 0 rgba(0, 0, 0, .24);
        }

        .logout {
            margin-bottom: 10px;
            background-color: Transparent;
            border-color: transparent;
            background-repeat: no-repeat;
            cursor: pointer;
            overflow: hidden;
            outline: none;
            font-size: 25px;
        }

        .logout:hover {
            color: #f2f2f2;
            cursor: pointer;
        }

        .filter {
            width: 90%;
            height: 10%;
            display: flex;
            overflow: hidden;
            margin: 0 auto;
            padding: 30px 30px;
            background: #fff;
            border-radius: 4px;
            justify-content: center;
        }

        .input{
            margin-left: 10px;
            margin-right: 10px;
            height: 32px;
            padding: 12px 20px;
            box-sizing: border-box;

        }

        .button_filter{
            margin-left: 10px;
            margin-right: 10px;
            height: 32px;
            background-color: #6BBE92;
            width: 90px;
            border: 0;
            padding: 10px 0;
            text-align: center;
            color: #fff;
            font-weight: bold;
        }

    </style>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script>
        let colors = {
            'login': '#2e5468',
            'products': '#45c490',
            'product': '#02c443',
            "tickets": '#f25042',
            "ticket": '#f29057',
            'account': '#caf270',
            'accounts': '#e7f237',
        };
        let dates = JSON.parse('{!! $dates !!}');
        let routeCount = JSON.parse('{!! $routeCount !!}');
        let dataSets = [];
        for (let key in routeCount) {
            let value = routeCount[key];
            dataSets.push({
                label: key,
                backgroundColor: colors[key],
                data: value,
            });
        }
    </script>
    <title>Requests</title>
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
</head>
<body>
<h1>Request Info Chart</h1>
<div class="logout" align="center">
    <form class="login-form" action="{{ route('logout') }}" method="get">
        <input type="submit" class="logout" value="logout">
    </form>
</div>
<div class="filter">
    <form class="filter" action="{{ route('request_chart') }}" method="get">
        from: <input class="input" type="date" name="from"><br>
        to  : <input class="input" type="date" name="to"><br>
        <button class="button_filter">Filter</button>
    </form>
</div>
<div class="wrapper">
    <div class="myclass">
        <canvas id="myChart4"></canvas>
    </div>
    <div class="myclass">
        <h4 align="center">Routes</h4>
        <div class="table-wrapper-scroll-y shadow-z-1 my-custom-scrollbar">
            <table class="table table-bordered table-striped mb-0">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Url</th>
                    <th scope="col">Count</th>
                    <th scope="col">date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($routes as $key)
                    @foreach($key as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item['name'] }}</td>
                            <td>{{ $item['url'] }}</td>
                            <td>{{ $item['count'] }}</td>
                            <td>{{ $item['date'] }}</td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="{{asset('js/request_chart.js')}}"></script>

</body>
</html>

