@extends('layouts.default')
@section('title')
    test dashboard
@stop

@section('content')
    <div class="container">
        <div class="row">
            @if(Session::has('successMSG'))
                <p class="alert alert-success">{{ Session::get('successMSG') }}</p>
            @endif
        </div>
    </div>
    @can('admin')
    <a href="{{route('account.logout')}}" class="btn btn-danger">logout</a>
    @endcan
@stop
