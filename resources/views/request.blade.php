<!doctype html>
<html lang="en">

<head>
    <title>Requests Info</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style type="text/css">
        .box {
            width: 600px;
            margin: 0 auto;
            border: 1px solid #ccc;
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>

<div class="jumbotron text-center">
    <h1>Request Info</h1>
    <form class="login-form" action="{{ route('logout') }}" method="get">
        <div class="CTA">
            <input type="submit" value="LogOut">
        </div>
    </form>
</div>


<div class="container">
    <div class="row">
        <table id="data-table"
               class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline >
        ">
            <thead>
            <tr>
                <th>item</th>
                <th>ip</th>
                <th>url</th>
                <th>method</th>
                <th>memory</th>
                <th>Response</th>
                <th>duration</th>
                <th>request time</th>
            </tr>
            <tfoot>
            <tbody>

            @foreach($requestInfo as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->ip }}</td>
                    <td>{{ $item->url }}</td>
                    <td>{{ $item->method }}</td>
                    <td>{{ $item->memory }}</td>
                    <td>{{ $item->response_status }}</td>
                    <td>{{ $item->duration }}</td>
                    <td>{{ $item->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    <div class="text-center">
        {{ $requestInfo->links() }}
    </div>
</div>
</body>
