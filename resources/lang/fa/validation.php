<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'فیلد :attribute باید آرایه باشد.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'فیلد :attribute باید درست یا غلط باشد.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'فیلد :attributeباید تاریخ باشد.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'فیلد :attribute باید از فرمت ایمیل برخودار باشد.',
    'ends_with' => 'The :attribute must end with one of the following: :values',
    'exists' => 'فیلد انتخاب شده :attribute موجود نمی باشد.',
    'file' => 'فیلد :attribute باید از نوع فایل باشد.',
    'filled' => 'فیلد :attribute باید مقدار داشته باشد.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'فیلد :attribute باید از نوع عکس باشد.',
    'in' => 'فیلد وارد شده :attribute اشتباه است.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'فیلد :attribute باید از نوع عدد باشد.',
    'ip' => 'فیلد :attribute باید آی پی معتبر باشد.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'فیلد :attribute باید از فرمت جی سان پیروی کند.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'فیلد :attribute باید کمتر از :max باشد.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'فیلد :attribute باید کمتر از  :max کاراکتر باشد.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'فیلد :attribute باید بیشتر از :min باشد.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'فیلد :attribute باید بیشتر از :min کاراکتر باشد.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'فیلد :attribute باید عددی باشد.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'درج :attribute الزامی می باشد',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'فیلد :attribute باید از نوع کاراکتر باشد.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'فیلد :attribute تکراری می باشد.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'فیلد :attribute باید فرمت آدرس سایت داشته باشد.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    */

    'attributes' => [
        "name" => "نام",
        "title" => "عنوان",
        "description" => "توضیحات",
        "img" => "عکس",
        "category_id" => "آی دی فهرست",
        "costs" => "هزینه",
        "status" => "وضعیت",
        "type" => "نوع",
        "from_user_id" => "از کاربر",
        "code" => "کد",
        "exchange_rate" => "نرخ تبدیل",
        "club_mode" => "مدل باشگاه",
        "point_to_card" => "انتقال اعتبار به کارت",
        "good_lock_delay" => "زمان تاخیر اعطای امتیاز خوش حسابی",
        "good_lock_point" => "امتیاز خوش حسابی",
        "good_lock_date" => "تاریخ امتیاز خوش حسابی",
        "delay_penalty" => "جریمه بدحسابی",
        "give_point_at" => "اعطای امتیاز در تاریخ",
        "point_expired_at" => "تاریخ انقضای امتیاز",
        "can_edit_point" => "قابلیت تغییر امتیاز",
        "can_refund" => "قابلیت استرداد",
        "can_refund_until" => "قابلیت استراد تا زمان",
        "can_cash_out_by_admin" => "نقد کردن امتیاز توسط مدیر",
        "can_cash_out" => "قابلیت نقد کردن امتیاز",
        "show_point" => "قابلیت نمایش امتیاز",
        "generate_card_immediately" => "اعطای سریع کارت",
        "card_expired_at" => "تاریخ انقضای کارت",
        "gift_card_expired_at" => "تاریخ انقضای کارت هدیه",
        "show_revocation_card_for_admin" => "نمایش کارتهای ابطال شده به مدیر",
        "minimum_shop_to_use_point" => "حداقل امتیاز برای استفاده از امتیاز",
        "free_shiping" => "ارسال رایگان محصول",
        "minimum_wage" => "حداقل کارمزد",
        "maximum_wage" => "حداکثر کارمزد",
        "card_type" => "نوع کارت",
        "checkout_time" => "تاریخ وارسی",
        "wallet_id" => "آی دی کیف پول",
        "email" => "ایمیل",
        "password" => "گذرواژه",
        "mobile" => "موبایل",
        "birthday" => "تاریخ تولد",
        "gender" => "جنسیت",
        "club_id" => "آی دی باشگاه",
        "parent_id" => "آی دی پدر",
        "register_type" => "ثبت از طریق",
        "account_type" => "نوع حساب",
        "revoked" => "حذف شده",
        "user_id" => "آی دی کاربر",
        "merchant_id" => "آی دی شعبه",
        "qr_code" => "کد کیو آر",
        "customer_id" => "آی دی مشتری",
        "family" => "نام خانوادگی",
        "card_number" => "شماره کارت",
        "expired_at" => "تاریخ انقضا",
        "sms_code" => "کد پیامک",
    ],

];
