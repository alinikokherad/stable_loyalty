<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'رمز عبود می بایست حداقل ۸ کاراکتر باشد',
    'reset' => 'رمز عبور با موفقیت تغییر یافت',
    'sent' => 'لینک تغییر رمز عبور برای ایمیل شما ارسال شد',
    'token' => 'توکن ارسالی اشتباه می باشد',
    'user' => "متاسفانه کاربر مورد نظر یافت نشد",

];
