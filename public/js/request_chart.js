var ctx = document.getElementById("myChart4").getContext('2d');
var w = window.innerWidth;
// var h = window.innerHeight - 189;
// jQuery('.wrapper').css('height',h+'px');
// jQuery('document').bind('resize',function () {
// });
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: dates,
        datasets: dataSets
    },
    options: {
        tooltips: {
            displayColors: true,
            callbacks:{
                mode: 'x',
            },
        },
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: {
                    display: false,
                }
            }],
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                },
                type: 'linear',
            }]
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: { position: 'top' },
    }
});
