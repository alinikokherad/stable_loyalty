<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('custom:fresh', function () {
    Schema::disableForeignKeyConstraints();
    $this->call("migrate:fresh");
    Schema::enableForeignKeyConstraints();
});

Artisan::command('custom:reset', function () {
    Schema::disableForeignKeyConstraints();
    $this->call("migrate:reset");
    Schema::enableForeignKeyConstraints();
});
