<?php


//for localization
$locale = request()->server("HTTP_ACCEPT_LANGUAGE") ?? "en";
App::setLocale($locale);

//Route::get('emails', 'MailController@sendMails');
//Route::get('fcm', 'FcmController@sendMessage');, "middleware" => "WebPrevent"

Route::group([
    'namespace' => 'Api'
], function () {
    Route::group([
//        'middleware' => 'auth:api',
        'prefix' => 'user',
    ], function () {
        Route::post('change_type', 'UserController@changeUserType');
        Route::resource('bootstrap', 'BootstrapController')->middleware('auth:api');
        Route::resource('white_list', 'WhiteListController')->middleware('auth:api');
//        Route::delete('white_list/{mobile}', 'WhiteListController@destroy')->middleware('auth:api');
        Route::get('get_waiter', 'UserController@waiterListByMerchantId')->middleware('auth:api');
        Route::post('login', 'AuthController@login');
        Route::post('login_app', 'AuthController@loginApp');
        Route::post('mobileLogin', 'AuthController@mobileLogin');
        Route::post('register', 'AuthController@register');
        Route::post('reg', 'AuthController@checkUser');
        Route::post('register/request', 'AuthController@registerRequest');
        Route::post('mobile_confirm', 'AuthController@mobileConfirm');
        Route::post('logout', 'AuthController@logout')->middleware('auth:api');
        Route::get('show/{mobile}', 'AuthController@getUser');
        Route::post('complete-profile', 'AuthController@completeProfile')->middleware('auth:api');
        Route::post('membership', 'AuthController@membership')->middleware('auth:api');
        Route::post('change-password', 'AuthController@changePassword')->middleware('auth:api');
        Route::post('forgot-password', 'AuthController@forgotPassword');

        Route::get('list', 'UserController@list')->middleware('auth:api');
        Route::get('tntSearch', 'UserController@tntSearch')->middleware('auth:api');
        Route::get('/profile', 'UserController@profile')->middleware('auth:api');
//            ->middleware(['auth:api', 'scope:customer,merchant,personnel']);
        Route::post('/renew-token', 'AuthController@renewToken')->middleware('auth:api');
        Route::post('/renew-qr', 'UserController@generateQr')->middleware('auth:api');
        Route::post('/check-qr', 'UserController@checkQr')->middleware('auth:api');
        Route::get('/unknown', 'UserController@unknown')->middleware('auth:api');
        Route::post('/check-mobile', 'UserController@checkMobile')->middleware('auth:api');
        Route::post('/auth-mobile', 'UserController@authMobile')->middleware('auth:api');
        Route::post('/send-password', 'UserController@sendPassword')->middleware('auth:api');

        /*--------------------------- dorris api -------------------------*/
        Route::post('/assign-card', 'UserController@assignCard')->middleware('auth:api');
        Route::post('/info', 'UserController@info')->middleware('auth:api');
    });
});

//route for menu
Route::resource('menus', 'MenuController');
Route::get('bootstrap', 'BootstrapController@get');


/*---------------------------------------- dorris route -------------------------------------------*/

Route::group([
    'namespace' => 'Api\Version1',
    'prefix' => 'version1',
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::resource("card", "CardController");
        Route::post("auth", "DorrisAuthController@auth");
        Route::post("card/resend-pwd", "DorrisAuthController@resendPassword");
        Route::post("user", "DorrisAuthController@manualRegister")->name("user_add");
        Route::post("user/{id}", "DorrisAuthController@edit")->name("user_edit");
        Route::post("excel/users", "ExcelController@import");
    });
    Route::post("panel/login", "DorrisAuthController@panelLogin");
    Route::post("panel/confirm", "DorrisAuthController@confirmSmsCode");

});

/*------------------ for test ----------------------*/
//this route for test
Route::post('test', function () {
    include("../app/forTest.php");

});
