<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {
    Route::get('/dashboard', 'homeController@dashboard')->name('dashboard')->middleware('auth');
    Route::get('/home', 'homeController@home')->name('home');
    Route::get('/', 'homeController@home');
});

Route::get('/requests','RequestInfoController@index')->name('requests')->middleware('WebAuth');
Route::get('/requests_chart','RequestInfoController@chart')->name('request_chart')->middleware('WebAuth');
Route::post('/login','RequestInfoController@login')->name('login');
Route::get('/logout','RequestInfoController@login')->name('logout');
