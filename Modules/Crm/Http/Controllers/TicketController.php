<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Crm\Entities\Ticket;
use Modules\Crm\Entities\TicketMessage;

class TicketController extends Controller
{
    public function createTicket(Request $r)
    {
        $ticket = Ticket::create([
            'user_id' => $r->user_id,
            'department_id' => $r->department_id,
            'status_id' => 1,
            'priority_id' => $r->priority_id,
            'title' => $r->title,
        ]);
        $ticket_id = $ticket->id;
        $uid = uidGenerate($ticket_id);
        $message = TicketMessage::create([
            'uid' => $uid,
            'sender' => $r->sender,
            'text' => $r->text,
        ]);
        if (Ticket::findOrFail($ticket->id)) {
            Ticket::where('id', $ticket_id)->update([
                'uid' => $uid
            ]);
            $ticket = Ticket::where('id', $ticket_id)->get();
            $message = TicketMessage::where('uid', $uid)->get();
            return Response()->json([
                'message' => __('messages.create_ticket'),
                'کد پیگیری' => $uid,
            ], 200);
        } else {
            return Response()->json([
                'data' => __('messages.try_again')
            ], 400);
        }
    }

    public function ticketMessage(Request $r)
    {
        $status = null;
        if ($r->sender == 'user') {
            $status = 2;
        } elseif ($r->sender == 'admin') {
            $status = 3;
        }

        $tId = $this->uidToId($r->uid);
        if ($this->ticketExist($r->uid)) {
            $message = TicketMessage::create([
                'uid' => $r->uid,
                'sender' => $r->sender,
                'text' => $r->text,
            ]);
            Ticket::findOrFail($tId)->update([
                'status_id' => $status
            ]);
            return Response()->json([
                'message' => __('messages.ticket_message'),
                Ticket::where('id', $this->uidToId($r->uid))->get(),
                $message
            ], 200);
        } else {
            return Response()->json([
                'data' => __('messages.ticket_not_exist')
            ], 400);
        }

    }

    public function ticketShow(Request $r)
    {
        if ($this->ticketExist($r->uid)) {
            $ticket = Ticket::where('uid', $r->uid)->get();
            $message = TicketMessage::where('uid', $r->uid)->get();
            return Response()->json([
                "اطلاعات تیکت" => $ticket,
                "پیام ها" => $message
            ], 200);
        } else {
            return Response()->json([
                'message' => __('messages.ticket_not_exist'),
            ], 400);
        }
    }

    public function ticketsList(Request $r)
    {
        return Response()->json([
            'data' => Ticket::where([[$r->filters[0][0], $r->filters[0][1], $r->filters[0][2]], ['revoked', 0]])
            ->paginate($r->input("page_limit"), ["*"], 'page', $r->input("page_number"))
        ], 200);
    }

    public function ticketClose(Request $r)
    {
        $tId = $this->uidToId($r->uid);
        if ($this->ticketExist($r->uid)) {
            Ticket::findOrFail($tId)
                ->update([
                    'active' => 1,
                    'status_id' => 4
                ]);
            return Response()->json([
                'message' => __('messages.ticket_closed'),
            ],200);
        } else {
            return Response()->json([
                'message' => __('messages.ticket_not_exist')
            ]);
        }
    }

    public function destroy(Request $r)
    {
        $tId = $this->uidToId($r->uid);
        if ($this->ticketExist($r->uid)) {
            Ticket::findOrFail($tId)
                ->update([
                    'revoked' => 1,
                    'status_id' => null
                ]);
            return Response()->json([
                'message' => __('messages.ticket_deleted')
            ],200);
        } else {
            return Response()->json([
                'message' => __('messages.ticket_not_exist')
            ],400);
        }
    }

    public function ticketStatus(Request $r)
    {
        $tId = $this->uidToId($r->uid);
        if ($this->ticketExist($r->uid)) {
            Ticket::findOrFail($tId)
                ->update([
                    'status_id' => $r->status_id
                ]);
            return Response()->json([
                'message' => __('messages.ticket_status_changed')
            ],200);
        } else {
            return Response()->json([
                'message' => __('messages.ticket_not_exist')
            ],400);
        }
    }

    private function uidToId($uid)
    {

        return Response()->json([
            'data' => Ticket::select('id')->where('uid', $uid)->get()[0]["id"]
        ],200);
    }

    private function ticketExist($uid): bool
    {
        if (isset($uid)) {
            $var = Ticket::where([['uid', $uid], ['revoked', '=', '0']])->first();
            if ($var instanceof Ticket) {
                return true;
            }
        }
        return false;
    }

}
