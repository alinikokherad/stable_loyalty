<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'ticket'], function () {
    Route::post('/create', 'TicketController@createTicket');
    Route::post('/message', 'TicketController@ticketMessage');
    Route::post('/show','TicketController@ticketShow');
    Route::get('/list','TicketController@ticketsList');
    Route::post('/close','TicketController@ticketClose');
    Route::post('/set-status','TicketController@ticketStatus');
    Route::delete('/destroy','TicketController@destroy');
});
Route::middleware('auth:api')->get('/crm', function (Request $request) {
    return $request->user();
});
Route::get('/search/test','CrmController@test');
