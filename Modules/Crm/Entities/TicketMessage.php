<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket_message whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TicketMessage extends Model
{
    public $validate = ['uid', 'text'];
    protected $fillable = ['uid', 'text','sender'];

    public function ticket()
    {
        return $this->belongsTo(Ticket1::class);
    }
}
