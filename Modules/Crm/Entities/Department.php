<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Department whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Department extends Model
{
    public $validate = [];
    protected $fillable = ['name', 'active', 'revoked'];

    public function departmentUser()
    {
        return $this->hasMany(DepartmentUser::class);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket1::class);
    }
}
