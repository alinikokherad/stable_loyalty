<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\DepartmentUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Modules\Crm\Entities\Department $department
 */
class DepartmentUser extends Model
{
    public $vlidate = [];
    protected $fillable = ['department_id', 'user_id', 'active'];

    public function department()
    {
        return $this->hasOne(Department::class, 'department');
    }
}
