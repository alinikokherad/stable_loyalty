<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * App\Ticket
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\ticket whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Modules\Crm\Entities\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Crm\Entities\TicketMessage[] $messages
 * @property-read int|null $messages_count
 * @property-read \Modules\Crm\Entities\Priority $priority
 * @property-read \Modules\Crm\Entities\Status $status
 */
class Ticket extends Model
{
    use Searchable;
    public $asYouType = true;

    public $validate = [];
    protected $fillable = [
        'user_id',
        'uid',
        'department_id',
        'status_id',
        'priority_id',
        'title',
        'archive',
        'active',
        'revoked'
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        ['title'];
        return $array;
    }


    public function messages()
    {
        return $this->hasMany(TicketMessage::class);
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'department_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'status_id');
    }

    public function priority()
    {
        return $this->hasOne(Priority::class, 'priority_id');
    }
}
