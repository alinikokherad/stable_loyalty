<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Status whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Modules\Crm\Entities\Ticket $ticket
 */
class Status extends Model
{
    public $validate = [];
    protected $fillable = ['name', 'color'];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

}
