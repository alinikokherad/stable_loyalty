<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Crm\Entities\Priority whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Priority extends Model
{
    public $validate = [];
    protected $fillable = ['name', 'color'];

    public function ticket()
    {
        return $this->belongsTo(Ticket1::class);
    }
}
