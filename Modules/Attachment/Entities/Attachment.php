<?php

namespace Modules\Attachment\Entities;


use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Attachment\Entities\Attachment
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $attachable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attachment\Entities\Attachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attachment\Entities\Attachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attachment\Entities\Attachment query()
 * @mixin \Eloquent
 */
class Attachment extends Model
{
    public $validate = [

    ];
    protected $table = "files";
    protected $guarded = [];

    public function attachable()
    {
        return $this->morphTo();
    }
}
