<?php

namespace Modules\Tier\Entities;

/**
 * Modules\Tier\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Tier\Entities\Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Tier\Entities\Group newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Tier\Entities\Group query()
 */

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $validate = [];
    protected $fillable = [];
}
