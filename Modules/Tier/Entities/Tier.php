<?php

namespace Modules\Tier\Entities;

/**
 * Modules\Tier\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Tier\Entities\Tier newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Tier\Entities\Tier newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Tier\Entities\Tier query()
 */

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tier extends Model
{
    public $validate = [];
    protected $guarded = [];

    public function user()
    {
        return $this->hasMany(User::class, "tier_id");
    }
}
