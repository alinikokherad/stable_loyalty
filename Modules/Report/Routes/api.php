<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["prefix" => "reports", "namespace" => "Api"], function () {
    Route::get('bought_product', "ReportController@getBoughtProduct");
    Route::get('/transactions', "ReportController@transactions");
});

Route::group(["prefix" => "charts", "namespace" => "Api"], function () {
    Route::get('transactions', "ChartController@transactionChart");
    Route::get('products', "ChartController@productChart");
});
