<?php

namespace Modules\Report\Http\Controllers\Api;

use App\User;
use Modules\Product\repo\ProductDB;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\repo\OrderDB;
use Modules\Wallet\repo\ProductOrderDB;
use Modules\Wallet\repo\WalletDB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //
        dd(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
        dd(2);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBoughtProduct()
    {
        $productOrderDB = new ProductOrderDB();
        $productDB = new ProductDB();
        list($page, $limit, $wallet, $product_id, $fromAmount, $toAmount, $from, $to, $day, $week, $month, $user_id, $merchant_id, $paid_at) = $this->getRequestParams();
        $productsInstance = $productDB->listProductsWithCondition($wallet, $product_id, $fromAmount, $toAmount, $from, $to, $day, $week, $month, $user_id, $merchant_id, $paid_at);

        if (empty($productsInstance)) {
            return response()
                ->json([
                    "message" => "some things went wrong",
                    "error" => "there is no order",
                ], 400);
        }

        $response = $productsInstance->paginate($limit, $page);
        return \response()
            ->json([
                "message" => "all bought product",
                "data" => $response
            ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function transactions()
    {
        $page = \request()->has("page") ? \request("page") : 1;
        $limit = \request()->has("limit") ? \request("limit") : 10;
        $from = \request()->has("from") ? \request("from") : null;
        $to = \request()->has("to") ? \request("to") : null;
        $day = \request()->has("day") ? \request("day") : null;
        $week = \request()->has("week") ? \request("week") : null;
        $month = \request()->has("month") ? \request("month") : null;
        $wallet_type = \request()->has("wallet") ? \request("wallet") : null;
        $customer_id = \Auth::id();
        $fromAmount = \request()->has("from_amount") ? \request("from_amount") : null;
        $toAmount = \request()->has("to_amount") ? \request("to_amount") : null;
        $products = \request()->has("products") ? \request("products") : null;
        $paid_at = \request()->has("paid") ? (bool)\request("paid") : null;
        $merchant_id = \request()->has("merchant_id") ? \request("merchant_id") : null;
        $table_id = \request()->has("table_id") ? \request("table_id") : null;
        $mobile = \request()->has("mobile") ? \request("mobile") : null;
        $fromAccount = \request()->has("fromAccount") ? \request("fromAccount") : null;
        $toAccount = \request()->has("toAccount") ? \request("toAccount") : null;

        $orderDB = new OrderDB();

        $orderInstance = $orderDB->list(
            $from,
            $to,
            $day,
            $week,
            $month,
            $wallet_type,
            $customer_id,
            $fromAmount,
            $toAmount,
            $products,
            $paid_at,
            $merchant_id,
            $table_id,
            $mobile,
            $fromAccount,
            $toAccount
        );

        return Response()->json([
            "message" => 'Report of Transaction',
//            "data" => collect($orderInstance)->paginate($limit, $page)
            "data" => $orderInstance->paginate($limit, $page)
        ], 200);
        /*    $treasuryAccountId = null;

            if (!is_null($wallet)) {
                $treasuryAccountId = AccountType::select("id")->where([["title", "=", $wallet], ["type", "=", "treasury"]])->get()->toArray()[0]["id"];
    //            $walletDB = new WalletDB();
    //            $treasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletType($wallet);
            }

            // get user accounts
            if (!is_null($user_id)) {
                $user_id = $this->userIdToAcc($user_id);
            }

            // get merchant accounts
            if (!is_null($merchant_id)) {
                $merchant_id = $this->userIdToAcc($merchant_id);
            }

            $orderDB = new OrderDB();
            $orderInstance = $orderDB->list(
                $from, $to, $day, $week, $month,
                $treasuryAccountId,
                $user_id, $fromAmount, $toAmount,
                $products, $paid, $merchant_id
            );

            $pointAmounts = null;
            $rialsAmounts = null;
            foreach ($orderInstance as $key) {
                if ($key->treasury_account_id == 2) $pointAmounts = $pointAmounts + $key->amount;
                elseif ($key->treasury_account_id == 1) $rialsAmounts = $rialsAmounts + $key->amount;
            }

            return Response()->json([
                "message" => 'Report of Transaction',
                "point_sum" => $pointAmounts,
                "rials_sum" => $rialsAmounts,
                "data" => collect($orderInstance)->paginate($limit, $page)
            ], 200);*/
    }

    private function accToUser($accId)
    {
        $fromUserInstance = Account::where("id", $accId)->with("user")->first()->user;
        return $fromUserInstance;
    }

    private function userIdToAcc($user_id)
    {
        $result = [];
        $instance = User::find($user_id)->with("accounts")->first()->accounts->toArray();
        foreach ($instance as $key) {
            array_push($result, $key["id"]);
        }
        return $result;
    }

    /**
     * @return array
     */
    private function getRequestParams(): array
    {
        $page = \request()->has("page") ? \request("page") : 1;
        $limit = \request()->has("limit") ? \request("limit") : 10;
        $wallet = \request()->has("wallet") ? \request("wallet") : null;
        $product_id = \request()->has("product_id") ? \request("product_id") : null;
        $from = \request()->has("from") ? \request("from") : null;
        $to = \request()->has("to") ? \request("to") : null;
        $day = \request()->has("day") ? \request("day") : null;
        $week = \request()->has("week") ? \request("week") : null;
        $month = \request()->has("month") ? \request("month") : null;
        $fromAmount = \request()->has("fromAmount") ? \request("fromAmount") : null;
        $toAmount = \request()->has("toAmount") ? \request("toAmount") : null;
        $user_id = \request()->has("user_id") ? \request("user_id") : null;
        $merchant_id = \request()->has("merchant_id") ? \request("merchant_id") : null;
        $paid_at = \request()->has("paid_at") ? \request("paid_at") : null;
        return array($page, $limit, $wallet, $product_id, $fromAmount, $toAmount, $from, $to, $day, $week, $month, $user_id, $merchant_id, $paid_at);
    }

}
