<?php

namespace Modules\Report\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Crud\Http\Controllers\CrudController;
use Modules\Product\Entities\Product;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\repo\OrderDB;

class ChartController extends CrudController
{

    public function transactionChart()
    {
        $from = \request()->has("from") ? \request("from") : null;
        $to = \request()->has("to") ? \request("to") : null;
        $chartType = \request()->has("chart_type") ? \request("chart_type") : null;
        return Response()->json([
            "message" => __('messages.transaction_chart'),
            "data" => $this->doTransactionChart($from, $to)
        ], 200);
    }

    public function productChart()
    {
        $from = \request()->has("from") ? \request("from") : null;
        $to = \request()->has("to") ? \request("to") : null;
        $product = \request()->has("product_id") ? \request("product_id") : null;
        $chartType = \request()->has("chart_type") ? \request("chart_type") : null;

        $response = $this->doProductChart($from, $to, json_decode($product, true), $chartType);
        if (empty($response[1])) {
            return Response()->json([
                'message' => __('messages.no_order_found')
            ], 400);
        }
        if (!is_null($product)) {
            return Response()->json([
                "message" => __("messages.product_chart"),
                "product_id" => $response[0],
                "data" => $response[1]
            ], 200);
        } else {
            return Response()->json([
                "message" => __("messages.product_chart"),
                "data" => $response[1]
            ], 200);
        }
    }

    private function doProductChart($from = null, $to = null, array $products = null, $chart_type = null)
    {
        /*
         * TODO fix this
         */
//        $rialsAccId = $this->walletToTreasuryAcc("rials")[0]["id"];
//        $pointAccId = $this->walletToTreasuryAcc("point")[0]["id"];

        $rialsAccId = 1;
        $pointAccId = 2;

        // create time where condition
        $timeWhere = [];
        if (isset($from, $to)) {
            array_push($timeWhere, ["created_at", ">=", $from], ["created_at", "<=", $to]);
        }

        $productOrderInstance = Order::where($timeWhere)->with('ProductOrder.costs.product');
        if (!is_null($products)) {
            $productOrderInstance = $productOrderInstance->whereHas('ProductOrder.costs.product', function ($q) use ($products) {
                $q->whereIn("id", $products);
            })->get()->toArray();
        } else {
            $productOrderInstance = $productOrderInstance->get()->toArray();
        }

        switch ($chart_type) {
            case null:
            {
                $result = [];
                foreach ($productOrderInstance as $key) {
                    if ($key["treasury_account_id"] == $pointAccId) {
                        $wallet = "point";
                    } elseif ($key["treasury_account_id"] == $rialsAccId) {
                        $wallet = "rials";
                    }
                    $date = date("d", strtotime($key["created_at"]));
                    $quantity = $key["product_order"]["costs"][0]["pivot"]["quantity"];
                    $key = $date . '-' . $wallet;
                    if (!isset($result[$key])) $result[$key] = [
                        "X" => (int)$date,
                        "Y" => 0,
                        "wallet" => $wallet
                    ];
                    $result[$key]['Y'] += $quantity;
                }
                $output = [];
                foreach ($result as $item) {
                    if (!isset($output[$item['wallet']])) $output[$item['wallet']] = [];
                    array_push($output[$item["wallet"]], ["X" => $item["X"], "Y" => $item["Y"]]);
                }
                return array($products, $output);
                break;
            }
            case "circle":
            {
                $result = [];
                foreach ($products as $key) {
                    $productInstance = Product::where("id", $key)->get()->toArray()[0];
                    foreach ($productOrderInstance as $key) {
                        $quantity = $key["product_order"]["costs"][0]["pivot"]["quantity"];
                        $key = $productInstance["name"];
                        if (!isset($result[$key])) $result[$key] = [
                            "value" => 0,
                            "product_name" => $productInstance["name"]
                        ];
                        $result[$key]['value'] += $quantity;
                    }
                }
                $output = [];
                foreach ($result as $item) {
                    if (!isset($output[$item['product_name']])) $output[$item['product_name']] = [];
                    array_push($output[$item["product_name"]], ["value" => $item["value"]]);
                }
                return array($products, $output);
                break;
            }
        }

    }

    private function doTransactionChart($from = null, $to = null)
    {
        /*
         * TODO fix this bullshit
         */
//        $rialsAccId = $this->walletToTreasuryAcc("rials")[0]["id"];
//        $pointAccId = $this->walletToTreasuryAcc("point")[0]["id"];

        $rialsAccId = 1;
        $pointAccId = 2;

        $output = [];
        if (!is_null($from) && !is_null($to)) {
            $orderInstance = Order::where([["created_at", ">=", $from], ["created_at", "<=", $to]])->get();
        } else {
            $orderInstance = Order::all();
        }
        foreach ($orderInstance as $item) {
            $date = date("d", strtotime($item->created_at));
            if ($item->treasury_account_id == $pointAccId) {
                $wallet = "point";
            } elseif ($item->treasury_account_id == $rialsAccId) {
                $wallet = "rials";
            }
            $key = $date . '-' . $wallet;
            if (!isset($output[$key])) $output[$key] = [
                "X" => (int)$date,
                "Y" => 0,
                "wallet" => $wallet
            ];
            $output[$key]['Y'] += $item->amount;
        }
        $result = [];
        foreach ($output as $item) {
            if (!isset($result[$item['wallet']])) $result[$item['wallet']] = [];
            array_push($result[$item["wallet"]], ["X" => $item["X"], "Y" => $item["Y"]]);
        }
        return $result;
    }

    private function walletToTreasuryAcc($wallet)
    {
        return Account::whereHas('accountType', function ($q) {
            $q->where("type", "=", "treasury");
        })->whereHas('accountType.wallet', function ($q) use ($wallet) {
            $q->where("title", "=", $wallet);
        })->get()->toArray();
    }
}
