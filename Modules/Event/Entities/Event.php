<?php

namespace Modules\Event\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Event\Entities\Event whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    protected $guarded = [];
}
