<?php


namespace Modules\Event\repo;


use Modules\Event\Entities\Event;
use Modules\Event\Entities\eventtest2;

class EventDB
{
    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|Event
     */
    public function create($data)
    {
        $instance=Event::create($data);
        if ($instance instanceof Event) {
            return $instance;
        }
        return false;
    }
}
