<?php

namespace Modules\Event\Events;

use Illuminate\Queue\SerializesModels;

class AfterTransactionCreatedEventOld
{
    use SerializesModels;
    public $eventInstance;
    public $orderInstance;

    /**
     * CreateEvent constructor.
     * @param $eventInstance
     * @param $orderInstance
     */
    public function __construct($eventInstance, $orderInstance)
    {
        $this->eventInstance = $eventInstance;
        $this->orderInstance = $orderInstance;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
