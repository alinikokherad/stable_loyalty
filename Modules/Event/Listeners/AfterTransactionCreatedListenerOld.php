<?php

namespace Modules\Event\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;
use Modules\Club\Entities\Club;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\Events\GetCampaignEvent;

class AfterTransactionCreatedListenerOld
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @throws \Exception
     */
    public function handle($event)
    {
        //get club repository
        $clubDB = new ClubDB();
        $eventInstance = $event->eventInstance;
        $orderInstance = $event->orderInstance;

        //get clubs id
        $customersClubsInstance = $clubDB->findUsersClubWithAccountId($eventInstance->from_account_id);
        $merchantsClubsInstance = $clubDB->findMerchantsClubWithAccountId($eventInstance->to_account_id);
//        $customerClubs=$customersClubsInstance->pluck("id")->toArray();
        $merchantClubsIds = $merchantsClubsInstance->pluck("id")->toArray();

        //when customer hasn't a club
        if ($customersClubsInstance == false) {
            throw new \Exception(__("messages.user_does_not_has_a_club"), 400);
        }

        foreach ($customersClubsInstance as $customersClubInstance) {

            if (in_array($customersClubInstance->id, $merchantClubsIds)) {
                //this club is same into two order accounts
                //call get clubs campaign event
                event(new GetCampaignEvent($eventInstance, $orderInstance, $customersClubInstance));
            }
        }


        //iteration into users clubs
        /*        foreach ($fromAccountClubInstance as $item) {
                    $toAccountInstanceClubs = $clubDB->whereInClub($eventInstance->to_account_id, $item->id);
                    if (!$toAccountInstanceClubs) {
                        throw new \Exception("to_account club is not in range from_account club");
                    }

                    //this club is same into two order accounts
                    //call get clubs campaign event
                    event(new GetCampaignEvent($eventInstance, $orderInstance, $item));

                }*/

    }
}
