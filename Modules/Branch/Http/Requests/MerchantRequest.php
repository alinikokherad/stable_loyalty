<?php

namespace Modules\Branch\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MerchantRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "string|max:50|min:3|required",
            "title" => "string|max:100|nullable",
            "description" => "string|nullable|max:300",
            "revoked" => "boolean|nullable",
            "status" => "string|nullable|max:30",
            "parent_id" => "numeric|nullable",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
