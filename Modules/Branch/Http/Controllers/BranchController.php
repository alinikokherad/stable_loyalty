<?php

namespace Modules\Branch\Http\Controllers;

use App\Exceptions\MerchantException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Branch\Events\AfterMerchantCreateEvent;
use Modules\Branch\Events\BeforeMerchantCreateEvent;
use Modules\Branch\Http\Requests\MerchantRequest;
use Modules\Branch\repo\MerchantDB;

class BranchController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        dd(11);
    }

    /**
     * @param MerchantRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws MerchantException
     */
    public function store(Request $request)
    {
        dd(1);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('branch::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('branch::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
