<?php

namespace Modules\Branch\Http\Controllers;

use App\Address;
use App\Exceptions\MerchantException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Attachment\Entities\Attachment;
use Modules\Branch\Entities\Merchant;
use Modules\Branch\Events\AfterMerchantCreateEvent;
use Modules\Branch\repo\MerchantDB;
use Modules\Branch\Transformers\MerchantResource;

class MerchantController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (\Gate::allows('merchant') || \Gate::allows("club_admin")) {
            $limit = \request()->has("limit") ? \request("limit") : 1;
            $page = \request()->has("page") ? \request("page") : 10;
            $parent = \request()->has("parent") ? \request("parent") : null;
            $children = \request()->has("children") ? \request("children") : 10;
            $merchantDB = new MerchantDB();

            if (\Auth::user()->type == "merchant") {
                $user_merchant_id = \Auth::id();
                $merchantsInstance = $merchantDB->getMerchantWithUserMerchant($user_merchant_id);
            } else {
                $merchantsInstance = Merchant::with(["addresses", "children", "parent"])->get();
            }

            if (!empty($parent)) {
                MerchantResource::$with_parent = true;
            }
            if (!empty($children)) {
                MerchantResource::$with_children = true;
            }
            $merchantsInstance = MerchantResource::collection($merchantsInstance);

            return Response()->json([
                "message" => __("messages.information"),
                'data' => $merchantsInstance->paginate($limit, $page),
            ], 200);
        } else {
            return Response()->json([
                "message" => __("messages.403"),
                'error' => __("messages.access_denied"),
            ], 403);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws MerchantException
     */
    public function store(Request $request)
    {
        $merchantDB = new MerchantDB();
        $merchantAdminInstance = \Auth::user();

        if (\Gate::allows('club_admin')) {
            try {
                \DB::beginTransaction();
                //fire before merchant create event
//            event(new BeforeMerchantCreateEvent());

                $merchantInstance = $merchantDB->insert($request->all());

                //create address
                $addressData = [
                    "address" => $request->address,
                    "phone" => $request->phone
                ];
                $addressInstance = new Address($addressData);
                $addressInstance->save();

                //save address on merchant
                $merchantInstance
                    ->addresses()
                    ->save($addressInstance);

                //sync club with merchant
                if (!is_array($request->clubs_id)) {
                    $clubs_id = (array)$request->clubs_id;
                } else {
                    $clubs_id = $request->clubs_id;
                }
                $merchantInstance->clubs()->sync($clubs_id);

                //when picture set for merchant
                if (!empty($request->file("img"))) {
                    $imageName = $request->file("img")->getClientOriginalName() . now()->format("H-i-s") . "." . $request->file("img")->getClientOriginalExtension();
                    $src = $request->file("img")->move(public_path("uploads/merchants"), $imageName);
                    $attach = new Attachment([
                        "type" => "avatar",
                        "src" => "uploads/merchants" . $imageName,
                        "status" => "active",
                    ]);
                    $merchantInstance
                        ->image()
                        ->save($attach);
                }

                //fire after merchant create event
                event(new AfterMerchantCreateEvent($merchantInstance));

                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollBack();
                throw new MerchantException($e->getMessage());
            }

            //success response
            return \response()
                ->json([
                    "message" => __("messages.merchant_create_successfully"),
                    "data" => $merchantInstance
                ], 200);
        }
        //if login user is not club admin role
        return \response()
            ->json([
                "message" => __("messages.403"),
                "error" => __("messages.login_with_club_admin_user")
            ], 403);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view('branch::edit');
    }


}
