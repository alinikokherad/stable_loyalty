<?php

namespace Modules\Branch\Listeners;

use App\Exceptions\CreateAccountException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Events\AccountCreatedEvent;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\WalletDB;

class AfterMerchantCreateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @throws CreateAccountException
     */
    public function handle($event)
    {
        $accountDB = resolve(AccountDB::class);
        $accountTypeDB = new AccountTypeDB();
        $walletDB = new WalletDB();
        try {
            /*---------- create account transaction --------*/
            \DB::beginTransaction();

            //get account type instance for merchant
            $data["account_type"] = "merchant";
            $accountTypeInstances = $accountTypeDB->getAccountTypeWithType($data["account_type"]);

            //if create account type dose not exist
            if (!$accountTypeInstances->first() instanceof AccountType) {
                throw new \Exception(__("messages.please_create_account_type_for_{$data["account_type"]}"), 500);
            }

            //create account for each wallet type
            foreach ($accountTypeInstances as $accountTypeInstance) {
                $walletTreasuryInstance = $walletDB->getTreasuryAccountInstanceWithWalletId($accountTypeInstance->wallet_id);
                //create account
                $accountData = [
                    "belongs_to" => $event->merchantInstance->id,
                    "belongs_type" => "merchant",
                    "treasury_account_id" => $walletTreasuryInstance->id,
                    "account_type_id" => $accountTypeInstance->id,
                ];
                $accountInstance = $accountDB->create($accountData);
                $data["treasury_account_id"] = $walletTreasuryInstance->id;

                if (!$accountInstance instanceof Account) {
                    throw new \Exception(__("messages.system_cannot_create_account_for_merchant"), 500);
                }

                //after account create this event happen (create credit for account)
                event(new AccountCreatedEvent($accountInstance, $data));
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw new CreateAccountException("some thing went wrong:{$e->getMessage()}");
        }
    }
}
