<?php

namespace Modules\Branch\Events;

use Illuminate\Queue\SerializesModels;

class AfterMerchantCreateEvent
{
    use SerializesModels;
    public $merchantInstance;

    /**
     * AfterMerchantCreateEvent constructor.
     * @param $merchantInstance
     */
    public function __construct($merchantInstance)
    {
        //
        $this->merchantInstance = $merchantInstance;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
