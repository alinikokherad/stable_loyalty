<?php

namespace Modules\Branch\Entities;

use App\Address;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Attachment\Entities\Attachment;
use Modules\Club\Entities\Club;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Table;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\ProductOrder;


/**
 * Modules\Branch\Entities\Merchant
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Branch\Entities\Merchant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Branch\Entities\Merchant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Branch\Entities\Merchant query()
 * @mixin \Eloquent
 */
class Merchant extends Model
{
    protected $guarded = [];
    protected $table = "merchants";

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, "merchant_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, "merchant_user", "merchant_id", "user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Attachment::class, "attachable");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Merchant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Merchant::class, "parent_id", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany(Club::class, "club_merchant", "merchant_id", "club_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(Account::class, 'belongs_to', "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function addresses()
    {
        return $this->morphOne(Address::class, "addressable");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class, "merchant_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tables()
    {
        return $this->hasMany(Table::class, "merchant_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productOrders()
    {
        return $this->hasMany(ProductOrder::class, "merchant_id");
    }
}


