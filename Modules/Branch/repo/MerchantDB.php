<?php


namespace Modules\Branch\repo;


use Modules\Branch\Entities\Merchant;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\repo\WalletDB;

/**
 * Class MerchantDB
 * @package Modules\Branch\repo
 */
class MerchantDB
{

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Merchant[]
     */
    public function list($children = null, $parent = null)
    {
        $with = ["accounts.accountType"];
        if (!empty($children)) {
            array_push($with, "children");
        }
        if (!empty($parent)) {
            array_push($with, "parent");
        }
        if (!empty($addresses)) {
            array_push($with, "addresses");
        }
        $instance = Merchant::with($with)->get();
//        ->whereHas("accounts.accountType", function ($q) {
//        $q->where("type", "merchant");
//    })->first();

        if ($instance->count() > 0) {
            return $instance;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getMerchantIds()
    {
        return Merchant::pluck("id")->all();
    }

    /**
     * @param $merchant_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function find($merchant_id)
    {
        $instance = Merchant::where('id', $merchant_id)
            ->with(["accounts", "accounts.accountType", "children", "parent"])
            ->whereHas("accounts.accountType", function ($q) {
                $q->where("type", "merchant");
            })->first();
        if ($instance instanceof Merchant) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $merchant_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Account[]
     */
    public function getMerchantAccountsWithMerchantId($merchant_id)
    {
        /*        $accountsInstance = Account::with(['merchant','accountType'])
                    ->whereHas("merchant", function ($q) use ($merchant_id) {
                    $q->where("id", $merchant_id);
                })->whereHas("accountType", function ($q) {
                    $q->where("type", "merchant");
                })->get();*/
        $accountsInstance = Account::whereHas("accountType", function ($q) {
            $q->where("type", "merchant");
        })->get();
        return $accountsInstance;
    }

    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|Merchant
     */
    public function insert($data)
    {
        //normalize input data
        $data = $this->normalizeInput($data);
        $instance = Merchant::create($data);
        if ($instance instanceof Merchant) {
            return $instance;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Merchant|object|null
     */
    public function getRandomMerchant()
    {
        return Merchant::first();
    }


    /**
     * @param $merchant_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getMerchantUser($merchant_id)
    {
        $instanceUser = Merchant::where("id", $merchant_id)->with("users")->first();
        $instanceUser = $instanceUser->users()->where("type", "=", "merchant")->first();
        return $instanceUser;
    }

    /**
     * @param int|null $user_merchant_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Merchant[]
     */
    public function getMerchantWithUserMerchant(?int $user_merchant_id)
    {
        $merchantsInstance = Merchant::with(["addresses", "children", "parent"])->whereHas("users", function ($q) use ($user_merchant_id) {
            $q->where("id", $user_merchant_id);
        })->get();
        return $merchantsInstance;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        $filter = [];
        if (isset($data["name"])) {
            $filter["name"] = $data["name"];
        }
        if (isset($data["title"])) {
            $filter["title"] = $data["title"];
        }
        if (isset($data["description"])) {
            $filter["description"] = $data["description"];
        }
        if (isset($data["author_id"])) {
            $filter["author_id"] = $data["author_id"];
        }
        if (isset($data["status"])) {
            $filter["status"] = $data["status"];
        }
        if (isset($data["parent_id"])) {
            $filter["parent_id"] = $data["parent_id"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }

    /**
     * @param int|null $merchant_id
     * @param string $walletType
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getMerchantInstanceWithAccount(?int $merchant_id, string $walletType)
    {
        //get wallet treasury id
        $walletDB = new WalletDB();
        $treasuryInstance = $walletDB->getTreasuryAccountInstanceWithWalletType($walletType);

        $accountInstance = Account::query()
            ->where("treasury_id", $treasuryInstance->id)
            ->whereHas("accountType", function ($query) {
                $query->where("type", "merchant");
            })
            ->with("merchant")
            ->whereHas("merchant", function ($query) use ($merchant_id) {
                $query->where("id", $merchant_id);
            })
            ->first();
        if ($accountInstance instanceof Account) {
            return $accountInstance;
        }
        return false;
    }

    /**
     * @param int|null $merchant_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getMerchantsPersonnel(?int $merchant_id)
    {
        $instanceUser = Merchant::where("id", $merchant_id)->with("users")->first();
        $instancesPersonnel = $instanceUser->users()->where("type", "=", "personnel")->orWhere("type", "merchant")->get();
        return $instancesPersonnel;
    }

    /**
     * @param int|null $merchant_id
     * @param $treasury_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getMerchantAccountInstance(?int $merchant_id, $treasury_id)
    {
        $merchantRialsAccountInstance = Account::where("treasury_id", $treasury_id)->whereHas("merchant", function ($query) use ($merchant_id) {
            $query->where("id", $merchant_id);
        })->first();
        if ($merchantRialsAccountInstance instanceof Account) {
            return $merchantRialsAccountInstance;
        }
        return false;
    }

    /**
     * @param $merchant_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Merchant|object|null
     * @throws \Exception
     */
    public function findOrFail($merchant_id)
    {
        $merchantInstance = Merchant::query()
            ->where("id", $merchant_id)
            ->first();
        if ($merchantInstance instanceof Merchant) {
            return $merchantInstance;
        }
        throw new \Exception("merchant_not_exist", 404);
    }


}
