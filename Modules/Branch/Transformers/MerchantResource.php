<?php

namespace Modules\Branch\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Branch\repo\MerchantDB;

class MerchantResource extends Resource
{
    public static $with_children = false;
    public static $with_parent = false;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        $merchantDB = new MerchantDB();
        $parent_name = $merchantDB->find($this->parent_id);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "parent_name" => isset($parent_name->name) ? $parent_name->name : null,
            "title" => $this->title,
            "description" => $this->description,
            "author" => $this->author,
            "revoked" => $this->revoked,
            "status" => $this->status,
            "parent_id" => $this->parent_id,
            "parent" => $this->when(self::$with_parent, $this->parent),
            "children" => $this->when(self::$with_children, $this->children),
        ];
    }
}
