<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string("title")->nullable();
            $table->text("description")->nullable();
            $table->unsignedInteger("parent_id")->default(0);
            $table->unsignedBigInteger("author_id")->nullable();
            $table->boolean("revoked")->default(false);
            $table->string("status")->default('active');
//            $table->foreign("parent_id")->references("id")->on("merchants")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
