<?php

namespace Modules\Product\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Product\repo\CostDB;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\repo\WalletDB;

class CreatedProductListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *create wallet for product
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $costDB = new CostDB();
        $walletDB = new WalletDB();
        $request = $event->data;
        $product = $event->productInstance;

        if (!is_array($request["costs"])) {
            $request["costs"] = json_decode($request["costs"], true);
        }
        foreach ($request["costs"] as $walletId => $cost) {
            //get wallet id from wallet type
            $walletInstance = $walletDB->find($walletId);

            //create costs
            $data = [
                "cost" => $cost,
                "wallet_id" => $walletInstance->id,
                "product_id" => $product->id,
            ];

            $test = $costDB->create($data);
        }

    }
}
