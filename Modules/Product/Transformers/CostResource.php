<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Wallet\repo\WalletDB;

class CostResource extends Resource
{
    public static $withProduct = false;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        $walletDB = new WalletDB();
        $wallet = $walletDB->find($this->wallet_id);

        $condition = $request->has("relations");
        return [
//            "product_id" => $this->product_id,
            "id" => $this->id,
            "cost" => $this->cost,
//            "wallet" => $wallet->title,
            "wallet_title" => $wallet->title,
            "wallet_type" => $wallet->type,
            "quantity" => $this->when(isset($this->pivot->quantity), function () {
                return $this->pivot->quantity;
            }),
            "product" => $this->when(self::$withProduct, function () {
                return (new ProductResource($this->product));
            }),
        ];
    }
}
