<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Product\Entities\Product;

class CategoryResource extends Resource
{
    public static $withCost = false;
    public static $with_product = false;
    public static $withCategory = false;
    public static $withImage = false;
    public static $withCatImage = false;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        /*        if (self::$with_cost) {
                    ProductResource::$withCost = true;
                    ProductResource::$withImage = true;
                }*/
        self::$withCost ? ProductResource::$withCost = true : false;
        self::$withImage ? ProductResource::$withImage = true : false;

        //        return parent::toArray($request);
        return [
            "category_id" => $this->id,
            "categoryName" => $this->name,
            "subtitle" => $this->subtitle,
            "description" => $this->description,
            "parent_id" => $this->parent_id,
            "status" => $this->status,
            "product" => $this->when(self::$with_product, ProductResource::collection($this->products)),
//            "product" => ProductResource::collection($this->products),
            "image" => $this->when(self::$withCatImage, function () {
                return $this->attaches;
            }),
        ];
    }
}
