<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Wallet\Transformers\OrderResource;
use Modules\Wallet\Transformers\ProductOrderResource;

class TableResource extends Resource
{
    /**
     * @var bool
     */
    public static $with_orders = false;

    /**
     * @var bool
     */
    public static $with_user_orders = false;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (self::$with_user_orders) {
            OrderResource::$with_user_orders = true;
        }
//        return parent::toArray($request);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "title" => $this->title,
            "description" => $this->description,
            "merchant_id" => $this->merchant_id,
            "qr_code" => $this->qr_code,
            "status" => $this->status,
            "revoked" => $this->revoked,
            "product_orders" => $this->when(self::$with_orders, ProductOrderResource::collection($this->productOrders)),
        ];
    }
}
