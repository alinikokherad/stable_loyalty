<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Product\Entities\Cost;

class ProductResource extends Resource
{
    public static $withCategory = false;
    public static $withCost = false;
    public static $withImage = false;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "productName" => $this->name,
            "title" => $this->title,
            "description" => $this->description,
            "status" => $this->status,
            "revoked" => $this->revoked,
            "category" => $this->when(self::$withCategory, $this->categories),
            "image_url" => $this->when(self::$withImage, $this->attaches),
//            "costs" => $this->when(self::$withCost, !($this->cost instanceof Cost) ? new CostResource($this->costs) : CostResource::collection($this->costs))
            "costs" => $this->when(self::$withCost, ($this->costs instanceof Cost) ? new CostResource($this->costs) : CostResource::collection($this->costs))
        ];
    }
}
