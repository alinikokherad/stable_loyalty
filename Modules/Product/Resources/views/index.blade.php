@extends('product::layouts.master')
@section('title')
    product list
@stop
@section('content')
    <div class="card">
        @if (Session::has('successMSG'))
            <div class="alert alert-success">
                <p>{{Session::get('successMSG')}}</p>
            </div>
        @elseif (Session::has('errorMSG'))
            <div class="alert alert-danger">
                <p>{{Session::get('errorMSG')}}</p>
            </div>
        @endif
        @component('product::components.validationError',['errors'=>$errors])@endcomponent
        <div class="card-header">
            <h1>product list</h1>
        </div>
        <div class="card-body">
            <table class="table table-striped table-info">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>price</th>
                    <th>description</th>
                    <th>status</th>
                    <th>image</th>
                    <th>operation</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{str_limit($product->description,50)}}</td>
                        <td>@php echo empty($product->status)?'inactive':'active'; @endphp</td>
                        <td><img src="{{$product->img}}" alt="{{$product->name}}"></td>
                        <td>
                            <a href="#" class="">
                                <span class="fa fa-edit text-info fa-2x"></span>
                            </a>
                            <a href="#" class="" data-target="#detailsModal" data-toggle="modal">
                                <span class="fa fa-eye text-primary fa-2x"></span>
                            </a>
                            <a href="#" product_id="{{$product->id}}" class="removeProduct">
                                <span class="fa fa-trash text-danger fa-2x"></span>
                            </a>
                        </td>
                    </tr>
                    @component('product::components.showModal',['product'=>$product])@endcomponent
                @endforeach
                </tbody>
            </table>

            <div class="row justify-content-center">
                {{$products->links()}}
            </div>
        </div>
    </div>
@stop

@push('js')
    <script>
        $(document).ready(function () {
            $(".removeProduct").click(function () {
                let id = $(this).attr("product_id");
                swal({
                    title: "Are you sure?",
                    // text: "Once deleted, you will not be able to recover this imaginary file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            // function ajaxRun(){
                            $.ajax({
                                type: "post",
                                url: "/product/delete/" + id,
                                // data: {id: id},
                                success: function () {
                                    swal("Poof! Your imaginary file has been deleted!", {
                                        icon: "success",
                                    });
                                    location.reload();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert('error');
                                }
                            });
                        }
                    });
            });
        });
    </script>
@endpush

