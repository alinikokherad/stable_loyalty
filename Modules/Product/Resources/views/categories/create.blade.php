@extends('layouts.master')Route::prefix('product')->group(function() {
    Route::get('/index', 'ProductController@index')->name('product.index');
    Route::get('/show/{id}', 'ProductController@show')->name('product.show');
    Route::get('/create', 'ProductController@create')->name('product.create');
    Route::post('/store', 'ProductController@store')->name('product.store');
    Route::get('/edit/{id}', 'ProductController@edit')->name('product.edit');
    Route::post('/update/{id}', 'ProductController@update')->name('product.update');
    Route::post('/delete/{id}', 'ProductController@destroy')->name('product.delete');

@section('title')
    create
@stop

@section('content')

@stop
