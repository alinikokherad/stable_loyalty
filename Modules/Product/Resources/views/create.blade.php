@extends('product::layouts.master')

@section('title')
    create product
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="card offset-3 col-md-6 mt-md-3 ">
                <div class="card-header">
                    insert product
                </div>
                <div class="card-body">
                    {{--show validation errors--}}
                    @component('product::components.validationError',['errors'=>$errors])@endcomponent
                    {{--show controller messages--}}
                    @if (Session::has('successMSG'))
                        <div class="alert alert-success">
                            <p>{{Session::get('successMSG')}}</p>
                        </div>
                    @elseif (Session::has('errorMSG'))
                        <div class="alert alert-danger">
                            <p>{{Session::get('errorMSG')}}</p>
                        </div>
                    @endif

                    {{--category add modal form--}}
                    <div class="col-md-2 mb-3">
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">
                            <span class="fa fa-plus"></span> add category
                        </button>
                    </div>
                    @component('product::components.modal',isset($categories)?['categories'=>$categories]:[])@endcomponent
                    {{--product store form--}}
                    <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row ">
                            <div class="form-group col-12">
                                <label for="product_quantity">product quantity:</label>
                                <input type="number" class="form-control" name="product_quantity" id="product_quantity"
                                       value="{{old('product_quantity')}}">
                            </div>

                            <div class="form-group col-12">
                                <label for="name">name:</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{old('name')}}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="cat_id">category:</label>
                                <select name="cat_id[]" class="form-control" id="cat_id" multiple>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="tag_id">tags:</label>
                                <select name="tag_id[]" class="form-control" id="tag_id" multiple>
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->name}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <label for="price">price:</label>
                                <input type="number" class="form-control" name="price" id="price"
                                       value="{{old('price')}}">
                            </div>
                            <div class="form-group col-12">
                                <label for="description">description:</label>
                                <textarea class="form-control" name="description">{{old('description')}}</textarea>
                            </div>
                            <div class="custom-file col-12">
                                <input type="file" class="custom-file-input" name="img" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="form-group col-12 my-3">
                                <input type="submit" class="btn btn-success" name="createProductBtn" value="save">
                                <input type="reset" class="btn btn-danger" value="reset">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script>
        $(document).ready(function () {
            $("#cat_id").select2();
            $("#tag_id").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });
        });
    </script>
@endpush
