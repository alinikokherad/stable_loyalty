<div class="modal" id="detailsModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">product details</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="media">
                    <img class="align-self-center mr-3" src="{{asset($product->img)}}" alt="{{$product->name}}">
                    <div class="media-body">
                        <h5 class="mt-0">{{$product->name}}</h5>
                        <div class="list-group list-group-flush ">
                            <a href="#" class="list-group-item ">
                                <div class="row">
                                    <span class="col-8">id</span>
                                    <span class="col-4 text-center">{{$product->id}}</span>
                                </div>
                            </a>
                            <a href="#" class="list-group-item ">
                                <div class="row">
                                    <span class="col-5">description</span>
                                    <span class="col-7 text-center">{{$product->description}}</span>
                                </div>
                            </a>
                            <a href="#" class="list-group-item ">
                                <div class="row">
                                    <span class="col-8">owner_id</span>
                                    <span class="col-4 text-center">{{$product->owner_id}}</span>
                                </div>
                            </a>
                            <a href="#" class="list-group-item ">
                                <div class="row">
                                    <span class="col-8">price</span>
                                    <span class="col-4 text-center">{{$product->price}}</span>
                                </div>
                            </a>
                            <a href="#" class="list-group-item ">
                                <div class="row">
                                    <span class="col-8">status</span>
                                    <span class="col-4 text-center">{{$product->status}}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
