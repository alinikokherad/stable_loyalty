<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">add category</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('product.category.store')}}" method="post" id="formModal">
                    @csrf
                    @if (isset($categories))
                        <div class="form-group">
                            <label for="parent_id">parent:</label>
                            <select name="parent_id" id="parent_id" class="form-control">
                                <option value="null">please select</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="cat_name">cat name:</label>
                        <input type="text" class="form-control" name="cat_name" id="cat_name">
                    </div>
                    <div class="form-group">
                        <label for="cat_description">category description:</label>
                        <textarea name="cat_description" id=cat_description"" cols="30" rows="5"
                                  class="form-control">
                        </textarea>
                    </div>
                    <div class="form-group col-12 my-3">
                        <input type="submit" class="btn btn-success" value="save">
                        <input type="reset" class="btn btn-danger" value="reset">
                    </div>
                    <div id="responseModal"></div>
                </form>
            </div>
        </div>
    </div>
</div>

@push("js")
    <script>
        $(function () {
            $("#formModal").on('submit', function (e) {
                $.post({
                    type: 'post',
                    url: 'category/store',
                    data: $(this).serialize(),
                    success: function (data) {
                        // $('.form').trigger("reset");
                        $('#responseModal').html(data);
                    },
                    error: function () {
                        alert('error')
                    }
                });
                // e.preventDefault();
            });
        });
    </script>
@endpush
