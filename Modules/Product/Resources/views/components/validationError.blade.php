@if ($errors->any())
    <div class="alert alert-danger text-right pb-0 text-right">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif
