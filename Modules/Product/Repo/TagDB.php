<?php


namespace Modules\Product\Repo;


use Illuminate\Http\Request;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Tag;

class TagDB
{

    /**
     * @param Product $productInstance
     * @param array $tags
     */
    public function insertTags($productInstance,$tags): void
    {
        $tagsInstance=[];
        foreach ($tags as $tag) {
            $tag=Tag::firstOrCreate(['name'=>$tag]);
            if ($tag instanceof Tag){
                array_push($tagsInstance,$tag->id);
            }
        }
        $productInstance->tags()->attach($tagsInstance);
    }
}
