<?php


namespace Modules\Product\Repo;


use Carbon\Carbon;
use Modules\Attachment\Entities\Attachment;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Product;
use Modules\Product\Transformers\CategoryResource;
use Modules\Product\Transformers\CategoryResourceCollection;

class CategoryDB
{

    public function create($data, $file)
    {
        $instance = Category::create($data);
        if ($instance instanceof Category) {
            //save image into public folder
            if (!empty($file["img"])) {
                $newName = 'image' . Carbon::now()->isoFormat('H-i-s') . '.' . $file["img"]->getClientOriginalExtension();
                $file["img"]->move(public_path('uploads/category'), $newName);
                $img = 'uploads/category/' . $newName;
                $attachable = new Attachment([
                    "src" => $img
                ]);
                $attachable->save();

                $a = $instance->attaches()->save($attachable);
                $instance->attachment = $a;
            }
            return $instance;
        }
        return false;
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function list($relations)
    {
        $list = Category::with('attaches')->get();
        if ($relations == "true") {
            CategoryResource::$with_cost = true;
            CategoryResource::$with_product = true;
            return CategoryResource::collection($list);
        }
        return $list;
    }

    /**
     * assign Category To Product
     * @param Product $product
     * @param array $categories
     * @return void
     * */
    public function assignCategoryToProduct(Product $product, $categories = [])
    {
        $categories = (array)$categories;
        $product->categories()->sync($categories);
    }
}
