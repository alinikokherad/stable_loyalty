<?php


namespace Modules\Product\Repo;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Product\Entities\Product;

class productRepo
{
    /**
     * @return mixed
     */
    public function productPaginate()
    {
        return Product::paginate(5);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function saveImage(Request $request): array
    {
        $newName = 'image' . Carbon::now()->isoFormat('H-i-s') . '.' . $request->file('img')->getClientOriginalExtension();
        $request->file('img')->move(public_path('uploads'), $newName);
        $img = 'uploads/' . $newName;
        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'img' => $img,
            'price' => $request->input('price'),
        ];
        return $data;
    }
}
