<?php


namespace Modules\Product\Repo;


use Modules\Product\Entities\Table;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Transformers\ProductOrderResource;

class TableDB
{
    /**
     * @param $merchant_id
     * @param $reserve
     * @param $with_orders
     * @param $tableList
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|Table[]|\Modules\Wallet\Transformers\ProductOrderResourceCollection
     */
    public function index($merchant_id, $reserve, $with_orders, $tableList)
    {
        //merchant simple list where merchant id is specific
        if ($tableList == true) {
            $tableList = Table::whereHas("merchant", function ($q) use ($merchant_id) {
                $q->where("id", $merchant_id);
            })->get();
            return $tableList;
        }

        $with = ["user", "tables"];

        if ($with_orders) {
            array_push($with, "orders");
        }
        $productOrder = ProductOrder::with($with);

        //get reserved tables
        if (!empty($reserve) && $reserve == true) {
            $productOrder->whereHas("tables", function ($q) {
                $q->where("status", "reserved");
            });
        }

        $productOrder->whereHas("orders", function ($q) {
            $q->whereNull("paid_at");
        });

        //get tables for specific merchant
        if (!empty($merchant_id)) {
            $productOrder = $productOrder->whereHas("tables.merchant", function ($q) use ($merchant_id) {
                $q->where("id", $merchant_id);
            });
        }

        if ($with_orders == true) {
            ProductOrderResource::$with_cost = true;
            ProductOrderResource::$with_user = true;
            ProductOrderResource::$with_user_credit = true;
            ProductOrderResource::$with_tables = true;
            ProductOrderResource::$with_product = true;
            ProductOrderResource::$user_total_credit = true;
            ProductOrderResource::$with_orders = true;
        }

        //get to array from resource
        $response = ProductOrderResource::collection($productOrder->get());

        return $response;
    }

    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|Table
     */
    public function insert($data)
    {
        $input = $this->inputNormalize($data);
        $instance = Table::create($input);
        if ($instance instanceof Table) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $table_ids
     * @return bool|\Illuminate\Support\Collection
     */
    public function get($table_ids)
    {
        $tables = Table::whereIn("id", $table_ids)->get();
        if ($tables->first() instanceof Table) {
            return $tables;
        }
        return false;
    }

    /**
     * @param $id
     * @param string $string
     * @return int
     */
    public function updateStatus($id, string $string)
    {
        return Table::where('id', $id)->update(["status" => $string]);
    }

    /**
     * @return array
     */
    public function getAllOpenTablesIds()
    {
        return Table::where("status", "open")->orWhere("status", "multiple")->pluck("id")->toArray();
    }

    /**
     * @param $data
     * @return array
     */
    public function inputNormalize($data)
    {
        $filter = [];
        if (!is_array($data)) {
            $data = (array)$data;
        }
        if ($data["name"]) {
            $filter["name"] = $data["name"];
        }
        if ($data["title"]) {
            $filter["title"] = $data["title"];
        }
        if ($data["qr_code"]) {
            $filter["qr_code"] = $data["qr_code"];
        }
        if ($data["description"]) {
            $filter["description"] = $data["description"];
        }
        if ($data["merchant_id"]) {
            $filter["merchant_id"] = $data["merchant_id"];
        }
        if ($data["status"]) {
            $filter["status"] = $data["status"];
        }
        if ($data["revoked"]) {
            $filter["revoked"] = $data["revoked"];
        }

        return $filter;
    }
}
