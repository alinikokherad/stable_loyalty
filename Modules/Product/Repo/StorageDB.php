<?php


namespace Modules\Product\Repo;


use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductStore;

class StorageDB
{

    /**
     * @param Product $product
     * @param int $quantity
     * @return void
     * */
    public function assignProductQuantity(Product $product,$quantity):void
    {
        $data=[
            "product_id"=>$product->id,
            "product_quantity"=>$quantity,
            "unitValue"=>$product->price,
        ];
        ProductStore::create($data);
    }
}
