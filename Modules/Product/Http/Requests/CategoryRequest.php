<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class categoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "name" => 'required|string|max:50',
            "subtitle" => 'nullable|string|min:10|max:100',
            "description" => 'nullable|string|min:10|max:300',
            "parent_id" => 'numeric|required',
            "status" => 'in:active,inactive|nullable',
        ];
    }

    /*    public function messages()
        {
            return[
                'name.required'=>'فیلد نام اجباری است'
            ];
        }*/
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
