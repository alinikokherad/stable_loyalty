<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "name" => 'required|string|max:50',
            "title" => 'nullable|string|max:100',
            "costs" => 'required|array',
            "category_id" => 'numeric|required',
            "description" => 'nullable|max:500',
            "img" => 'nullable|image',
        ];
    }

    /**
     * @return array
     */
    /*    public function messages()
        {
            return [
                'name.required' => 'فیلد نام اجباری می باشد',
                'name.string' => 'فیلد نام باید مجموعه ای از کاراکترها باشد',
                'name.max' => 'تعداد کاراکتر های فیلد نام نباید بیش از ۲۵۵ کاراکتر باشد',
                'title.max' => 'تعداد کاراکتر های فیلد عنوان نباید بیش از ۲۵۵ کاراکتر باشد',
                'title.string' => 'فیلد عنوان باید کاراکتر از حروف باشد',
                'description.max' => 'حداکثر کاراکتر برای توضیحات ۲۰۰ می باشد',
                'img.required' => 'فیلد عکس اجباری می باشد',
            ];
        }*/

    public function attributes()
    {
        return [

        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
