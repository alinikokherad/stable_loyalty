<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'description' => 'max:200'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'فیلد نام اجباری است',
            'description.max' => 'تعداد کاراکتر های توضیح نباید بیشتر از 200 باشد',
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
