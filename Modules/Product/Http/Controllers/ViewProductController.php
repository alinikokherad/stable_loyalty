<?php

namespace Modules\Product\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Tag;
use Modules\Product\Http\Requests\ProductRequest;
use Modules\Product\Repo\productDB;
use Modules\Product\Repo\productRepo;
use Modules\Product\Repo\TagDB;
use Modules\Wallet\repo\WalletDB;

class ViewProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $products = (new productRepo())->productPaginate();
        return view('product::index', compact('products'));
    }


    /**
     * Show the form for creating a new resource.$request
     * @return Response$request
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('product::create', compact(['categories', 'tags']));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        $productData = (new productRepo())->saveImage($request);
        $productInstance = (new productDB())->insertProduct($productData);
        $tags = $request->input('tag_id');
        (new TagDB())->insertTags($productInstance, $tags);
        if ($productInstance instanceof Product) {
            return redirect()->route('product.index')->with(['successMSG' => 'محصول با موفقیت ثبت شد']);
        }
        return redirect()->back()->with(['errorMSG' => 'مشکلی در ثبت محصول رخ داده است']);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $product=(new productDB())->getProduct($id);
        $product = Product::paginate(1);
        return view('product::show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $product=(new productDB())->getProduct($id);
        return view('product::edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ProductRequest $request, $id)
    {
        //
        dd($request, $id);
    }

    /**
     * Remove the specified product from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        (new productDB())->deleteProduct($id);
        return redirect()->back()->with(["successMSG"=>'محصول با موفقیت حذف شد']);
    }
}
