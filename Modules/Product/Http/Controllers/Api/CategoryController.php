<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Http\Requests\categoryRequest;
use Modules\Product\Repo\CategoryDB;

class CategoryController extends Controller
{
    /**
     * return category list (pagination)
     */
    public function index()
    {
        $limit = \request()->has("limit") ? \request("limit") : null;
        $page = \request()->has("page") ? \request("page") : 1;
        $relations = \request()->has("relations") ? \request("relations") : false;
        $categoryDB = new CategoryDB();

        $list = $categoryDB->list($relations);

        if ($limit != null) {
            $list = $list->paginate($limit, $page);
        }
        return \response()
            ->json([
                "message" => __('messages.category_list'),
                "data" => $list
            ], 200);
    }

    /**
     * @param categoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(categoryRequest $request)
    {
        $categoryDB = new CategoryDB();
        $data = $request->all();
        $file["img"] = $request->has("img") ? \request()->file("img") : null;
        $instance = $categoryDB->create($data, $file);
        if ($instance) {
            return \response()
                ->json([
                    "message" => __('messages.category_create'),
                    "data" => $instance
                ], 200);
        }
        return \response()
            ->json([
                "error" => "bad request"
            ], 400);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
