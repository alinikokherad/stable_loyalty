<?php

namespace Modules\Product\Http\Controllers\Api;

use App\User;
use Doctrine\DBAL\Schema\AbstractAsset;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Branch\Entities\Merchant;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\ProductRequest;
use Modules\Product\repo\ProductDB;
use Modules\Product\Transformers\ProductResource;
use Modules\Wallet\repo\WalletDB;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (\Gate::allows("merchant_admin")) {
            $limit = \request()->has("limit") ? \request("limit") : 5;
            $promoted = \request()->has("promoted") ? (int)\request("promoted") : 0;
            $cost = \request()->has("cost") ? \request("cost") : 1;

            $request = \request();
            $productDB = new ProductDB();

            if (\Auth::check() && \Auth::user()->type == "merchant") {
                $merchant_id = \Auth::user()->merchant_id;
            } elseif (\Auth::user()) {
                $merchant_id = $request->merchant_id;
            }

            $data = $productDB->list($limit, $merchant_id, $cost, $promoted);

            if ($cost) {
                ProductResource::$withCost = true;
                ProductResource::$withImage = true;
                ProductResource::$withCategory = true;
            }

            $data = ProductResource::collection($data);

            return \response()
                ->json([
                    "message" => __('messages.product_list'),
                    "data" => $data
                ], 200);
        }

        return \response()
            ->json([
                "message" => __('messages.400'),
                "error" => __("messages.403")
            ], 403);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function gift()
    {
        if (\Gate::allows('merchant_admin')) {
            $walletDB = new WalletDB();
            $customerID = \request()->has("customer_id") ? \request("customer_id") : null;
            $limit = \request()->has("limit") ? \request("limit") : 10;
            $page = \request()->has("page") ? \request("page") : 1;

            //when customer id is not set in input
            if (is_null($customerID)) {
                return \response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.customer_id_not_set")
                    ], 400);
            }
            $treasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType("point");
            $walletInstance = $walletDB->getByType("point");
            $customerInstance = User::where("id", \request("customer_id"))->with(["accounts.credits"])->first();
            $customerPointCredit = $customerInstance->accounts()->where("treasury_id", $treasuryAccountInstance->id)->first()->credits()->first();

            //when customer point credit is 0
            if ($customerPointCredit->amount <= 0) {
                return \response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.not_enough_credit")
                    ], 400);
            }

            //when customer have gift product
            $products = Product::with("costs")->wherehas("costs", function ($q) use ($walletInstance, $customerPointCredit) {
                $q->where([["wallet_id", "=", $walletInstance->id], ["cost", "<=", $customerPointCredit->amount]]);
            })->get();
            if (isset($products) && $products->count() >= 1) {
                return \response()
                    ->json([
                        "message" => __("messages.your_gift"),
                        "data" => $products->paginate($limit, $page)
                    ], 200);
            }

            //when customer points not enough for buy product
            $costs = Cost::all();
            $minCost = $costs->min('cost');
            $productInstance = $costs->where("cost", $minCost)->first()->product()->first();
            return \response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => __("messages.you_dont_have_gift"),
                    "data" => [
                        "need_point" => $minCost - $customerPointCredit->amount,
                        "product" => $productInstance,
                    ],
                ], 400);

        } else {
            return \response()
                ->json([
                    "message" => __("messages.403"),
                    "error" => __("messages.access_denied")
                ], 403);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        //get login user
        $userInstance = \Auth::user();
        if ($userInstance->type != 'merchant_admin') {
            return Response()->json([
                'message' => __('messages.400'),
                'error' => __('messages.must_be_merchant')
            ], 400);
        }

        //initialize data
        $productDB = new ProductDB();

        //create product
        $productInstance = $productDB->create($request);
        if ($productInstance == false) {
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => __("messages.input_not_correct"),
                ], 400);
        }

        $sync = is_array($request->category_id) ? $request->category_id : [$request->category_id];

        $productInstance->categories()->sync($sync);

        return Response()->json([
            "message" => __('messages.product_create'),
            "data" => $productInstance
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $productInstance = (new productDB())->get($id);
        if (!$productInstance) {
            return Response()->json([
                "message" => __('messages.product_ifo'),
                "data" => $productInstance
            ], 200);
        }
        return Response()->json([
            "message" => __('messages.400'),
            "error" => __("messages.product_not_exist")
        ], 400);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        (new productDB())
            ->deleteProduct($id);
        return redirect()
            ->back()
            ->with(["successMSG" => 'محصول با موفقیت حذف شد']);
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function promoteProduct(Request $r)
    {
        $productArray = json_decode($r->all()["products"], true);
        $products = Product::whereIn('id', $productArray)->get();
        if (empty($products->toArray())) {
            return Response()->json([
                'message' => __('messages.not_exist'),
                'error' => __('messages.400')
            ], 400);
        }
        foreach ($productArray as $item) {
            Product::where('id', $item)->update(['promoted' => true]);
        }
        return Response()->json([
            'message' => __('messages.ok'),
        ], 200);
    }

}
