<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Category;
use Modules\Product\Http\Requests\categoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('product::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('product::create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = [
            "merchant_id" => $request->has('merchant_id') ? $request->input("merchant_id") : null,
            "parent_id" => $request->has('parent_id') ? $request->input("parent_id") : null,
            "name" => $request->input("name"),
            "description" => $request->has('description') ? $request->input("description") : null,
            "subtitle" => $request->has('subtitle') ? $request->input("subtitle") : null,
        ];

        // filter $data item without null and "null"
        $data = array_filter($data, function ($item) {
            return ($item != null)and($item != 'null');
        });
//        dd($data);

        $instance = Category::updateOrCreate(['name'=>$data['name']],$data);
        if ($instance instanceof Category) {
            return back()->with(['successMSG'=>'دسته بندی با موفقیت ایجاد شد']);
        }
        return back()->with(['errorMSG'=>'مشکلی در ثبت دسته بندی به وجود آمده است']);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('product::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(categoryRequest $request, $id)
    {
        //
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        dd($id);
    }
}
