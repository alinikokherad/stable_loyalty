<?php

namespace Modules\Product\Http\Controllers;

use App\ObjectFactory\ModelFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class TableController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (\Gate::allows('merchant_admin')) {
            $tableDB = ModelFactory::build("TableDB");
            $merchant_id = \request()->has("merchant_id") ? \request("merchant_id") : \Auth::user()->merchant_id;
            $reserve = \request()->has("reserve") ? (bool)\request("reserve") : false;
            $with_orders = \request()->has("with_orders") ? (bool)\request("with_orders") : null;
            $tableList = \request()->has("list") ? (bool)\request("list") : false;
            $tableInstance = $tableDB->index($merchant_id, $reserve, $with_orders, $tableList);
            if (empty($tableInstance)) {
                return \response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.table_not_exist")
                    ], 400);
            }

            return \response()
                ->json([
                    "message" => __("messages.table_information"),
                    "data" => $tableInstance
                ], 200);
        }
        return \response()
            ->json([
                "message" => __("messages.403"),
                "error" => __("messages.access_denied")
            ], 403);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (\Gate::allows('merchant_admin')) {
            $tableDB = ModelFactory::build("TableDB");
            $tableInstance = $tableDB->insert($request->all());
            if (!$tableInstance) {
                return \response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.table_not_created")
                    ], 400);
            }
            return \response()
                ->json([
                    "message" => __("messages.table_added_successfully"),
                    "data" => $tableInstance
                ], 200);
        }
        return \response()
            ->json([
                "message" => __("messages.403"),
                "error" => __("messages.access_denied")
            ], 403);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
