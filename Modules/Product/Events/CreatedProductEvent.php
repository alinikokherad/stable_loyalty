<?php

namespace Modules\Product\Events;

use Illuminate\Queue\SerializesModels;

class CreatedProductEvent
{
    use SerializesModels;
    public $productInstance;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($productInstance,$data)
    {
        $this->productInstance=$productInstance;
        $this->data=$data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
