<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$locale = request()->server("HTTP_ACCEPT_LANGUAGE") ?? "en";
App::setLocale($locale);

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(["namespace" => "Api"], function () {
        Route::resource('products', 'ProductController');
        Route::post('products/promote', 'ProductController@promoteProduct');
        Route::resource('/product/category', "CategoryController");
        Route::get('/product/gift', "ProductController@gift");
    });
    Route::resource('/tables', "TableController");
});

