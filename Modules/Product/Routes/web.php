<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('product')->group(function () {
    Route::get('/index', 'ProductController@index')->name('product.index');
    Route::get('/show/{id}', 'ProductController@show')->name('product.show');
    Route::get('/create', 'ProductController@create')->name('product.create');
    Route::post('/store', 'ProductController@store')->name('product.store');
    Route::get('/edit/{id}', 'ProductController@edit')->name('product.edit');
    Route::post('/update/{id}', 'ProductController@update')->name('product.update');
    Route::post('/delete/{id}', 'ProductController@destroy')->name('product.delete');
});

Route::prefix('product/category')->group(function () {
    Route::get('/index', 'CategoryController@index')->name('product.category.index');
    Route::get('/show/{id}', 'CategoryController@show')->name('product.category.show');
    Route::get('/create', 'CategoryController@create')->name('product.category.create');
    Route::post('/store', 'CategoryController@store')->name('product.category.store');
    Route::get('/edit/{id}', 'CategoryController@edit')->name('product.category.edit');
    Route::post('/update/{id}', 'CategoryController@update')->name('product.category.update');
    Route::post('/delete/{id}', 'CategoryController@destroy')->name('product.category.delete');
});

Route::prefix('product/tag')->group(function () {
    Route::get('/index', 'TagController@index')->name('product.tag.index');
    Route::get('/show/{id}', 'TagController@show')->name('product.tag.show');
    Route::get('/create', 'TagController@create')->name('product.tag.create');
    Route::post('/store', 'TagController@store')->name('product.tag.store');
    Route::get('/edit/{id}', 'TagController@edit')->name('product.tag.edit');
    Route::post('/update/{id}', 'TagController@update')->name('product.tag.update');
    Route::post('/delete/{id}', 'TagController@destroy')->name('product.tag.delete');
});
