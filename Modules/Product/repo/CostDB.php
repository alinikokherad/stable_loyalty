<?php


namespace Modules\Product\repo;


use Modules\Product\Entities\Cost;
use Modules\Wallet\repo\WalletDB;

/**
 * Class CostDB
 * @package Modules\Product\repo
 */
class CostDB
{

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $input = $this->normalize($data);
        $instance = Cost::create($input);
        if ($instance instanceof Cost) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function find($id)
    {
        $instance = Cost::find($id);
        if ($instance instanceof Cost) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $cost_ids
     * @param $wallet_id
     * @return mixed
     */
    public function findType($cost_ids, $wallet_id)
    {
        return Cost::whereIn("id", $cost_ids)->where("wallet_id", $wallet_id)->get();
    }

    /**
     * @param $cost_id
     * @return mixed
     */
    public function getCost($cost_id)
    {
        $cost = Cost::where("id", $cost_id)
            ->pluck("cost")
            ->first();
        return $cost;
    }

    /**
     * @param $walletType
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getCostWithWalletType($walletType)
    {
        $walletDB = new WalletDB();
        $walletInstance = $walletDB->getByType($walletType);
        $instance = Cost::where("wallet_id", $walletInstance->id)->first();
        if ($instance instanceof Cost) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalize($data)
    {
        $filter = [];
        if ($data["product_id"]) {
            $filter["product_id"] = $data["product_id"];
        }
        if ($data["cost"]) {
            $filter["cost"] = $data["cost"];
        }
        if ($data["wallet_id"]) {
            $filter["wallet_id"] = $data["wallet_id"];
        }
        return $filter;
    }
}
