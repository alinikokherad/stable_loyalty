<?php


namespace Modules\Product\repo;


use Carbon\Carbon;
use Modules\Attachment\Entities\Attachment;
use Modules\Product\Entities\Product;
use Modules\Product\Events\CreatedProductEvent;
use Modules\Wallet\Entities\Wallet;

class ProductDB
{

    /**
     * @param $request
     * @return bool|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|Product
     * @throws \Exception
     */
    public function create($request)
    {
        $productData = $this->normalizeInput($request->all());

        try {
            \DB::beginTransaction();
            //save product into database
            $productInstance = Product::create($productData);

            if (!($productInstance instanceof Product)) {
                return response()
                    ->json([
                        "message" => __("messages.wrong_input"),
                        "error" => __("messages.wrong_product_params"),
                    ], 400);
            }

            //after product insert into data base image save into public folder
            //save image into public folder
            if (!empty($request->file("img"))) {
                $newName = 'image' . Carbon::now()->isoFormat('H-i-s') . '.' . $request->file('img')->getClientOriginalExtension();
                $permission = $request->file('img')->move(public_path('uploads/products'), $newName);
                if (!$permission) {
                    return response()
                        ->json([
                            "message" => __("messages.400"),
                            "error" => __("messages.public_folder_denied")
                        ], 400);
                }

                $img = 'uploads/products/' . $newName;
                $attachable = new Attachment([
                    "src" => $img
                ]);
                $attachable->save();
                $productInstance->attaches()->save($attachable);
            }

            $test = event(new CreatedProductEvent($productInstance, $request->all()));

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return false;
        }
        return $productInstance;
    }

    /**
     * @param $limit
     * @param $merchant_id
     * @param null $cost
     * @param null $promoted
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder
     */
    public function list($limit, $merchant_id, $cost = null, $promoted = null)
    {
        $products = Product::where("merchant_id", $merchant_id);

        if (!empty($promoted)) {
            $products->where("promoted", 1);
        }

        $products->with(["products", "categories", "attaches"]);

        if (!empty($cost) && $cost == "point") {
            $wallet_id = Wallet::where("type", "point")->first()->id;
            $products->whereHas("products", function ($q) use ($wallet_id) {
                $q->whereNotNull("cost")->where("wallet_id", "=", $wallet_id);
            });
        }

        return $products->paginate($limit)->sortByDesc("products.cost");
    }

    /**
     * @param null $wallet
     * @param $product_id
     * @param $fromAmount
     * @param $toAmount
     * @param $from
     * @param $to
     * @param $day
     * @param $week
     * @param $month
     * @param $user_id
     * @param null $merchant_id
     * @param null $paid_at
     * @return bool|\Illuminate\Support\Collection
     */
    public function listProductsWithCondition
    ($wallet = null, $product_id, $fromAmount, $toAmount, $from, $to, $day, $week, $month, $user_id, $merchant_id = null, $paid_at = null)
    {
        $wallets = [
            "rials" => 1,
            "point" => 2,
        ];
        $userWhere = [];
        $walletWhere = [];
        if (!is_null($paid_at)) {
            $paid_at = [
                ["product_orders.paid_at", "=", null]
            ];
        } else {
            $paid_at = [
                ["product_orders.paid_at", "!=", null]
            ];
        }

        $productIdWhere = [];
        if (isset($product_id)) {
            array_push($productIdWhere, ["id", "=", $product_id]);
        }

        if (isset($wallet)) {
            $wallet = $wallets[$wallet];
            array_push($walletWhere, ["wallet_id", "=", $wallet]);
        }

        if (isset($fromAmount) && isset($toAmount)) {
            array_push($walletWhere, ["cost", ">=", $fromAmount], ["cost", "<=", $toAmount]);
        }

        if (isset($user_id)) {
            array_push($userWhere, ["to_user_id", "=", $user_id]);
        }

        if (isset($merchant_id)) {
            array_push($userWhere, ["from_user_id", "=", $merchant_id]);
        }
        if (isset($day) || isset($week) || isset($month)) {
            if (isset($day)) {
                array_push($paid_at, ["product_orders.created_at", ">=", Carbon::now()->subDay($day)]);
            } elseif (isset($week)) {
                array_push($paid_at, ["product_orders.created_at", ">=", Carbon::now()->subDay($week)]);
            } else {
                array_push($paid_at, ["product_orders.created_at", ">=", Carbon::now()->subDay($month)]);
            }
        }

        if (isset($from) && isset($to)) {
            $from = date("Y-m-d" . " 00:00:00", strtotime($from));
            $to = date("Y-m-d" . " 23:59:59", strtotime($to));
            array_push($paid_at, ["product_orders.created_at", ">=", $from]);
            array_push($paid_at, ["product_orders.created_at", "<=", $to]);
        }

        if (!empty($walletWhere) && !empty($userWhere)) {
            //product filter
            if (!empty($productIdWhere)) {
                $productOrdersInstance = Product::
                where($productIdWhere)
//                    ->has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }, "costs" => function ($q) use ($walletWhere) {
                        $q->where($walletWhere);
                    }])
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->wherehas("costs.productOrders.user", function ($q) use ($userWhere) {
                        $q->where($userWhere);
                    })
                    ->first();
            } else {

                $productOrdersInstance = Product::
                has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }, "costs" => function ($q) use ($walletWhere) {
                        $q->where($walletWhere);
                    }])
                    ->wherehas("costs.productOrders.user", function ($q) use ($userWhere) {
                        $q->where($userWhere);
                    })
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            }
//            dd($productOrdersInstance->toArray());

        } elseif (!empty($walletWhere) && empty($userWhere)) {

            //product filter
            if (!empty($productIdWhere)) {
                $productOrdersInstance = Product::
                where($productIdWhere)
                    ->has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }, "costs" => function ($q) use ($walletWhere) {
                        $q->where($walletWhere)->groupby("wallet_id");
                    }])
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            } else {
                $productOrdersInstance = Product::has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }, "costs" => function ($q) use ($walletWhere) {
                        $q->where($walletWhere)->groupby("wallet_id");
                    }])
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            }

        } elseif (empty($walletWhere) && !empty($userWhere)) {

            //product filter
            if (!empty($productIdWhere)) {
                $productOrdersInstance = Product::
                where($productIdWhere)
                    ->has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }])
                    ->wherehas("costs.productOrders.user", function ($q) use ($userWhere) {
                        $q->where($userWhere);
                    })
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            } else {
                $productOrdersInstance = Product::has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }])
                    ->wherehas("costs.productOrders.user", function ($q) use ($userWhere) {
                        $q->where($userWhere);
                    })
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            }

        } else {

            if (!empty($productIdWhere)) {
                $productOrdersInstance = Product::
                where($productIdWhere)
                    ->has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }])
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            } else {
                $productOrdersInstance = Product::has("costs.productOrders")
                    ->with(["costs.productOrders" => function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    }])
                    ->whereHas('costs.productOrders', function ($q) use ($paid_at) {
                        $q->where($paid_at);
                    })
                    ->get();
            }

        }

        if (!($productOrdersInstance instanceof Product)) {
            if ($productOrdersInstance->isEmpty()) {
                return false;
            }
            $finalResult = $this->createFinalArray($productOrdersInstance);
        } else {
//            dd($productOrdersInstance->costs->toArray());
            $finalResult = [];
            $finalResult["product_id"] = $productOrdersInstance->id;
            $finalResult["name"] = $productOrdersInstance->name;
            $finalResult["title"] = $productOrdersInstance->title;
            $finalResult["description"] = $productOrdersInstance->description;
            $image = Product::find($productOrdersInstance->id)->attaches;
            if (isset($image)) {
                $finalResult["image_url"] = $image->src;
            }
            $finalResult["costs"] = [];
            foreach ($productOrdersInstance->costs as $costInstance) {
                $cost["cost_id"] = $costInstance->id;
                $cost["cost"] = $costInstance->cost;
                $cost["cost_id"] = $costInstance->id;
                $quantity = 0;
                foreach ($costInstance->productOrders as $productOrderInstance) {
//                    dd($productOrderInstance->toArray());
                    $quantity += $productOrderInstance->pivot->quantity;
                }
                $cost["quantity"] = $quantity;
                array_push($finalResult["costs"], $cost);
            }
        }
        return collect($finalResult);
//        return $productOrdersInstance;
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function get($id)
    {
        $productInstance = Product::where("id", $id)->first();
        if ($productInstance instanceof Product) {
            return $productInstance;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Product[]
     */
    public function minCost()
    {
        $instances = Product::with("costs")->get();
        $minvalue = $instances->costs;
        dd($minvalue);
        return $instances;
    }

    /**
     * @param $productOrdersInstance
     * @return array
     */
    private function createFinalArray($productOrdersInstance)
    {
        $finalResult = [];
        foreach ($productOrdersInstance as $productOrderInstance) {
            $product["product_id"] = $productOrderInstance->id;
            $product["name"] = $productOrderInstance->name;
            $product["title"] = $productOrderInstance->title;
            $product["description"] = $productOrderInstance->description;
            $image = Product::find($productOrderInstance->id)->attaches;
            if (isset($image)) {
                $product["image_url"] = $image->src;
            }
            $product["costs"] = [];
            $costs = [];
            foreach ($productOrderInstance->costs as $costInstance) {
//            dd($costInstance->toArray());
                $costs["cost_id"] = $costInstance->id;
                $costs["cost"] = $costInstance->cost;

                $wallet = Wallet::find($costInstance->wallet_id)->title;
                $costs["wallet"] = $wallet;
                $quantity = 0;
                foreach ($costInstance->productOrders as $productOrder) {
                    $quantity += $productOrder->pivot->quantity;
                }
                $costs["quantity"] = $quantity;

                array_push($product["costs"], $costs);
            }

            array_push($finalResult, $product);
        }
        return $finalResult;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["merchant_id"])) {
            $filter["merchant_id"] = $data["merchant_id"];
        } else {
            $filter["merchant_id"] = \Auth::user()->merchant_id;
        }
        if (isset($data["name"])) {
            $filter["name"] = $data["name"];
        }
        if (isset($data["title"])) {
            $filter["title"] = $data["title"];
        }
        if (isset($data["description"])) {
            $filter["description"] = $data["description"];
        }
        if (isset($data["status"])) {
            $filter["status"] = $data["status"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }
}
