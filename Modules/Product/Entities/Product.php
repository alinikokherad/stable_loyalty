<?php

namespace Modules\Product\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Attachment\Entities\Attachment;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Entities\Wallet;

/**
 * Modules\Product\Entities\Product
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\ProductAttribute[] $Attributes
 * @property-read int|null $attributes_count
 * @property-read \Modules\Attachment\Entities\Attachment $attaches
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Cost[] $costs
 * @property-read int|null $costs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\ProductOrder[] $productOrders
 * @property-read int|null $product_orders_count
 * @property-read \Modules\Product\Entities\ProductStore $productStore
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Product query()
 * @mixin \Eloquent
 */
class Product extends Model
{
    public $validate = [
        "name" => "required",
    ];
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function productOrders()
    {
        return $this->hasManyThrough(ProductOrder::class, Cost::class, "product_id", "order_id", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tags_products');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'products_categories', 'product_id', 'cat_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Attributes()
    {
        return $this->belongsToMany(ProductAttribute::class, 'product_value', 'product_id', 'attribute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productStore()
    {
        return $this->belongsTo(ProductStore::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function costs()
    {
        return $this->hasOne(Cost::class, "product_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function attaches()
    {
        return $this->morphOne(Attachment::class, "attachable");
    }

    /**
     * @return mixed
     */
    public function products()
    {
//        $wallet_id = Wallet::where("type", "point")->first()->id;
        return $this->hasOne(Cost::class, "product_id")->latest();
    }
}
