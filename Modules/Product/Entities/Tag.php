<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Product\Entities\Tag
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Tag query()
 * @mixin \Eloquent
 */
class Tag extends Model
{
    public $validate = [
        "name" => "required",
    ];
    protected $fillable = ['name'];
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany(Category::class,'tags_categories','tag_id','cat_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'tags_products','tag_id','product_id');
    }
}
