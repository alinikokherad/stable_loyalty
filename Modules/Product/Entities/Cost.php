<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Entities\Wallet;

/**
 * Modules\Product\Entities\Cost
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\ProductOrder[] $ProductOrders
 * @property-read int|null $product_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $account
 * @property-read int|null $account_count
 * @property-read \Modules\Product\Entities\Product $product
 * @property-read \Modules\Wallet\Entities\Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Cost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Cost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Cost query()
 * @mixin \Eloquent
 */
class Cost extends Model
{
    protected $fillable = [
        "type",
        "wallet_id",
        "cost",
        "product_id",
    ];

    public function ProductOrders()
    {
        return $this->belongsToMany(
            ProductOrder::class,
            "order_cost",
            "cost_id",
            "order_id"
        )->withPivot("quantity", "title", "description", "revoked");
    }

    public function account()
    {
        return $this->hasManyThrough(Account::class, Wallet::class);
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
