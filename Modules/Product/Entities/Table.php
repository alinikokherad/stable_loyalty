<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Branch\Entities\Merchant;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\ProductOrder;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Table whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Table extends Model
{
    protected $guarded = [];

    public function orders()
    {
        return $this->belongsToMany(Order::class, "order_table", "table_id", "order_id");
    }

    public function productOrders()
    {
        return $this->belongsToMany(ProductOrder::class, "order_table", "table_id", "order_id");
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }
}
