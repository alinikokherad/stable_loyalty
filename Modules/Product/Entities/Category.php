<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Attachment\Entities\Attachment;
use Modules\Branch\Entities\Merchant;

/**
 * Modules\Product\Entities\Category
 *
 * @property-read \Modules\Attachment\Entities\Attachment $attaches
 * @property-read \Modules\Product\Entities\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Tag[] $tags
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\Category query()
 * @mixin \Eloquent
 */
class Category extends Model
{
    public $validate = [
        "name" => "required",
        "description" => "required",
        "parent_id" => "required",
        "status" => "required",
    ];
    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(Product::class, "products_categories", "cat_id", "product_id");
    }

    public function category()
    {
        return $this->hasOne(Category::class, "parent_id");
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,'tags_categories','cat_id','tag_id');
    }

    public function attaches()
    {
        return $this->morphOne(Attachment::class, "attachable");
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }
}
