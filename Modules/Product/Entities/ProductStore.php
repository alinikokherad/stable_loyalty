<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Product\Entities\ProductStore
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\ProductStore newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\ProductStore newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\ProductStore query()
 * @mixin \Eloquent
 */
class ProductStore extends Model
{
    public $validate = [
        "product_id" => "required",
        "product_quantity" => "required",
        "unitValue" => "required",
    ];
    protected $fillable = [
        'product_id',
        'product_quantity',
        'unitValue',
    ];

    protected $table='product_stores';

    public function products()
    {
        return $this->hasMany(Product::class,'store_id');
    }
}
