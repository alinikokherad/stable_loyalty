<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Product\Entities\ProductAttribute
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\ProductAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\ProductAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Entities\ProductAttribute query()
 * @mixin \Eloquent
 */
class ProductAttribute extends Model
{
    public $validate = [
        "revoked" => "required",
        "author" => "required",
        "attribute" => "required",
    ];
    protected $table = 'product_attribute';
    protected $fillable = [
        'revoked',
        'author',
        'attribute',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'product_value','attribute_id','product_id');
    }
}
