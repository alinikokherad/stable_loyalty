<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Product\Entities\Product::class, function (Faker $faker) {
    return [
        //
        "name" => $faker->name,
        "description" => $faker->text(100),
        "img" => $faker->imageUrl(150,100),
        "price" => $faker->randomFloat(2,1000,10000),
        "status" => $faker->boolean(80),
    ];
});
