<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Product\Entities\Category::class, function (Faker $faker) {
    return [
        //
        "name" =>$faker->name,
        "description" =>$faker->text(100),
        "status" =>$faker->boolean(80),
    ];
});
