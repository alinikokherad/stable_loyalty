<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Product\Entities\Tag::class, function (Faker $faker) {
    return [
        //
        "name" => $faker->name,
    ];
});
