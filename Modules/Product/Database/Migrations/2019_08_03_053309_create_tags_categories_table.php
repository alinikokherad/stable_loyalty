<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags_categories', function (Blueprint $table) {
            $table->bigInteger('tag_id')->unsigned();
//            $table->foreign('tag_id')->references('id')->on('tags');
            $table->bigInteger('cat_id')->unsigned();
//            $table->foreign('cat_id')->references('id')->on('categories');
            $table->primary(['tag_id',"cat_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_categories');
    }
}
