<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_categories', function (Blueprint $table) {
            $table->bigInteger("cat_id")->unsigned();
//            $table->foreign('cat_id')->references('id')->on('categories');
            $table->bigInteger("product_id")->unsigned();
//            $table->foreign('product_id')->references('id')->on('products');
            $table->primary(['cat_id',"Product_id"]);
            $table->timestamp('expired_at')->nullable();
            $table->boolean("revoked")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_categories');
    }
}
