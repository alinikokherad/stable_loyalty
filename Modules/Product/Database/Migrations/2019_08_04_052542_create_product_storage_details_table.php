<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStorageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_storage_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('storage_id');
//            $table->foreign('storage_id')->references('id')->on("product_storage");
            $table->integer('sold');
            $table->integer('remain');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_storage_details');
    }
}
