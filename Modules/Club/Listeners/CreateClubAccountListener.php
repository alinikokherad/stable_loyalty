<?php

namespace Modules\Club\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Account\repo\AccountDB;
use Modules\Club\Events\CreatedClubEvent;
use Modules\Club\repo\ClubDB;

class CreateClubAccountListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CreatedClubEvent $event
     * @param AccountDB $accountDB
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle($event, AccountDB $accountDB)
    {
        //create credit account for club
        $creditAccountData = [
            "name" => "credit_treasury",
            "email" => "credit@treasury.com",
            "mobile" => "09120000000",
            "mobile_verified_at" => Carbon::now(),
            "password" => bcrypt("credit@treasury.com"),
            "status" => "active",
            "type" => "treasury",
        ];

        //create wallet account for club
        $walletAccountData = [
            "name" => "wallet_treasury",
            "email" => "wallet@treasury.com",
            "mobile" => "09120000001",
            "mobile_verified_at" => Carbon::now(),
            "password" => bcrypt("wallet@treasury.com"),
            "type" => "treasury",
        ];

        //create credit account for club
        $instanceCreditAccount = $accountDB->create($creditAccountData);
        if (!$instanceCreditAccount) {
            return \response()
                ->json([
                    "message" => "some things went wrong",
                    "error" => "bad request user is not created"
                ], 400);
        }

        //create wallet account for club
        $instanceWalletAccount = $accountDB->create($walletAccountData);
        if (!$instanceWalletAccount) {
            return \response()
                ->json([
                    "message" => "some things went wrong",
                    "error" => "bad request user is not created"
                ], 400);
        }

        //sync club with account
        $instanceCreditAccount->clubs()->sync([$event->clubInstance->id]);
        $instanceWalletAccount->clubs()->sync([$event->clubInstance->id]);
    }
}
