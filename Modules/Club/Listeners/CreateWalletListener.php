<?php

namespace Modules\Club\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Wallet\repo\WalletDB;

class CreateWalletListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CreateClubEvent $event
     * @param WalletDB $walletDB
     * @return mixed
     */
    public function handle(CreateClubEvent $event, WalletDB $walletDB)
    {
        $instanceAccount = $event->instanceClubAccount;
        //create wallet for club
        $walletData = [
            "account_id" => $instanceAccount->id,
            "title" => "point",
            "currency" => 0,
            "treasury_id" => 0,
        ];
        $walletInstance = $walletDB->create($walletData);
        if (!$walletInstance) {
            return $this->errorResponse();
        }
    }
}
