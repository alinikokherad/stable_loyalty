<?php

namespace Modules\Club\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Account\repo\CreditDB;

class CreateClubCreditListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CreateClubEvent $event
     * @param CreditDB $creditDB
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(CreateClubEvent $event, CreditDB $creditDB)
    {
        $instanceAccount = $event->instanceClubAccount;
        //create credit for club
        $creditData = [
            "account_id" => $instanceAccount->id,
            "amount" => 0,
            "treasury_id" => 0,
        ];
        $creditInstance = $creditDB->insert($creditData);
        if (!$creditInstance) {
            return response()
                ->json([
                    "message" => "some things went wrong"
                ], 400);
        }

    }
}
