<?php

namespace Modules\Club\Listeners;

use App\Events\CustomerRegisterEvent;
use App\repo\UserDB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\CreditDB;
use Modules\Wallet\repo\WalletDB;

class CreatedClubListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function handle($event)
    {
        //initials variable
        $clubInstance = $event->clubInstance;
        $data = $event->data;

        try {
            /*---------- create wallet && account type && user transaction ------------*/
            \DB::beginTransaction();
            switch ($data["club_mode"]) {
                case "reward_only":
                    //if club mode reward only create wallet and account type
//                    $this->rewardOnlyMethod($clubInstance);
                    break;
                default:
                    //create default wallet and account type
//                    $this->rewardOnlyMethod($clubInstance);
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw new \Exception($e->getMessage(), 400);
        }

    }

    /**
     * @param $clubInstance
     */
    private function rewardOnlyMethod($clubInstance)
    {

        $clubDB = new ClubDB();
        $userDB = new UserDB();
        $walletDB = new WalletDB();
        $accountDB = new AccountDB();
        $accountTypeDB = new AccountTypeDB();

        //TODO اگه لازم نیست که هر کلاب یک کاربر داشته باشد باید این تیکه از کد پاک شود
        //create user for club
        $userData = [
            "name" => $clubInstance->name,
            "email" => $clubInstance->name . '@club.com',
            "mobile" => null,
            "password" => bcrypt(123),
            "type" => "club_admin",
            "author_id" => null,
            "family" => "",
            "merchant_id" => "",
            "pinCode" => "",
            "degree" => "",
            "mobile_verified_at" => "",
            "birthday" => "",
            "gender" => "",
            "otp" => "",
            "tier_id" => "",
        ];
        $userInstance = $userDB->create($userData);

        //sync user with club
        $userInstance->clubs()->sync([$clubInstance->id]);

        //TODO برای کاربر پیشفرض باشگاه یک نوع اکانت و یک اکانت ایجاد شود(آیا یک کاربر باید ایجاد شود یا یک حساب کاربری؟)
        //get wallet id for create club account_type
        /*       $walletInstances = $walletDB->list();

               //create account_type for club
               $accountTypeData = [
                   "type" => "club",
                   "wallet_id" => 2,
                   "title" => "club",
                   "subtitle" => "subtitle",
                   "description" => "description",
                   "balance_type" => "ziro",
                   "min_account_amount" => 0,
                   "max_account_amount" => 0,
                   "min_transaction_amount" => 0,
                   "max_transaction_amount" => 0,
                   "legal" => false,
                   "interest_rate" => 1,
                   "interest_period" => "daily",
               ];
               $accountTypeInstance = $accountTypeDB->create($accountTypeData);

               //call all wallet
               foreach ($walletInstances as $walletInstance) {
                   $treasuryInstance = $accountDB->getTreasuryAccount($walletInstance->id);
                   //create account for club
                   $accountData = [
                       "user_id" => null,
                       "treasury_id" => $treasuryInstance->id,
                       "account_type_id" => $accountTypeInstance->id,
                   ];
                   $clubAccountInstance = $accountDB->create($data);
               }


               //create two wallet and its stuff
               foreach ($walletData as $item) {

                   //create wallet
                   $walletInstance = $walletDB->create($item);

                   //create account type if wallet created successfully
                   if ($walletInstance) {
                       //create account type
                       $accountTypeData = [
                           "type" => "treasury",
                           "wallet_id" => $walletInstance->id,
                           "title" => "IRR",
                           "subtitle" => "for test",
                           "description" => "for test description",
                           "balance_type" => "ziro",
                           "min_account_amount" => 0,
                           "max_account_amount" => 0,
                           "min_transaction_amount" => 0,
                           "max_transaction_amount" => 0,
                           "legal" => false,
                           "interest_rate" => 1,
                           "interest_period" => 1,
                           "revoked" => false,
                       ];
                       $accountTypeInstance = $accountTypeDB->create($accountTypeData);

                       //create account for treasury
                       $accountData = [
                           "account_type_id" => $accountTypeInstance->id,
                           "user_id" => $userInstance->id,
                           "min_transaction" => 0,
                           "revoked" => false,
                       ];
                       $instanceAccount = $accountDB->create($accountData);

                       //treasury account credit
                       $creditData = [
                           "account_id" => $instanceAccount->id,
                           "wallet_id" => $walletInstance->id,
                           "treasury_id" => 0,
                           "club_id" => $clubInstance->id,
                           "amount" => 0,
                           "usable_at" => null,
                           "expired_at" => null,
                           "revoked" => false,
                       ];
                       $creditDB->create($creditData);

                   }

    }*/
    }
}
