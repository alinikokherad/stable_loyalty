<?php

namespace Modules\Club\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClubRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "string|required|max:50|min:3",
            "code" => "numeric|max:12|nullable",
            "exchange_rate" => "numeric|nullable",
            "club_mode" => "required",
            "point_to_card" => "boolean|nullable",
            "good_lock_delay" => "boolean|nullable",
            "good_lock_point" => "numeric|nullable",
            "good_lock_date" => "nullable",
            "delay_penalty" => "numeric|nullable",
            "give_point_at" => "nullable",
            "point_expired_at" => "nullable",
            "can_edit_point" => "boolean|nullable",
            "can_refund" => "boolean|nullable",
            "can_refund_until" => "boolean|nullable",
            "can_cash_out_by_admin" => "boolean|nullable",
            "can_cash_out" => "boolean|nullable",
            "show_point" => "boolean|nullable",
            "generate_card_immediately" => "boolean|nullable",
            "card_expired_at" => "nullable",
            "gift_card_expired_at" => "nullable",
            "show_revocation_card_for_admin" => "boolean|nullable",
            "minimum_shop_to_use_point" => "numeric|nullable",
            "free_shiping" => "boolean|nullable",
            "minimum_wage" => "numeric|nullable",
            "maximum_wage" => "numeric|nullable",
            "card_type" => "nullable",
            "checkout_time" => "nullable",
            "wallet_id" => "numeric|nullable"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
