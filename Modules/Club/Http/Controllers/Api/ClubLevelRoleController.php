<?php

namespace Modules\Club\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Club\Entities\ClubLevelRole;
use Modules\Club\Transformers\ClubLevelRoleResource;

class ClubLevelRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return mixed
     */
    public function index()
    {
        $limit = \request()->has("limit") ? \request()->input("limit") : 5;
        $clubs = ClubLevelRole::paginate($limit);
        $resource = ClubLevelRoleResource::collection($clubs);
        return $resource;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $instance = ClubLevelRole::create($data);
        if ($instance instanceof ClubLevelRole) {
            return \response()
                ->json([
                    "message" => "ClubLevelRole successfully create",
                    "data" => $instance
                ], 200);
        }
        return \response()
            ->json([
                "message" => "try again",
                "error" => "bad request"
            ], 400);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return mixed
     */
    public function show($id)
    {
        $clubLevel = ClubLevelRole::find($id);
        $resource = new ClubLevelRoleResource($clubLevel);
        return $resource;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return mixed
     */
    public function destroy($id)
    {
        ClubLevelRole::where("id", $id)->delete();
        return \response()
            ->json([
                null
            ], 204);
    }
}
