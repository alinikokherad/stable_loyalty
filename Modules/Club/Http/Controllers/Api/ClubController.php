<?php

namespace Modules\Club\Http\Controllers\Api;

use App\Traits\Response;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Club\Entities\Club;
use Modules\Club\Events\CreatedClubEvent;
use Modules\Club\Http\Requests\ClubRequest;
use Modules\Club\repo\ClubDB;
use Modules\Club\Transformers\ClubResource;

class ClubController extends Controller
{
    use Response;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = \request()->has("type") ? \request("type") : null;
        $limit = \request()->has("limit") ? \request()->input("limit") : 5;

        $user_id = auth()->id();
        if (is_null($user)) {
            $clubs = Club::paginate($limit);
        } else {
            $clubs = Club::with('users')->whereHas('users', function ($q) use ($user_id) {
                $q->where('user_id', $user_id);
            })->get();
        }
        $resource = ClubResource::collection($clubs);
        return response()
            ->json([
                "message" => __("messages.club_information"),
                "data" => $resource->paginate($limit, 1),
            ], 200);
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $clubDB = new ClubDB();
        $data = $request->all();
        try {
            /*-----create club transaction----*/
            \DB::beginTransaction();

            //create club
            $clubInstance = $clubDB->create($data);
            if (!$clubInstance) {
                return $this->errorResponse("400", __("messages.club_not_created"));
            }

            //after club create this event happen
//            event(new CreatedClubEvent($clubInstance, $data));

            //commit transaction
            \DB::commit();
            return $this->successResponse("200", __("messages.club_successfully_created"), 201);
        } catch
        (\Exception $e) {
            //rollback database
            \DB::rollBack();
            return $this->errorResponse("400", $e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param $id
     * @param ClubDB $clubDB
     * @return \Illuminate\Http\JsonResponse|ClubResource
     */
    public function show($id, ClubDB $clubDB)
    {
        $club = $clubDB->show($id);
        if ($club) {
            $resource = new ClubResource($club);
            return $resource;
        }
        return \response()
            ->json([
                "message" => __("messages.400"),
                "error" => __("messages.bad_request")
            ], 400);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Club::where("id", $id)->delete();
        return \response()
            ->json([
                null
            ], 204);
    }
}
