<?php

namespace Modules\Club\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Account\Entities\Account;
use Modules\Account\repo\CreditDB;
use Modules\Club\Entities\Club;

class ClubBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param CreditDB $creditDB
     * @param Request $request
     * @return mixed
     * @throws
     */
    public function store(Request $request, CreditDB $creditDB)
    {
        $data = $request->all();
        $data["settings"] = json_encode($request->input("settings"));
        \DB::transaction(function () use ($data, $request, $creditDB) {
            //create account for club
            $accountData = [
                "name" => $data["name"],
                "email" => "{$data["name"]}@treasury.com",
                "mobile" => "09120000000",
                "mobile_verified_at" => Carbon::now(),
                "password" => bcrypt("{$data["name"]}@treasury.com"),
                "settings" => null,
                "is_treasury" => true,
                "status" => 1,
//            "club_id" => $instance->id,
                "treasury_id" => 0,
                "parent_id" => 0,
            ];
            $instanceAccount = Account::create($accountData);
            if (!($instanceAccount instanceof Account)) {
                return \response()
                    ->json([
                        "message" => __("messages.try_again"),
                        "error" => __("messages.bad_request")
                    ], 400);
            }

            //create club
            $data["account_id"] = $instanceAccount->id;
            $instance = Club::create($data);
            if (!($instance instanceof Club)) {
                return \response()
                    ->json([
                        "message" => __("messages.try_again"),
                        "error" => __("messages.bad_request")
                    ], 400);
            }

            //sync club with account
            $instance->accounts()->sync([$accountData->id]);

            //create credit for club
            $creditData = [
                "account_id" => $instanceAccount->id,
                "amount" => 0,
                "treasury" => 0,
                "wallet_id" => 0,
            ];
            $credit = $creditDB->insert($creditData);
            if ($credit instanceof Credit) {
                $instance["settings"] = json_decode($instance->settings);
                return \response()
                    ->json([
                        "message" => __("messages.club_create"),
                        "data" => $instance
                    ], 200);
            }
        });
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
