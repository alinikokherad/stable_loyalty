<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Club\Entities\Club::class, function (Faker $faker) {
    return [
        "name" => "loviuona",
        "code" => $faker->randomNumber(2),
        "exchange_rate" => 1,
        "club_mode" => "reward_only",
        "point_to_card" => false,
        "good_lock_delay" => false,
        "good_lock_point" => 1,
        "good_lock_date" => null,
        "delay_penalty" => 1,
        "give_point_at" => "immediately",
        "point_expired_at" => null,
        "can_edit_point" => false,
        "can_refund" => true,
        "can_refund_until" => null,
        "can_cash_out_by_admin" => false,
        "can_cash_out" => false,
        "show_point" => true,
        "generate_card_immediately" => false,
        "card_expired_at" => null,
        "gift_card_expired_at" => null,
        "show_revocation_card_for_admin" => false,
        "minimum_shop_to_use_point" => 1,
        "free_shiping" => false,
        "minimum_wage" => 1,
        "maximum_wage" => 1,
        "card_type" => "magnet",
        "checkout_time" => "daily",
    ];
});
