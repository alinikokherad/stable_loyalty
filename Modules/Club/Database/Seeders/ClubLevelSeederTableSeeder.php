<?php

namespace Modules\Club\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Club\Entities\ClubLevel;

class ClubLevelSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        ClubLevel::truncate();
        factory(ClubLevel::class, 20)->create();
        // $this->call("OthersTableSeeder");
    }
}
