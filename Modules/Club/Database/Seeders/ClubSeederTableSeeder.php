<?php

namespace Modules\Club\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Club\Entities\Club;

class ClubSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Club::truncate();
        factory(Club::class, 1)->create();
        // $this->call("OthersTableSeeder");
    }
}
