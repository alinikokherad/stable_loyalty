<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaitingCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiting_cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum("waiting_type", ["waiting_to_create", "sending_to_branch", "delivering_to_member", "request_for_check_out"])->nullable();
            $table->string("name")->nullable();
            $table->string("username", 12)->comment("mobile number");
            $table->string("card_number");
            $table->enum("level", [1, 2, 3, 4, 5]);
            $table->boolean("payment_status")->nullable();
            $table->string("card_type");
            $table->string("gift_text");
            $table->bigInteger("request_amount");
            $table->timestamp("request_date");
            $table->string("icon");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiting_cards');
    }
}
