<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("club_id");
            $table->string("level");
            $table->string('title');
            $table->string('sub_title')->nullable();
            $table->boolean('cash_back')->nullable();
            $table->enum('discount_type', ["fixed", "percent"])->nullable();
            $table->float('discount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_levels');
    }
}
