<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_accounts', function (Blueprint $table) {
            $table->unsignedBigInteger("club_id");
            $table->unsignedBigInteger("account_id")->nullable();
            $table->enum("type", ["merchant", "customer"]);
            $table->timestamp("expired_at");
            $table->boolean("revoked")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_accounts');
    }
}
