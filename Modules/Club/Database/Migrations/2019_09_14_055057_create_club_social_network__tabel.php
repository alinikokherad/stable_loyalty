<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubSocialNetworkTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_social_networks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("club_id");
            $table->enum('link', ["facebook", 'instagram', 'linked_in', 'pinterest', 'telegram']);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_social_networks');
    }
}
