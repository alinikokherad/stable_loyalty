<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubXChangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_x_change_rates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger("wallet_id");
//            $table->foreign("wallet_id")->references("id")->on("wallets");

            $table->integer("treasury1");
            $table->bigInteger("amount1");
            $table->integer("treasury2");
            $table->bigInteger("amount2");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_x_change_rates');
    }
}
