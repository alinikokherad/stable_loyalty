<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum("reception", ["all", "branch"]);
            $table->enum("notice_type", ["notification", "sms", "email"]);
            $table->string("title")->nullable();
            $table->text("description");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_branches');
    }
}
