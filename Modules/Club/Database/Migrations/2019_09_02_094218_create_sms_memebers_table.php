<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsMemebersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_memebers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum("reception", ["all", "club_level", "branch", "member"]);
            $table->enum("notice_type", ["notification", "sms", "email"]);
            $table->text("description");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_memebers');
    }
}
