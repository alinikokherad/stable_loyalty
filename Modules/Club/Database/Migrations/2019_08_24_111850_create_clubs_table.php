<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name")->nullable();
            $table->tinyInteger("code");
            $table->smallInteger("priority")->default(0);
            $table->integer("exchange_rate_point")->default(1);
            $table->bigInteger("exchange_rate_price")->default(1);
            $table->enum("club_mode", ["reward_only", "discount", "cash_back", "reward_discount", "reward_cash_back"])->default("reward_only");
            $table->boolean("point_to_card")->nullable();
            $table->boolean("good_lock_delay")->nullable();
            $table->bigInteger("good_lock_point")->nullable();
            $table->timestamp("good_lock_date")->nullable();
            $table->integer("delay_penalty")->nullable();
            $table->enum("give_point_at", ["immediately", "one_day_later", "one_month_later"])->nullable();
            $table->timestamp("point_expired_at")->nullable();
            $table->boolean("can_edit_point")->nullable();
            $table->boolean("can_refund")->default(true);
            $table->timestamp("can_refund_until")->nullable();
            $table->boolean("can_cash_out_by_admin")->nullable();
            $table->boolean("can_cash_out")->default(false);
            $table->boolean("show_point")->default(false);
            $table->boolean("generate_card_immediately")->default(false);
            $table->timestamp("card_expired_at")->nullable();
            $table->timestamp("gift_card_expired_at")->nullable();
            $table->boolean("show_revocation_card_for_admin")->default(false);
            $table->integer("minimum_shop_to_use_point")->nullable();
            $table->boolean("free_shiping")->default(false);
            $table->float("minimum_wage")->nullable();
            $table->float("maximum_wage")->nullable();
            $table->enum("card_type", ["mifare", "magnet"])->default("magnet");
            $table->enum("checkout_time", ["daily", "mid_term", "monthly", "seasonal"])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
