<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubLevelRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_level_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('club_level_id');
            $table->enum("type", ["earn_point_to_now", "point", "first_shop", "last_shop", "count_shop"]);
            $table->enum("operator", ["==", "<=", ">=", "<>"]);
            $table->text("description");
            $table->tinyInteger("level");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_level_roles');
    }
}
