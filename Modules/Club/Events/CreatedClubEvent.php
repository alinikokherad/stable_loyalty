<?php

namespace Modules\Club\Events;

use Illuminate\Queue\SerializesModels;

class CreatedClubEvent
{
    use SerializesModels;
    public $clubInstance;
    public $data;

    /**
     * CreateClubEvent constructor.
     * @param $clubInstance
     * @param $data
     */
    public function __construct($clubInstance, $data)
    {
        $this->clubInstance = $clubInstance;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
