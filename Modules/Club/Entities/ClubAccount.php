<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubAccount query()
 */

use Illuminate\Database\Eloquent\Model;

class ClubAccount extends Model
{
    public $validate = [
    ];
    protected $table = "club_accounts";
    protected $fillable = [
        "club_id",
        "account_id",
        "type",
        "expired_at",
        "revoked",
    ];
}
