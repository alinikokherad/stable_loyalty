<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\MessageType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\MessageType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\MessageType query()
 */

use Illuminate\Database\Eloquent\Model;

class MessageType extends Model
{
    public $validate = [
        "type" => "required",
    ];
    protected $table = "message_types";
    protected $fillable = [
        "type"
    ];
}
