<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\WaitingCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\WaitingCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\WaitingCard query()
 */

use Illuminate\Database\Eloquent\Model;

class WaitingCard extends Model
{
    public $validate = [
        "waiting_type" => "required",
        "name" => "required",
        "username" => "required",
        "card_number" => "required",
        "level" => "required",
        "payment_status" => "required",
        "card_type" => "required",
        "gift_text" => "required",
        "request_amount" => "required",
        "request_date" => "required",
        "icon" => "required",
    ];

    protected $fillable = [
        "waiting_type",
        "name",
        "username",
        "card_number",
        "level",
        "payment_status",
        "card_type",
        "gift_text",
        "request_amount",
        "request_date",
        "icon",
    ];
}
