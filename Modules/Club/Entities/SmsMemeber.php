<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\SmsMemeber newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\SmsMemeber newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\SmsMemeber query()
 */

use Illuminate\Database\Eloquent\Model;

class SmsMemeber extends Model
{
    public $validate = [
    ];
    protected $fillable = [];
}
