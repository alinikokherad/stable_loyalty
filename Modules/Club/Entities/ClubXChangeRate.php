<?php

namespace Modules\Club\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Wallet\Entities\Wallet;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubXChangeRate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClubXChangeRate extends Model
{
    protected $fillable = [];

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }
}
