<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $accounts
 * @property-read int|null $accounts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Campaign[] $campaigns
 * @property-read int|null $campaigns_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Club\Entities\ClubAccount[] $clubAccounts
 * @property-read int|null $club_accounts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Club\Entities\ClubLevel[] $clubLevels
 * @property-read int|null $club_levels_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\Club newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\Club newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\Club query()
 */

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Branch\Entities\Merchant;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Campaign;
use Modules\Wallet\Entities\Wallet;

class Club extends Model
{
    public $validate = [
        "name" => "required",
        "code" => "required",
        "image" => "required",
        "exchange_rate_point" => "required",
        "exchange_rate_price" => "required",
        "club_mode" => "required",
        "point_to_card" => "required",
        "good_lock_delay" => "required",
        "good_lock_point" => "required",
        "good_lock_date" => "required",
        "delay_penalty" => "required",
        "give_point_at" => "required",
        "point_expired_at" => "required",
        "can_edit_point" => "required",
        "can_refund" => "required",
        "can_refund_until" => "required",
        "can_cash_out_by_admin" => "required",
        "can_cash_out" => "required",
        "show_point" => "required",
        "title_point" => "required",
        "generate_card_immediately" => "required",
        "card_expired_at" => "required",
        "gift_card_expired_at" => "required",
        "show_revocation_card_for_admin" => "required",
        "minimum_shop_to_use_point" => "required",
        "free_shiping" => "required",
        "minimum_wage" => "required",
        "maximum_wage" => "required",
        "card_type" => "required",
        "checkout_time" => "required",
        "parent_id" => "required",
    ];
    protected $guarded = [];

    /*    protected $fillable = [
            "name",
            "code",
            "exchange_rate_point",
            "exchange_rate_price",
            "club_mode",
            "point_to_card",
            "good_lock_delay",
            "good_lock_point",
            "good_lock_date",
            "delay_penalty",
            "give_point_at",
            "point_expired_at",
            "can_edit_point",
            "can_refund",
            "can_refund_until",
            "can_cash_out_by_admin",
            "can_cash_out",
            "show_point",
            "title_point",
            "generate_card_immediately",
            "card_expired_at",
            "gift_card_expired_at",
            "show_revocation_card_for_admin",
            "minimum_shop_to_use_point",
            "free_shiping",
            "minimum_wage",
            "maximum_wage",
            "card_type",
            "checkout_time",
        ];*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function merchants()
    {
        return $this->belongsToMany(Merchant::class, "club_merchant", "club_id", "merchant_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubLevels()
    {
        return $this->hasMany(ClubLevel::class, "club_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, "user_club", "club_id", "user_id")->withPivot("access");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    /*    public function accounts()
        {
            return $this->belongsToMany(Account::class, "club_accounts", "club_id", "account_id")->withPivot("type", "expired_at", "revoked");
        }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class, "club_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    //TODO what is this????
    public function clubAccounts()
    {
        return $this->hasMany(ClubAccount::class, "club_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function wallets()
    {
        return $this->belongsToMany(Wallet::class, "club_wallet", "club_id", "wallet_id");
    }
}
