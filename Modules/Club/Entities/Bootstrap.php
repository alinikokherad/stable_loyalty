<?php

namespace Modules\Club\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Club\Entities\Bootstrap
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\Bootstrap newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\Bootstrap newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\Bootstrap query()
 * @mixin \Eloquent
 */
class Bootstrap extends Model
{
    protected $guarded = [];
}
