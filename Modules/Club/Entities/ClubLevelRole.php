<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Modules\Club\Entities\ClubLevel $clubLevel
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubLevelRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubLevelRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubLevelRole query()
 */

use Illuminate\Database\Eloquent\Model;

class ClubLevelRole extends Model
{
    public $validate = [
        "club_level_id" => "required",
        "type" => "required",
        "operator" => "required",
        "level" => "required",
    ];
    protected $fillable = [
        "club_level_id",
        "type",
        "operator",
        "level",
    ];

    public function clubLevel()
    {
        return $this->belongsTo(ClubLevel::class);
    }
}
