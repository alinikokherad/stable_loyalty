<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\SmsBranch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\SmsBranch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\SmsBranch query()
 */

use Illuminate\Database\Eloquent\Model;

class SmsBranch extends Model
{
    public $validate = [
        "reception" => "required",
        "notice_type" => "required",
        "title" => "required",
        "description" => "required",
    ];
    protected $fillable = [
        "reception",
        "notice_type",
        "title",
        "description",
    ];
}
