<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $accounts
 * @property-read int|null $accounts_count
 * @property-read \Modules\Club\Entities\Club $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Club\Entities\ClubLevelRole[] $clubLevelRoles
 * @property-read int|null $club_level_roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\ClubLevel query()
 */

use Illuminate\Database\Eloquent\Model;
use Modules\Wallet\Entities\Account;

class ClubLevel extends Model
{
    public $validate = [
        "club_id" => "required",
        "level" => "required",
        "title" => "required",
        "sub_title" => "required",
        "cash_back" => "required",
        "discount_type" => "required",
        "discount" => "required",
    ];
    protected $fillable = [
        "club_id",
        "level",
        "title",
        "sub_title",
        "cash_back",
        "discount_type",
        "discount",
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function clubLevelRoles()
    {
        return $this->hasMany(ClubLevelRole::class, "club_level_id");
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, "club_level_id");
    }
}
