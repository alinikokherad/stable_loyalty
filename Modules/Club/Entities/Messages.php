<?php

namespace Modules\Club\Entities;

/**
 * Modules\Club\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\messages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\messages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Club\Entities\messages query()
 */

use Illuminate\Database\Eloquent\Model;

class messages extends Model
{

    public $validate = [
        "title" => "required",
        "subtitle" => "required",
        "description" => "required",
        "status" => "required",
    ];

    protected $fillable = [
        "title",
        "subtitle",
        "description",
        "status",
    ];
}
