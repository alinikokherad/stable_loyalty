<?php

namespace Modules\Club\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ClubResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "image" => $this->image,
            "exchange_rate" => $this->exchange_rate,
            "settings" => $this->settings,
            "active_point_at" => $this->active_point_at,
            "point_expired_at" => $this->point_expired_at,
            "club_mode" => $this->club_mode,
            "can_edit_point" => $this->can_edit_point,
            "can_refund" => $this->can_refund,
            "can_cash_out" => $this->can_cash_out,
            "minimum_wage" => $this->minimum_wage,
            "maximum_wage" => $this->maximum_wage,
            "card_type" => $this->card_type,
            "checkout_time" => $this->checkout_time,
        ];
    }
}
