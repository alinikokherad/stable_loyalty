<?php

namespace Modules\Club\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ClubLevelResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "club_id" => $this->club_id,
            "level" => $this->level,
            "title" => $this->title,
            "sub_title" => $this->sub_title,
            "cash_back" => $this->cash_back,
            "discount_type" => $this->discount_type,
            "discount" => $this->discount,
        ];
    }
}
