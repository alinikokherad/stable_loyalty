<?php

namespace Modules\Club\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ClubLevelRoleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "club_level_id" => $this->club_level_id,
            "type" => $this->type,
            "operator" => $this->operator,
            "level" => $this->level,
        ];
    }
}
