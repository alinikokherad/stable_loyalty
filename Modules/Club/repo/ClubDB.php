<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/17/19
 * Time: 6:07 PM
 */

namespace Modules\Club\repo;


use App\clubArriveRules;
use App\repo\UserDB;
use App\User;
use Carbon\Carbon;
use Modules\Branch\Entities\Merchant;
use Modules\Club\Entities\Club;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\repo\AccountDB;

/**
 * Class ClubDB
 * @package Modules\Club\repo
 */
class ClubDB
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function idList()
    {
        return Club::all()->pluck('id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Club[]
     */
    public function list()
    {
        return Club::all();
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $instance = Club::query()
            ->create($data);
        if ($instance instanceof Club) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function show($id)
    {
        $instance = Club::find($id);
        if ($instance) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $account_id
     * @param null $type
     * @return mixed
     */
    public function findUsersClubWithAccountId($account_id)
    {
        //attain to accounts user
        $accountDB = new AccountDB();
        $userInstance = $accountDB->getAccountsUser($account_id);

        //attain to users club
        $userDB = new UserDB();
        $clubInstance = $userDB->getUsersClub($userInstance->id);
        if (!$clubInstance) {
            return false;
        }
        return $clubInstance;
    }

    /**
     * @param $account_id
     * @param null $type
     * @return mixed
     */
    public function findMerchantsClubWithAccountId($account_id)
    {
        //attain to accounts user
        $accountDB = new AccountDB();
        $merchantInstance = $accountDB->getAccountsMerchant($account_id);

        //attain to users club
        $userDB = new UserDB();
        $clubInstance = $userDB->getMerchantsClub($merchantInstance->id);
        if (!$clubInstance) {
            return false;
        }
        return $clubInstance;
    }

    /**
     * @param $account_id
     * @param $clubId
     * @return bool
     */
    public function whereInClub($account_id, $clubId)
    {
        $accountInstance = Account::where("id", $account_id)
            ->with("clubs")
            ->first();

        $customerClubsIds = $accountInstance->clubs->pluck("id")->toArray();
        if (!in_array($clubId, $customerClubsIds)) {
            return false;
        }
        return true;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function findClubWithUserId($userId)
    {
        $instance = User::where("id", $userId)->with("clubs.campaigns.attaches")->first();
        return $instance->clubs->first();
    }

    /**
     * @param $merchant_id
     * @return bool
     */
    public function getClubWithMerchantID($merchant_id)
    {
        $instance = Merchant::where("id", $merchant_id)->with("clubs")->first();
        if ($instance instanceof Merchant) {
            return $instance->clubs->first();
        }
        return false;
    }

    /**
     * @param string $string
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getClubWithName(string $string)
    {
        $clubInstance = Club::query()->where("name", $string)->first();
        if ($clubInstance instanceof Club) {
            return $clubInstance;
        }
        return false;
    }

    /**
     * return free clubs
     * @return array
     */
    public function getFreeClubs()
    {
        $clubs = clubArriveRules::query()->whereNull("user_id")->get()->pluck("id")->toArray();
        if (is_array($clubs) && !empty($clubs)) {
            $response = [];
            foreach ($clubs as $club) {
                $response[$club] = ["access" => "subscriber", "created_at" => Carbon::now()->format("Y-m-d H:i:s"), "updated_at" => Carbon::now()->format("Y-m-d H:i:s")];
            }
            return $response;
        }
        return false;
    }

    /**
     * @param $account_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection\
     */
    public function findClubsWithAccountId($account_id)
    {
        $clubsInstances = Club::query()
            ->whereHas("users.accounts", function ($query) use ($account_id) {
            })
            ->orWhereHas("merchants.accounts", function ($query) use ($account_id) {
                $query->where("id", $account_id);
            })
            ->orderBy("priority", "desc")
            ->get();
        return $clubsInstances;
    }

    /**
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUsersClubsInstances($user_id)
    {
        $response = Club::query()
            ->whereHas("users", function ($query) use ($user_id) {
                $query->where("users.id", $user_id);
            })
            ->get();
        return $response;
    }

    /**
     * @param $merchant_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMerchantsClubsInstances($merchant_id)
    {
        $response = Club::query()
            ->WhereHas("merchants", function ($query) use ($merchant_id) {
                $query->where("merchants.id", $merchant_id);
            })
            ->get();
        return $response;
    }
}
