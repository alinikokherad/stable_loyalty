# Crud Sample

  - sample for crud
  
# New Features!

  - you can use filters and relations on list ! 

# Usage Sample

> 
> Filter and Relaions on User model
> you can use just one of theme (filters or Relations or)
>

```
{
    "filters": [
        [
            [
                "products.name",
                "like",
                "%تارت%"
            ],
            [
                "id",
                ">",
                "2"
            ],
            [
                "type",
                "like",
                "%customer%"
            ]
        ]
    ],
    "relations": [
        [
            "roles",
            "clubs"
        ]
    ]
}
```

>
> also you can use orWhere like blow
>
```
{
    "filters": [
        [
            [
                "products.name",
                "like",
                "%تارت%"
            ],
            [
                "id",
                ">",
                "2"
            ],
            [
                "type",
                "like",
                "%customer%"
            ]
        ],
        // or 
        [
            [
               "products.name",
                "like",
                "%تارت%"
        ],
            [
                "id",
                "=",
                "2"
            ]
        ]
    ]
    
    // it merge both result in paginate
}
```

# Other Feature
>
> use page_number and page_limit for list on params (for custome paginate)
>

Wish You Luck :) 
#MentaSystem
