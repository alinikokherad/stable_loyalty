<?php

namespace Modules\Wallet\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Queue;
use Modules\Wallet\Events\AfterCreatedOrderEvent;
use Modules\Wallet\Jobs\CreateTransactionJob;

class AfterCreatedOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     */
    public function handle($event)
    {
        //call job to create transaction
        $test = CreateTransactionJob::dispatchNow($event->orderInstance, $event->type);

//            ->onQueue("transaction")
//            ->delay(10);
    }
}
