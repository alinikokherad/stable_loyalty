<?php

namespace Modules\Wallet\Listeners;

use App\repo\UserDB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;
use Modules\Wallet\Http\Controllers\Api\OrderController;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\WalletDB;

class AfterPaymentSuccessListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @return bool|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function handle($event)
    {
        $orderController = new OrderController();
        $accountDB = new AccountDB();
        $userDB = new UserDB();
        $walletDB = new WalletDB();
        $request = $event->requestInstance;

        //get customer account instance
        $customerUserInstance = $userDB->get($request->customer_id, true);
        $customerAccountInstance = $accountDB->get($customerUserInstance->accounts->first()->id);

        //check user account type is rials
        $treasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType('rials');
        if ($customerAccountInstance->treasury_id != $treasuryAccountInstance->id) {
            return \response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => __("messages.invalid_account_type")
                ], 400);
        }
        $bankAccountInstance = $accountDB->get($request->bank_account_id);

        //check bank treasury_id with customer treasury_id
        if ($customerAccountInstance->treasury_id != $bankAccountInstance->treasury_id) {
            return \response()
                ->json([
                    "message" => __("messages.404"),
                    "error" => __("messages.treasury_not_match"),
                ], 400);
        }

        //create stuff for payment
//            $orderStuffInstance = $orderController->createOrderStuff($request->all());

        //create order
        $orderData = [
//                "goods_id" => $orderStuffInstance->id,
            "goods_id" => null,
            "from_account_id" => $request->bank_account_id,
            "to_account_id" => $customerAccountInstance->id,
            "amount" => $request->amount,
            "type" => "payment",
            "paid_at" => Carbon::now(),
            "treasury_account_id" => $customerAccountInstance->treasury_id,
            "author_id" => null,
        ];

        $orderInstance = $orderController->store($orderData, "bank");
        return $orderInstance;
    }
}
