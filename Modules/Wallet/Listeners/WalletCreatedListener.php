<?php

namespace Modules\Wallet\Listeners;

use App\Exceptions\CreateTreasuryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Club\Entities\messages;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\CreditDB;
use Modules\Wallet\repo\WalletDB;

class WalletCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @throws \Exception
     */
    public function handle($event)
    {
        $accountDB = new AccountDB();
        $walletDB = new WalletDB();
        $accountTypeDB = new AccountTypeDB();
        $creditDB = new CreditDB();
        $data = $event->data;

        try {
            \DB::beginTransaction();


            //define treasury account type
            $accountTypeData = [
                "type" => "treasury",
                "wallet_id" => $event->wallet->id,
                "title" => isset($data["title"]) ? $data["title"] : null,
                "subtitle" => isset($data["subtitle"]) ? $data["subtitle"] : null,
                "description" => isset($data["description"]) ? $data["description"] : null,
                "balance_type" => "negative",
                "min_account_amount" => 0,
                "max_account_amount" => 0,
                "min_transaction_amount" => 0,
                "max_transaction_amount" => 0,
                "legal" => false,
                "interest_rate" => 1,
                "interest_period" => 1,
                "revoked" => false,
            ];
            $treasuryInstanceTypeAccount = $accountTypeDB->create($accountTypeData);

            //create account for treasury
            $accountData = [
                "account_type_id" => $treasuryInstanceTypeAccount->id,
                "belongs_to" => $event->wallet->id,
                "belongs_type" => "wallet_treasury",
                "treasury_id" => null,
//                "min_transaction" => 0,
                "revoked" => false,
            ];
            $treasuryInstanceAccount = $accountDB->create($accountData);

            //treasury account credit
            $creditData = [
                "account_id" => $treasuryInstanceAccount->id,
//                "treasury_account_id" => null,
                "amount" => 0,
                "usable_at" => null,
                "expired_at" => null,
                "revoked" => false,
            ];
            $creditDB->create($creditData);

            /*------------------ create agent for current wallet ------------------*/
            //define agent account type for other wallets
            $accountTypeData = [
                "type" => "agent",
                "wallet_id" => $event->wallet->id,
                "title" => $event->wallet->type . $event->wallet->id,
                "subtitle" => isset($data["subtitle"]) ? $data["subtitle"] : null,
                "description" => isset($data["description"]) ? $data["description"] : null,
                "balance_type" => "negative",
                "min_account_amount" => 0,
                "max_account_amount" => 0,
                "min_transaction_amount" => 0,
                "max_transaction_amount" => 0,
                "legal" => false,
                "interest_rate" => 1,
                "interest_period" => 1,
                "revoked" => false,
            ];
            $agentInstanceTypeAccount = $accountTypeDB->create($accountTypeData);

            //get all clubs wallets instances
            $allWallets = $walletDB->getAllWalletWithoutMe($event->wallet->id);

            foreach ($allWallets as $wallet) {
                //create agent account(belongs to other wallet) for my wallet
                $agentAccountTypeInstance = $walletDB->getAgentAccountType($wallet->id);
                if ($agentAccountTypeInstance) {
                    $accountData = [
                        "account_type_id" => $agentAccountTypeInstance->id,
                        "belongs_to" => $event->wallet->id,
                        "belongs_type" => "wallet_agent",
                        "treasury_account_id" => $treasuryInstanceAccount->id,
                        "revoked" => false,
                    ];
                    $agentInstanceAccount = $accountDB->create($accountData);

                    //treasury account credit
                    $creditData = [
                        "account_id" => $agentInstanceAccount->id,
//                "treasury_account_id" => null,
                        "amount" => 0,
                        "usable_at" => null,
                        "expired_at" => null,
                        "revoked" => false,
                    ];
                    $creditDB->create($creditData);
                }


                //attain wallets treasury account
                $walletTreasuryAccount = $walletDB->getTreasuryAccountInstanceWithWalletId($wallet->id);

                //create agent account (belongs to me) for other wallets
                $accountData = [
                    "account_type_id" => $agentInstanceTypeAccount->id,
                    "belongs_to" => $wallet->id,
                    "belongs_type" => "wallet_agent",
                    "treasury_account_id" => $walletTreasuryAccount->id,
//                "min_transaction" => 0,
                    "revoked" => false,
                ];
                $agentInstanceAccount = $accountDB->create($accountData);

                //treasury account credit
                $creditData = [
                    "account_id" => $agentInstanceAccount->id,
//                "treasury_account_id" => null,
                    "amount" => 0,
                    "usable_at" => null,
                    "expired_at" => null,
                    "revoked" => false,
                ];
                $creditDB->create($creditData);
            }


            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw new \Exception("some thing went wrong{$e->getMessage()}", 400);
        }
    }

    public function handle2($event)
    {
        //create treasury account for wallet
        $accountDB = new AccountDB();
        $accountTypeDB = new AccountTypeDB();
        $creditDB = new CreditDB();
        $data = $event->data;
        try {
            \DB::beginTransaction();

            //create account type for treasury
            $accountTypeData = [
                "type" => "treasury",
                "wallet_id" => $event->wallet->id,
                "title" => "IRR",
                "subtitle" => "for test",
                "description" => "for test description",
                "balance_type" => "ziro",
                "min_account_amount" => 0,
                "max_account_amount" => 0,
                "min_transaction_amount" => 0,
                "max_transaction_amount" => 0,
                "legal" => false,
                "interest_rate" => 1,
                "interest_period" => 1,
                "revoked" => false,
            ];
            $instanceTypeAccount = $accountTypeDB->create($accountTypeData);

            //create account for treasury
            $accountData = [
                "account_type_id" => $instanceTypeAccount->id,
                "user_id" => null,
                "min_transaction" => 0,
                "revoked" => false,
            ];
            $instanceAccount = $accountDB->create($accountData);

            //treasury account credit
            $creditData = [
                "account_id" => $instanceAccount->id,
                "wallet_id" => $event->wallet->id,
                "treasury_id" => 0,
                "club_id" => $data["club_id"],
                "amount" => 0,
                "usable_at" => null,
                "expired_at" => null,
                "revoked" => false,
            ];
            $creditDB->create($creditData);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw new CreateTreasuryException("some thing went wrong{$e->getMessage()}");
        }
    }
}
