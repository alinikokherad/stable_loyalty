<?php

namespace Modules\Wallet\Listeners;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Events\AfterCreatedOrderEvent;
use Modules\Wallet\Http\Controllers\Api\OrderController;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\WalletDB;

$className = "Modules\Wallet\Listeners\Testing";


class GetCampaignListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     * @throws \Exception
     */
    public function handle($event)
    {

        $eventInstance = $event->eventInstance;
        $orderInstance = $event->orderInstance;
        $amount = $orderInstance->amount;
        $clubInstance = $event->clubInstance;

        //getter all campaign
        $campaigns = $clubInstance->campaigns;

        //call every campaign for one club
        foreach ($campaigns as $campaign) {
            //get campaign rules
            $rules = $campaign->rules;

            //call every rule for one campaign
            foreach ($rules as $rule) {
                $params = $rule->params;
                $operators = $rule->operators;
                $values = $rule->values;
                $statement = false;

                $phpEvalString = '$statement = ' . $params . $operators . $values . ';';

                //run rule from database
                eval($phpEvalString);

                //run rule
                if ($statement) {
                    //get rule process
                    $processes = $rule->processes;

                    //call class for each rule
                    foreach ($processes as $process) {

                        //do action
                        $processClass = $process->class;
                        $processMethod = $process->methods;
                        $processParams = $process->params;
                        $methodName = $process->method;

                        $processParams = str_replace('{{amount}}', $eventInstance->amount, $processParams);

//                        foreach ($processParams as $processParam) {
//                            $process  = str_replace('{{amount}}', $event->amount,$processParam);
//                        }

                        //create class and its methods

                        $fileContent = file_get_contents(app_path("Process/ClassName.stub"));
                        $newFile = str_replace('$CLASSNAME$', $processClass, $fileContent);
                        $newFile = str_replace('$METHODNAMES$', $processMethod, $newFile);
                        file_put_contents(app_path("Process/{$processClass}.php"), $newFile);
                        //call class and its methods & params
                        $processClass = "App\\Process\\$processClass";
                        $instanceOfClass = new $processClass();

                        //call class method and run it
//                        $instanceOfClass->$methodName($processParams);

                        //get point treasury account id
                        $walletDB = new WalletDB();
                        $pointTreasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletType("point");

                        //get point account id for customer
                        /*                        $pointAccountID = \DB::table("accounts")
                                                    ->where("accounts.id", "=", $orderInstance->from_account_id)
                                                    ->join("users", "accounts.user_id", "=", "users.id")
                                                    ->join("accounts as acc", "users.id", "=", "acc.user_id")
                                                    ->where("acc.treasury_id", "=", $pointTreasuryAccountInstance->id)
                                                    ->select("acc.id")
                                                    ->first()->id;*/

//                        dd($pointAccountID,$orderInstance->toArray());
                        $fromAccountUserInstance = User::whereHas("accounts", function ($query) use ($orderInstance) {
                            $query->where("id", $orderInstance->from_account_id);
                        })->first();
                        $pointAccountInstance = $fromAccountUserInstance->accounts->where("treasury_id", $pointTreasuryAccountInstance->id)->first();

                        //get exchange rate
                        $exchangeRatePrice = $clubInstance->exchange_rate_price;
                        $exchangeRatePoint = $clubInstance->exchange_rate_point;

                        //TODO شرط اعمال سقف خرید برای اعطای امتیاز
                        if ($orderInstance->amount >= $exchangeRatePrice) {
                            //create function from insert order (submit campaign to customer)
                            $generateNewOrderByCampaign = [
                                "from_account_id" => $campaign->account_id,
                                "to_account_id" => $pointAccountInstance->id,
                                //todo میزان اعطای امتیاز بابت خرید ریالی(فلان قدر خرید فلان قدر امتیاز)
                                "amount" => (int)(((int)($orderInstance->amount / $exchangeRatePrice)) * $exchangeRatePoint),
                                "goods_id" => $orderInstance->goods_id,
                                "type" => "gift",
                                "author_id" => null,
                                "treasury_account_id" => $pointTreasuryAccountInstance->id,
                                "paid_at" => date("Y-m-d H:i:s", strtotime(time())),
                            ];
                            $type = "gift";
                            if ($orderInstance->type == "dorris_rials") {
                                $type = "dorris_gift";
                            }
                            $this->createWalletOrder($generateNewOrderByCampaign, $type);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $orderData
     * @throws \Exception
     */
    private function createWalletOrder($orderData, $type)
    {
        /*-------------------- create order ---------------------*/
        //create wallet order
        $orderController = new OrderController();
        $orderController->store($orderData, $type);
    }
}
