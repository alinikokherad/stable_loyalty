<?php

namespace Modules\Wallet\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;

class   OrderResource extends Resource
{
    /**
     * this attribute for table information
     * @var bool
     */
    public static $with_user_orders = false;

    //if from user have any product order goes into this attribute
    private $productOrders;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);


        //       wallet type
        $walletType = AccountType::find
        (Account::find($this->treasury_account_id)
            ->toArray()["account_type_id"])
        ["title"];

        $fromUserInstance = $this->getUser($this->from_account_id, self::$with_user_orders);
        $toUserInstance = $this->getUser($this->to_account_id);

        return [
            "id" => $this->id,
            "goods_id" => $this->goods_id,
            "customer_mobile" => isset($fromUserInstance) ? $fromUserInstance->mobile : null,
            "customer_name" => isset($fromUserInstance) ? $fromUserInstance->name : null,
            "customer_orders" => $this->when(self::$with_user_orders, $this->productOrders),
            "merchant_mobile" => isset($toUserInstance) ? $toUserInstance->mobile : null,
            "merchant_name" => isset($toUserInstance) ? $toUserInstance->name : null,
            "amount" => $this->amount,
            "paid_at" => $this->paid_at,
            "treasury_account_id" => $this->treasury_account_id,
            "walletType" => isset($walletType) ? $walletType : null,
            "created_at" => date("Y-m-d H:i:s", strtotime($this->created_at)),
            "updated_at" => date("Y-m-d H:i:s", strtotime($this->updated_at)),
            "transaction" => $this->whenLoaded("transactions", TransactionResource::collection($this->transactions))
        ];
    }

    /**
     * @param $accId
     * @param $withUserOrders
     * @return mixed
     */
    private function getUser($accId, $withUserOrders = false)
    {
        $instance = Account::where("id", $accId)->with("user")->first()->user;
        if ($withUserOrders) {
            $this->productOrders = $instance->load("productOrders")->productOrders->last();
        }
        return $instance;
    }
}
