<?php

namespace Modules\Wallet\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CreditResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "id" => $this->id,
            "account_id" => $this->account_id,
            "club_id" => $this->club_id,
            "treasury_id" => $this->treasury_id,
            "amount" => $this->amount,
            "usable_at" => $this->usable_at,
            "expired_at" => $this->expired_at,
            "revoked" => $this->revoked,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
        ];
    }
}
