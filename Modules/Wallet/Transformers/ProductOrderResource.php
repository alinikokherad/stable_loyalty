<?php

namespace Modules\Wallet\Transformers;

use App\Http\Resources\UserResource;
use App\repo\UserDB;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Branch\repo\MerchantDB;
use Modules\Product\Transformers\CostResource;
use Modules\Product\Transformers\TableResource;
use Modules\Wallet\repo\WalletDB;

class ProductOrderResource extends Resource
{
    protected $total_rial_cost;
    public static $with_user = false;
    public static $with_user_credit = false;
    public static $with_cost = false;
    public static $with_orders = false;
    public static $with_product = false;
    public static $with_tables = false;
    public static $user_total_credit = false;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (self::$with_user_credit) {
            UserResource::$with_credit = true;
            UserResource::$with_account = true;
        }

        if (self::$with_product) {
            CostResource::$withProduct = true;
        }

        if (self::$user_total_credit) {
            UserResource::$user_total_credit;
        }
//        return parent::toArray($request);

        $userDB = new UserDB();
        $merchantDB = new MerchantDB();
        $walletDB = new WalletDB();
        $from = $merchantDB->find($this->merchant_id);
        $to = $userDB->find($this->user_id);
        $author = $userDB->find($this->author_id);

        //get total order cost
        $rialsTreasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType("rials");
        $pointTreasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType("point");
        $total_rial_cost = 0;
        $total_point_cost = 0;
        if (isset($this->orders)) {
            foreach ($this->orders as $order) {
                if ($order->treasury_account_id == $rialsTreasuryAccountInstance->id) {
                    $total_rial_cost += $order->amount;
                }
                if ($order->treasury_account_id == $pointTreasuryAccountInstance->id) {
                    $total_point_cost += $order->amount;
                }
            }
        }

        return [
            "id" => $this->id,
            "order_total_cost_rials" => $total_rial_cost,
            "order_total_cost_point" => $total_point_cost,
//            "order_total_cost_rials" => isset($this->total_rial_cost) ? $this->total_rial_cost : null,
            "merchant_name" => isset($from->name) ? $from->name : null,
            "user_name" => isset($to->name) ? $to->name : null,
            "user_mobile" => isset($to->mobile) ? $to->mobile : null,
            "paid_at" => $this->paid_at,
            "description" => $this->description,
            "waiter_name" => isset($author->name) ? $author->name : null,
            "merchant_user_id" => $this->merchant_user_id,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "user" => $this->when(self::$with_user, (new UserResource($this->user))),
            "old_order" => $this->when(self::$with_orders, $this->orders),
            "tables" => $this->when(self::$with_tables, TableResource::collection($this->tables)),
            "costs" => $this->when(self::$with_cost, function () {
                return CostResource::collection($this->costs);
            }),
            /*            "wallet_order" => $this->when(isset($this->orders), function () {
                            return OrderResource::collection($this->orders);
                        }),*/
        ];
    }

    /**
     * @return $this
     */
    public function getOrderCost($total_rial_cost)
    {
        $this->total_rial_cost = $total_rial_cost;
        return $this;
    }

    /**
     * @param mixed $resource
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|ProductOrderResourceCollection
     */
    public static function collection($resource)
    {
        return new ProductOrderResourceCollection($resource);
    }
}
