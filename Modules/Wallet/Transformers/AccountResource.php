<?php

namespace Modules\Wallet\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class AccountResource extends Resource
{
    public static $with_credit = false;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "user_id" => $this->user_id,
            "treasury_id" => $this->treasury_id,
            "account_type_id" => $this->account_type_id,
            "revoked" => $this->revoked,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "credits" => $this->when(self::$with_credit, CreditResource::collection($this->credits))
        ];
    }
    /*    public function toArray($request)
        {
            $account = (array)$this;
                    dd($account);
            $attrs = [];
            foreach ($account['account_attributes'] as $attr) {
                $attrs[$attr['id']] = $attr['attribute'];
            }
    //        dd($attrs);
            $vals = [];
            foreach ($account['account_attributes'] as $attr) {
                $pivot = $attr['pivot'];
                $vals[$attrs[$pivot['attribute_id']]]= $pivot['value'];
            }
            foreach ($vals as $key=>$val){
                $account[$key]=$val;
            }
    //        dd($vals);
            unset($account['account_attributes']);
            unset($account['password']);
            return $account;
        }*/
}
