<?php

namespace Modules\Wallet\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Wallet\repo\WalletDB;

class ProductOrderResourceCollection extends ResourceCollection
{
//    protected $total_rial_cost;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        return $this->collection->map(function (ProductOrderResource $resource) use ($request) {
            return $resource->getOrderCost()->toArray($request);
        })->all();
    }

    public function getOrderCost()
    {
        $walletDB = new WalletDB();
        //get total order cost
        $treasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType("rials");
        $total_rial_cost = 0;
        foreach ($this->orders as $order) {
            if ($order->treasury_account_id == $treasuryAccountInstance->id) {
                $total_rial_cost += $order->amount;
            }
        }
        $this->total_rial_cost = $total_rial_cost;
        return $this;
    }
}
