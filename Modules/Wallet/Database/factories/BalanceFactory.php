<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\Balance::class, function (Faker $faker) {
    return [
        //
        'from_account_id' => $faker-> numberBetween(1,10),
        'to_account_id' => $faker-> numberBetween(1,10),
        'good_id' => $faker-> numberBetween(1,10),
        'amount' => $faker-> randomFloat(3,1000,1000000),
        'revoked' => $faker-> boolean(50),
        'author' => $faker-> numberBetween(10,10),
        'uuid' => $faker-> postcode,
        'reverse' => $faker-> boolean(10),
        'extraValue' => $faker->randomFloat(2,1,100),
        'goodExtraValue' => $faker-> randomFloat(2,1,100),
        'parent_id' => $faker-> numberBetween(1,10),
        'application_id' => $faker-> numberBetween(1,100),
    ];
});
