<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\Goods::class, function (Faker $faker) {
    return [
        //
        "title"  => $faker->title,
        "subtitle"  => $faker->title,
        "description"  => $faker->text(100),
        "title"  => $faker->boolean(50),
    ];
});
