<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\Role::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->randomElement(['admin', 'company', 'user']),
    ];
});
