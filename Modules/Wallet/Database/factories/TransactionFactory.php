<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\Transaction::class, function (Faker $faker) {
    return [
        "order_id" => $faker->numberBetween(1.20),
        "account_id" => $faker->numberBetween(1.10),
        "amount" => $faker->randomFloat(2, 10, 20),
        "reverse" => $faker->boolean(10),
    ];
});
