<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Account\Entities\Credit::class, function (Faker $faker) {
    return [
        //
        "account_id" => $faker->numberBetween(1, 6),
        "amount" => $faker->randomFloat(2, 1, 20),
        "treasury" => $faker->randomFloat(2, 1, 60),
    ];
});
