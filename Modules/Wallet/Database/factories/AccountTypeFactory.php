<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\AccountType::class, function (Faker $faker) {

        return [
            "type" => $faker->randomElements(["merchant","customer"]),
            "wallet_id" => 1,
            "title" => $faker->text(20),
            "subtitle" => $faker->text(30),
            "description" => $faker->text(100),
            "balance_type" => "positive",
            "min_account_amount" => 0,
            "max_account_amount" => 0,
            "min_transaction_amount" => 0,
            "max_transaction_amount" => 0,
            "legal" => false,
            "interest_rate" => 1,
            "interest_period" => "daily",
            "revoked" => false
        ];

});
