<?php

use Faker\Generator as Faker;
use Modules\Wallet\Entities\Account;


$factory->define(Account::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'email'=>$faker->email(),
        'mobile'=>"0".$faker->numberBetween(9351111111,9389999999),
        'password'=>bcrypt(123),
//        'credit'=>$faker->numberBetween(10,100),
        'is_treasury' => $faker->boolean(10),
//        'api_token'=>$faker->randomDigit,
        'club_id'=>$faker->numberBetween(1,99),
        'status'=>$faker->randomElement(["active","block"])
    ];
});

