<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\Order::class, function (Faker $faker) {
    return [
        "goods_id" => $faker->numberBetween(1, 20),
        "from_account_id" => $faker->numberBetween(1, 10),
        "to_account_id" => $faker->numberBetween(1, 20),
        "amount" => $faker->randomFloat(2, 5, 1000),
        "treasury_account_id" => $faker->numberBetween(1, 10),
    ];
});
