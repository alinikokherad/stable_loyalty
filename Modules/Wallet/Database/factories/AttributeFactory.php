<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\AccountAttribute::class, function (Faker $faker) {
    return [
        //
        "attribute" => $faker->randomElement(["fatherName","fax"])
    ];
});
