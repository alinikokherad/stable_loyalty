<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Wallet\Entities\AccountValue::class, function (Faker $faker) {
    $accountQuantity = count(\Modules\Wallet\Entities\Account::all()->toArray());
    return [
        //
        "value"  => $faker-> name,
        "status"  => $faker-> randomElement(["active","block"]),
        "attribute_id"  => $faker-> numberBetween(1,2),
        "account_id"  => $faker-> numberBetween(1,$accountQuantity),
    ];
});
