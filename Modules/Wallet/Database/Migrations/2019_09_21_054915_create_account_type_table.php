<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum("type",
                ["wallet",
                    "customer",
                    "merchant",
                    "treasury",
                    "agent",
                    "cash",
                    "bank",
                    "gift",
                    "personnel",
                    "taxi",
                    "club",
//                    "bank-mellat",
//                    "bank-passargad",
//                    "bank-melli",
//                    "bank-persian",
//                    "bank-resalat",
//                    "bank-keshavarzi",
                    "campaign",
                    "unknown",
                ]);
            $table->unsignedInteger('wallet_id');
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
            $table->enum('balance_type', ["negative", "positive", "zero"])->nullable();
            $table->integer('min_account_amount')->nullable();
            $table->integer('max_account_amount')->nullable();
            $table->integer('min_transaction_amount')->nullable();
            $table->integer('max_transaction_amount')->nullable();
            $table->boolean('legal')->nullable();
            $table->integer('interest_rate')->nullable();
            $table->enum('interest_period', ["daily", "weekly", "yearly"])->nullable();
            $table->boolean('revoked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_types');
    }
}
