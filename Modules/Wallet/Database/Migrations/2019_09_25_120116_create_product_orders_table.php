<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->unsignedBigInteger("order_id")->nullable();
            $table->unsignedBigInteger("merchant_id");
            $table->unsignedBigInteger("customer_id");
            $table->string("description")->nullable();
            $table->timestamp("paid_at")->nullable();
            $table->bigInteger("author_id")->nullable();
            $table->bigInteger("merchant_user_id")->nullable();
            $table->boolean("reverse")->default(false);
            $table->boolean("revoked")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_orders');
    }
}
