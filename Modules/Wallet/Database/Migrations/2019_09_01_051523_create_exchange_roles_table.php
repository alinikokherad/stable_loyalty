<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum("discount_type",["null","discount_price","discount_percent"]);
            $table->integer("minimum_discount_every_purchase")->nullable();
            $table->integer("minimum_point_for_exchange")->nullable();
            $table->integer("minimum_discount_for_single_product")->nullable();
            $table->integer("maximum_discount_at_every_purchase")->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_roles');
    }
}
