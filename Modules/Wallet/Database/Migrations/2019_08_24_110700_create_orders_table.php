<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("from_account_id");
            $table->unsignedInteger("to_account_id");
            $table->bigInteger("amount")->nullable();
            $table->string("UID")->nullable();
            $table->unsignedInteger("goods_id")->nullable();
            $table->bigInteger("author_id")->nullable();
            $table->unsignedInteger("from_treasury_account_id")->nullable();
            $table->unsignedInteger("to_treasury_account_id")->nullable();
            $table->timestamp("paid_at")->nullable();
            $table->unsignedBigInteger("order_payment_id")->nullable();
            $table->boolean("refund")->default(false);
            $table->boolean("cash_out")->default(false);
            $table->string("type")->default("request");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
