<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletXChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_x_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("from_wallet_id");
            $table->unsignedInteger("to_wallet_id");
            $table->integer("unit_amount");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_x_changes');
    }
}
