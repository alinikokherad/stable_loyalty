<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('belongs_to')->nullable();
            $table->enum('belongs_type', [
                "user",
                "wallet",
                "merchant",
                "cash",
                "unknown",
                "campaign",
                "wallet_treasury",
                "wallet_agent",
                "bank-mellat",
                "bank-passargad",
                "bank-melli",
                "bank-persian",
                "bank-resalat",
                "bank-keshavarzi",
            ])->nullable();
            $table->unsignedInteger('treasury_account_id')->nullable();
            $table->unsignedInteger('account_type_id');
            $table->boolean('revoked')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
