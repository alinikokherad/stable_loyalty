<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubWalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_wallet', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("club_id")->index();
//            $table->foreign("club_id")->references("id")->on("clubs");

            $table->unsignedInteger("wallet_id")->index();
//            $table->foreign("wallet_id")->references("id")->on("wallets");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_wallet');
    }
}
