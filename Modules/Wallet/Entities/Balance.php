<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Wallet\Entities\Balance
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Balance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Balance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Balance query()
 * @mixin \Eloquent
 */
class Balance extends Model
{
    public $validate = [
        "from_account_id" => "required",
        "to_account_id" => "required",
        "good_id" => "required",
        "amount" => "required",
        "revoked" => "required",
        "author" => "required",
        "uuid" => "required",
        "reverse" => "required",
        "extraValue" => "required",
        "goodExtraValue" => "required",
        "application_id" => "required",
    ];
    protected $fillable = [
        'from_account_id',
        'to_account_id',
        'good_id',
        'amount',
        'revoked',
        'author',
        'uuid',
        'reverse',
        'extraValue',
        'goodExtraValue',
        'parent_id',
        'application_id',
    ];

}
