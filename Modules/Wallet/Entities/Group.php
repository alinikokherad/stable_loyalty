<?php

namespace Modules\Wallet\Entities;

/**
 * Modules\Account\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $accounts
 * @property-read int|null $accounts_count
 * @property-read \Modules\Club\Entities\ClubLevel $clubLevel
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\Group newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\Group query()
 */

use Illuminate\Database\Eloquent\Model;
use Modules\Club\Entities\ClubLevel;
use Modules\Wallet\Entities\Account;

class Group extends Model
{
    public $validate = [
        "group_type" => "required",
        "Formula" => "required",
        "Rate_of_receiving_points" => "required",
    ];
    protected $guarded = [];

    public function accounts()
    {
        return $this->belongsToMany(Account::class, "account_group", "group_id", "account_id");
    }

    public function clubLevel()
    {
        return $this->hasOne(ClubLevel::class);
    }
}
