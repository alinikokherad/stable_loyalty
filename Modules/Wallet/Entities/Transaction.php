<?php

namespace Modules\Wallet\Entities;

/**
 * Modules\Wallet\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Modules\Wallet\Entities\Order $orders
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Transaction query()
 */

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $validate = [
        "order_id" => "required",
        "from_account_id" => "required",
        "to_account_id" => "required",
        "amount" => "required",
        "type" => "required",
        "reverse" => "required",
    ];
    protected $fillable = [
        "order_id",
        "from_account_id",
        "to_account_id",
        "amount",
        "revoked",
        "reverse",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orders()
    {
        return $this->belongsTo(Order::class);
    }
}
