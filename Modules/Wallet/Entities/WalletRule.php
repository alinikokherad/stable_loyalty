<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereFontIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule wherePriorityView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\WalletRule whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class WalletRule extends Model
{
    protected $fillable = [];

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }
}
