<?php

namespace Modules\Wallet\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Branch\Entities\Merchant;
use Modules\Club\Entities\Club;

/**
 * Modules\Account\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Modules\Wallet\Entities\Account $account
 * @property-read \Modules\Wallet\Entities\AccountType $accountType
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Club\Entities\Club[] $clubs
 * @property-read int|null $clubs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Credit[] $credits
 * @property-read int|null $credits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Group[] $groups
 * @property-read int|null $groups_count
 * @property-read int|null $notifications_count
 * @property-read int|null $tokens_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Account newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Account newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Account query()
 */
class Account extends Model
{

    public $validate = [
        "account_type_id" => "required",
        "treasury_id" => "required",
        "user_id" => "required",
        "revoked" => "required",
    ];
    use Notifiable, HasApiTokens;
    protected $table = 'accounts';
//    protected $guarded = [];

    protected $fillable = [
        "belongs_to",
        "belongs_type",
        "treasury_account_id",
        "account_type_id",
        "revoked",
    ];

    //delete credit record when we delete account
    /*    public static function boot()
        {
            parent::boot();

            static::deleting(function ($account) { // before delete() method call this
                $account->credit()->delete();
                // do the rest of the cleanup...
            });
        }*/

    //authentication with custom username
    /*    public function findForPassport($username)
        {
            return $this->where('username', $username)->first();
        }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function merchant()
    {
        return $this->belongsTo(Merchant::class, "belongs_to", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "belongs_to", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->hasMany(Credit::class, "account_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function accountType()
    {
        return $this->belongsTo(AccountType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, "account_group", "account_id", "group_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany(Club::class, "club_accounts", "account_id", "club_id")->withPivot("type", "expired_at", "revoked");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);

    }
}
