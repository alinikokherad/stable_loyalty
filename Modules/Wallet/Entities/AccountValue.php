<?php

namespace Modules\Account\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Account\Entities\AccountValue
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\AccountValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\AccountValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\AccountValue query()
 * @mixin \Eloquent
 */
class AccountValue extends Model
{
    public $validate = [
        "value" => "required",
        "status" => "required",
    ];
    protected $fillable = ['value', 'status'];
}
