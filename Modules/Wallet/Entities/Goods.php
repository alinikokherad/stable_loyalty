<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Wallet\Entities\Goods
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Order[] $orders
 * @property-read int|null $orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Goods newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Goods newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Goods query()
 * @mixin \Eloquent
 */
class Goods extends Model
{
    public $validate = [
        "title" => "required",
        "subtitle" => "required",
        "description" => "required",
        "revoked" => "required",
    ];
    protected $fillable = [
        "title",
        "subtitle",
        "description",
        "revoked",
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, "goods_id");
    }
}
