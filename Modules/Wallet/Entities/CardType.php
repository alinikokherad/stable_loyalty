<?php

namespace Modules\Account\Entities;

/**
 * Modules\Account\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\CardType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\CardType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\CardType query()
 */

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    public $validate = [];
    protected $fillable = [];
}
