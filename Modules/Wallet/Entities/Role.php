<?php

namespace Modules\Account\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Wallet\Entities\Account;

/**
 * Modules\Account\Entities\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $accounts
 * @property-read int|null $accounts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\Role query()
 * @mixin \Eloquent
 */
class Role extends Model
{
    public $validate = [
        "name" => "required",
        "status" => "required",
    ];
    protected $fillable = ['name','status'];

    public function accounts()
    {
        return $this->belongsToMany(Account::class,'roles_accounts');
    }
}
