<?php

namespace Modules\Wallet\Entities;

/**
 * Modules\Wallet\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Modules\Wallet\Entities\Credit $credit
 * @property-read \Modules\Wallet\Entities\Goods $goods
 * @property-read \Modules\Wallet\Entities\ProductOrder $productOrder
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Transaction[] $transactions
 * @property-read int|null $transactions_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Order query()
 */

use Illuminate\Database\Eloquent\Model;
use Modules\Branch\Entities\Merchant;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Table;

class Order extends Model
{
    public $validate = [
        "user_id" => "required",
        "goods_id" => "required",
        "from_account_id" => "required",
        "to_account_id" => "required",
        "refund" => "required",
        "cashout" => "required",
        "amount" => "required",
        "treasury_account_id" => "required",
    ];
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, "order_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function credit()
    {
        return $this->belongsTo(Credit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productOrder()
    {
        return $this->belongsTo(ProductOrder::class, "goods_id", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderStuff()
    {
        return $this->belongsTo(OrderStuff::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class, "order_post", "order_id", "post_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Order::class, "order_payment_id");
    }
}
