<?php

namespace Modules\Wallet\Entities;

/**
 * Modules\Wallet\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\AccountType[] $accountTypes
 * @property-read int|null $account_types_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Cost[] $costs
 * @property-read int|null $costs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\Wallet query()
 */

use Illuminate\Database\Eloquent\Model;
use Modules\Club\Entities\Club;
use Modules\Club\Entities\ClubXChangeRate;
use Modules\Product\Entities\Cost;

class Wallet extends Model
{
    protected $table = "wallets";

    public $validate = [
        "title" => "required",
        "type" => "required",
    ];
    protected $fillable = [
        "title",
        "type",
        "revoked",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accountTypes()
    {
        return $this->hasMany(AccountType::class, "wallet_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function costs()
    {
        return $this->hasMany(Cost::class, "wallet_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany(Club::class, "club_wallet", "wallet_id", "club_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function xChangeRates()
    {
        return $this->hasMany(WalletXChange::class, "from_wallet_id");
    }

    public function rules()
    {
        return $this->hasMany(WalletRule::class, "wallet_id");
    }
}
