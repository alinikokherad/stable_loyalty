<?php

namespace Modules\Wallet\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Branch\Entities\Merchant;
use Modules\Product\Entities\Cost;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Table;

/**
 * Modules\Wallet\Entities\ProductOrder
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Entities\Cost[] $costs
 * @property-read int|null $costs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Modules\Product\Entities\Product $product
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\ProductOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\ProductOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wallet\Entities\ProductOrder query()
 * @mixin \Eloquent
 */
class ProductOrder extends Model
{
    protected $guarded = [];

    public function costs()
    {
        return $this->belongsToMany(Cost::class,
            "order_cost",
            "order_id",
            "cost_id")
            ->withPivot("quantity", "title", "description", "revoked");
    }

    public function orders()
    {
        return $this->hasMany(Order::class, "goods_id", "id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function tables()
    {
        return $this->belongsToMany(Table::class, "order_table", "order_id", "table_id");
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, "merchant_id");
    }
}
