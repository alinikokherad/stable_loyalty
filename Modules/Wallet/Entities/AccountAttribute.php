<?php

namespace Modules\Account\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Wallet\Entities\Account;

/**
 * Modules\Account\Entities\AccountAttribute
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wallet\Entities\Account[] $accounts
 * @property-read int|null $accounts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\AccountAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\AccountAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Account\Entities\AccountAttribute query()
 * @mixin \Eloquent
 */
class AccountAttribute extends Model
{
    public $validate = [
        "attribute" => "required",
        "status" => "required",
    ];
    protected $table = 'attributes';
    protected $fillable = ['attribute', 'status'];

    public function accounts()
    {
        return $this->belongsToMany(Account::class, 'values', 'attribute_id', 'account_id')->withPivot(["value", "status"]);
    }
}
