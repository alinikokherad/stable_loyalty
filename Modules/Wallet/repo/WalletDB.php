<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/15/19
 * Time: 6:21 PM
 */

namespace Modules\Wallet\repo;


use Modules\Club\Entities\Club;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\Entities\WalletXChange;

/**
 * Class WalletDB
 * @package Modules\Wallet\repo
 */
class WalletDB
{

    /**
     * get all wallets
     * @return \Illuminate\Database\Eloquent\Collection|Wallet[]
     */
    public function list()
    {
        return Wallet::all();
    }

    /**
     * @param $walletType
     * @return mixed
     */
    public function wallet_ids($walletType)
    {
        return Wallet::where("type", $walletType)->pluck("id")->first();
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $wallet = Wallet::create($data);
        if ($wallet instanceof Wallet) {
            return $wallet;
        }
        return false;
    }

    /**
     * @param $type
     * @return bool
     */
    public function getByType($type)
    {
        $instance = Wallet::where("type", $type)->first();
        if ($instance instanceof Wallet) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $instance = Wallet::find($id)->delete();
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data): bool
    {
        return $instance = Wallet::where("id", $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $instance = Wallet::find($id);
    }

    /**
     * @return bool|\Illuminate\Support\Collection
     */
    public function getAllIds()
    {
        $ids = Wallet::all()->pluck("id");
        if (!empty($ids)) {
            return $ids;
        }
        return false;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAccountTreasuryWithWalletId($id)
    {
        $accountInstance = AccountType::
        where("wallet_id", $id)
            ->where("type", "=", "treasury")
            ->with('accounts')
            ->first()->accounts;
        return $accountInstance->first();
    }

    /**
     * @param $treasuryAccountId
     * @return bool|mixed
     */
    public function getWalletWithAccountTreasuryId($treasuryAccountId)
    {
        $accountInstance = Account::where("id", $treasuryAccountId)->with("accountType")->first();
        $walletInstance = $this->find($accountInstance->accountType->wallet_id);
        if ($walletInstance instanceof Wallet) {
            return $walletInstance;
        }
        return false;
    }

    /**
     * @param $wallet_type
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getTreasuryAccountWithWalletType($wallet_type)
    {
        $instance = Account::with(["accountType.wallet", "credits"])->whereHas("accountType.wallet", function ($q) use ($wallet_type) {
            $q->where("type", $wallet_type);
        })->first();
        if ($instance instanceof Account) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $walletType
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getTreasuryAccountInstanceWithWalletType($walletType)
    {
        $accountInstance = Account::whereHas("accountType", function ($query) {
            $query->where("type", "treasury");
        })->whereHas("accountType.Wallet", function ($query) use ($walletType) {
            $query->where("type", $walletType);
        })->first();
        if ($accountInstance instanceof Account) {
            return $accountInstance;
        }
        return false;
    }

    /**
     * @param $wallet_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getTreasuryAccountInstanceWithWalletId($wallet_id)
    {
        $accountInstance = Account::query()
            ->whereHas("accountType", function ($query) {
                $query->where("type", "treasury");
            })->whereHas("accountType.Wallet", function ($query) use ($wallet_id) {
                $query->where("id", $wallet_id);
            })->first();

        if ($accountInstance instanceof Account) {
            return $accountInstance;
        }
        return false;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllWalletWithoutMe($id)
    {
        return Wallet::query()->where("id", "!=", $id)->get();
    }

    /**
     * @param $wallet_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAgentAccountType($wallet_id)
    {
        $accountTypeInstance = AccountType::query()
            ->where("type", "agent")
            ->whereHas("wallet", function ($query) use ($wallet_id) {
                $query->where("id", $wallet_id);
            })
            ->first();
        if ($accountTypeInstance instanceof AccountType) {
            return $accountTypeInstance;
        }
        return false;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllTreasuryAccountInstance()
    {
        $treasuryAccountList = Account::query()
            ->whereHas("accountType", function ($query) {
                $query->where("type", "treasury");
            })
            ->get()->pluck("id");
        return $treasuryAccountList;
    }

    /**
     * @param string $string
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getWalletByType(string $string)
    {
        return Wallet::query()->where("type", $string)->get();
    }

    /**
     * @param string $club_name
     * @param string $wallet_type
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getClubByNameAndWalletByType(string $club_name, string $wallet_type)
    {
        return Wallet::query()
            ->where("type", $wallet_type)
            ->whereHas("clubs", function ($query) use ($club_name) {
                $query->where("name", $club_name);
            })->first();
    }

    /**
     * @param $treasury_account_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getWalletInstanceWithTreasuryAccountId($treasury_account_id)
    {
        $walletInstance = Wallet::query()
            ->whereHas("accountTypes.accounts", function ($query) use ($treasury_account_id) {
                $query->where("id", $treasury_account_id);
            })
            ->first();
        if ($walletInstance instanceof Wallet) {
            return $walletInstance;
        }
        return false;
    }

    /**
     * @param $from_wallet_id
     * @param $to_wallet_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|WalletXChange|object|null
     */
    public function getWalletXChange($from_wallet_id, $to_wallet_id)
    {
        $xChangeInstance = WalletXChange::query()
            ->where("from_wallet_id", $from_wallet_id)
            ->where("to_wallet_id", $to_wallet_id)
            ->first();
        if ($xChangeInstance instanceof WalletXChange) {
            return $xChangeInstance;
        }
        return false;
    }

    /**
     * @param int $cost_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getWalletInstanceByCostId(int $cost_id)
    {
        $walletInstance = Wallet::query()
            ->whereHas("costs", function ($query) use ($cost_id) {
                $query->where("id", $cost_id);
            })
            ->first();
        return $walletInstance;
    }
}
