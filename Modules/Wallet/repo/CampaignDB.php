<?php


namespace Modules\Wallet\repo;


use Modules\Wallet\Entities\Campaign;

class CampaignDB
{
    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|Campaign
     */
    public function create($data)
    {
        $input = $this->normalizeInput($data);
        $instance = Campaign::create($input);
        if ($instance instanceof Campaign) {
            return $instance;
        }
        return false;
    }

    /**
     * @return bool|\Illuminate\Database\Eloquent\Model|Campaign|object|null
     */
    public function getRandomCampaign()
    {
        $instances = Campaign::get();
        if ($instances->first() instanceof Campaign) {
            return $instances;
        }
        return false;
    }

    /**
     * @param $campaign_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getCampaignWithRelationShips($campaign_id)
    {
        $instance = Campaign::where("id", $campaign_id)->with(["accounts.accountType"])->first();
        if ($instance instanceof Campaign) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["club_id"])) {
            $filter["club_id"] = $data["club_id"];
        }

        if (isset($data["started_at"])) {
            $filter["started_at"] = $data["started_at"];
        }
        if (isset($data["expired_at"])) {
            $filter["expired_at"] = $data["expired_at"];
        }
        if (isset($data["budget"])) {
            $filter["budget"] = $data["budget"];
        }
        if (isset($data["budget_consumed"])) {
            $filter["budget_consumed"] = $data["budget_consumed"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Campaign[]
     */
    public function all()
    {
        return Campaign::query()
            ->get();
    }


}
