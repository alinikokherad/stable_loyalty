<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/21/19
 * Time: 2:57 PM
 */

namespace Modules\Wallet\repo;

use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;

class AccountTypeDB
{
    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $input = $this->normalizeInput($data);
        $instance = AccountType::create($input);
        if ($instance instanceof AccountType) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $account_id
     * @return mixed
     */
    public function getAccountTypeWithAccountId($account_id)
    {
        $account = Account::where("id", $account_id)->with("accountType")->first();
        return $account->accountType->type;
    }

    /**
     * @param $condition
     * @param $data
     * @return bool
     */
    public function firstOrCreate($condition, $data)
    {
        $accountTypeInstance = AccountType::firstOrCreate($condition, $data);
        if ($accountTypeInstance instanceof AccountType) {
            return $accountTypeInstance;
        }
        return false;
    }

    /**
     * @param $account_id
     * @return bool|mixed
     */
    public function getTreasuryAccountWithAccountId($account_id)
    {
        $treasuryId = Account::where("id", $account_id)
            ->pluck("treasury_id")
            ->first();
        if (isset($treasuryId)) {
            return $treasuryId;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["type"])) {
            $filter["type"] = $data["type"];
        }
        if (isset($data["wallet_id"])) {
            $filter["wallet_id"] = $data["wallet_id"];
        }
        if (isset($data["title"])) {
            $filter["title"] = $data["title"];
        }
        if (isset($data["subtitle"])) {
            $filter["subtitle"] = $data["subtitle"];
        }
        if (isset($data["description"])) {
            $filter["description"] = $data["description"];
        }
        if (isset($data["balance_type"])) {
            $filter["balance_type"] = $data["balance_type"];
        }
        if (isset($data["min_transaction_amount"])) {
            $filter["min_transaction_amount"] = $data["min_transaction_amount"];
        }
        if (isset($data["min_account_amount"])) {
            $filter["min_account_amount"] = $data["min_account_amount"];
        }
        if (isset($data["max_account_amount"])) {
            $filter["max_account_amount"] = $data["max_account_amount"];
        }
        if (isset($data["max_transaction_amount"])) {
            $filter["max_transaction_amount"] = $data["max_transaction_amount"];
        }
        if (isset($data["legal"])) {
            $filter["legal"] = $data["legal"];
        }
        if (isset($data["interest_period"])) {
            $filter["interest_period"] = $data["interest_period"];
        }
        if (isset($data["interest_rate"])) {
            $filter["interest_rate"] = $data["interest_rate"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }

    /**
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAccountTypeWithType(string $type)
    {
        return AccountType::query()->where("type", $type)->get();
    }
}
