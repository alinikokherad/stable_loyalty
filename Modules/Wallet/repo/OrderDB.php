<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/7/19
 * Time: 12:26 PM
 */

namespace Modules\Wallet\repo;


use App\repo\UserDB;
use App\User;
use Carbon\Carbon;
use Modules\Branch\Entities\Merchant;
use Modules\Branch\repo\MerchantDB;
use Modules\Branch\Transformers\MerchantResource;
use Modules\Product\repo\CostDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Events\AfterCreatedOrderEvent;
use Modules\Wallet\Transformers\OrderResource;
use Modules\Wallet\Transformers\ProductOrderResource;
use mysql_xdevapi\Exception;

class OrderDB
{
    /**
     * @param null $from
     * @param null $to
     * @param null $day
     * @param null $week
     * @param null $month
     * @param null $wallet_type
     * @param null $customer_id
     * @param null $fromAmount
     * @param null $toAmount
     * @param null $products
     * @param null $paid_at
     * @param null $merchant_id
     * @param null $table_id
     * @param null $mobile
     * @param $fromAccount
     * @param $toAccount
     * @return \Illuminate\Database\Eloquent\Builder|Order
     */
    public function list($from = null, $to = null, $day = null, $week = null, $month = null,
                         $wallet_type = null, $customer_id = null,
                         $fromAmount = null, $toAmount = null,
                         $products = null, $paid_at = null, $merchant_id = null,
                         $table_id = null, $mobile = null, $fromAccount = null, $toAccount = null)
    {
        $where = [];
        if (isset($day)) {
            array_push($where, ["created_at", ">=", Carbon::now()->subDay($day)]);
        } elseif (isset($week)) {
            array_push($where, ["created_at", ">=", Carbon::now()->subWeek($week)]);
        } else {
            array_push($where, ["created_at", ">=", Carbon::now()->subMonth($month)]);
        }

        $productOrder = ProductOrder::query()->with(["costs.product.categories", "orders", "tables", "user.accounts.credits", "merchant"]);

        $productOrder = $productOrder->where($where);
        if (isset($from)) {
            $productOrder = $productOrder->where("created_at", ">=", $from);
        }

        if (isset($to)) {
            $productOrder = $productOrder->where("created_at", "<=", $to);
        }


        $productOrder = $productOrder->where("user_id", $customer_id);

        $productOrder = $productOrder->whereHas("user", function ($query) use ($customer_id) {
            $query->where("id", $customer_id);
        });

        if (isset($merchant_id)) {
            /*        $productOrder = $productOrder->whereHas("merchant", function ($query) use ($merchant_id) {
            $query->where("merchant_id", $merchant_id);
        });*/
        }

        $productOrder = $productOrder->get();

        ProductOrderResource:: $with_user = true;
        ProductOrderResource:: $with_user_credit = true;
        ProductOrderResource:: $with_orders = true;
        ProductOrderResource:: $with_tables = true;
        ProductOrderResource:: $user_total_credit = true;
        $productOrder = ProductOrderResource::collection($productOrder);
        return $productOrder;
        /*        //initialize query
                if (((bool)$paid_at) == true) {
                    $ordersInstance = Order::with(["productOrder.costs.product"]);
                } else {
                    $ordersInstance = Order::whereNull("paid_at")->with(["productOrder.costs.product"]);
                }

                // filter by from account id
                if (!is_null($fromAccount)) {
                    array_push($where, ["from_account_id", $fromAccount]);
                }

                // filter by from account id
                if (!is_null($toAccount)) {
                    array_push($where, ["to_account_id", $toAccount]);
                }

                // wallet filter
                if (!is_null($wallet_type)) {
                    $walletDB = new WalletDB();
                    $treasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType($wallet_type);
                    array_push($where, ["treasury_account_id", $treasuryAccountInstance->id]);
                }

                // user filter
                if (!is_null($customer_id)) {
                    $customersInstance = User::where("id", $customer_id)->with("accounts")->first();
                    $customerAccountsIds = $customersInstance->accounts()->get()->pluck("id")->toArray();
                    $ordersInstance = $ordersInstance->wherein("from_account_id", $customerAccountsIds);
                }

                // user mobile filter
                if (!is_null($mobile)) {
                    $customersInstance = User::where("mobile", $mobile)->with("accounts")->first();
                    $customerAccountsIds = $customersInstance->accounts()->get()->pluck("id")->toArray();
                    $ordersInstance = $ordersInstance->wherein("from_account_id", $customerAccountsIds);
                }

                // merchant filter
                if (!is_null($merchant_id)) {
                    $merchantInstance = Merchant::where("id", $merchant_id)->with("accounts")->first();
                    if ($merchantInstance instanceof Merchant) {
                        $customerAccountsIds = $merchantInstance->accounts()->get()->pluck("id")->toArray();
                        $ordersInstance = $ordersInstance->wherein("to_account_id", $customerAccountsIds);
                    }
                }

                // amount filter
                if (!is_null($fromAmount) && !is_null($toAmount)) {
                    array_push($where, ["amount", ">=", $fromAmount], ["amount", "<=", $toAmount]);
                }

                //filter by one day ago one week ago and one month ago or (from to) date ago
                if (isset($day) || isset($week) || isset($month)) {
                    if (isset($day)) {
                        array_push($where, ["created_at", ">=", Carbon::now()->subDay($day)]);
                    } elseif (isset($week)) {
                        array_push($where, ["created_at", ">=", Carbon::now()->subWeek($week)]);
                    } else {
                        array_push($where, ["created_at", ">=", Carbon::now()->subMonth($month)]);
                    }

                } elseif (isset($from, $to)) {
                    //convert request date to laravel created_at date type
                    $from = date("Y-m-d" . " 00:00:00", strtotime($from));
                    $to = date("Y-m-d" . " 23:59:59", strtotime($to));
                    array_push($where, ["created_at", ">=", $from], ["created_at", "<=", $to]);
                }

                //if isset (day or week or month) filter
                $ordersInstance = $ordersInstance->where($where);

                if (!is_null($products)) {
                    $ordersInstance = $ordersInstance->whereHas("productOrder.costs.product", function ($q) use ($products) {
                        $q->wherein("id", $products);
                    });
                }

                $ordersInstance = OrderResource::collection($ordersInstance->get());
                return $ordersInstance;
        */
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $input = $this->normalizeInput($data);
        $instance = Order::create($input);
        if ($instance instanceof Order) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function paid_at($order_id)
    {
        $response = Order::where("id", $order_id)
            ->update(["paid_at" => Carbon::now()->format("Y-m-d H:i:s")]);
        return $response;
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function updatePaid_at($order_id)
    {
        $response = Order::where("id", $order_id)->update(["paid_at" => Carbon::now()->format("Y-m-d H:i:s")]);
        return $response;
    }

    /**
     * @param $orderData
     * @return bool
     */
    public function cardOrder($orderData)
    {
        $input = $this->normalizeInput($orderData);
        $instance = Order::create($input);
        if ($instance instanceof Order) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function find($order_id)
    {
        $instance = Order::where("id", $order_id)->first();
        if ($instance instanceof Order) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        //type casting
        if (!is_array($data)) {
            $data = (array)$data;
        }
        $filter = [];
        if (isset($data["from_account_id"])) {
            $filter['from_account_id'] = $data["from_account_id"];
        }
        if (isset($data["to_account_id"])) {
            $filter['to_account_id'] = $data["to_account_id"];
        }
        if (isset($data["amount"])) {
            $filter['amount'] = $data["amount"];
        }
        if (isset($data["goods_id"])) {
            $filter['goods_id'] = $data["goods_id"];
        }
        if (isset($data["author_id"])) {
            $filter['author_id'] = $data["author_id"];
        }
        if (isset($data["from_treasury_account_id"])) {
            $filter['from_treasury_account_id'] = $data["from_treasury_account_id"];
        }
        if (isset($data["to_treasury_account_id"])) {
            $filter['to_treasury_account_id'] = $data["to_treasury_account_id"];
        }
        if (isset($data["order_payment_id"])) {
            $filter['order_payment_id'] = $data["order_payment_id"];
        }
        if (isset($data["paid_at"])) {
            $filter['paid_at'] = $data["paid_at"];
        }
        if (isset($data["refund"])) {
            $filter['refund'] = $data["refund"];
        }
        if (isset($data["cashout"])) {
            $filter['cashout'] = $data["cashout"];
        }
        if (isset($data["type"])) {
            $filter['type'] = $data["type"];
        }
        return $filter;
    }

    /**
     * @param $order_id
     * @return int
     */
    public function updateRevoked($order_id)
    {
        return Order::query()
            ->where("id", $order_id)
            ->update(["revoked" => true]);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getProductOrder($order_id)
    {
        $orderInstance = Order::query()
            ->where("id", $order_id)
            ->with("productOrder")
            ->first();
        return $orderInstance
            ->productOrder;
    }

    /**
     * @param $order_id
     * @return int
     */
    public function updateReverse($order_id)
    {
        return Order::query()
            ->where("id", $order_id)
            ->update(["reverse" => true]);
    }
}
