<?php


namespace Modules\Wallet\repo;


use Modules\Wallet\Entities\Payment;

class PaymentDB
{

    /**
     * @param $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Payment
     */
    public function insert($data)
    {
        return Payment::query()->create($data);
    }
}
