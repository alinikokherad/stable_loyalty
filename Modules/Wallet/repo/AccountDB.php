<?php


namespace Modules\Wallet\repo;


use App\ObjectFactory\ModelFactory;
use App\User;
use Modules\Account\Transformers\AccountResource;
use Modules\Account\Transformers\getAccountInfoResource;
use Modules\Branch\Entities\Merchant;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\Transaction;

/**
 * Class AccountDB
 * @package Modules\Wallet\repo
 */
class AccountDB
{

    /**
     * insert account to database
     * @param  $data
     * @return mixed
     */
    public function create($data)
    {
        $data = $this->normalizeInput($data);
        $instance = Account::create($data);
        if ($instance instanceof Account) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function get($id)
    {
        $response = Account::where("id", $id)->with(["credits", "accountType", "user"])->first();
        if ($response instanceof Account) {
            return $response;
        }
        return false;
    }

    /**
     * @param int $club_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getCashAccountInstance($club_id = 1)
    {
        $response = Account::with(["credits", "accountType", "user", "clubs"])->whereHas("accountType", function ($query) {
            $query->where("type", "like", "%cash%");
        })->whereHas("clubs", function ($query) use ($club_id) {
            $query->where("id", $club_id);
        })->first();
        if ($response instanceof Account) {
            return $response;
        }
        return false;
    }

    /**
     * @param $id
     * @param bool $withCredit
     * @param bool $withUser
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAccount($id, $withCredit = false, $withUser = false)
    {
        $accountInstance = Account::where("id", $id);
        $with = [];
        if ($withCredit) {
            array_push($with, "credits");
        }

        if ($withUser) {
            array_push($with, "user");
        }

        $accountInstance = $accountInstance->first();
        if ($accountInstance instanceof Account) {
            return $accountInstance;
        }

        return false;
    }

    /**
     * @param $wallet_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Account|Account[]|object|null
     */
    public function getTreasuryAccount($wallet_id)
    {
        $treasuryId = \DB::table("wallets")
            ->where("wallets.id", $wallet_id)
            ->join("account_types as types", "wallets.id", "=", "types.wallet_id")
            ->where("types.type", "treasury")
            ->join("accounts", "types.id", "=", "accounts.account_type_id")
            ->select("accounts.id")
            ->first();
        return $this->getAccount($treasuryId->id);
    }

    /**
     * @return mixed
     */
    public function getAllTreasuryIds()
    {
        return Account::where("treasury_id", null)->get()->pluck("id")->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Account[]
     */
    public function bankAccountList()
    {
        $bankAccountInstance = Account::with("accountType")->whereHas("accountType", function ($q) {
            $q->where("type", "like", 'bank%');
        })->get();
        return $bankAccountInstance;
    }

    /**
     * @param $wallet_type
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Account|Account[]|object|null
     */
    public function getTreasuryAccountIdByWalletType($wallet_type)
    {
        $treasuryAccountId = \DB::table("wallets")
            ->where("wallets.type", $wallet_type)
            ->join("account_types as types", "wallets.id", "=", "types.wallet_id")
            ->where("types.type", "treasury")
            ->join("accounts", "types.id", "=", "accounts.account_type_id")
            ->select("accounts.id")
            ->first()->id;
        return $treasuryAccountId;
    }

    /**
     * @param $user_id
     * @param $treasury_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserAccount($user_id, $treasury_id)
    {
        $instance = User::where("id", "=", $user_id)
            ->where('type', '=', 'customer')
            ->first();
        $instance = $instance->accounts()
            ->where("treasury_id", "=", $treasury_id)
            ->whereHas("accountType", function ($q) {
                $q->where("type", "=", "customer");
            })->first();
        return $instance;
    }

    /**
     * @param $merchant_id
     * @param $treasury_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getMerchantAccount($merchant_id, $treasury_id)
    {
        $instance = Merchant::where("id", "=", $merchant_id)
            ->with("accounts.accountType")
            ->first();
        $instance = $instance->accounts()->where("treasury_id", "=", $treasury_id)->whereHas("accountType", function ($q) {
            $q->where("type", "=", "merchant");
        })->first();
        return $instance;
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getAccountTreasuryType($id)
    {
        $accountTreasuryType = \DB::table("accounts")
            ->where("id", $id)
            ->join("account_types as type", "accounts.account_type_id", "type.id")
            ->join("wallets", "type.wallet_id", "=", "wallets.id")
            ->select("wallets.type")
            ->get();
        return $accountTreasuryType;
    }

    /**
     * @param $limit
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAccounts($limit)
    {
        $accounts = Account::paginate($limit);
        $resource = GetAccountInfoResource::collection($accounts);
        return $resource;
    }

    /**
     * @param $where
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAccountWhere($where)
    {
        $instance = Account::where($where)->first();
        if ($instance instanceof Account) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function deleteAccount($id)
    {
        $account = Account::find($id);
        $account->delete();
    }

    /**
     * @param $account_id
     * @param null $wallet_id
     * @return mixed
     */
    public function getAccountCredit($account_id, $wallet_id = null)
    {
        $accountInstance = Account::where("id", $account_id)->with("credits")->first();
        $creditInstance = $accountInstance->credits->first();
        return $creditInstance;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        $filter = [];
        if (isset($data["belongs_to"])) {
            $filter["belongs_to"] = $data["belongs_to"];
        }
        if (isset($data["belongs_type"])) {
            $filter["belongs_type"] = $data["belongs_type"];
        }
        if (isset($data["treasury_account_id"])) {
            $filter["treasury_account_id"] = $data["treasury_account_id"];
        }
        if (isset($data["account_type_id"])) {
            $filter["account_type_id"] = $data["account_type_id"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }

    /**
     * @param $account_id
     * @return User|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAccountsUser($account_id)
    {
        $userInstance = User::query()->whereHas("accounts", function ($query) use ($account_id) {
            $query->where("id", $account_id);
        })->first();
        if ($userInstance instanceof User) {
            return $userInstance;
        }
        return false;
    }

    /**
     * @param $account_id
     * @return User|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getAccountsMerchant($account_id)
    {
        $userInstance = Merchant::query()->whereHas("accounts", function ($query) use ($account_id) {
            $query->where("id", $account_id);
        })->first();
        if ($userInstance instanceof Merchant) {
            return $userInstance;
        }
        return false;
    }

    /**
     * @param $from_account_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Account|Account[]
     */
    public function findOrFail($from_account_id)
    {
        return Account::query()->findOrFail($from_account_id);
    }

    /**
     * @param $treasury_id
     * @param $wallet_id
     * @return bool|\Illuminate\Database\Eloquent\Builder|Account
     */
    public function getAgentAccountInstance($treasury_id, $wallet_id)
    {
        $agentInstance = Account::query()
            ->where("treasury_account_id", $treasury_id)
            ->where("belongs_type", "wallet_agent")
            ->whereHas("accountType", function ($query) {
                $query->where("type", "agent");
            })
//            ->where("belongs_to", $wallet_id)
            ->whereHas("accountType.wallet", function ($query) use ($wallet_id) {
                $query->where("id", $wallet_id);
            })
            ->first();
        if ($agentInstance instanceof Account) {
            return $agentInstance;
        }
        return false;
    }

    /**
     * @param $wallet_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getTreasuryAccountByWalletId($wallet_id)
    {
        $accountInstance = Account::query()
            ->whereHas("accountType.wallet", function ($query) use ($wallet_id) {
                $query->where("id", $wallet_id);
            })
            ->whereHas("accountType", function ($query) {
                $query->where("type", "treasury");
            })
            ->first();
        return $accountInstance;
    }

    /**
     * @param $customer_id
     * @param $treasury_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getAccountByUserIdAndTreasuryAccountId($customer_id, $treasury_id)
    {
        $accountInstance = Account::query()
            ->where("treasury_account_id", $treasury_id)
            ->whereHas("user", function ($query) use ($customer_id) {
                $query->where("id", $customer_id);
            })
            ->first();
        return $accountInstance;
    }

    /**
     * @param $merchant_id
     * @param $treasury_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Account|object|null
     */
    public function getAccountByMerchantIdAndTreasuryAccountId($merchant_id, $treasury_id)
    {
        $accountInstance = Account::query()
            ->where("treasury_account_id", $treasury_id)
            ->whereHas("merchant", function ($query) use ($merchant_id) {
                $query->where("id", $merchant_id);
            })
            ->first();
        return $accountInstance;
    }
}
