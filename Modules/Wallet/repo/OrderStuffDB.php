<?php


namespace Modules\Wallet\repo;


use Modules\Wallet\Entities\OrderStuff;

class OrderStuffDB
{

    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|OrderStuff
     */
    public function insert($data)
    {
        $input = $this->normalizeInput($data);
        $instance = OrderStuff::create($input);
        if ($instance instanceof OrderStuff) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $goods_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function find($goods_id)
    {
        return OrderStuff::where("id", $goods_id)->first();
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["title"])) {
            $filter["title"] = $data["title"];
        }
        if (isset($data["subtitle"])) {
            $filter["subtitle"] = $data["subtitle"];
        }
        if (isset($data["description"])) {
            $filter["description"] = $data["description"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }


}
