<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/17/19
 * Time: 10:17 AM
 */

namespace Modules\Wallet\repo;


use Modules\Wallet\Entities\Credit;

class CreditDB
{
    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $input = $this->normalizeInput($data);
        $creditInstance = Credit::create($input);
        if ($creditInstance instanceof Credit) {
            return $creditInstance;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["account_id"])) {
            $filter["account_id"] = $data["account_id"];
        }
        if (isset($data["club_id"])) {
            $filter["club_id"] = $data["club_id"];
        }
        if (isset($data["treasury_id"])) {
            $filter["treasury_id"] = $data["treasury_id"];
        }
        if (isset($data["amount"])) {
            $filter["amount"] = $data["amount"];
        }
        if (isset($data["usable_at"])) {
            $filter["usable_at"] = $data["usable_at"];
        }
        if (isset($data["expired_at"])) {
            $filter["expired_at"] = $data["expired_at"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }
}
