<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/7/19
 * Time: 12:26 PM
 */

namespace Modules\Wallet\repo;


use App\repo\UserDB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Product\repo\CostDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\Events\AfterCreatedOrderEvent;
use Modules\Wallet\Transformers\ProductOrderResource;
use Modules\Wallet\Transformers\ProductOrderResourceCollection;

class ProductOrderDB
{
    /**
     * @param null $from
     * @param null $to
     * @param null $day
     * @param null $week
     * @param null $month
     * @return bool|\Illuminate\Http\Resources\Json\AnonymousResourceCollection|ProductOrderResourceCollection
     */
    public function list($from = null, $to = null, $day = null, $week = null, $month = null)
    {

        if (isset($day) || isset($week) || isset($month)) {

            $where = [];
            if (isset($day)) {
                array_push($where, ["created_at", ">=", Carbon::now()->subDay($day)]);
            } elseif (isset($week)) {
                array_push($where, ["created_at", ">=", Carbon::now()->subWeek($week)]);
            } else {
                array_push($where, ["created_at", ">=", Carbon::now()->subMonth($month)]);
            }

            //if isset (day or week or month) filter
            $instance = ProductOrder::where($where)
                ->where("paid_at", "!=", null)
                ->with(["orders", "costs"])
                ->get();

        } elseif (isset($from, $to)) {

            $from = date("Y-m-d" . " 00:00:00", strtotime($from));
            $to = date("Y-m-d" . " 23:59:59", strtotime($to));

            //if is set (from & to) filter
            $instance = ProductOrder::where([
                ["created_at", ">=", $from],
                ["created_at", "<=", $to]])
                ->where("paid_at", "!=", null)
                ->with(["orders", "costs"])
                ->get();

        } else {

            //if is not set any filter
            $instance = ProductOrder::with(["orders", "costs"])
                ->where("paid_at", "!=", null)
                ->get();

        }
        if (!$instance) {
            return false;
        }
        $resource = ProductOrderResource::collection($instance);
        return $resource;
    }

    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|ProductOrder
     */
    public function create($data)
    {
        $input = $this->normalizeInput($data);
        $instance = ProductOrder::create($input);
        if ($instance instanceof ProductOrder) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|ProductOrder
     */
    public function firstOrCreate($data)
    {
        $instance = ProductOrder::firstOrCreate($data);
        if ($instance instanceof ProductOrder) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function productOrderInstance($id)
    {
        $instance = ProductOrder::where("id", $id)->with("orders")->first();
        if ($instance instanceof ProductOrder) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $payment
     * @param $mobile
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|ProductOrderResourceCollection
     */
    public function requestOrder($payment, $mobile)
    {
        //initialize query
        $productOrdersInstance = ProductOrder::with(["costs.product", "user"]);

        //payment status filter
        if ($payment == false) {
            $productOrdersInstance = $productOrdersInstance->whereNull('paid_at');
        } elseif ($payment == true) {
            $productOrdersInstance = $productOrdersInstance->whereNotNull("paid_at");
        }


        if (!empty($mobile)) {
            $productOrdersInstance->whereHas("user", function ($q) use ($mobile) {
                $q->where('mobile', '=', $mobile);
            });
            ProductOrderResource::$with_user = true;
        }

        /*        $walletDB = new WalletDB();
                //get total order cost
                $treasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType("rials");
                $total_rial_cost = 0;
                foreach ($this->orders as $order) {
                    if ($order->treasury_account_id == $treasuryAccountInstance->id) {
                        $total_rial_cost += $order->amount;
                    }
                }*/
//        $productOrders = ProductOrderResourceCollection::make($productOrders->get())->getOrderCost();
//        $productOrders = ProductOrderResource::collection($productOrders->get())->getOrderCost($total_rial_cost);
//        (new UserResourceCollection($user))->foo('bar');
        $productOrders = ProductOrderResource::collection($productOrdersInstance->get());

        return $productOrders;
    }

    /**
     * @param $productOrderId
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOrdersWithProductOrders($productOrderId)
    {
        $productOrderInstance = ProductOrder::where("id", $productOrderId)->with("orders")->first();
        return $productOrderInstance;
    }

    /**
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getProductOrderWithUserId($user_id)
    {
//        $productOrders = ProductOrder::where('user_id', $user_id)->with(['orders', 'costs.product'])->get();
        $productOrders = ProductOrder::where('user_id', $user_id)->with(['orders', 'costs.product'])->whereHas("orders", function ($query) {
            $query->whereNull("paid_at");
        })->get();
        return $productOrders;
    }

    /**
     * @param $productOrder_id
     * @return bool|int
     */
    public function updatePaid_at($productOrder_id)
    {
        return ProductOrder::query()->where("id", $productOrder_id)->update(["paid_at" => Carbon::now()->format("Y-m-d H:i:s")]);
    }

    /**
     * @param $data
     * @return array
     */
    private function normalizeInput($data)
    {
        $filter = [];
        if (isset($data["merchant_id"])) {
            $filter["merchant_id"] = $data["merchant_id"];
        }
        if (isset($data["customer_id"])) {
            $filter["customer_id"] = $data["customer_id"];
        }
        if (isset($data["description"])) {
            $filter["description"] = $data["description"];
        }
        if (isset($data["paid_at"])) {
            $filter["paid_at"] = $data["paid_at"];
        }
        if (isset($data["author_id"])) {
            $filter["author_id"] = $data["author_id"];
        }
        if (isset($data["merchant_user_id"])) {
            $filter["merchant_user_id"] = $data["merchant_user_id"];
        }
        if (isset($data["revoked"])) {
            $filter["revoked"] = $data["revoked"];
        }
        return $filter;
    }

    /**
     * get all productOrders order instance
     * @param $productOrder_id
     * @return mixed
     */
    public function getOrdersInstance($productOrder_id)
    {
        $productOrders = ProductOrder::query()
            ->where("id", $productOrder_id)
            ->with("orders")
            ->first();
//        dd($productOrders->orders->toArray());
        if (isset($productOrders->orders) && $productOrders->orders->first() instanceof Order) {
            return $productOrders->orders;
        }
        return null;
    }

    /**
     * @param $productOrder_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|ProductOrder|object|null
     */
    public function find($productOrder_id)
    {
        return ProductOrder::query()->where("id", $productOrder_id)->first();
    }

    /**
     * @param $productOrder_id
     * @return bool|int
     */
    public function updateRevoked($productOrder_id)
    {
        return ProductOrder::query()->where("id", $productOrder_id)->update(["revoked" => true]);
    }

    /**
     * @param $productOrder_id
     * @return bool|int
     */
    public function updateReverse($productOrder_id)
    {
        return ProductOrder::query()->where("id", $productOrder_id)->update(["reverse" => true]);
    }


}
