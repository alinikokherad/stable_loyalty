<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\repo\UserDB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Modules\Branch\repo\MerchantDB;
use Modules\Wallet\Entities\Payment;
use Modules\Wallet\Events\AfterPaymentSuccessEvent;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\PaymentDB;
use Modules\Wallet\repo\WalletDB;

class PaymentController extends Controller
{
//    use \App\Traits\Response;

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        return Payment::paginate(10);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function success(Request $request)
    {
        if (\Gate::allows("merchant") || \Gate::allows("personnel") || \Gate::allows("waiter")) {
            try {
                \DB::beginTransaction();

                //insert log to payment model
                $paymentDB = new PaymentDB();
                $paymentData = [
                    "merchant_id" => isset($request->merchant_id) ? $request->merchant_id : \Auth::user()->merchant_id,
                    "terminal_id" => isset($request->terminal_id) ? $request->terminal_id : null,
                    "error_code" => isset($request->error_code) ? $request->error_code : null,
                    "amount" => isset($request->amount) ? $request->amount : null,
                    "order_id" => isset($request->order_id) ? $request->order_id : null,
                    "card_number" => isset($request->card_number) ? $request->card_number : null,
                    "extra" => isset($request->extra) ? $request->extra : null,
                ];
                $paymentInstance = $paymentDB->insert($paymentData);
                if (!$paymentInstance instanceof Payment) {
                    Log::error("transaction failed: card number is =>" . $request->card_number . " status_code: " . $request->error_code);
                    return $this->errorResponse("400", __("messages.payment_cannot_paid"), 400);
                }

                if (!isset($request->error_code) or $request->error_code !== 0) {
                    Log::info("transaction failed: card number is =>" . $request->card_number . " status_code: " . $request->error_code);
                    return $this->errorResponse("payment_cannot_paid", __("messages.error_code") . " $request->error_code", 400);
                }

                event(new AfterPaymentSuccessEvent($request));
                Log::info("transaction paid successfully: card number is =>" . $request->card_number . " status_code: " . $request->error_code);


                \DB::commit();
                return $this->successResponse("payment_success", $paymentInstance, 201);
            } catch (\Exception $e) {
                \DB::rollBack();
                return $this->errorResponse("400", $e->getMessage(), 400);
            }
        } else {
            return $this->errorResponse("400", __("messages.access_denied"), 403);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function cashPayment(Request $request)
    {
        $this->validate($request, [
            "uid" => "required",
            "amount" => "required",
        ], [
            "uid.required" => __("messages.uid_required"),
            "amount.required" => __("messages.amount_required"),
        ]);
        $orderController = new OrderController();
        $accountDB = new AccountDB();
        $userDB = new UserDB();
        $walletDB = new WalletDB();
        $amount = $request->amount;
        $cash_account_id = $request->has("cash_account_id") ? null : null;
        $uid = $request->has("uid") ? $request->uid : null;

        //when amount is zero
        if ($amount == 0) {
            return $this->successResponse("payment_success", __("messages.payment_success"));
        }

        try {
            \DB::beginTransaction();

            //get customer account instance
            $customerUserInstance = $userDB->get($request->customer_id, true);
            $customerAccountInstance = $accountDB->get($customerUserInstance->accounts->first()->id);

            //check uid
            $redisUid = Redis::get("uid:" . $customerUserInstance->mobile);
            if ($uid == $redisUid) {
                return $this->errorResponse("400", __("messages.this_payment_is_paid"));
            }

            //get cash account with merchant id
            $rialsTreasuryInstance = $walletDB->getTreasuryAccountInstanceWithWalletType("rials");
            if (empty($cash_account_id)) {
                $merchantDB = new MerchantDB();
                //get rials treasury account id
                $cashAccountInstance = $merchantDB->getMerchantAccountInstance(\Auth::user()->merchant_id, $rialsTreasuryInstance->id);
            } else {
                $cashAccountInstance = $accountDB->getAccount($cash_account_id, true);
            }

            //create order for payment
            $orderData = [
                "from_account_id" => $cashAccountInstance->id,
                "to_account_id" => $customerAccountInstance->id,
                "amount" => $amount,
                "treasury_account_id" => $rialsTreasuryInstance->id,
                "paid_at" => now()->format("Y-m-d H:i:s"),
                "type" => "cash_payment_table_order"
            ];
            $orderInstance = $orderController->store($orderData, "cashPaymentTableOrder");

            //submit payment into redis for 60 minutes
            $res = Redis::set("uid:" . $customerUserInstance->mobile, $uid, "EX", 3600);
            \DB::commit();
            return $this->successResponse("payment_success", $orderInstance);
        } catch (\Exception $e) {
            \DB::rollBack();
            return $this->errorResponse("400", __("messages.payment_not_success"));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('wallet::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('wallet::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('wallet::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
