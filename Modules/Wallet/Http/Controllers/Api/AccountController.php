<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\ObjectFactory\ModelFactory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Http\Requests\updateAccountRequset;
use Modules\Wallet\repo\CreditDB;
use Modules\Wallet\repo\AccountDB;

class AccountController extends Controller
{
    /**
     * Display a listing of the account.
     * @param AccountDB $repository
     * @return false|string
     */
    public function index(AccountDB $repository)
    {
        $limit = request()->has("limit") ? request()->input("limit") : 5;
        return $repository->getAccounts($limit);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function bankList()
    {
        if ((!auth()->check()) && (!auth()->user()->type == "merchant")) {
            return response()
                ->json([
                    "message" => __("messages.404"),
                    "error" => __("messages.merchant_user_needed")
                ], 403);
        }

        //get all bank accounts
        $bankList = ModelFactory::build("AccountDB");
        $bankAccountList = $bankList->bankAccountList();
        if ($bankAccountList->isEmpty()) {
            return response()
                ->json([
                    "message" => __("messages.404"),
                ], 400);
        }
        return response()
            ->json([
                "message" => __("messages.bank_list"),
                "data" => $bankAccountList
            ], 200);
    }

    /**
     * Show the specified account.
     * @param int $id
     * @param AccountDB $repository
     * @return mixed
     */
    public function show(AccountDB $repository, $id)
    {
        $account = $repository->getAccount($id);
        return $account;
        /*        $account = $repository->getAccount($id);
                return new getAccountInfoResource($account);*/
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $accountDB = new AccountDB();
        $creditDB = new CreditDB();
        try {
            \DB::beginTransaction();

            //insert account into database
            $accountData = $request->all();
            $accountInstance = $accountDB->create($accountData);

            //check for successfully create account
            if (!$accountInstance) {
                return \response()
                    ->json([
                        'message' => __("messages.account_create_fail"),
                        "error" => __("messages.account_create_fail")
                    ], 409);
            }

            //create credit
            $creditData = [
                "account_id" => $accountInstance->id,
                "treasury_id" => $accountInstance->treasury_id,
                "amount" => $accountData['amount'],
            ];
            $creditDB->create($creditData);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();

        }

        //success response after create account
        return response()
            ->json([
                "message" => "account successfully created"
            ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param updateAccountRequset $request
     * @param int $id
     * @param AccountDB $repository
     * @return mixed
     */
    public function update(updateAccountRequset $request, AccountDB $repository, $id)
    {
        $data = $repository
            ->convertRequestToArray($request);
        $account = Account::where('id', $id)
            ->update($data);
        return $account;
    }

    /**
     * @param AccountDB $repository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(AccountDB $repository, $id)
    {
        $repository->deleteAccount($id);
        return \response()
            ->json([
                'message' => __("messages.user_success_deleted")
            ], 204);
    }
}
