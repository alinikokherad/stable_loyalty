<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\Traits\Response;
use Modules\Wallet\Events\AfterCreatedOrderEvent;
use Modules\Wallet\Http\Requests\WalletRequest;
use Modules\Wallet\Jobs\CreateTransactionJob;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\OrderDB;
use Modules\Wallet\repo\OrderStuffDB;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\Credit;
use Modules\Wallet\Events\WalletCreatedEvent;
use Modules\Wallet\Events\WalletDeletedEvent;
use Modules\Wallet\repo\WalletDB;
use Modules\Wallet\Entities\Wallet;

class WalletController extends Controller
{
    use Response;

    /**
     * @param WalletRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(WalletRequest $request)
    {
        $walletDB = new WalletDB();
        $data = $request->all();
        try {
            \DB::beginTransaction();

            //create wallet (wallet type)
            $walletData = [
                "title" => $request->title,
                "type" => $request->type,
                "x_change" => $request->x_change
            ];
            $walletInstance = $walletDB->create($walletData);

            //after create wallet event(create two treasury account)
            $result = event(new WalletCreatedEvent($walletInstance, $data));

            //assign wallet to club
            if (is_array($request->club_id)) {
                $club_id = (array)$request->club_id;
            }
            $walletInstance->clubs()->sync($club_id ?? [$request->club_id]);

            \DB::commit();
            return $this->successResponse("wallet_create", $walletInstance, 201);
        } catch (\Exception $e) {
            \DB::rollBack();
            return $this->errorResponse("400", $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param WalletDB $walletDB
     * @throws \Exception
     */
    /*    public function store(Request $request, WalletDB $walletDB)
        {
            $this->create($request, $walletDB);
        }*/

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $r)
    {
        return \Response()->json([
            'data' => Wallet::where($r->filters[0][0], $r->filters[0][1], $r->filters[0][2])
                ->paginate($r->input("page_limit"), ["*"], 'page', $r->input("page_number"))
        ], 200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return Wallet::where("id", $id)->first();
    }

    /**
     * @param Request $r
     * @param $id
     * @param WalletDB $walletDB
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $r, $id, WalletDB $walletDB)
    {
        $data = $r->all();
        $walletInstance = $walletDB->find($id);
        if (is_null($walletInstance)) {
            return response()
                ->json([
                    'message' => $id . __('messages.not_exist')
                ], 400);
        }
        if ($walletDB->update($id, $data)) {
            return response()->json($walletDB->find($id), 201);
        } else {
            return Response()
                ->json([
                    'message' => __('messages.wallet_update_error')
                ], 400);
        }
    }

    /**
     * @param $id
     * @param WalletDB $walletDB
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id, WalletDB $walletDB)
    {
        event(new WalletDeletedEvent($id));
        if (is_null($walletDB->find($id))) return response()->json([$id . __('messages.not_exist')]);
        try {
            \DB::beginTransaction();
            //delete wallet (wallet type)
            $walletDB->delete($id);

            //after delete wallet delete [accType, acc, credit]
            $accTypeId = AccountType::select("id")->where("wallet_id", $id)->get()->toArray()[0]["id"];
            AccountType::where("wallet_id", $id)->delete();
            Account::where("account_type_id", $accTypeId)->delete();
            credit::where("account_id", $accTypeId)->delete();

            \DB::commit();
            return response()
                ->json('', 204);
        } catch (\Exception $e) {
            \DB::rollBack();
            return \response()
                ->json([
                    "message" => __('messages.400'),
                    "error" => "{$e->getMessage()}"
                ], 400);
        }
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function walletCredit(Request $r)
    {
        $idUser = auth()->user()["id"];
        $account = Account::where('user_id', $idUser)
            ->with(['credits', 'accountType'])->whereHas('accountType', function ($q) {
                $q->with("wallet");
            })->get();
        $walletTitle = [];
        $creditInfo = [];
        foreach ($account->toArray() as $key) {
            $credit = $key["credits"][0];
            $walletTitle = Wallet::where("id", $key["account_type"]["id"])->get()->toArray()[0]["title"];
            $Arr = ["id" => $credit["id"],
                "club_id" => $credit["club_id"],
                "amount" => $credit["amount"],
                "type" => $walletTitle,
                "expired_at" => $credit["expired_at"],
                "usable_at" => $credit["usable_at"]];

            array_push($creditInfo, $Arr);
        };

        $walletTitle = $account[0]->accountType->wallet->toArray()["title"];
        return Response()->json([
            'data' => $creditInfo
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function creditCharge(Request $request)
    {
        $orderDB = new OrderDB();
        $orderStuffDB = new OrderStuffDB();
        $accountTypeDB = new AccountTypeDB();
        try {
            DB::beginTransaction();
            //stuff create
            $stuffData = [
                "title" => $request->title,
                "subtitle" => $request->subtitle,
                "description" => $request->description,
            ];
            $stuffInstance = $orderStuffDB->insert($stuffData);

            //get treasury account instance
            $treasuryId = $accountTypeDB->getTreasuryAccountWithAccountId($request->bank_account_id);

            //order create
            $orderData = [
                "type" => "accountChargeWithCard",
                "goods_id" => $stuffInstance->id,
                "from_account_id" => $request->bank_account_id,
                "to_account_id" => $request->customer_account_id,
                "amount" => $request->amount,
                "paid_at" => date("Y-m-d H:i:s", time()),
                "treasury_account_id" => $treasuryId,
            ];
            $req = new Request();
            $request = $req->replace($orderData);
            $orderInstance = $orderDB->create($request);
            CreateTransactionJob::dispatchNow(
                $orderInstance,
                $request
//                $fromAccountInstance,
//                $toAccountInstance,
//                $treasuryInstance,
//                $productOrderInstance
            );
//            event(new CreatedOrderEvent($orderInstance,$request,null,null,null,null));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => $e->getMessage(),
                ], 400);
        }
        return response()
            ->json([
                "message" => __("messages.credit_charged_successfully")
            ], 200);
    }
}
