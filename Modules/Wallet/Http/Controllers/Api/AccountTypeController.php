<?php

namespace Modules\Wallet\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\repo\AccountTypeDB;

class AccountTypeController extends Controller
{
    /**
     *
     */
    public function index(Request $r)
    {
        return Response()->json([
            'data' => AccountType::where($r->filters[0][0], $r->filters[0][1], $r->filters[0][2])
                ->paginate($r->input("page_limit"), ["*"], 'page', $r->input("page_number"))
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $accountTypeDB = new AccountTypeDB();
        $accountTypeData = $request->all();

        try {
            //create account type for club
            $accountTypeDB->create($accountTypeData);

        } catch (\Exception $e) {
            return \response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => $e->getMessage(),
                ], 400);
        }
        return \response()
            ->json([
                "message" => __('messages.account_type_create')
            ], 200);

    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        //
    }
}
