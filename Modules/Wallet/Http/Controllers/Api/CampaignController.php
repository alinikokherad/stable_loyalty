<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\ObjectFactory\ModelFactory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Attachment\Entities\Attachment;
use Modules\Attachment\Http\Controllers\AttachmentController;
use Modules\Club\repo\ClubDB;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\CampaignDB;
use Modules\Wallet\repo\CreditDB;
use Modules\Wallet\repo\WalletDB;

class CampaignController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = \request("limit") ?? 5;
        $page = \request("page") ?? 1;
        $clubDB = new ClubDB();

        $loginUserId = \Auth::user()->id;
        if ($loginUserId) {
            $userClub = $clubDB->findClubWithUserId($loginUserId);
            $clubCampaigns = $userClub->campaigns;
            $result = [];
            foreach ($clubCampaigns as $clubCampaign) {
                $a = Account::find($clubCampaign->account_id);
                $b = $a->accountType()->first();

                //check if campaign is deleted wrongly(delete campaign just from campaign table by manually)
                if (!isset($b->type)) {
                    continue;
                }

                $clubCampaign->type = $b->type;
                $clubCampaign->wallet_id = $b->wallet_id;
                $clubCampaign->title = $b->title;
                $result[] = $clubCampaign;
            }
            $resource = collect($result)->paginate($limit, $page);
            return \response()
                ->json([
                    "data" => $resource
                ], 200);
        }

        return \response()
            ->json([
                "message" => __('messages.400'),
                "error" => __('messages.campaign_index_error')
            ], 400);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        /*        if (\Gate::allows("club_admin")) {
                    return \response()
                        ->json([
                            "message" => __('messages.400'),
                            "error" => __('messages.403')
                        ], 403);
                }*/
        $data = $request->all();
        $campaignDB = new CampaignDB();
        $accountTypeDB = new AccountTypeDB();
        $accountDB = new AccountDB();
        $walletDB = new WalletDB();
        $creditDB = new CreditDB();
        try {
            \DB::beginTransaction();
            //create campaign
            $campaignData = [
                "club_id" => $request->club_id,
                "started_at" => $request->started_at,
                "expired_at" => $request->expired_at,
                "budget" => $request->budget,
                "budget_consumed" => $request->budget_consumed,
            ];
            $campaignInstance = $campaignDB->create($campaignData);

            //create account_type
            $accountTypeData = [
                "type" => $request->type,
                "wallet_id" => $request->wallet_id,
                "title" => $request->title,
                "subtitle" => $request->subtitle,
                "description" => $request->description,
                "balance_type" => $request->balance_type,
                "min_account_amount" => $request->min_account_amount,
                "max_account_amount" => $request->max_account_amount,
                "min_transaction_amount" => $request->min_transaction_amount,
                "max_transaction_amount" => $request->max_transaction_amount,
                "legal" => $request->legal,
                "interest_rate" => $request->interest_rate,
                "interest_period" => $request->interest_period,
            ];
            $accountTypeInstance = $accountTypeDB->create($accountTypeData);

            $treasuryAccountInstance = $walletDB->getAccountTreasuryWithWalletId($data["wallet_id"]);

            //create account
            $accountData = [
                "belongs_to" => $campaignInstance->id,
                "belongs_type" => "campaign",
                "treasury_account_id" => $treasuryAccountInstance->id,
                "account_type_id" => $accountTypeInstance->id,
                "revoked" => $request->revoked,
            ];
            $accountInstance = $accountDB->create($accountData);

            //create credit
            $creditData = [
                "account_id" => $accountInstance->id,
                "amount" => $request->budget,
                "usable_at" => $request->started_at,
                "expired_at" => $request->expired_at,
            ];
            $creditInstance = $creditDB->create($creditData);

            $campaignInstance->account_type = $accountTypeInstance->toArray();

            //assign image to campaign when isset
            if (!empty($request->file("img"))) {
                $newName = 'image' . Carbon::now()->isoFormat('H-i-s') . '.' . $request->file('img')->getClientOriginalExtension();
                $permission = $request->file('img')->move(public_path('uploads/campaigns'), $newName);
                if (!$permission) {
                    return response()
                        ->json([
                            "message" => __("messages.400"),
                            "error" => __("messages.public_folder_access_denied")
                        ], 400);
                }
                $attachment = new Attachment([
                    "type" => "avatar",
                    "src" => 'uploads/campaigns/' . $newName,
                    "status" => "active"
                ]);
                $campaignInstance->attaches()->save($attachment);
                if ($attachment instanceof Attachment) {
                    $campaignInstance->attachment = $attachment->toArray();
                }
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return \response()
                ->json([
                    "message" => __('messages.400'),
                    "error" => __('messages.campaign_create_error')
                ], 400);
        }

        //if successfully create campaign
        return \response()
            ->json([
                "message" => __('messages.campaign_create'),
                "data" => $campaignInstance
            ], 200);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function charge(Request $request)
    {
        if (\Gate::allows("merchant")) {
            try {
                \DB::beginTransaction();
                //get campaign instance
                $campaignDB = ModelFactory::build("CampaignDB");
                $campaignInstance = $campaignDB->getCampaignWithRelationShips($request->campaign_id);


                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollBack();
                return \response()
                    ->json([
                        "message" => __("messages.400")
                    ], 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
