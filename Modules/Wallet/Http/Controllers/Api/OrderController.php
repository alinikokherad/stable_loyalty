<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\Events\SendDorisGiftSmsEvent;
use App\Http\Resources\UserResource;
use App\ObjectFactory\ModelFactory;
use App\repo\UserDB;
use App\Traits\Response;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Account\Entities\Account;
use Modules\Branch\repo\MerchantDB;
use Modules\Club\repo\ClubDB;
use Modules\Product\repo\CostDB;
use Modules\Product\Repo\TableDB;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\OrderStuff;
use Modules\Wallet\Http\Requests\TableOrderCheckoutRequest;
use Modules\Wallet\Http\Requests\TableOrderRequest;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\OrderDB;
use Modules\Wallet\repo\OrderStuffDB;
use Modules\Wallet\repo\ProductOrderDB;
use Modules\Wallet\repo\TransactionDB;
use Modules\Wallet\repo\WalletDB;
use Modules\Wallet\Transformers\ProductOrderResource;
use mysql_xdevapi\Exception;

/**
 * @property Order refund
 */
class OrderController extends Controller
{
    use Response;
    private $refund;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = \request()->has("limit") ? \request()->input("limit") : 5;
        $page = \request()->has("page") ? \request()->input("page") : 1;
        $from = \request()->has("from") ? \request()->input("from") : null;
        $to = \request()->has("to") ? \request()->input("to") : null;
        $productOrderDB = new ProductOrderDB();

        //get all orders by pagination
        $productOrders = $productOrderDB->list($from, $to);

        //push list of order into resource collection
        $resource = ProductOrderResource::collection($productOrders);

        $resource = $resource->paginate($limit, $page);

        if (!$resource) {
            return response()
                ->json([
                    "message" => __('messages.400'),
                    "error" => __('messages.no_order')
                ], 400);
        }
        return response()
            ->json([
                "message" => __('messages.order_index'),
                "data" => $resource
            ], 200);
    }

    /**
     * @param $orderData
     * @param null $type
     * @return bool|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store($orderData, $type = null)
    {
        $orderDB = new OrderDB();
        try {
            DB::beginTransaction();

            //create wallet order
            $orderInstance = $orderDB->create($orderData);

            if (!$orderInstance instanceof Order) {
                throw new \Exception("order is not created");
            }

            if ($orderInstance instanceof Order && $type == "dorris_gift") {
                $walletDB = new WalletDB();
                $userDB = new UserDB();

                //get user with orders to_account_id
                $walletInstance = $walletDB->getByType("point");
                $userInstance = $userDB->getUserWithAccountId($orderInstance->to_account_id);
                $userPointCredit = $userInstance->accounts()->where("treasury_id", $walletInstance->id)->first()->credits->first()->amount;
                $totalPoint = $userPointCredit + $orderInstance->amount;

                //send gift sms for doris customer
                event(new SendDorisGiftSmsEvent($userInstance->mobile, $orderInstance->amount, $totalPoint));
            }

            //TODO ثبت تراکنش برای گیفت و شارژ اکانت از اینجا درست شود
            if ($type == "gift" || $type == "bank" || $type == "table" || $type == "dorris_rials" || $type == "dorris_gift" || $type == "cashPaymentTableOrder") {
                $transactionController = new TransactionController();
                $res = $transactionController->store($orderInstance, $type);
            }
            DB::commit();
            return $orderInstance;

        } catch (\Exception $e) {
            DB::rollBack();
            return response()
                ->json([
                    "message" => __('messages.400'),
                    "error" => __('messages.create_order_error') . " stack: " . $e->getMessage()
                ], 400);
        }
    }

    /**
     * @param $orderInstance
     * @param null $type
     */
    public function submitTransaction($orderInstance, $type = null)
    {
        $transactionController = new TransactionController();
        $transactionController->store($orderInstance);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productOrderCheckout(Request $request)
    {
        //get product order instance
        $productOrderDB = new ProductOrderDB();
        $walletDB = new WalletDB();
        $userDB = new UserDB();
        $productOrderInstance = $productOrderDB->getOrdersWithProductOrders($request->product_order_id);

        //check to paid or not
        if ($productOrderInstance->paid_at != null) {
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => __("messages.this_order_paid_before")
                ], 400);
        }

        //get customer credit value
        $customerInstance = $userDB->get($productOrderInstance->user_id, true);

        //sum product order total value
        $rials = 0;
        $point = 0;
        foreach ($productOrderInstance->orders as $orderInstance) {
            $walletInstance = $walletDB->getWalletWithAccountTreasuryId($orderInstance->treasury_account_id);
            switch ($walletInstance->type) {
                case "rials":
                    $rials += $orderInstance->amount;
                    break;
                case "point":
                    $point += $orderInstance->amount;
            }
        }

        //check for can checkout
        $rialsOldValue = $customerInstance->accounts->first()->credits()->first()->amount;
        $pointOldValue = $customerInstance->accounts->last()->credits()->first()->amount;
        if ($rialsOldValue >= $rials && $pointOldValue >= $point) {
            $orderResponse = [];
            foreach ($productOrderInstance->orders as $orderInstance) {
                //check to can buy this product order
                //call submit transaction with transaction controller
                $response = $this->submitTransaction($orderInstance);
//                $orderResponse = array_push($orderResponse, $response->toArray());
            }
            return \response()
                ->json([
                    "message" => __("messages.transaction_submit_successfully"),
                    "data" => $orderResponse
                ], 200);
        }
        return response()
            ->json([
                "message" => __("messages.400"),
                "error" => __("messages.dont_have_enough_money")
            ], 400);
    }

    /**
     * @param $order_id
     */
    public function paid_at($order_id)
    {
        $orderDB = ModelFactory::build("OrderDB");
        $response = $orderDB->paid_at($order_id);
//        if ($response == true) {
//
//        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        //get user with him orders
        $userDB = new UserDB();
        $mobile = \request()->has("mobile") ? \request("mobile") : \Auth::user()->mobile;
        $page = \request()->has("page") ? \request("page") : 1;
        $limit = \request()->has("limit") ? \request("limit") : 10;
        $from = \request()->has("from") ? \request("from") : null;
        $to = \request()->has("to") ? \request("to") : null;
        $day = \request()->has("day") ? \request("day") : null;
        $week = \request()->has("week") ? \request("week") : null;
        $month = \request()->has("month") ? \request("month") : null;

        $resource = $userDB->getUserProductOrder($mobile, $from, $to, $day, $week, $month);

        if (empty($resource)) {
            return response()
                ->json([
                    "message" => __('messages.400'),
                    "error" => __('messages.no_order'),
                ], 400);
        }

        //convert resource data to pagination
        $response = $resource->paginate($limit, $page);

        return response()
            ->json([
                "message" => __('messages.user_orders'),
                "data" => $response
            ], 200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Order::where("id", $id)->delete();
        return \response()
            ->json([
                null
            ], 204);
    }

    /**
     * @param $data
     * @return bool|\Illuminate\Database\Eloquent\Model|OrderStuff
     */
    public function createOrderStuff($data)
    {
        $orderStuffDB = new OrderStuffDB();
        $orderStuffInstance = $orderStuffDB->insert($data);
        return $orderStuffInstance;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function refund(Request $request)
    {
        $order = Order::where(["id" => $request->order_id, "refund" => false])->with("transactions")->first();
        if (!($order instanceof Order)) {
            return \response()
                ->json([
                    "message" => __('messages.order_refund'),
                    "error" => __('messages.no_data')
                ], 404);
        }

        /*-----------refund transaction---------------*/
        DB::transaction(function () use ($order) {
            $transaction = new TransactionDB();
            Order::where("id", $order->id)->update(["refund" => true]);

            //first transaction
            $data = [
                "order_id" => $order->id,
                "account_id" => $order->from_account_id,
                "amount" => $order->amount,
                "type" => "add",
                "reverse" => 1,
            ];
            $transaction->insert($data);

            //second transaction
            $data['account_id'] = $order->to_account_id;
            $data['type'] = "low_off";
            $transaction->insert($data);

            $from = Account::find($order->from_account_id);
            $to = Account::find($order->to_account_id);

            //update from account credit
            $old_credit = $from->credit->toArray();
            $new_treasury = ($old_credit["treasury"]) + ($order->amount);
            $from->credit->update([
                "treasury" => $new_treasury,
            ]);

            //update to account credit
            $old_credit = $to->credit->toArray();
            $old_treasury = ($old_credit["treasury"]) - ($order->amount);
            $to->credit->update([
                "treasury" => $old_treasury,
            ]);

            $this->refund = $order;
        });

        return \response()
            ->json([
                "message" => __('messages.refund_success'),
                "data" => $this->refund
            ], 200);
    }

    /**
     * @param TableOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function tableOrder(TableOrderRequest $request)
    {
        if (!\Gate::allows("merchant")) {
            return $this->errorResponse("403", __("access_denied"), 403);
        }
        try {
            DB::beginTransaction();
            //get merchant account id
            $merchantDB = new MerchantDB();
            $loginUserMerchantId = \Auth::user()->merchant_id;
            $merchantInstance = $merchantDB->find($loginUserMerchantId);

            //submit point product (gift)
            if ($request->has("cost_id")) {

                //get customer instance and accounts
                $walletDB = new WalletDB();
                $costDB = new CostDB();
                $userDB = new UserDB();
                $treasury_account = $walletDB->getTreasuryAccountWithWalletType("point");
                $customerInstance = $userDB->get($request->customer_id, true);
                $customerAccountInstance = $customerInstance->accounts()->where("treasury_id", $treasury_account->id)->first();

                //get cost id for check credit
                $costInstance = $costDB->find($request->cost_id);
                if (($costInstance->cost) > ($customerAccountInstance->credits()->first()->amount)) {
                    return response()
                        ->json([
                            "message" => __("messages.400"),
                            "error" => __("messages.your_credit_is_not_enough")
                        ], 400);
                }

                $productOrderController = new ProductOrderController();
                $productOrderRequest = new Request();
                $productOrderData = [
                    "costs" => [
                        "point" => [
                            $request->cost_id => 1
                        ]
                    ],
                    "customer_id" => $request->customer_id,
                    "author_id" => $request->author_id,
                    "description" => "table order waiter trust",
                    "type" => "gift",
                ];
                $productOrderData = $productOrderRequest->replace($productOrderData);
                $productOrderController->store($productOrderData);
            }

            if ($request->has("tables")) {

                //get customer instance and accounts
                $userDB = new UserDB();
                $customerInstance = $userDB->get($request->customer_id, true);

                //get rials treasury account
                $walletDB = new WalletDB();
                $treasury_account = $walletDB->getTreasuryAccountWithWalletType("rials");

                $merchantRialsAccount = $merchantInstance->accounts->where("treasury_id", $treasury_account->id)->first();

                //get customer rials account
                $customerRialsAccountInstance = $customerInstance->accounts->where("treasury_id", $treasury_account->id)->first();

                // create order for table
                $orderData = [
                    "from_account_id" => $customerRialsAccountInstance->id,
                    "to_account_id" => $merchantRialsAccount->id,
                    "amount" => null,
                    "goods_id" => null,
                    "author_id" => $request->author_id,
                    "treasury_account_id" => $treasury_account->id,
                    "paid_at" => null,
                    "type" => "table_order_request",
                ];
                $res = $orderInstance = $this->store($orderData);

                //sync order instance with tables
                $res = $orderInstance->tables()->sync($request->tables);

                //update table to reserve
                $tableDB = new TableDB();
                foreach ($request->tables as $table) {
                    $tableDB->updateStatus($table, "reserved");
                }
            }
            DB::commit();
            if (isset($orderInstance)) {
                return response()
                    ->json([
                        "message" => __("messages.order_create_successfully"),
                        "data" => $orderInstance
                    ], 200);
            } else {
                return response()
                    ->json([
                        "message" => __("messages.order_create_successfully"),
                    ], 200);

            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()
                ->json([
                    "message" => __("messages.400"),
                    "error" => $e->getMessage(),
                    "error1" => $e->getLine(),
                ], 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function testTableOrder(Request $request)
    {
        if (!\Gate::allows("merchant")) {
            return $this->errorResponse("403", __("access_denied"), 403);
        }
        //get request tables
        $requestTables = $request->tables;

        //get all tables
        $tableDB = new TableDB();
        $tablesId = $tableDB->getAllOpenTablesIds();

        //check table status
        foreach ($requestTables as $requestTable) {
            if (!in_array($requestTable, $tablesId)) {
                return $this->errorResponse("400", __("messages.table_reserved_before"), 400);
            }
        }

        //validation of selected table
        if (empty($requestTables) or !isset($requestTables)) {
            return $this->errorResponse("400", __("messages.table_not_selected"), 400);
        }

        try {
            DB::beginTransaction();
            $userDB = new UserDB();
            $walletDB = new WalletDB();
            $costDB = new CostDB();
            $merchantDB = new MerchantDB();
            $productOrderDB = new ProductOrderDB();
            $allOrders = [];

            //cost instance
            $pointCost = $costDB->getCost($request->cost_id);

            //get merchant instance
            $merchantInstance = $merchantDB->find(\Auth::user()->merchant_id);
            $merchantAccountInstance = $merchantDB->getMerchantAccountsWithMerchantId($merchantInstance->id);

            //customer instance
            $customerInstance = $userDB->get($request->customer_id, true);
            $customerAccountsInstance = $customerInstance->accounts;

            //create product order
            $productOrderInstance = $productOrderDB->create([
                "merchant_id" => $merchantInstance->id,
                "user_id" => $customerInstance->id,
                "author_id" => isset($request->author_id) ? $request->author_id : \Auth::id(),
                "merchant_user_id" => \Auth::id(),
                "description" => isset($request->description) ? $request->description : null,
            ]);


            //sync product order instance with tables
            $productOrderInstance->tables()->sync($request->tables);

            if (!empty($request->cost_id)) {
                //sync product order instance with cost
                $productOrderInstance->costs()->sync([$request->cost_id]);

                //get point wallet treasury account
                $pointTreasuryAccount = $walletDB->getTreasuryAccountInstanceWithWalletType("point");

                //create order for point
                $orderData = [
                    "from_account_id" => $customerAccountsInstance->where("treasury_id", $pointTreasuryAccount->id)->first()->id,
                    "to_account_id" => $merchantAccountInstance->where("treasury_id", $pointTreasuryAccount->id)->first()->id,
                    "amount" => $pointCost,
                    "goods_id" => $productOrderInstance->id,
                    "author_id" => $request->author_id,
                    "treasury_account_id" => $pointTreasuryAccount->id,
                    "paid_at" => Carbon::now(),
                    "type" => "gift_order",
                ];
                $pointOrderInstance = $orderInstance = $this->store($orderData, "gift");
                $allOrders[] = $pointOrderInstance;
            }

            //create order for rials or tables
            $rialsTreasuryAccount = $walletDB->getTreasuryAccountInstanceWithWalletType("rials");
            $orderData = [
                "from_account_id" => $customerAccountsInstance->where("treasury_id", $rialsTreasuryAccount->id)->first()->id,
                "to_account_id" => $merchantAccountInstance->where("treasury_id", $rialsTreasuryAccount->id)->first()->id,
                "amount" => null,
                "goods_id" => $productOrderInstance->id,
                "author_id" => isset($request->author_id) ? $request->author_id : null,
                "treasury_account_id" => $rialsTreasuryAccount->id,
                "paid_at" => null,
                "type" => "table_order_request",
            ];
            $rialsOrderInstance = $orderInstance = $this->store($orderData);

            $allOrders[] = $rialsOrderInstance;
            $allOrders[] = $productOrderInstance;

            //TODO this code most be enhanced
            //get order all related tables and change table status to open
            $tables = $productOrderInstance->tables()->get();
            foreach ($tables as $table) {
                if ($table->status == "multiple") {
                    continue;
                }
                $tableDB->updateStatus($table->id, "reserved");
            }
            DB::commit();
            return $this->successResponse("success_info", $allOrders, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse("400", __("messages.orders_not_create"), 400);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function tableShow()
    {
        if (!\Gate::allows("merchant")) {
            return $this->errorResponse("403", __("access_denied"), 403);
        }
        try {
            $orderDB = new OrderDB();
            $page = \request()->has("page") ? \request("page") : null;
            $limit = \request()->has("limit") ? \request("limit") : null;
            $from = \request()->has("from") ? \request("from") : null;
            $to = \request()->has("to") ? \request("to") : null;
            $payment = \request()->has("payment") ? \request("payment") : null;
            $table_id = \request()->has("table_id") ? \request("table_id") : null;
            $customer_id = \request()->has("customer_id") ? \request("customer_id") : null;
            $mobile = \request()->has("mobile") ? \request("mobile") : null;
            $day = \request()->has("day") ? \request("day") : null;
            $week = \request()->has("week") ? \request("week") : null;
            $month = \request()->has("month") ? \request("month") : null;
            $treasuryAccountId = \request()->has("treasuryAccountId") ? \request("treasuryAccountId") : null;
            $fromAmount = \request()->has("fromAmount") ? \request("fromAmount") : null;
            $fromAccount = \request()->has("fromAccount") ? \request("fromAccount") : null;
            $toAmount = \request()->has("toAmount") ? \request("toAmount") : null;
            $toAccount = \request()->has("toAccount") ? \request("toAccount") : null;
            $products = \request()->has("products") ? \request("products") : null;
            $merchant_id = \request()->has("merchant_id") ? \request("merchant_id") : null;
            $orders = $orderDB->list($from, $to, $day,
                $week, $month, $treasuryAccountId,
                $customer_id, $fromAmount, $toAmount,
                $products, $payment, $merchant_id, $table_id,
                $mobile, $fromAccount, $toAccount);
            return $this->successResponse("order_list", $orders->paginate($limit, $page), 200);
        } catch (\Exception $e) {
            return $this->errorResponse("400", $e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function tableOrderEdit(Request $request)
    {
        if (!\Gate::allows("merchant")) {
            return $this->errorResponse("403", __("access_denied"), 403);
        }
        //when customer want submit new point order
        $costDB = new CostDB();
        $userDB = new UserDB();
        $merchantDB = new MerchantDB();
        $walletDB = new WalletDB();
        $productOrderDB = new ProductOrderDB();

        //get product order and assign cost id on it
        $productOrderInstance = $productOrderDB->productOrderInstance($request->product_order_id);

        $pointTreasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletType("point");

        //check old point order
        $oldPointOrder = $productOrderInstance->orders->where("treasury_account_id", $pointTreasuryAccountInstance->id)->first();

        if ($oldPointOrder instanceof Order) {
            return $this->errorResponse("400", __("messages.can_not_submit_new_point_order"), 400);
        }

        //assign cost to product order
        $productOrderInstance->costs()->sync([$request->cost_id]);

        //create order for cost id
        $pointCost = $costDB->getCost($request->cost_id);

        //get merchant instance
        $merchantInstance = $merchantDB->find(\Auth::user()->merchant_id);
        $merchantAccountInstance = $merchantDB->getMerchantAccountsWithMerchantId($merchantInstance->id);

        //customer instance
        $customerInstance = $userDB->get($request->customer_id, true);
        $customerAccountsInstance = $customerInstance->accounts;

        //create order for point
        $orderData = [
            "from_account_id" => $customerAccountsInstance->where("treasury_id", $pointTreasuryAccountInstance->id)->first()->id,
            "to_account_id" => $merchantAccountInstance->where("treasury_id", $pointTreasuryAccountInstance->id)->first()->id,
            "amount" => $pointCost,
            "goods_id" => $productOrderInstance->id,
            "author_id" => $request->author_id,
            "treasury_account_id" => $pointTreasuryAccountInstance->id,
            "paid_at" => Carbon::now(),
            "type" => "gift_order",
        ];
        $pointOrderInstance = $orderInstance = $this->store($orderData, "gift");
    }

    /**
     * @param TableOrderCheckoutRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tableOrderCheckout(TableOrderCheckoutRequest $request)
    {
        //check authorization
        if (!\Gate::allows("merchant")) {
            return $this->errorResponse("403", __("messages.access_denied"), 403);
        }
        try {
            //get merchant account id
            $accountDB = new AccountDB();
            $walletDB = new WalletDB();
            $tableDB = new TableDB();
            $orderDB = new OrderDB();
            $productOrderDB = new ProductOrderDB();

            //get product order and assign cost id on it
            $productOrderInstance = $productOrderDB->productOrderInstance($request->product_order_id);

            //rials treasury account
            $rialsTreasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletType("rials");

            //rials order
            $rialsOrderInstance = $productOrderInstance->orders->where("treasury_account_id", $rialsTreasuryAccountInstance->id)->first();

            //get customer instance and accounts
            $orderInstance = $orderDB->find($request->order_id);

            //check order not paid before
            if ($rialsOrderInstance->paid_at != null) {
                throw new \Exception(__("messages.order_is_paid_before"), 400);
            }

            //check customer credit
            $customerAccountInstance = $accountDB->get($rialsOrderInstance->from_account_id);
            if (($customerAccountInstance->credits()->first()->amount) < ($request->amount)) {
                throw new \Exception(__("messages.your_credit_is_not_enough"), 400);
            }

            $rialsOrderInstance->amount = $request->amount;

            $this->submitTransaction($rialsOrderInstance);

            //get order all related tables and change table status to open
            $tables = $productOrderInstance->tables()->get();
            foreach ($tables as $table) {
                //\Log::info(json_encode($table->toArray()));
                if ($table->status == "multiple") {
                    continue;
                }
                $tableDB->updateStatus($table->id, "open");
            }

            //return point information for print stuff
            //get club exchange rate
            $clubDB = new ClubDB();
            $clubInstance = $clubDB->getClubWithMerchantID(\Auth::user()->merchant_id);
            $achievePoint = $request->amount / $clubInstance->exchange_rate_price;
            $userInformation["achieve_point"] = $achievePoint;

            return $this->successResponse("payment_order_successfully", $userInformation, 200);
        } catch (\Exception $e) {
            return response()
                ->json([
                    "message" => $e->getMessage(),
                    "error" => $e->getCode()
                ], 410);
        }
    }

    /*------------------------- dorris methods ---------------------------*/
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function dorrisOrder(Request $request)
    {
        try {
            DB::beginTransaction();

            //initial data
            $amount = $request->rials;
            $ordersInstance = [];

            //get customer instance
            $userDB = new UserDB();
            $customerInstance = $userDB->get($request->customer_id, true);

            //check user credential
            if (isset($request->point) && $request->point > 0) {

                $res = $userDB->getUserWithCardPassword($customerInstance->mobile, $request->password);
                if (!$res) {
                    return $this->errorResponse("403", __('messages.wrong_password'), 403);
                }

                //get merchant club
                $clubDB = new ClubDB();
                $clubInstance = $clubDB->getClubWithMerchantID(\Auth::user()->merchant_id);

                //TODO نرخ تبدیل ارز از اینجا مشخص شود
                $amount = $request->rials - ($request->point * $clubInstance->good_lock_point);
                $ordersInstance["rial_amount"] = $amount;

                //when order amount is minus
                if ($amount < 0) {
                    return $this->errorResponse("400", __("messages.minus_amount"));
                }
            }

            //call wallet repository
            $walletDB = new WalletDB();

            //get merchant instance
            $merchantDB = new MerchantDB();

            //create product order
            $productOrderDB = new ProductOrderDB();
            $productOrderData = [
                "merchant_id" => \Auth::user()->merchant_id,
                "user_id" => $customerInstance->id,
                "author_id" => !empty($request->author_id) ? $request->author_id : \Auth::id(),
                "merchant_user_id" => \Auth::id(),
                "paid_at" => null,
                "description" => !empty($request->description) ? $request->description : null,
            ];
            $productOrderInstance = $productOrderDB->create($productOrderData);
            $ordersInstance["product_order"] = $productOrderInstance;

            //call order controller for create order
            $orderController = new OrderController();

            //when create point order
            if (!empty($request->point) || $request->point > 0) {
                $pointTreasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletType("point");
                $merchantPointAccountInstance = $merchantDB->getMerchantInstanceWithAccount(\Auth::user()->merchant_id, "point");
                $customerPointAccountInstance = $customerInstance->accounts->where("treasury_id", $pointTreasuryAccountInstance->id)->first();

                $orderData = [
                    "goods_id" => $productOrderInstance->id,
                    "from_account_id" => $customerPointAccountInstance->id,
                    "to_account_id" => $merchantPointAccountInstance->id,
                    "amount" => $request->point,
                    "type" => "dorris_rials_order",
                    "author_id" => isset($request->author_id) ? \request("author_id") : \Auth::id(),
                    "paid_at" => null,
                    "treasury_account_id" => $pointTreasuryAccountInstance->id,
                ];
                $orderInstance = $orderController->store($orderData, "gift");
                array_push($ordersInstance, $orderInstance);
            }
            $ordersInstance["rial_amount"] = $amount;

            DB::commit();
            return $this->successResponse("info", $ordersInstance, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse("400", $e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function dorrisOrderCheckout(Request $request)
    {
        try {
            DB::beginTransaction();

            //get product order and all its relation
            $walletDB = new WalletDB();
            $merchantDB = new MerchantDB();

            //get customer instance
            $userDB = new UserDB();
            $customerInstance = $userDB->get($request->customer_id, true);

            //call order controller for create order
            $orderController = new OrderController();

            //when create rials order
            $rialsTreasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletType("rials");
            $merchantRialsAccountInstance = $merchantDB->getMerchantInstanceWithAccount(\Auth::user()->merchant_id, "rials");
            $customerRialsAccountInstance = $customerInstance->accounts->where("treasury_id", $rialsTreasuryAccountInstance->id)->first();

            $orderData = [
                "goods_id" => $request->product_order_id,
                "from_account_id" => $customerRialsAccountInstance->id,
                "to_account_id" => $merchantRialsAccountInstance->id,
                "amount" => $request->amount,
                "type" => "dorris_rials",
                "author_id" => isset($request->author_id) ? \request("author_id") : \Auth::id(),
//                    "paid_at" => null,
                "treasury_account_id" => $rialsTreasuryAccountInstance->id,
            ];
            $orderInstance = $orderController->store($orderData, "dorris_rials");

            //get user point
            $res = User::query()->where("id", $customerInstance->id)->with("accounts.credits")->first();
            $userResource = new UserResource($res);
            $orderInstance["userInfo"] = $userResource;

            DB::commit();
            return $this->successResponse("info", $orderInstance, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse("400", $e->getMessage(), $e->getCode());
        }
    }
}
