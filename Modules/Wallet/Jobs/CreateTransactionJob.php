<?php

namespace Modules\Wallet\Jobs;

use App\Exceptions\TransactionException;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Modules\Event\Events\AfterTransactionCreatedEventOld;
use Modules\Event\repo\EventDB;
use Modules\Wallet\Entities\AccountType;
use Modules\Wallet\Entities\ProductOrder;
use Modules\Wallet\repo\AccountDB;
use Modules\Wallet\repo\AccountTypeDB;
use Modules\Wallet\repo\OrderStuffDB;
use Modules\Wallet\repo\ProductOrderDB;
use Modules\Wallet\repo\TransactionDB;
use Modules\Wallet\repo\WalletDB;

class CreateTransactionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $orderInstance;
    private $type;

    /**
     * CreateTransactionJob constructor.
     * @param $orderInstance
     * @param null $type
     */
    public function __construct($orderInstance, $type = null)
    {
        $this->orderInstance = $orderInstance;
        $this->type = $type;
    }

    /**
     * @throws TransactionException
     */
    public function handle()
    {
        //initial variable
        $transaction = new TransactionDB();
        $eventDB = new EventDB();
        $accountDB = new AccountDB();
        $orderInstance = $this->orderInstance;

        //create real shop order
        $amount = $orderInstance->amount;
        $fromAccountInstance = $accountDB->get($orderInstance->from_account_id);
        $toAccountInstance = $accountDB->get($orderInstance->to_account_id);
        $treasuryAccountInstance = $accountDB->get($orderInstance->treasury_account_id);

        /*----- begin transaction ------*/
        try {
            DB::beginTransaction();
            /*------------ create first transaction ---------*/
            $transactionData1 = [
                "from_account_id" => $fromAccountInstance->id,
                "to_account_id" => $treasuryAccountInstance->id,
                "amount" => $amount,
                "order_id" => $orderInstance->id,
            ];
            $test1 = $transaction->create($transactionData1);

            //get from_account credit
            $old_credit = DB::table("accounts")
                ->where("accounts.id", "=", $fromAccountInstance->id)
                ->join("credits", "accounts.id", "=", "credits.account_id")
                ->where("credits.treasury_id", "=", $treasuryAccountInstance->id)
                ->select("credits.amount")
                ->first()->amount;
            $new_credit = ($old_credit) - ($amount);

            //update from_account credit
            DB::table("credits")
                ->where("credits.treasury_id", "=", $treasuryAccountInstance->id)
                ->join("accounts", "credits.account_id", "=", "accounts.id")
                ->where("accounts.id", "=", $fromAccountInstance->id)
                ->update([
                    'amount' => $new_credit,
                ]);

            /*------------- create second transaction -----------*/
            $transactionData2 = [
                "from_account_id" => $treasuryAccountInstance->id,
                "to_account_id" => $toAccountInstance->id,
                "amount" => $amount,
                "order_id" => $orderInstance->id,
            ];
            $transactionInstance2 = $transaction->create($transactionData2);

            //get to_account credit
            $old_credit_amount = DB::table("accounts")
                ->where("accounts.id", "=", $toAccountInstance->id)
                ->join("credits", "accounts.id", "=", "credits.account_id")
                ->where("credits.treasury_id", "=", $treasuryAccountInstance->id)
                ->select("credits.amount")
                ->first()->amount;

            $new_credit_amount = ($old_credit_amount) + ($amount);

            //update to_account credit
            DB::table("credits")
                ->where("credits.treasury_id", "=", $treasuryAccountInstance->id)
                ->join("accounts", "credits.account_id", "=", "accounts.id")
                ->where("accounts.id", "=", $toAccountInstance->id)
                ->update([
                    'amount' => $new_credit_amount,
                ]);

            //save paid_at into order instance
            $orderInstance->paid_at = date("Y-m-d H:i:s", time());
            $orderInstance->save();

            //save paid_at into product order instance
            if (isset($orderInstance->goods_id)) {
                $productOrderDB = new ProductOrderDB();
                $productOrderInstance = $productOrderDB->productOrderInstance($orderInstance->goods_id);
                if ($productOrderInstance != false || $productOrderInstance instanceof ProductOrder) {
                    $productOrderInstance->paid_at = date("Y-m-d H:i:s", time());
                    $productOrderInstance->save();
                }
            }

            $ins = AccountType::where("type", "treasury")
                ->with("accounts")
                ->wherehas("wallet", function ($q) {
                    $q->where("type", "=", "rials");
                })
                ->first();

            $walletDB = new WalletDB();
            $rialsTreasuryAccountInstance = $walletDB->getTreasuryAccountWithWalletType("rials");

            //just rial transaction create event
            if ((($treasuryAccountInstance->id == $rialsTreasuryAccountInstance->id) || $this->type == "table" || $this->type == "dorris_rials") && $this->type != "cashPaymentTableOrder" && $this->type != "bank" && $this->type != "gift" && $this->type != "dorris_gift") {
                //save event into Event module
                $eventData = [
                    "from_account_id" => $fromAccountInstance->id,
                    "to_account_id" => $toAccountInstance->id,
                    "amount" => $orderInstance->amount,
                    "type_id" => 1,
                    "done_at" => null,
                    "UID" => Str::uuid()->toString(),
                    "extra_value" => null,
                ];
                $eventInstance = $eventDB->create($eventData);

                //get club event and verify to get some campaign
                $test = event(new AfterTransactionCreatedEventOld($eventInstance, $orderInstance));

            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Throw new TransactionException("transaction is not create");
        }

    }
}
