<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$locale = request()->server("HTTP_ACCEPT_LANGUAGE") ?? "en";
App::setLocale($locale);

/*------------------------ wallet routes --------------------------------*/
Route::group([
    "middleware" => "auth:api",
    'namespace' => 'Api'
], function () {
    //account type
    Route::resource("accountType", "AccountTypeController");
    Route::group(["prefix" => "account"], function () {
        Route::get('types', 'AccountTypeController@index');
    });

    Route::group([
        'prefix' => 'wallet',
    ], function () {
        Route::get('/banks', 'AccountController@bankList');
        Route::post('/create', 'WalletController@store');
        Route::get('/list', 'WalletController@list');
        Route::get('/show/{id}', 'WalletController@show');
        Route::put('/update/{id}', 'WalletController@update');
        Route::delete('/delete/{id}', 'WalletController@destroy');
        Route::get('/wallet-credit', 'WalletController@walletCredit');
        Route::post('/credit-charge', 'WalletController@creditCharge');
    });

    Route::resource("wallets", "WalletController");

    //payment
    Route::post("payment", "PaymentController@success");
    //TODO this api is incomplete! complete this api as soon possible
    Route::post("payment/cash", "PaymentController@cashPayment");

    //transaction
    Route::resource("goods", "GoodsController");

    //create campaign
    Route::resource('campaigns', 'CampaignController');
    Route::post('campaigns/charge', 'CampaignController@charge');

    /*---------------------------------------- ordering ---------------------------------------------*/
    //order routes
    Route::group([
        "prefix" => "product",
    ], function () {
        Route::post("orders", "ProductOrderController@store");
        Route::get("order", "OrderController@show");
        Route::post("orders/refund", "OrderController@refund");
    });
    Route::get("product_orders", "ProductOrderController@index");
    Route::post("product_orders/cancel", "ProductOrderController@cancel");
    Route::post("orders/checkout", "OrderController@productOrderCheckout");

    //table order routes
    Route::group([
        "prefix" => "table",
    ], function () {
        Route::post("order", "OrderController@testTableOrder");
        Route::post("order/edit", "OrderController@tableOrderEdit");
        Route::post("order/checkout", "OrderController@tableOrderCheckout");
        Route::get("order", "OrderController@tableShow");
    });

    /*------------ dorris api --------------*/
    //dorris order routes
    Route::group([
        "prefix" => "dorris",
    ], function () {
        Route::post("order", "OrderController@dorrisOrder");
        Route::post("order/checkout", "OrderController@dorrisOrderCheckout");

//        Route::post("order/edit", "OrderController@tableOrderEdit");
//        Route::post("order/checkout", "OrderController@tableOrderCheckout");
//        Route::get("order", "OrderController@tableShow");
    });

    /*------------------------------------------------------------------------------------------*/
    //account model route
    Route::resource('accounts', 'AccountController');
    Route::get('/{id}', 'AccountController@show');
    Route::post('/', 'AccountController@store');
    Route::delete('/{id}', 'AccountController@destroy');

    Route::resource("groups", "GroupController");
    Route::resource("credits", "CreditController");
});
//Route::resource("campaign", "Api\CampaignController");


