<?php

namespace Modules\Wallet\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Wallet\Entities\Order;

class AfterCreatedOrderEvent
{
    use SerializesModels;
    public $orderInstance;
    public $type;

    /**
     * AfterCreatedOrderEvent constructor.
     * @param Order $orderInstance
     */
    public function __construct(Order $orderInstance, $type)
    {
        $this->orderInstance = $orderInstance;
        $this->type = $type;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
