<?php

namespace Modules\Wallet\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Wallet\Entities\Account;
use Modules\Wallet\Entities\AccountType;

class AccountCreatedEvent
{
    use SerializesModels;
    public $accountInstance;
    public $data;

    /**
     * AccountCreatedEvent constructor.
     * @param Account $accountInstance
     * @param $data
     */
    public function __construct(Account $accountInstance, $data)
    {
        $this->accountInstance = $accountInstance;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
