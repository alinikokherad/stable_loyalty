<?php

namespace Modules\Wallet\Events;

use Illuminate\Queue\SerializesModels;

class AfterPaymentSuccessEvent
{
    use SerializesModels;
    public $requestInstance;

    /**
     * AfterPaymentSuccessEvent constructor.
     * @param $requestInstnce
     */
    public function __construct($requestInstnce)
    {
        $this->requestInstance = $requestInstnce;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
