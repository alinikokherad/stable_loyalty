<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA0YyG3_4:APA91bEMvA-1ufAsg1e3Oz2tNQzy3Ko-gzSSDYLG6HoqI__7THg89wmvCMCyv5M5RdKs6-PMUHeIt-ueYbUnpBUrKXcALMoSSG61k-pbLxsUEKwv6miPdEI32Dyplv5zezeP-OoQqYHh'),
        'sender_id' => env('FCM_SENDER_ID', '900005814270'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
